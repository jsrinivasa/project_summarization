from os import listdir
import os as os
import shutil
import string

# load doc into memory
def load_doc(filename):
    # open the file as read only
    #print(filename)
    file = open(filename,'r')
    # read all text
    text = file.read()
    # close the file
    file.close()
    return text

def write_story_highlight_diff_files(filename, story, highlights):
    h_filename = filename.replace(".story", ".highlights")
    print(h_filename)
    file = open(filename,'w')
    story = "@story: \n" + story
    file.write(story)
    file.close()
    h_file = open(h_filename, 'w')
    h_file.write("@abstract: \n")
    highlights = list(map(lambda s: "<s>"+s+"<\s>", highlights))
    h_file.write(' '.join(highlights))
    h_file.close()

def write_story_highlight(filename, story, highlights):
    file = open(filename,'a+')
    file.write("@abstract: \n")
    highlights = list(map(lambda s: "<s>"+s+"<\s>", highlights))
    file.write(' '.join(highlights))                 
    story = "\n@story: \n" + story
    file.write(story)
    file.write("\n\n")
    file.close()
                      
                      
# split a document into news story and highlights
def split_story(doc):
	# find first highlight
	index = doc.find('@highlight')
	# split into story and highlights
	story, highlights = doc[:index], doc[index:].split('@highlight')
	# strip extra white space around each highlight
	highlights = [h.strip() for h in highlights if len(h) > 0]
	return story, highlights

# load all stories in a directory
def load_stories(directory):
    stories = list()
    rs_dir = "./processed"
    if  os.path.exists(rs_dir):
        shutil.rmtree(rs_dir)
    os.makedirs(rs_dir)
    list_of_files = listdir(directory)
    count1 = -1
        
    for count,name in enumerate(list_of_files):
        if ".story" not in name:
            continue
        urlhash = os.path.splitext(name)
        filename = directory + '/' + name
        # load document
        # print(filename)
        doc = load_doc(filename)
        # split into story and highlights
        story, highlights = split_story(doc)
        if(story.isspace()):
            print(story+" is empty!!!! "+filename)
            continue
        if (count%1000 == 0):
            count1 += 1
            trainName = 'train'+ str(count1).zfill(3) 
        else:
            trainName
        fn = rs_dir + '/' + trainName
        #write_story_highlight(fn, story, highlights)
        # store
        stories.append({'hash':urlhash[0], 'story':story, 'highlights':highlights})
    return stories

# clean a list of lines
def clean_lines(lines):
    cleaned = list()
    # prepare a translation table to remove punctuation
    table = str.maketrans('', '', string.punctuation)
    for line in lines:
        # strip source cnn office if it exists
        index = line.find('(CNN) -- ')
        if index > -1:
            line = line[index+len('(CNN)'):]
        index = line.find('CNN')
        if index > -1:
            line = line[index+len('CNN'):]
        # tokenize on white space
        line = line.split()
        # convert to lower case
        line = [word.lower() for word in line]
        # remove punctuation from each token
        line = [w.translate(table) for w in line]
        # remove tokens with numbers in them
        line = [word for word in line if word.isalpha()]
        # store as string
        cleaned.append(' '.join(line))
    # remove empty strings
    cleaned = [c for c in cleaned if len(c) > 0]
    return cleaned

