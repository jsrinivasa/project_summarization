import os
import pickle
# Helper libraries
import collections
import hashlib
import nltk
import json 
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk.tokenize.treebank import TreebankWordTokenizer
from rouge import Rouge
from multiprocessing.dummy import Pool as ThreadPool 
import itertools
import rouge
from w266_common import utils, vocabulary, tf_embed_viz, treeviz
from w266_common import patched_numpy_io
from tmp import load_doc, clean_lines, load_stories, split_story, write_story_highlight
import time


def hashhex(s):
    """Returns a heximal formated SHA1 hash of the input string."""
    h = hashlib.sha1()
    h.update(s.encode())
    return h.hexdigest()

def read_text_file(text_file):
    lines = []
    with open(text_file, "r") as f:
        for line in f:
            lines.append(line.strip())
    return lines

def get_url_hashes(url_list):
    return [hashhex(url) for url in url_list]

def convert_to_hash_dictionary(stories):
    hash_dict = {}
    for index, story in enumerate(stories):
        hash_dict[story['hash']] = index
    return hash_dict

def get_story_for_hash_append_to_list(url_file, stories,  hash_dict):
    url_list = read_text_file(url_file)
    url_hashes = get_url_hashes(url_list)
    url_hashes = [i for i in url_hashes if i in hash_dict.keys()]
    appendStoryList = [stories[hash_dict[hashV]]['story'] for hashV in url_hashes if hash_dict[hashV]]
    appendHashList = [stories[hash_dict[hashV]]['hash'] for hashV in url_hashes if hash_dict[hashV]]
    appendHighlightsList = [stories[hash_dict[hashV]]['highlights'] for hashV in url_hashes if hash_dict[hashV]]
    return appendStoryList, appendHashList, appendHighlightsList


def get_train_test_stories(stories_d, urls, split=0.8, shuffle=False):
    """Generate train/test split for unsupervised tasks.

    Args:
      stories(list): list of stories
      split (double): fraction to use as training set
      shuffle (int or bool): seed for shuffle of input data, or False to just
      take the training data as the first xx% contiguously.

    Returns:
      train_sentences, test_sentences ( list(list(string)) ): the train and test
      splits
    """
    stories = list(map(lambda s: s['story'], stories_d))
    highlights = list(map(lambda s: s['highlights'], stories_d))
        
    sentences = np.array(list(stories), dtype=list)
    fmt = (len(sentences), sum(map(len, sentences)))
    print("Loaded {:,} stories ({:g} sentences)".format(*fmt))
    train_stories = []
    train_hash = []                 
    train_highlights = []
    dev_stories = []
    dev_hash = []
    dev_highlights = []
    test_stories = []
    test_hash = []
    test_highlights = []
    
    if urls:
        hash_dict = convert_to_hash_dictionary(stories_d)
        all_train_urls = urls + "/all_cnn_train.txt"
        all_val_urls = urls + "/all_cnn_val.txt"
        all_test_urls = urls + "/all_cnn_test.txt"
        
        train_stories, train_hash, train_highlights = get_story_for_hash_append_to_list(all_train_urls, stories_d, hash_dict)
        dev_stories, dev_hash, dev_highlights = get_story_for_hash_append_to_list(all_val_urls, stories_d, hash_dict)
        test_stories, test_hash, test_highlights = get_story_for_hash_append_to_list(all_test_urls, stories_d, hash_dict)       

    else: 
        if shuffle:
            rng = np.random.RandomState(shuffle)
            all_sents = list(zip(sentences, highlights))
            rng.shuffle(all_sents)
            sentences, highlights = zip(*all_sents)
           # rng.shuffle(sentences)  # in-place
           # rng.shuffle(highlights)
        split_idx = int(split * len(sentences))
        test_dev_split_idx = int((len(sentences) - split_idx)/2)+ split_idx
        print(split_idx, test_dev_split_idx)
        train_stories = sentences[:split_idx]
        dev_stories = sentences[split_idx:test_dev_split_idx]
        test_stories = sentences[test_dev_split_idx:]
        train_highlights = highlights[:split_idx]
        dev_highlights = highlights[split_idx:test_dev_split_idx]
        test_highlights = highlights[test_dev_split_idx:]
    
    
    fmt = (len(train_stories), sum(map(len, train_stories)))
    print("Training set: {:,} stories ({:,} sentences)".format(*fmt))
    fmt = (len(dev_stories), sum(map(len, dev_stories)))
    print("Dev set: {:,} stories ({:,} sentences)".format(*fmt))
    fmt = (len(test_stories), sum(map(len, test_stories)))
    print("Test set: {:,} stories ({:,} sentences)".format(*fmt))

    return train_stories, dev_stories, test_stories, train_hash, dev_hash, test_hash, train_highlights, dev_highlights, test_highlights


def combine_vocab(tokenList, corpus=None):
    token_feed = [utils.canonicalize_word(w) for w in tokenList]
    print(len(token_feed))
    if corpus:
        token_feed.extend([utils.canonicalize_word(w) for w in corpus.words()])
        print(len(token_feed))
    return token_feed


NOUNS = ['NN', 'NNS', 'NNP', 'NNPS']
def rank_sentences(sents, doc_matrix, feature_names, sentence_ordering=0, top_n=4):
    sentences = [nltk.word_tokenize(sent) for sent in sents]
    sentences = [[w for w in sent if nltk.pos_tag([w])[0][1] in NOUNS]
                  for sent in sentences]
    tfidf_sent = [[doc_matrix[feature_names.index(w.lower())]
                   for w in sent if w.lower() in feature_names]
                 for sent in sentences]
    #print(len(sents))
    #print(len(tfidf_sent))
    #print(len(sentences))

    # Calculate Sentence Values
    doc_val = sum(doc_matrix)
    sent_values = [sum(sent) / doc_val for sent in tfidf_sent]
    
    #print(len(sent_values))
    if sentence_ordering == 1:
        #print("Coming to ordering 1")
        # Apply Position Weights
        sent_values = [sent*(i/len(sent_values))
                        for i, sent in enumerate(sent_values)]
    elif sentence_ordering == 2:
        #print("Coming to ordering 2")
        sent_values = [sent*((len(sent_values) - i)/len(sent_values))
                        for i, sent in enumerate(sent_values)]
        

    ranked_sents = [pair for pair in zip(range(len(sent_values)), sent_values)]
    ranked_sents = sorted(ranked_sents, key=lambda x: x[1] *-1)

    return ranked_sents[:top_n]



def get_base_line_rouge_score(test_stories_list, test_highlights_list, n=4):
    t = []
    h = []
    r = []
    for i in range(len(test_stories_list)):
        t.append(' '.join(test_stories_list[i][:n]))
        h.append(' '.join(train_highlights_list[i]))
        rouge = Rouge()
        rg_score = rouge.get_scores(t[i],h[i])
        r.append(rg_score)
    
        #print(rouge.get_scores(t[i],h[i]))
        if(i%400==0):
            print(i)
            print(t[i]+'\n\n'+h[i])
            print("\n\ntfidf_score: ",tfidf_s[i])
    return r

def get_rouge_score(test_stories_list, test_highlights_list, count_vect, tfidf, sentence_ordering=0, n=4):
    t = []
    h = []
    tfidf_s = []
    r = []
    len_stories = len(test_stories_list)
    for i in range(len_stories):
        story_freq_term = count_vect.transform(test_stories_list[i])
        story_tfidf_matrix = tfidf.transform(story_freq_term)
        story_dense = story_tfidf_matrix.todense()
        doc_matrix = story_dense.tolist()[0]
        rank = rank_sentences(test_stories_list[i], doc_matrix, count_vect.get_feature_names(), sentence_ordering, n)
        t.append(' '.join([test_stories_list[i][id[0]] for id in rank]))
        tfidf_s.append([id[1] for id in rank])
        h1 = ' '.join(test_highlights_list[i])
        rouge = Rouge()
        rg_score = rouge.get_scores(t[i],h1)
        r.append(rg_score)

        #print(rouge.get_scores(t[i],h[i]))
        if(i%400==0):
            print(i)
            print(t[i]+'\n\n'+h1)
            print("\n\ntfidf_score: ",tfidf_s[i])
    return r, t, tfidf_s

def makeresult_file(r_scores_tmp, filename):
    scores = [[score_type for rouge_type in score[0].values() for score_type in rouge_type.values()] for score in r_scores_tmp]
    df = pd.DataFrame(scores, columns=[key1+'_'+key2 for dict1 in r_scores_tmp[0] for key1 in dict1.keys() for key2 in dict1[key1]])
    df.to_csv(filename + '.csv')
    
    return df

def makeprocessed_file(t, hash_list, highlight_list):
    for i,story in enumerate(t):
        with open('cnn/stories/processed/'+test_hash_list[i]+'.story', 'w') as pfile:
            pfile.write(string(story))
            pfile.write("\n")
            for highlight in test_highlight_list[i]:
                pfile.write('@highlight\n')
                pfile.write(highlight)
                pfile.write("\n")
                      
                      
                      
if __name__ == '__main__':
    
    logfileName = 'logfile_for_run'
    logfile = open(logfileName,'w')
    logfile.write("Starting run @ time {:s}".format(time.ctime()))
    logfile.close()
    try:
        file = open('cnn/stories/stories.json')
        stories = json.load(file)
        file.close()
    except:
        directory = 'cnn/stories/'
        stories = load_stories(directory)
        print('Loaded Stories %d' % len(stories))
        # clean stories
        for i,example in enumerate(stories):
            example['story'] = clean_lines(example['story'].split('\n'))
            example['highlights'] = clean_lines(example['highlights'])
            stories[i] = example
        with open('cnn/stories/stories.json', 'w') as outfile:
            json.dump(stories, outfile)
            
    
#     try:
#         with open('cnn/stories/train_stories.json') as file:
#             train_stories_list = json.load(file)
#         with open('cnn/stories/train_highlights.json') as file:
#             train_highlights_list = json.load(file)
#         with open('cnn/stories/dev_stories.json') as file:
#             dev_stories_list = json.load(file)
#         with open('cnn/stories/dev_highlights.json') as file:
#             dev_highlights_list = json.load(file)
#         with open('cnn/stories/test_stories.json') as file:
#             test_stories_list = json.load(file)
#         with open('cnn/stories/test_highlights.json') as file:
#             test_highlights_list = json.load(file)
#     except:
    if(1):
        train_stories_list, dev_stories_list, test_stories_list, train_hash_list, dev_hash_list, test_hash_list, train_highlights_list, dev_highlights_list, test_highlights_list \
= get_train_test_stories(stories, 'cnn-dailymail/url_lists', split=0.9, shuffle=42)
#         with open('cnn/stories/train_stories.json', 'w') as outfile:
#             json.dump(train_stories_list, outfile)
#         with open('cnn/stories/dev_stories.json', 'w') as outfile:
#             json.dump(dev_stories_list, outfile)
#         with open('cnn/stories/test_stories.json', 'w') as outfile:
#             json.dump(test_stories_list, outfile)
#         with open('cnn/stories/train_highlights.json', 'w') as outfile:
#             json.dump(train_highlights_list, outfile)
#         with open('cnn/stories/dev_highlights.json', 'w') as outfile:
#             json.dump(dev_highlights_list, outfile)
#         with open('cnn/stories/test_highlights.json', 'w') as outfile:
#             json.dump(test_highlights_list, outfile)
    
    stories_list = list(map(lambda s: s['story'], stories))
    highlights_list = list(map(lambda s: s['highlights'], stories))
    all_sentences = [item for sublist in stories_list for item in sublist]
    
#     try:
#         with open('cnn/stories/words.json') as file:
#             combined_words = json.load(file)
#     except:
#         tokenizer = TreebankWordTokenizer()
#         x_tokens = [tokenizer.tokenize(ts) for ts in all_sentences]
#         print("Number of tokens is",len(x_tokens))
#         x_tokens_flat= [item for sublist in x_tokens for item in sublist]
#         #corpus = utils.get_corpus("brown")
#         combined_words = combine_vocab(x_tokens_flat, None)
#         #combined_words = x_tokens_flat
#         with open('cnn/stories/words.json', 'w') as outfile:
#             json.dump(combined_words, outfile)
            
            
#     try:
#         with open('cnn/stories/vocab.pkl', 'rb') as vocabf:
#             vocab = pickle.load(vocabf)
#     except:
#         vocab = utils.build_vocab(combined_words, V=None)
#         print("Vocabulary size: {:,}".format(vocab.size))
#         with open('cnn/stories/vocab.pkl', 'wb') as outfile:
#             pickle.dump(vocab, outfile)
    
    
#     try:
#         with open('cnn/stories/sents.json', 'r') as sentsf:
#             sents_list = json.load(sentsf)
#     except:
#         corpus = utils.get_corpus("brown")
#         sents_list = [" ".join(sent) for sent in corpus.sents()]
#         with open('cnn/stories/sents.json', 'w') as outfile:
#             json.dump(sents_list, outfile) 
   
    all_train_sentences = [item for sublist in train_stories_list for item in sublist]
    all_dev_sentences = [item for sublist in dev_stories_list for item in sublist]
    all_highlights_sentences = [item for sublist in train_highlights_list+dev_highlights_list for item in sublist]
   
    combined_list = all_train_sentences + all_dev_sentences + all_highlights_sentences
    
    count_vect = CountVectorizer(preprocessor=utils.canonicalize_word, stop_words={'English'})
    count_vect = count_vect.fit(combined_list)
    #freq_term_matrix = count_vect.transform(train_stories_list[0])
    freq_term_matrix = count_vect.transform(combined_list)
    feature_names = count_vect.get_feature_names()
    
    tfidf = TfidfTransformer(norm = 'l2')
    tfidf.fit(freq_term_matrix)
    #tfidf.fit(freq_term_matrix)
    logfile = open(logfileName,'w+')
    logfile.write("Completed pre-processing @ time {:s}".format(time.ctime()))
    logfile.close()
    print(time.ctime())
    r_score, t, tfidif_scores = get_rouge_score(test_stories_list, test_highlights_list, count_vect, tfidf, sentence_ordering=1, n=4)
    print(time.ctime())
    
    filename = 'tfidf_algo_results_with_order_ranking'
    makeresult_file(r_score, filename )
    
    filename_highlights = 'test_generated_highlights_with_order_ranking_last.txt'
    output_file = open(filename_highlights,'w')
    
    for index,item in enumerate(t):
        output_file.write("#"+str(index)+"\n")
        output_file.write("%s\n\n" % item)
    output_file.close()
    
    logfile = open(logfileName,'w+')
    logfile.write("Completed run @ time {:s}".format(time.ctime()))
    logfile.close()