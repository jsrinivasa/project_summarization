import os
import sys
import rouge
import pandas as pd

def makeresult_file(r_scores_tmp, filename):
  scores =[]
  scores = [[score_type for rouge_type in score[0].values() for score_type in rouge_type.values()] for score in r_scores_tmp]
  df = pd.DataFrame(scores, columns=[key1+'_'+key2 for dict1 in r_scores_tmp[0] for key1 in dict1.keys() for key2 in dict1[key1]])
  if filename:
    df.to_csv(filename + '.csv')

  return df

def rouge_eval(ref_dir, dec_dir):
  """Evaluate the files in ref_dir and dec_dir with pyrouge, returning results_dict"""
  #r = pyrouge.Rouge155()
  ref_file_list = sorted(os.listdir(ref_dir))
  dec_file_list = sorted(os.listdir(dec_dir))
  rg_scores, ref, dec = [], [], []

  # print(range(len(dec_file_list)), len(ref_file_list), len(dec_file_list))
  for i in range(len(dec_file_list)):
    #print(i)
    #if ref_file_list[i] == '000197_reference.txt':
    #  continue

    #print(i, ref_file_list[i])
    with open(ref_dir + '/' + ref_file_list[i], 'r') as f:
      txt = f.readlines()

    ref.append(' '.join(txt[:]))

    with open(dec_dir + '/' + dec_file_list[i], 'r') as f:
      txt2 = f.readlines()

    dec.append(' '.join(txt2[:]))

    r = rouge.Rouge()
    rg_score = r.get_scores(dec[i], ref[i])
    rg_scores.append(rg_score)

  return rg_scores


ref_dir_arg = sys.argv[1]
dec_dir_arg = sys.argv[2]

rg_scores_eric = rouge_eval(ref_dir_arg, dec_dir_arg)
#print(rg_scores_eric)
makeresult_file(rg_scores_eric, './PG_Rouge')
