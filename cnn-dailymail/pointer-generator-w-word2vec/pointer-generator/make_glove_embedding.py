import numpy as np
filename = 'glove.6B/glove.6B.200d.txt'
def loadGloVe(filename):
    vocab = []
    embd = []
    vocabIndex = []
    file = open(filename,'r')
    for index,line in enumerate(file.readlines()):
        row = line.strip().split(' ')
        vocab.append(row[0])
        embd.append(row[1:])
        vocabIndex.append((row[0], index))
    print('Loaded GloVe!')
    file.close()
    return vocab,embd,vocabIndex

vocab,embd,vocab2Index = loadGloVe(filename)
vocab_size = len(vocab)
embedding_dim = len(embd[0])
embedding = np.zeros(shape=(len(embd)+4, len(embd[0])))
tmp = (np.array([.1, .11, .111, .1111]) * np.ones(shape=(4,embedding.shape[1])).T).T
embedding[0:4] = tmp
embedding[4:] = np.asarray(embd)

with open('../../finished_files_pointer_generator_w_glove/glove_vocab','w') as glove_vocab:
    for v2i in vocab2Index:
        glove_vocab.write(v2i[0]+' '+str(v2i[1]))
        glove_vocab.write('\n')

with open('../../finished_files_pointer_generator_w_glove/word_embeddings','wb') as glove_embed:
    np.save(glove_embed, embedding)
