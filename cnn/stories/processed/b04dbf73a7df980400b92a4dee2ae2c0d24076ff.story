just eight months ago a young woman named fatu kekula was singlehandedly trying to save her ebolastricken family donning trash bags to protect herself against the deadly virus. kekula was just a year away from finishing up her nursing degree in liberia when ebola struck and her mother father sister and cousin came down with the disease hospitals were full and no doctors would visit her home so with just advice from a physician on the phone kekula took care of all four of her relatives at the same time. david smith an associate dean at emorys nursing school said they accepted kekula because they were struck by how both she and emory each treated four ebola patients at around the same time last year and emory had dozens of doctors and nurses and millions of dollars in technology while kekula had nobody and nearly no supplies. story and the generosity of donors from around the world kekula wears scrubs bearing the emblem of the emory university nell hodgson woodruff school of nursing in atlanta where shes learning skills she can take back home to care for her fellow liberians

@highlight
fatu kekula saved most of her family from ebola

@highlight
thanks to donors she is being trained at emory university

@highlight
kekula was one year away from finishing a nursing degree when ebola struck
