as art film stills are often overlooked. working on film sets was really important to magnum and all of the photographers working with magnum in the did films says jacob now the mcevoy family curator for photography at the smithsonian american art museum not only did the work pay off financially but the photographers also established relationships with members of the industry he said. haas one of the centurys great photojournalists and imagemakers an early member of the great magnum photos cooperative who was famous enough in his prime to have been the subject of a museum of modern art exhibit in was also a regular on movie sets it was a necessity in those early days said john jacob the editor of a new book of haas movie photography ernst haas on set. theres a striking picture of orson welles in the third man hemmed in by the darkness in a way that suggests the shadowy themes of the classic film another of gregory peck and chuck connors in the big country is as dramatic as any fullscale movie duel westerns were a specialty

@highlight
ernst haas a celebrated photographer was a regular on movie sets

@highlight
his work from the industry has been brought together in a new book ernst haas on set
