chinese police on monday released five female activists who were detained last month family and friends of the women tell cnn. the women will be under police surveillance for a year and have their movements and activities restricted attorney liang xiaojun said police can summon the women for questioning at any time he added. each and every one of us has the right to speak out against sexual harassment and the many other injustices that millions of women and girls suffer around the world us secretary of state john kerry said in a statement friday we strongly support the efforts of these activists to make progress on these challenging issues and we believe that chinese authorities should also support them not silence them. the united states had urged china to free them and the international community harshly criticized keeping the women in custody

@highlight
wei tingting wang man zheng churan li tingting and wu rongrong are free

@highlight
they will be under police surveillance for a year and have their activities restricted attorney says

@highlight
the international community has harshly criticized keeping the women in custody
