the seventh installment of the fast and furious franchise furious is sure to draw fans curious about how the film handles the reallife death of costar paul walker. richard lawson vanity fair the latest film which opens friday cant help but take on some deeper meaning as the death of main cast member paul walker killed in a car accident in late looms large throughout but it doesnt overwhelm furious is respectful even solemn when it needs to be but is still thank god plenty of crazy fun. peter travers rolling stone furious is the best ff by far two hours of pure pow fueled by dedication and passionate heart this one sticks with you the usual flaws plot bumps muscle acting tweetlength dialogue fade in the face of the camaraderie on and off screen finishing the film in walkers honor clearly brought out the best in everyone its bittersweet seeing walker in action again but its also a kick to watch him take the wheel or hang off a bus in azerbaijan that happens to be hanging off a cliff he feels at home. chris nashawaty entertainment weekly no one forks over bucks to see one of these flicks for its logic we go for the bananas demolitionderby mayhem furious delivers that with the direct visceral rush of an epipen for two hours and change were treated to a highoctane orgy of some of the most exhilarating stunts ever put on film including one showstopper where walker balances on an overturned bus thats teetering on the edge of a cliff

@highlight
the film is out in theaters today

@highlight
costar paul walker died during production

@highlight
critics say furious is bittersweet and plenty of crazy fun
