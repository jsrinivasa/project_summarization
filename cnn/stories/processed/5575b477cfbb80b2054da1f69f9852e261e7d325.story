sitting on a sunny bench in istanbuls gezi park fadime gurgen dismisses the controversy surrounding the anniversary friday of the massacre of armenians under the ottoman empire with a wave of her hand. most turks agree with gurgen ninetyone percent of turks do not believe that the events of when according to armenians million ethnic armenians were systematically killed in the final years of the ottoman empire were genocide according to a recent poll. last year the turkish government expressed condolences to armenians and accepted that hundreds of thousands of their ancestors died as they were marched out of cities and towns in central and eastern anatolia in the waning years of the ottoman empire. gurgen a cleaner says her family has had close friendships with armenians going back generations

@highlight
massacre of million ethnic armenians under the ottoman empire is widely acknowledged by scholars as a genocide

@highlight
turkish government officially denies it saying hundreds of thousands of turkish muslims and armenian christians died in intercommunal violence
