sky watchers in western north america are in for a treat a nearly fiveminute total lunar eclipse this morning. a lunar eclipse happens when the sun earth and moon form a straight line in space with the earth smack in the middle. nasa says lunar eclipses typically happen at least twice a year but this eclipse is the third in a series of four in a row known as a tetrad the first was on april the second was in september the next is saturday and there will be one more on september. parts of south america india china and russia also will be able to see the eclipse but it wont be visible in greenland iceland europe africa or the middle east

@highlight
the total eclipse will only last minutes and seconds

@highlight
people west of the mississippi river will have the best view

@highlight
parts of south america india china and russia also will see the eclipse
