billboardthe key to rocks longevity is it never defines itself into irrelevance so while there were some loud dirty guitars at the rock and roll hall of fame induction ceremony in cleveland on saturday night there was as much recognition for rocks antecedents in soul and blues speaking less to a particular taxonomy than a spirit thats beyond words. rock hall induction ceremony lou reed would be amused says sister. others receiving rock hall honors included paul butterfield blues band early soul act the royales singer bill withers punk rockers green day lou reed joan jett the blackhearts and stevie ray vaughan. the old soul legend hasnt performed live in many years but had hinted in the months leading up to the induction ceremonies that he might sing once more withers sounded great though he may have an even brighter future in standup

@highlight
paul mccartney honors ringo starr at rock and roll hall of fame induction ceremony

@highlight
green day lou reed joan jett the blackhearts also honored
