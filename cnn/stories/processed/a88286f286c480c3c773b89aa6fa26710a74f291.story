universals furious is about to make historyfurious the final film from the late paul walker is expected to gross million or more when opening at the north american box office this weekend the top showing ever for an april title not accounting for inflationdomestically it is getting the widest release in universals history with a theater count of roughly including imax locations eclipsing despicable me anything north of is usually reserved for summer tentpoles and yearend titles. furious is expected to usher in a string of megaopenings at the box office this year disney and marvels the avengers age of ultron set to open may hasnt come on tracking yet but some forecasters are already suggesting it could score the top opening of all time domestically eclipsing the recordbreaking start of the avengers million in. the current recordholder for top april opening domestically is captain america the winter soldier which debuted to million from theaters last year. overseas the movie is also poised to do massive business putting its global debut north of million furious is opening dayanddate around the world on screens in territories save for a few major markets including china russia and japan

@highlight
the film is expected to gross million or more

@highlight
paul walker died in a car crash during filming

@highlight
furious poised to nab the biggest opening of so far
