theres an old saying which states the cape verde islands are home to a greater number of musicians per square kilometer than any other country in the world. we try to invite lots of producers and a lot of journalists from around the world to see the festival and the musicians from cape verde said jose da silva long time manager of the late cape verdean songstress cesaria evora. he hopes that by exposing musicians to a range of experienced industry professionals and toplevel musicians they will become equipped with the tools and ambition to take the music of cape verde across the globe. besides fish it is pretty common for cape verde to say our biggest richness is in music and culture said christine semba of womex an international networking platform for the world music genre

@highlight
cape verde seeking to tapinto rich cultural heritage

@highlight
tiny island nation wants to grow creative economy
