from banking to hospitality and real estate to ecommerce ashish thakkar built his vast business empire from scratch the ugandan tycoon started small with a little computer shop that kept him busy after leaving school at these days his mara group spans over countries and hes been called africas youngest billionaire. ashish thakkar right now in the last years i have been active on the continent i have never seen so much global excitement around africa that ive seen today the climate and the ease of doing business on the continent has drastically improved unfortunately its one of those cases where perception is so different from reality the reality on the ground is so much better than the perception youve got to remember as africa we are countries some of the countries may be challenging and may have issues but that cannot be an excuse to generalize the entire continent which is unfortunately something that happens quite a bit. in late thakkar joined forces with the former boss of barclays bank bob diamond to start an investment fund focused on africa called atlas mara the powerful duo raised million through a share flotation well above the million target. but his vision spans beyond africa and earth thakkar was the first african to sign up for the opportunity to travel to space with richard bransons company virgin galactic

@highlight
ugandan ashish thakkar built a vast business empire

@highlight
the entrepreneur says the answer to unemployment lies in nurturing small businesses

@highlight
africas lack of legacy systems has sped up innovation on the continent
