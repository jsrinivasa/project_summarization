amid tight security a chinese court has convicted veteran journalist gao yu for revealing state secrets and sentenced her to seven years in prison. an outspoken journalist and press freedom advocate gao began her career as a reporter for the staterun china news service in and in recent years had been writing columns for overseas chineselanguage publications. there is no defense against state secret charges in china anything the party or the government want to label as state secrets will be labeled and treated as such they can even do it retroactively said nicholas bequelin the hong kongbased east asia director of amnesty international. she was arrested after the tiananmen square protests in and released more than a year later she was imprisoned for another six years for leaking state secrets in though the government has never disclosed details of that case

@highlight
accused of leaking a document revealing partys ideological battle plan to counter advocates of constitutional democracy

@highlight
amnesty her sentencing is in line with the very stern approach president xi jinpings team has taken on dissent

@highlight
gao was arrested in april last year ahead of the sensitive anniversary of the tiananmen square crackdown
