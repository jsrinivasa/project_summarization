sending boston marathon bomber dzhokhar tsarnaev to prison for the rest of his life would bring years of punishment and rob him of martyrdom jurors were told monday. no martyrdom just years and years of punishment day after day as he grows up to deal with the lonely struggle of dealing with what he did. bruck told jurors there are only two punishments for them to choose from death or life in prison without any possibility of parole. in deciding whether the former college student is executed for his crimes or spends the rest of his days in a highsecurity federal prison jurors must weigh the heinousness of his crime and the toll on his victims against socalled mitigating factors such as his relative youth mental health and family background and whether or not he is remorseful

@highlight
defense expected to show dzhokhar tsarnaev as a puppet of his dominant older brother

@highlight
a deadlocked jury would result in an automatic life sentence for tsarnaev
