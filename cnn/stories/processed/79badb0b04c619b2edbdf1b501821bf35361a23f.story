an arizona police chief on wednesday supported an officers decision to drive his car into an armed suspect saying that although the move could have killed the suspect deadly force was justified. video of the incident recorded february by the dashboard cameras of two marana police cars shows one of the cars running into a suspect with who had a rifle in the city about a half hour from tucson. my clients back was turned and the officer drove right into him she said it isnt that dissimilar to a police officer shooting a fleeing suspect in the back. in one of the dashcam videos an officer who was tailing a walking valencia at slow speed reports over the radio that the suspect has fired one round in the air with a rifle he is accused of stealing that morning from a walmart

@highlight
that deadly force was warranted

@highlight
chief if suspect ended up shooting people police would be answering different questions

@highlight
incident happened february in town near tucson arizona
