the lawyer for robert bates an oklahoma reserve deputy who fatally shot a man he meant to subdue with a taser on saturday released documents that he says verify some of bates training as a law enforcement officer. in his statement to investigators bates said he became an advanced tcso reserve deputy in wood said bates started working for the sheriffs office in late or but the sheriffs office has said bates had been a reserve deputy since. bates had one taser training class on march according to a document with a heading from the council on law enforcement education and training which sets the standards for training peace officers in the state wood said the council requires only one handson class on use of a taser. bates an insurance company executive has gone to his own defense in an interview friday with the today show on nbc bates said he had the documentation to show he had completed the necessary training required of reserve deputies

@highlight
reserve deputy robert bates said he meant to use a taser but accidentally shot and killed a man

@highlight
lawyer for slain mans family says bates wasnt qualified to be on the force and received preferential treatment

@highlight
robert bates has met all the requisite training required by oklahoma to be a reserve deputy bates lawyer says
