police in the indian city of malegaon in the western state of maharashtra are requiring identity cards for an unusual group of residents cattle. following a recent statewide ban on the sale and consumption of beef authorities in the city have asked residents to take a mugshot of their cattle and submit it to the police. maharashtra is not the only indian state to tighten its laws on cow slaughter. so far over owners have complied with the police order and more are lining up outside police stations across the city to get their livestock photographed

@highlight
authorities in the indian city of malegaon have asked residents to take a mugshot of their cattle

@highlight
cows are revered by the majority hindu population and many parts of the country have laws banning the slaughter of cattle

@highlight
officials in malegaon believe this is the best way to solve cow slaughter cases and enforce the law
