atop the amazon bestselling books list this month sat an unexpected title secret garden it wasnt frances hodgson burnetts novel about a sour little girls magical place making a book club comeback. johanna basfords secret garden an inky treasure hunt coloring book now at no on amazon along with her second effort enchanted forest an inky quest coloring book no balance angies extreme stress menders volume by angie grace no and the mindfulness colouring book antistress art therapy for busy people by emma farrarons no on amazon uk are selling at a rapid clip. mindfulness and meditative coloring are recurring themes in the growing adult coloring book industry a search for adult coloring books on amazon or barnes and noble will yield several books of mandalas a ritual symbol in buddhism and hinduism that represents the universe waiting to be colored in. the trend doesnt seem to be letting up basford is working on a third title farrarons has been commissioned for a second book and mucklow and porter will release color me stressfree in september

@highlight
coloring books made for adults are popular on amazons bestselling books list

@highlight
books with calming meditative and spiritual themes appeal to adults looking to unwind
