when drinkers in claytons a beachfront bar in south padre island texas belly up for a round of shots bartender casey belue can usually guess what theyll order. its so easy to drink that you forget youre drinking alcohol its very sweet you hardly taste the whiskey at all said zachary jones a bartender at community smith in atlanta ive known people who cant do shots but they can do fireball. fireball is especially popular among young drinkers and women many of whom say they like that it doesnt singe their throat and leaves a dentynelike aftertaste. fireball wasnt always fireball it used to be known as dr mcgillicuddys fireball whisky before its maker the sazerac co rebranded it in sales were modest until about five years ago when sazerac hired a national brand ambassador richard pomes to spread the word about fireball through event planning and bartender outreach

@highlight
fireball cinnamon whisky is the fastestgrowing big brand of liquor in america

@highlight
the liquor has dethroned jagermeister as americas party shot of choice

@highlight
whisky expert fireball is an incredible phenomenon the growth has just been astounding
