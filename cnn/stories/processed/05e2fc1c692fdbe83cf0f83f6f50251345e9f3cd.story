it really depends what you want boy girl young old. at the camp where we finally met the man face to face there was no attempt at subterfuge we spoke in normal tones in full view of the children playing i could have had one of them i was told but because id specified a younger child theyd only identified one so far a did i want to consider an older girl a maybe she could look after the and cook and clean either way two girls would be ready tomorrow he said i could see them then. when our colleague want to see them he was shown a group of children and asked which one he wanted to take one two maybe he escaped by saying he needed to check with his madam me. i called the man picked up and began referring to me as sister i told him we wanted to know what wed need to do if we decided we did want to foster the children

@highlight
team finds a man at unofficial displaced camp willing to provide children to be fostered

@highlight
he says he cant take money for them but eventually demands for two girls
