for years warren weinsteins family frantically searched for details about his whereabouts and pushed for his release. as he announced weinsteins death thursday us president barack obama praised what he said was weinsteins lifelong dedication to service first as a peace corps volunteer and later as a usaid contractor weinstein obama said was someone who willingly left the comforts of home to help the people of pakistan focusing his work on helping families escape poverty to give their children a better life. that month captors released us army sgt bowe bergdahl and that buoyed hopes from weinsteins family that he could also be freed. just a few months after weinsteins capture al qaeda leader ayman alzawahiri released a recording claiming the terror group was holding weinstein and demanding among other things that the united states end airstrikes in pakistan

@highlight
warren weinsteins wife says the family is still searching for answers

@highlight
officials say weinstein and another al qaeda hostage were accidentally killed in a us drone strike

@highlight
gunmen abducted the usaid contractor from his home in pakistan in
