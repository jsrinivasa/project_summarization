as soon as the buses parked the people scrambled onto the airport tarmac. at the airport the landing strips and airport terminal were untouched by saudi bombs but buildings on the outskirts of the airport and planes along the airstrip had been blown to bits. the houthi rebels control sanaa including the airport but the saudis are bombing the city and thus control air access in a way so getting people out requires coordination. the passengers were mostly indian nationals plus yemenis and people from other countries who had been working in yemens capital sanaa they sprinted or walked with deliberation to the airplanes the stress of living in a war zone showed on the passengers faces nobody wanted to be left behind

@highlight
air india has evacuated people in recent days from yemen indian official says

@highlight
passengers could only bring carryon luggage onto the airplane

@highlight
the saudis have not destroyed the airstrips which are controlled by houthis
