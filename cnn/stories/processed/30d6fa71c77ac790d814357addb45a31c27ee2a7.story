sofia vergaras exfiance is speaking out about their dispute over frozen embryos created while they dated. usually when embryos are created whether the couple is married or just consenting adults theres usually a power of attorney that is described to these embryos if they are frozen for future use he said. after two attempts failed to bring fertilized embryos to term they created two more embryos using her eggs and his sperm. when we create embryos for the purpose of life should we not define them as life rather than as property he said a woman is entitled to bring a pregnancy to term even if the man objects shouldnt a man who is willing to take on all parental responsibilities be similarly entitled to bring his embryos to term even if the woman objects

@highlight
loeb says he filed a complaint against the actress to prevent her from destroying their two embryos

@highlight
the couple created the embryos while they were engaged
