while the fight against isis in syria and iraq holds the worlds gaze a simultaneous transformation is getting less attention the deterioration of al qaeda. indeed al qaeda has virtually no capacity to carry out attacks in the west the last successful al qaeda attack in the west was the london transportation system bombings a decade ago. the creation of the terror groups south asia branch was seen by some terrorism analysts as an attempt to steal some of the limelight from isis which is embroiled in a public dispute with al qaeda for leadership of the global jihad movement. meanwhile isis continues to attract western recruits and also inspire homegrown terrorists in the west but the core al qaeda organization that killed almost men women and children on is on life support

@highlight
al qaeda confirms that two of its leaders were killed in january drone strikes

@highlight
other leaders have been killed or captured recently

@highlight
the terrorist groups capacity to carry out attacks in the west has been greatly diminished
