four months after the end of the massive occupy protests that clogged hong kongs streets in a bid for greater voting rights another confrontation is heating up in the former british colony. hong kongs leaders argue its latest proposal fulfills the longawaited promise of universal suffrage by giving each resident the right to cast a vote. to become law the proposal must receive the support of two thirds of hong kongs legislators. hong kongs leadership has warned prodemocracy legislators against the veto saying the city might never again receive a chance for political reform

@highlight
reform proposal would give hong kongers right to vote for their next leader in

@highlight
but candidates would have to be approved by a mostly probeijing committee

@highlight
prodemocracy legislators have vowed to veto proposal
