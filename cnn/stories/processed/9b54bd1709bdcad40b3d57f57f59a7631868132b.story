washington was rocked late thursday by shootings one at the gates of the us census bureaus headquarters and another in a popular area packed with restaurant patrons the shootings were connected authorities said. they began with what authorities believe was a domestic kidnapping incident according to dc police chief cathy lanier the suspects vehicle was spotted outside the census bureau which is in suitland maryland. a guard apparently approached the vehicle and saw two people arguing. that guard was then shot at least once in the upper body said prince georges county fire department spokesman mark brady

@highlight
authorities believe the two shootings are connected

@highlight
a suspect leads police on a wild chase firing at multiple locations

@highlight
a census bureau guard is in critical condition a fire official says
