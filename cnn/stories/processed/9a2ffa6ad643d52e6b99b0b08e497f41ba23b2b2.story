israeli prime minister benjamin netanyahu criticized the deal six world powers struck to thwart irans nuclear ambitions saying he sees better options than this bad deal or war. it does not roll back irans nuclear program it keeps a vast nuclear infrastructure in place not a single centrifuge is destroyed not a single nuclear facility is shut down including the underground facilities that they built illicitly thousands of centrifuges will keep spinning enriching uranium netanyahu said sunday thats a very bad deal. netanyahus most recent argument against the iran nuclear deal was similar to the one hed made in a march trip to washington when he addressed a joint session of congress fueling a republican push to have the deal sent to congress before its implemented. netanyahu said iran is a country of congenital cheating and that it cant be trusted to abide by the terms of the deal which lasts years with some provisions extending well beyond that

@highlight
netanyahu says third option is standing firm to get a better deal

@highlight
political sparring continues in us over the deal with iran
