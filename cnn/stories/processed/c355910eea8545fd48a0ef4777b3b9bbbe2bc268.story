chanting no justice no peace protesters rallied in baltimore late tuesday the same day police released the names of the officers involved in the arrest of freddie gray. earlier in the day the baltimore police department released the names of six police officers suspended with pay. of the six officers three were on bikes and initially approached gray another made eye contact with gray another officer joined in the arrest after it was initiated and one drove the police van kowalczyk said. while the court documents allege that one of the arresting officers garrett miller took gray into custody after finding a switchblade in his pocket the gray family attorney called the allegation a sideshow gray was carrying a pocket knife of legal size attorney william murphy said

@highlight
we have the power and today shows we have the numbers says a protester

@highlight
the justice department is looking into whether a civil rights violation occurred

@highlight
autopsy results on gray show that he died from a severe injury to his spinal cord
