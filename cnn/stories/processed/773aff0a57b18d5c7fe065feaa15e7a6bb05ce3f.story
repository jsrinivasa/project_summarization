shortly after being elected chief prosecutor baltimore city states attorney marilyn mosby said prosecutors in the hardscrabble town had the toughest job in america. mosby is married to baltimore city councilman nick mosby who represents areas of west baltimore where riots erupted earlier this week the couple have two young daughters. mosby who grew up in boston is the youngest chief prosecutor of any major city in the united states according to the states attorneys website. baltimore prosecutors get to see it all in court weve got the toughest job in america she said in a statement after the election

@highlight
prosecutor marilyn mosby has only been on the job since january

@highlight
she comes from a long line of police officers

@highlight
i think that she will follow where the evidence leads i do not think she will follow just public opinion says a supporter
