feeling so happy you just cant stand it you might want to pop some acetaminophen. a new study has found that acetaminophen the main ingredient in tylenol most forms of midol and more than other medicines reduces not only pain but pleasure as well. the authors of the study which was published this week in psychological science say that it was already known that acetaminophen blunted psychological pain but their new research led them to the conclusion that it also blunted joy in other words that it narrowed the range of feelings experienced. this means that using tylenol or similar products might have broader consequences than previously thought said geoffrey durso a doctoral student in social psychology at ohio state university and the lead author of the study rather than just being a pain reliever acetaminophen can be seen as an allpurpose emotion reliever

@highlight
subjects taking acetaminophen reacted less strongly to both pleasant and unpleasant photos

@highlight
each week million americans use the pain reliever

@highlight
unknown whether other pain products produce the same effect
