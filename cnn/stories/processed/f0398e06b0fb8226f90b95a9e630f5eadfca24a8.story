lane bryant has come up with a devil of an idea to market its lingerie while poking fun at a competitor. our imnoangel campaign is designed to empower all women to love every part of herself chief executive officer linda heasley said in a statement lane bryant firmly believes that she is sexy and we want to encourage her to confidently show it in her own way. the campaign is a notsosubtle dig at victorias secret and its very popular angels line which caters to smaller women the lingerie giant was criticized last year for a campaign featuring the words perfect body over images of slender supermodels and business insider reports that it is under increasing pressure from consumers to offer larger sizes. lane bryants campaign is getting positive buzz in social media land with the company being hailed for celebrating beauty of all shapes and sizes

@highlight
the company says it is seeking to redefine sexy

@highlight
victorias secret was criticized for its perfect body campaign
