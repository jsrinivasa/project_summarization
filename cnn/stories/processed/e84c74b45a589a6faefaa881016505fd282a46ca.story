former new england patriots star aaron hernandez will need to keep his lawyers even after being convicted of murder and other charges in the death of odin lloyd. the families of de abreu and furtado filed civil suits against hernandez and a judge froze his million in assets pending the outcome of the doublemurder trial the freeze includes the disputed million signing bonus payment hernandez claims he is owed by the new england patriots. who was odin lloyd. hernandez is also being sued by a man who claims hernandez shot him while they were in a limousine in miami in february

@highlight
aaron hernandez has been found guilty in odin lloyds death but his troubles are not over

@highlight
he also faces murder charges in suffolk county massachusetts but trial was postponed

@highlight
in addition hernandez will face two civil lawsuits one is in relation to suffolk county case
