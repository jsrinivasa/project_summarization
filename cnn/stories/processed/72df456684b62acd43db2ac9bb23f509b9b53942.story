as aides politely tried to rush ted cruz from an event in cedar falls to one in cedar rapids iowa on thursday the presidential candidate continued shaking hands with anyone who wanted to meet him. hes more than comfortable talking about his own faith and telling the story of how his father became a christian and a pastor rafael cruz whos become a celebrity among christian conservatives will frequently visit iowa over the next year cruz told voters and cruzs iowa director bryan english is a former pastor. later in april voters in iowa will see the bulk of the gop field tackle these issues when they take the stage at an event hosted by the iowa faith and freedom coalition while the past two winners of the iowa caucuses santorum and huckabee are likely running for president again steve scheffler president of the group argued that the field is wide open in terms of whos going to win favor among evangelicals. if were going to win weve got to bring that coalition together he said in cedar falls and i think we can do that

@highlight
ted cruz has built a brand as a stalwart conservative on fiscal issues

@highlight
but hes also eager to champion social issues at a time when many republicans are eager to avoid them

@highlight
cruz says the gop needs to unite young libertarianminded voters and evangelicals
