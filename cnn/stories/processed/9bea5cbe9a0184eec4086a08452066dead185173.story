imprisoned soldier chelsea manning can now communicate with the world in characters or less. last year a kansas judge granted her request to be formally known as chelsea elizabeth manning. manning who is serving a prison sentence for leaking thousands of classified documents appears to have joined twitter this week. in a series of tweets the prisoner formerly known as bradley manning said she will be using a voice phone to dictate her tweets to communications firm fitzgibbon media which will post them on her behalf

@highlight
manning is serving a sentence for leaking thousands of classified documents

@highlight
she says she will be using a voice phone to dictate her tweets
