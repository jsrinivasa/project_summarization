an offduty member of the uniformed division of secret service was arrested friday in washington and charged with firstdegree attempted burglary a felony and one misdemeanor count for destruction of property the dc metropolitan police department reported. his next court date is april the us attorneys office said the charge of attempted firstdegree burglary carries a sentence of up to five years in prison the charge of destruction of property carries a sentence of up to days in jail andor a fine of up to. while the dc metropolitan police officer was at the scene baldwin drove up said he was a police officer and asked to speak with the woman the police document said. arthur baldwin was arrested at a womans residence in southeast washington according to documents provided by the police department he has been placed on administrative leave and his security clearance has been suspended the secret service said

@highlight
offduty member of the uniformed division of secret service arrested friday

@highlight
police said he was charged with trying to break into a womans residence
