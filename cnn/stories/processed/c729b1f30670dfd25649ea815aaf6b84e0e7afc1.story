parts of miamidade countys skyline was hidden from view monday as smoke from a growing wildfire loomed over portions of the florida county. the florida forest service and miamidade fire rescue have worked around the clock to protect southwest miamidade county florida agriculture commissioner adam h putnam said in a statement. what started as a nonthreatening and seemingly shrinking grass fire on sunday consuming fewer than acres according to miamidade fire rescue battalion chief al cruz grew to be more than times that within the next hours. by monday night the fire had burned nearly acres and was contained the fire department said

@highlight
the wildfire started in miamidade county on sunday

@highlight
by monday night it had grown to nearly acres

@highlight
the fire was contained officials said
