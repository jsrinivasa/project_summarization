easter is a cornerstone event in the christian faith but its surrounded by interesting quirks. heres a journey from the vatican to the holy land around the moon and the earths tilting axis to germany and the united states to try to explain the complex holiday called easter and youll learn to how to color easter eggs with koolaid. when christian bishops first convened at the council of nicaea in the year they made a rule to determine the date of easter so as to fairly reliably pin it to passover. but like christmas with its tree ornaments and santa claus easter has picked up its peripheral trappings the bunny and colorful eggs unlike christmas it doesnt fall on the same day every year but shifts around in spring depending upon cosmic events

@highlight
easter is a key event in the christian faith but where did the easter bunny come from

@highlight
why is the date different every year and what does it have to do with the moon
