the number of new hiv infections in a rural indiana county has grown according to the centers for disease control and prevention the institute is working with state health leaders to control the severe outbreak which has spread among users of a prescription opioid called opana. scott county the epicenter of the outbreak has only one doctor who deals with infectious disease but the doctor is not an hiv specialist the state department of health said since the rampant hiv outbreak was first noticed in middecember the state has tried to flood the area with additional resources indiana declared a public health emergency in that county in march. health leaders worry about the spread of hiv and other diseases such as hepatitis c around the country as the number of illegal prescription drug users has grown there has been a increase in hepatitis c between and the majority of the increase believed to be from injection drug abusers the cdc said. another reason this infection has spread so rapidly is the nature of the drug itself opana as the prescription opioid is known needs to be injected more than once a day duwve said residents have reported injecting it four to times a day to stay under its influence when people start to feel the drug wear off after about four hours they begin to feel sick and go into withdrawal often theyll turn to an injecting partner in the same house who will share their needle and their drug to give the person relief from these symptoms

@highlight
the number of new hiv infections in indiana has grown to cases

@highlight
some families in isolated communities use illegal drugs and share needles as a community activity a health official says

@highlight
public health officials urge vigilance to stop the outbreak from gaining ground
