eight iranian border guards have been killed in clashes with militants near the border with pakistan iranian state media reported. one of the deadliest was in october when iranian border guards were killed near the city of saravan. the news agency cited ali asghar mirshekari the deputy governor of irans sistanbaluchestan province who said the militants crossed into the country from pakistan iranian officials have reportedly asked pakistani authorities to catch the surviving assailants. if the militants entered pakistan after the attack they will be apprehended and brought to justice it said

@highlight
the pakistani government says its security agencies are investigating

@highlight
a group believed to be based in pakistans balochistan province claims responsibility
