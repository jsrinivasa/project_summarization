on october the italian government announced the end of mare nostrum a naval mission that rescued wouldbe migrants in peril as they tried to cross the mediterranean to seek security and a new life in europe. triton has about onethird of the funding of mare nostrum with just six ships and patrol boats two planes and one helicopter it was designed as a policing rather than a humanitarian mission at its inception klaus rosler operations director for frontex said triton is not a replacement for mare nostrum nor was frontex a coordinating body for search and rescue operations. without european support the italian government cut back the naval assets dedicated to rescuing migrants mare nostrum which had been launched after some people died when two migrant ships sank in was replaced by the more modest operation triton under the auspices of the european unions border agency frontex. read more migrant deaths at sea what is europe going to doread more why migrants are risking their lives to reach italyread more i enter europe or i die desperate migrants rescuedread more how do illegal immigrants get into europe

@highlight
italian navys mare nostrum mission to rescue wouldbe migrants in peril rescued an estimated people

@highlight
operation ended in october but the tide of people trying to cross the mediterranean has not abated

@highlight
italy has borne brunt of task of picking up sheltering and providing food and medical help to illegal migrants
