a prosecutor has dismissed allegations that argentine president cristina fernandez de kirchner tried to cover up irans involvement in a bombing in buenos aires. the bombing of a jewish community center in the argentine capital is the deadliest terror attack in the countrys history eightyfive people were killed and hundreds were injured. the case became of high interest globally after the original prosecutor who brought the allegations was found dead in january. a second prosecutor took the reins after nismans death and took the case to court in february a judge dismissed the case saying that nismans allegations did not hold up

@highlight
the prosecutor looking at allegations against argentinas president says no crime committed

@highlight
the original prosecutor who brought the case was found dead in january
