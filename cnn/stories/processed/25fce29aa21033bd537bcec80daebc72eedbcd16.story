its with some trepidation that i set off for the al marmoum camel race in dubai as the only gulf national in the cnn team i am expected to be familiar with camel racing an ancient tradition in the region but despite my attempts to dress in a kandora a uae national dress my modern outlook on life means i am a total outsider in this world. as we excitedly watch the final race draw to a close the sun sets across the sky of this metropolis camel racing bears no relevance to the modernity embraced here but this oldfashioned sport is equally as fascinating. we take our place on the floor and slowly the world of camel racing begins to open up to us. a cheerful farm owner leads us inside the majlis meeting area where the camel owners are gathering the wide space of the majlis feels like an ornate theater

@highlight
camel racing is a centuriesold tradition in the gulf

@highlight
modern technology is changing the sport

@highlight
camels compete for thousands of dollars in prize money
