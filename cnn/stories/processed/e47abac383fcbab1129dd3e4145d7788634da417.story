an egyptian court has sentenced people to life in prison for their role in the august burning of a christian church in the giza province village of kafr hakim state news reports. the virgin mary church was torched and looted by a mob some of whom chanted against coptic christians and called for egypt to become an islamic state one of at least churches and many more businesses and homes targeted that august the advocacy group human rights watch reports others attacked included st george church in sohag a city south of cairo on the nile river and prince tadros church in fayoum which is southwest of cairo according to reports. in addition to those getting life sentences two minors were sentenced to years in prison and fined egyptian pounds about egypts official egynews reported. christian churches across egypt stormed torched

@highlight
minors were sentenced to years in prison in addition to adults getting life

@highlight
of the defendants were sentenced in absentia

@highlight
the virgin mary church was burned along with dozens of others in august
