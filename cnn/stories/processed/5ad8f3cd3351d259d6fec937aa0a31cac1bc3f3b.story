a man is in custody after he called for an ambulance only to have french authorities come and discover weapons ammunition and evidence of his plans to target churches an attack that someone in syria requested a top prosecutor said wednesday. there was no indication he ever made it but molins said computer searches revealed that the man had been communicating with someone in syria who asked him to target a church. but authorities found more than a man with a gunshot wound in his thigh molins said ghlam was someone suspected of killing a woman hours earlier and who then intended to launch a largerscale terror attack in the near future. he had been noticed wishing to go to syria molins explained the prosecutor added that authorities found nothing to suggest he was imminent threat but he was under surveillance

@highlight
suspect identified by french authorities as sid ahmed ghlam

@highlight
prosecutor someone in syria asked the arrested man to target french churches

@highlight
evidence connects terror plot suspect to the killing of aurelie chatelain he says
