at least two people were taken into custody as protesters upset over the death of freddie gray scuffled thursday evening with police on the streets of baltimore. baltimore police requested and received additional personnel from maryland state police thirtytwo troopers arrived to help with crowd control and serve in a backup capacity for police according to erin montgomery a spokesperson for gov larry hogan. gray died sunday one week after he was arrested by baltimore police. while baltimore police say five of the six officers involved in the arrest have provided statements to investigators the department has not released details of what the officers said or how gray might have suffered the fatal injury

@highlight
two people are taken into custody but the protests on the whole are peaceful

@highlight
baltimore police commissioner sits down with the gray family
