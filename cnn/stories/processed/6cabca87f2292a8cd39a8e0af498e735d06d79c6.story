judge jeffrey sutton doesnt have a lot of company on the appeals courts these days. sutton is the former state solicitor for ohio where he handled appeals for the states attorney general he clerked for justices antonin scalia and lewis powell scalia once called sutton one of the very best law clerks i ever had. sutton who sits on the us circuit court of appeals in cincinnati penned the only recent appellate court decision to uphold state bans on samesex marriage his opinion issued in november goes up against an avalanche of judicial rulings striking down such bans. its an ideal piece of judicial craftsmanship said ryan t anderson of the heritage foundation who believes that state bans on samesex marriage are constitutional sutton more or less takes each and every argument that the other side has made and then one by one by one he explains why it doesnt work

@highlight
avalanche of appellate rulings have struck down state bans on samesex marriage

@highlight
judge jeffrey sutton is behind only recent appellate decision to uphold such state bans

@highlight
judge suttons opinion stands alone says official with gay rights advocacy group
