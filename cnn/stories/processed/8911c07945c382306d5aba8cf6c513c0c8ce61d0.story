four workers died in a massive oil rig fire that raged for hours off the coast of mexico wednesday. the state oil company hasnt said what caused the fire on the platform which is located in the gulf of mexicos campeche sound the fire began in the platforms dehydration and pumping area pemex said. mexican state oil company pemex said workers were injured in the blaze which began early wednesday morning two of them are in serious condition the company said. the company denied rumors that the platform had collapsed and said there was no oil spill as a result of the fire

@highlight
the fire on a platform in the gulf of mexico has been extinguished pemex says

@highlight
workers were injured in the blaze according to the state oil company

@highlight
four workers were killed in the oil rig fire which started early wednesday
