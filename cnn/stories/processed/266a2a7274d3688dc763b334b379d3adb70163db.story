theres a booming black market in hong kong but its not for fake apple watches or the iphone. the frenzy in hong kong over the buttery treats is by no means an isolated example. its popularity has spurred bakeries to make and sell knockoffs and the original store has signs warning against buying fake jennys cookies. instead people are going crazy for tins of butter cookies

@highlight
tourists and locals queue for several hours to get their hands on jennys butter cookies

@highlight
people are even hired to stand in line to buy the cookies which are later sold at an markup

@highlight
food frenzies have also taken place in other parts of the world
