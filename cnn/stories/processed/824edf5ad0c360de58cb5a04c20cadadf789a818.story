slamming world powers framework nuclear deal with iran israeli prime minister benjamin netanyahu on friday demanded that any final deal include a clear and unambiguous iranian recognition of israels right to exist. obama called netanyahu after thursdays announcement of the framework agreement to reassure him that israels security will remain a prime concern in the ensuing negotiations and the final deal which faces a june deadline. netanyahu has lobbied hard for a deal that dismantles and disassembles irans nuclear infrastructure rather than limiting its usage or repurposing irans facilities but such stipulations were not part of the framework agreement he has also called for the removal of sanctions to be tied to the reduction of what he characterizes as irans aggression in the region not just its moves on its nuclear program. theres never been any dispute about his take but the prime minister has sharpened his rhetoric in recent days saying the deal increases the risk of a horrific war

@highlight
israeli prime minister benjamin netanyahu slams nuclear framework deal with iran

@highlight
he says it would legitimize irans nuclear program and bolster its economy
