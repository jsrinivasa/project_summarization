the french prosecutor leading an investigation into the crash of germanwings flight insisted wednesday that he was not aware of any video footage from on board the plane. robins comments follow claims by two magazines german daily bild and french paris match of a cell phone video showing the harrowing final seconds from on board germanwings flight as it crashed into the french alps all on board were killed. spohr traveled to the crash site wednesday where recovery teams have been working for the past week to recover human remains and plane debris scattered across a steep mountainside. germanwings crash compensation what we know

@highlight
marseille prosecutor says so far no videos were used in the crash investigation despite media reports

@highlight
journalists at bild and paris match are very confident the video clip is real an editor says

@highlight
andreas lubitz had informed his lufthansa training school of an episode of severe depression airline says
