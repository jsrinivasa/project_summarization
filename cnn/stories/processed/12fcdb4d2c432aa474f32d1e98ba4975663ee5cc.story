as tourists stroll between yellowstones active geysers taking selfies in front of thousands of bubbling boiling mud pots and hissing steam vents they are treading on one of the planets greatest time bombs. compared to yellowstones past mount st helens was a picnic when it covered washington state with an ash bed about the size of lake michigan in mount pinatubo which exploded in the philippines in doesnt begin to scratch the surface of yellowstones roar. even if the next explosion is many thousands of years away yellowstones cavernous heat tanks poke up an occasionally surprise the last lava flow was some years ago usgs says. yellowstones magma reserves are many magnitudes greater than previously thought say scientists from the university of utah

@highlight
scientist measured the thousands of small earthquakes in yellowstone to scan the earth underneath it

@highlight
they discovered a vast magma reservoir fueling a vast one scientists already knew about

@highlight
prehistoric eruptions of yellowstone supervolcano were some of earths largest explosions
