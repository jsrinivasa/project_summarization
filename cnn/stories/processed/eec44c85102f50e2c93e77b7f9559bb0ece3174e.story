it has all the key points youd expect on a birth certificate babys weight length and date of birth confirmed with an official insignia the difference here is the governing authoritys stamp the islamic state in iraq and syria. if someone does complain especially in syria isis does actually try to deal with it he says thats why theyve been seen by many in syria as imposing order especially in areas where multiple parties rebel factions and the syrian government were previously in control. a general theme for isis is that they try initially when they seize control to portray themselves as more just more fair to the inhabitants than the previous ruler explains altamimi for example in syria the first thing isis did was lower the price of bread this is as much about winning over the population as it is about religious rulings. families flee isis in iraq

@highlight
isis is known for brutal takeovers and medieval justice but it sees itself as a state

@highlight
official documents show just how far their rules affect daily life
