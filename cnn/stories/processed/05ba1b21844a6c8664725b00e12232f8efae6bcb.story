the hatton garden heist as it will surely come to be known was every safe deposit box holders nightmare every movie directors dream. officers anticipate the process to take approximately two days the statement said at this stage it is believed that approximately safe deposit boxes were opened during the burglary officers are working closely with hatton garden safe deposit ltd to establish the identities of those affected police will be contacting victims directly as and when they are identified. police were offering few details wednesday of the robbery at hatton garden safe deposit ltd detectives on the scene were carrying out a slow and painstaking forensic examination police said in a statement. the website of hatton garden safe deposit ltd says the company was founded in and offers a secure and costeffective solution to store and protect important and irreplaceable personal belongings

@highlight
robbers may have taken advantage of a fourday holiday weekend

@highlight
estimates of the value of the items taken rage from hundreds of thousands of pounds to million pounds

@highlight
the heist took place in a historic heart of londons jewelry business
