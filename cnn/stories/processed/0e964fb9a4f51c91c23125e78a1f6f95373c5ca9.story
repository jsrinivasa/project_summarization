combining healthy eating with moral support jean nidetch became a heavyweight in the weightloss industry the founder of weight watchers died wednesday at her home in florida. jean was an inspiration and an innovator who leaves behind a legacy and program that has positively impacted the health and wellbeing of millions of people around the world said jim chambers president and ceo weight watchers international it is our honor and responsibility to carry on her legacy to help more people to transform their lives. after countless fad diets nidetch figured that accountability was the key to keeping the weight off at the time she was years old and weighed more than pounds. through the program of regular weighins peer support and accountability not only did nidetch lose pounds but she inspired millions more to shed pounds too

@highlight
jean nidetch started weight watchers in

@highlight
nidetchs philosophy its choice not chance that determines your destiny
