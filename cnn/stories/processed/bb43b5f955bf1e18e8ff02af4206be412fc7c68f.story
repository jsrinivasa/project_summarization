a photo of a baby boy being pulled from the rubble of the nepal earthquake has become the defining image of a disaster that has devastated the country. the house collapsed burying the child leaving his father shyam awal frantically searching for him amid the rubble. sonits face was exposed during the whole ordeal though a hooded top and shawl protected his head and body during the cold night spent under rubble. the baby was taken to bhaktapur hospital and found to be uninjured

@highlight
baby sonit awal found in rubble of nepal earthquake sunday morning

@highlight
spent hours buried under his home after quake
