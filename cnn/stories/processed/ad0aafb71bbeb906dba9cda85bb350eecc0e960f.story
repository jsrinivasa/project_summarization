a measles outbreak that affected more than californians since december is over the california department of public health declared friday. the outbreak began with dozens of visitors to two disney theme parks in the state the health department said of the cases occurred from december. prompt investigation of cases interviewing hundreds of contacts of infected people vaccinating hundreds of at risk people and increasing awareness among health care providers about measles helped to control this outbreak smith said. it has been days since the last known case of strain of measles the equivalent of two successive incubation periods said dr karen smith director of the health department

@highlight
officials say californians were affected by one strain five by other strains

@highlight
about of the people who could show health records were unvaccinated

@highlight
outbreak began in december among visitors to two disney theme parks
