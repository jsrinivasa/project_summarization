thursday will mark three weeks since saudi arabia began airstrikes on houthi rebels in yemen but there is as yet little sign that the rebels are being driven back that the fighting in yemen is dying down or that lives there are being saved. since it began on march saudi arabia has launched more than airstrikes saudi officials claim to have killed more than houthi rebels. to the contrary increasingly more yemenis appear to be fleeing the country attempting the dangerous trip in rickety fishing boats across the red sea to the horn of africa a trip historically made by people fleeing africa rather than the other way around hopes for stability not only in yemen but in the middle east in general are fading as fears grow that saudia arabia and iran are fighting a proxy war in yemen for regional domination. the houthis forced yemeni president abdu rabu mansour hadi from power in january though hadi still claims he is yemens legitimate leader and is working with the saudis and other allies to return to yemen those allied with hadi have accused the iranian government of supporting the houthis in their uprising in yemen

@highlight
saudi officials say houthi rebels killed but signs of progress appear scant

@highlight
civilian casualties continue to mount

@highlight
un security council favors houthi arms embargo
