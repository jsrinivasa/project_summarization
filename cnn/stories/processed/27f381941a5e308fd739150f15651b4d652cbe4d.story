the first step according to swartzberg is to go to the food and drug administrations website and find the official report for the recalled product youre worried about each report will list recalled items with their product codes which are typically categorized by the universal product code the number adjacent to the barcode or the stock keeping unit a specific number that would only be valid at the store where the product is being sold they will also include the recalled products useby dates and the geographical areas affected. the question how can i know if my food is safe to eat after a specific product recall. after crosschecking these details you should have a strong sense of whether your food product is safe to eat or needs to be trashed right away but as far as avoiding potential problems before learning such details about a food recall the consumer is at an automatic disadvantage. the recall is not based on the useby dates though said swartzberg if the product is within the useby date it should still be recalled this makes sense because the product was contaminated prior to purchase and no matter how fresh the product is it still may be contaminated

@highlight
find the fdas official report for the recalled product

@highlight
if the product is within the useby date it should still be recalled
