never mind. north korean leader kim jong un has backed out of next months visit to moscow for world war ii anniversary celebrations kremlin spokesman dmitry peskov said thursday. we were informed of the decision via diplomatic channels peskov said the decision is connected with north korean domestic affairs. the visit was highly anticipated because it would have marked kims first official foreign trip since inheriting the leadership of north korea in late

@highlight
next months visit to moscow by the north korean leader is off

@highlight
this victory day marks the years since the soviet victory over germany in world war ii
