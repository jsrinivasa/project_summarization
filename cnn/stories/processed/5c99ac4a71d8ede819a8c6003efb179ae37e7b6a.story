on day six of nepals tragedy life triumphed as rescuers pulled an from the rubble of a multistory residential building in one of kathmandus hardhit neighborhoods. other search and rescue teams continued to scour through kathmandus rubble thursday they are looking for survivors from the earthquake that struck saturday killing at least people and wounding almost according to nepali authorities another people were reported dead in india and in china. andrew olvera the head of the us team said his men rushed over with search dogs and equipment ranging from breaching tools to sophisticated cameras that can probe under the rubble. other people have been saved from under collapsed buildings in previous days including a man on tuesday and a boy on sunday the nepali military also released a photo of a dustcaked girl who they said was rescued wednesday after hours under the rubble

@highlight
death toll rises to more than

@highlight
pemba tamang shows no apparent signs of serious injury after rescue

@highlight
us special forces helicopter including americans to safety
