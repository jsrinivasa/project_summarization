robert boardwines path to fatherhood was unconventional but virginias appeals court said tuesday he is legally entitled to be a part of his sons life. the court of appeals of virginia decided differently in weighing the commonwealths assisted conception statute and denying bruces appeal to deny boardwine visitation. the plain meaning of the term medical technology does not encompass a kitchen implement such as a turkey baster the appeals court wrote in its decision. things were ok he thought he was going to be able to see the newborn as often as he wanted she thought he could have some involvement the appeals court decision says but she would be the sole parent hed be like any other friend certainly not have formal visitation

@highlight
in july joyce bruce got pregnant in an unusual way with repeated attempts using a turkey baster

@highlight
the man who gave her his sperm wanted to have a role in his sons life

@highlight
they ended up in court and he has won joint custody and visitation rights
