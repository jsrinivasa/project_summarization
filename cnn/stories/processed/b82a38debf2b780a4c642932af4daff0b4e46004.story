in case you needed a reminder that president barack obama isnt running for office again he just alienated not only republicans who have largely resented him from day one but the progressive base of democratic voters. in other words any political leader with even the dimmest grasp of economics let alone political pragmatism should run away from a new trade deal modeled on imitating and expanding nafta while its not surprising that republicans are siding with big business and against working americans in supporting the tpp its befuddling that president obama supports it. obama has argued with the progressive potentate elizabeth warren calling her wrong on trade policy the massachusetts senator is the same potentate to whom hillary clinton has been religiously prostrating what everyone does next will be critical for the elections and the future of democratic politics. at a town hall with msnbcs chris matthews on tuesday president obama said i love elizabeth were allies on a whole host of issues but shes wrong on this

@highlight
sen elizabeth warren has publicly criticized socalled fast track trade authority

@highlight
sally kohn why does president obama call her wrong and why is hillary clinton equivocating
