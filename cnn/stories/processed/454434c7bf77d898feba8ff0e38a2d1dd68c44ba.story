hamlet romeo and juliet a midsummer nights dream. for centuries these plays and three dozen more by william shakespeare have formed historys most heralded literary canon. but now they may have to make room for an addition to shakespeares famous oeuvre. new research indicates that double falsehood a play first published in by lewis theobald was actually written more than a century earlier by shakespeare himself with help from his friend john fletcher the findings were published this week by two scholars who used computer software to analyze the writings of the three men and compare it with the language of the newer play

@highlight
new research indicates that a play published in was written by william shakespeare

@highlight
scholar lewis theobald had passed the work off as his own

@highlight
texas researchers used software to analyze and compare the language of the men
