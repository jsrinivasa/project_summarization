debates on climate change can break down fairly fast there are those who believe that mankinds activities are changing the planets climate and those who dont. these factors include the effectiveness of a communitys public health and safety systems to address or prepare for the risk and the behavior age gender and economic status of individuals affected the epa says on its website impacts will likely vary by region the sensitivity of populations the extent and length of exposure to climate change impacts and societys ability to adapt to change. but a new way to talk about climate change is emerging which shifts focus from impersonal discussions about greenhouse gas emissions and power plants to a very personal one your health. this new way of talking about climate change and linking it to public health issues was part of a roundtable discussion tuesday at howard universitys college of medicine president barack obama joined us surgeon general dr vivek murthy and epa administrator gina mccarthy for a roundtable discussion on the topic as part of national public health week

@highlight
president obama attends howard university roundtable on climate change and public health

@highlight
linking climate change to how it affects a persons health is a new way to talk about the subject
