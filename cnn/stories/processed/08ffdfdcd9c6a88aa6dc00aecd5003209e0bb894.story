talk show host dr mehmet oz is defending himself against a group of doctors who accuse him of manifesting an egregious lack of integrity in his tv and promotional work and who call his faculty position at columbia university unacceptable. the email sent to columbias faculty dean for health sciences and medicine dr lee goldman said the group is surprised and dismayed that oz is on faculty and that he holds a senior administrative position oz is vice chair of the department of surgery at columbia university college of physicians and surgeons. the email was sent by dr henry miller a fellow in scientific philosophy and public policy at stanford universitys hoover institute it was signed by nine other physicians from across the country none of whom is affiliated with columbia they accuse oz of what they call manifesting an egregious lack of integrity by promoting quack treatments and cures in the interest of personal financial gain. in an email that the groups goal is for dr oz to resign from the columbia faculty and decide that hed prefer a career as a tv celebrity doctor

@highlight
ten physicians across the country have banded together to tell columbia they think having oz on faculty is unacceptable

@highlight
radiology professor says that he just wants oz to follow the basic rules of science

@highlight
tvs dr oz holds a faculty position at columbia universitys college of physicians and surgeons
