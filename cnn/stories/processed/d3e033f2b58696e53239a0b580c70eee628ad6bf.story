anthony ray hinton is years old. hinton had to get used to much worse for years he spent most of his time locked in a cell there was no going outside at night the only permissible eating utensil was a plastic spoon and life in the outside world passed him by hinton said his hardest day came in when he learned his mother had died. little things like using a fork going out at night tucking into bed without anyone checking on you theyre all strange for someone like hinton who spent nearly years on alabamas death row. he didnt get to see firsthand how the world changed in the decades since his life was defined by a preordained routine when to eat when to leave his cell when to sleep hinton didnt even see the moon and stars really for all these years

@highlight
anthony ray hinton was freed apri decades after conviction for two murders

@highlight
things like using a fork getting used to the dark are challenges now that hes out

@highlight
he says his sense of humor helped him survive years in prison
