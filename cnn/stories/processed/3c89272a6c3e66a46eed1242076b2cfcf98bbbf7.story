ahmed farouq didnt have the prestige of fellow al qaeda figure osama bin laden the influence of anwar alawlaki or the notoriety of adam gadahn. osama mehmood a spokesman for al qaeda in the indian subcontinent said that farouq and another top figure qari abdullah mansur were killed in a january drone strike in pakistans shawal valley. farouq an american died in a us counterterrorism airstrike in january according to the white house two al qaeda hostages warren weinstein of the united states and giovanni lo porto from italy were killed in the same strike while gadahn died in another us operation that month. before that farouq was the deputy emir of al qaeda in the indian subcontinent or aqis a branch of the islamist extremist group that formed in recent years

@highlight
ahmed farouq was a leader in al qaedas india branch

@highlight
he was killed in a us counterterrorism airstrike in january

@highlight
like adam gadahn farouq was american and part of al qaeda
