sarah brady who with her husband james brady pushed for stricter gun control laws including the brady handgun violence prevention act died friday her family said. sarah brady was also involved in gun violence prevention for the past years she was the chairwoman of the brady campaign to prevent gun violence and the brady center to prevent gun violence from until she died. james brady president ronald reagans press secretary was shot in the head by john hinckley jr during his attempt to assassinate reagan in brady spent the rest of his life in a wheelchair advocating against gun violence. brady campaign and center president dan gross said in a written statement that few people are responsible for saving as many lives as sarah and james brady

@highlight
nra says although it disagreed with sarah brady she was an honorable and respected woman

@highlight
sarah brady became involved in campaigns against gun violence after her son to picked up a loaded gun

@highlight
her husband died in august having spent the last part of his life in a wheelchair from being shot
