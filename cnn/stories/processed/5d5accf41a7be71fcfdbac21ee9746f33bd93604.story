three orthodox rabbis accused of planning and participating in the torture of jewish men who refused to divorce their wives were convicted tuesday of conspiring to commit kidnapping. the rabbis were part of a ring accused of accepting tens of thousands of dollars to orchestrate the kidnappings of jewish husbands to persuade them through torture involving electric cattle prods and screwdrivers to grant gets a document that jewish law requires a husband to present to his wife in order to be issued a divorce court papers said. rabbis mendel epstein jay goldstein and binyamin stimler were found guilty on one count of conspiracy to commit kidnapping in new jersey federal court goldstein and stimler were also convicted on charges of attempted kidnapping. the jury returned not guilty verdicts on the attempted kidnapping charges against mendel epstein and on more severe kidnapping charges against the three rabbis

@highlight
three rabbis found guilty of conspiracy to commit kidnapping in new jersey federal court

@highlight
they were accused of orchestrating kidnapping torture of jewish men who refused to allow their wives a religious divorce

@highlight
lawyers for the rabbis said they plan on appealing the verdicts
