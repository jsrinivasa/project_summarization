build a wall the length of the border with somalia recruit and train up thousands of new security officers give them better tactics and equipment. the notion of building a wall is somewhat grandiose since the border runs some kilometers or miles even with a wall militants could arrive by sea to coastal towns in kenya that have been islamist strongholds. security officers from multiple agencies raided homes buildings and shops carting away money cell phones and other goods hrw said they harassed and detained thousands including journalists refugees kenyan citizens and international aid workers without charge and in appalling conditions for periods well beyond the legal limit. some kenyans are now asking why security forces did not react to those warnings in advance by putting security in place

@highlight
kenyas security has been bogged down by concerns over civil rights

@highlight
kenyan muslims have been targeted in raids and robbed says human rights watch
