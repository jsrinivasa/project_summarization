hundreds of decomposed corpses were discovered buried in shallow graves in the streets of the northeastern nigerian town of damasak this past weekend according to local officials and a resident. collected over corpses from the streets and in shallow graves during our visit in damasak said idris karimbe one of the volunteers who took part in the burial. mustapha said the bodies were buried in clearly marked mass graves although mustapha did not give a precise number of corpses recovered damasak residents who participated in the exhumations put the figure at more than. those bodies in shallow graves have badly decomposed while those found on the streets were desiccated from exposure to dry mustapha added

@highlight
the town had recently been freed from the boko haram terror group

@highlight
volunteer from burial collected over corpses from the streets and in shallow graves
