heres a pop quiz whats better for americas status in the world. instead of losing valuable contributors to our economy because of their status as exoffenders we can develop apprenticeship and training programs that improve worker skills and jump start our economy. its an obvious answer but the unfortunate reality is that the united states leads the world in incarceration not education. where we were once the driving force of the global economy we now rank fifth in the world economic forums global competitiveness index key metrics in this index include the quality of a nations primary secondary and higher education systems

@highlight
cory booker the unfortunate reality is that the united states leads the world in incarceration not education

@highlight
at the same time we are losing the increasingly important race to educate our citizens
