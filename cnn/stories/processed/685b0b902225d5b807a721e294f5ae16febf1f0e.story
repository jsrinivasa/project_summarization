if you were mourning the loss of a loved one in china at least the entertainment might have cheered you up until now. the ministrys report said that stripteases undermined the cultural value of the entertainment business and asserted that such acts were uncivilized. in rural china hiring exotic dancers to perform at wakes is an increasingly common practice but is now the latest focus of the countrys crackdown on vice. in some areas of china the hiring of professional mourners known as kusangren is commonplace these can include performances although in recent times the dance acts have increasingly tended towards the erotic

@highlight
funeral strippers in rural china are the latest focus of the countrys crackdown on vice

@highlight
in some areas of china the hiring of professional mourners is commonplace but some performances are getting racy

@highlight
government report says that stripteases undermine the cultural value of the entertainment business
