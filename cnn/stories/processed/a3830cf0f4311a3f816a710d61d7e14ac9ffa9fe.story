pope francis risked turkish anger on sunday by using the word genocide to refer to the mass killings of armenians a century ago under the ottoman empire. armenian groups and many scholars say that turks planned and carried out genocide starting in when more than a million ethnic armenians were massacred in the final years of the ottoman empire. pope francis said sunday that catholic and orthodox syrians assyrians chaldeans and greeks were also killed in the bloodshed a century ago. this consternation over the use of the word genocide occurs regularly and armenians are equally upset when turkey protests it armenian foreign minister edward nalbandian rebuked turkey

@highlight
former turkish ambassador to the vatican calls use of the word genocide a onesided evaluation

@highlight
pope discusses massacres of armenians a century after they took place

@highlight
turkey denies the mass killings constituted a genocide
