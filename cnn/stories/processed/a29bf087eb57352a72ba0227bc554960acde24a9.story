when les moonves sits down at a restaurant in hollywood its usually the waiters lucky night as the president and ceo of cbs corp which includes showtime moonves runs a powerful television network in a town where nearly every waiter also wants to be an actor. but when he walked into craigs in west hollywood last year those roles were reversed this time moonves waiter gabriel salvador was the one serving up a mouthwatering opportunity that had nothing to do with the shrimp diavolo instead he was offering moonves an in to the most coveted matchup in boxing floyd mayweather vs manny pacquiao. thats when salvador told moonves his son trained at the hollywood gym owned by freddie roach pacquiaos trainer and that he would put in a good word for moonves. salvadors role ended after that dinner what followed was a series of meetings between moonves and both sides that eventually ended the stalemate keeping pacquiao and mayweather from entering the same ring the may fight on the payperview services of showtime and hbo is expected to gross as much as million

@highlight
gabriel salvador set up an initial meeting between a tv exec and manny pacquiaos trainer

@highlight
salvador is an actor and waiter at craigs restaurant in los angeles
