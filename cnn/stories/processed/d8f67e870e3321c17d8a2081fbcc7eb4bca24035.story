rick santorum says hed hoped indiana gov mike pence would veto the fix to his states religious freedom law rather than limiting its scope. its easier to ignore religious freedom than it is today the more popular issues dolan said in a way i appreciate the fact that we have political leaders like gov pence that are saying whoa wait a minute without questioning the rights of the gay community we also have to make sure that the rights of the religious community are protected. gay rights groups meanwhile have used the outrage over indianas law as well as a similar one that arkansas gov asa hutchinson signed into law and debate in at least other states over similar measures this year to renew their calls for state laws that bar discrimination based on sexual orientation. santorum the former republican senator from pennsylvania who is likely to mount another presidential campaign in said on cbs face the nation on sunday that pences decision to sign a followup bill which made clear the law couldnt be used to refuse services based on sexual orientation led to a limited view of religious freedom

@highlight
rick santorum says religious freedom debate is about government telling people what to do

@highlight
santorum a likely gop presidential candidate weighs in on indiana gov mike pences decision
