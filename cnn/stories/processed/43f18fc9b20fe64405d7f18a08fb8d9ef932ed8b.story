a mammoth fire broke out friday morning in a kentucky industrial park sending plumes of thick smoke over the area as authorities worked to contain the damage. according to a ge website its facility in the louisville appliance park is revitalizing manufacturing in the united states the park is large such that football fields could fit in one of its warehouses in the facility. that authorities didnt know what had caused the fire which had gone to at least four alarms. video showed both smoke and bright orange flames firefighters took up positions around the affected buildings spraying water from the periphery

@highlight
fire breaks out at the general electric appliance park in louisville kentucky

@highlight
city official no is believed to be injured or trapped
