last months soso jobs numbers confirm the challenges our economy still faces despite recent gains only jobs were added the lowest since december wages remain sluggish and unemployment may still be at but there is wide concern among labor experts and economists that too much job growth is in parttime lowincome work. lowwage workers have been the hardest hit since the onset of the financial crisis and lowwage jobs remain a fixture of the new economy nearly of the people in americas workforce are paid hourly and work parttime. hourly lowincome workers endure significantly greater fluctuations in their hours and less predictability in how much they earn than fulltime employees many dont even know their weekly schedules until the last moment a university of chicago study found that of early career hourly workers and who work parttime received a week or less of notice of their work schedules. our leaders have the unique responsibility to protect america and attend to the critical economic challenges of our time even with the weakerthanexpected jobs report we know our overall economy is improving yet incomeinequality is deepening congress must endorse a recovery thats inclusive livable and one that enables every family to balance competing obligations they can start by passing legislation that better protects shift workers from arbitrary and unpredictable scheduling practices

@highlight
vijay das soso jobs numbers contain truth that worries labor experts too much american job growth is in parttime lowincome work

@highlight
he says erratic work schedules tied to customer traffic wreaks havoc with lowwage workers lives congress can fix this
