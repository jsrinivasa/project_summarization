as the model for norman rockwells rosie the riveter mary doyle keefe became the symbol of american women working on the home front during world war ii. rosie the riveter is often confused with another popular image from the same era. still many folks on social media paid tribute to keefe using the image both show the key role women played in the war effort. other than the red hair and my face norman rockwell embellished rosies body keefe said in a interview with the hartford courant i was much smaller than that and did not know how he was going to make me look like that until i saw the finished painting

@highlight
rosie the riveter appeared on the cover of the saturday evening post on may

@highlight
mary doyle keefe was a telephone operator at the time
