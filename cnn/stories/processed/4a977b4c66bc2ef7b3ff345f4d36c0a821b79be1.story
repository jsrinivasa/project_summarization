two people one of them a licensed greyhound trainer have been arrested in australia after at least dog carcasses were found dumped in queensland bush according to australian media reports. a lot of people in the area who were involved in the greyhound industry were questioned and it was as a result of information given to us that the joint queensland police service and rspca investigation team made those arrests he said. at a press conference thursday queensland police minister joann miller described the discovery of the carcasses which followed a tipoff from a member of the public as sickening. michael beatty spokesman for rspca queensland told abc that inquiries were continuing the animal rights organization is assisting queensland police in their investigation

@highlight
australian police have arrested two people after the discovery of greyhound carcasses in bushland

@highlight
they believe the dogs were dumped by people involved in the lucrative greyhound racing industry

@highlight
the industry has been under fire since a television investigation revealed live baiting and other abuses
