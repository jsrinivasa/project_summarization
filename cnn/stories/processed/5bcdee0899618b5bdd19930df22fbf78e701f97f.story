early clinton campaign calculations the favored way for one of her opponents to channel his concerns a gop ticket for the generations and republican calendar concerns filled our sunday trip around the inside politics table. in democratic politics kamala is the harris sister who gets the most national buzz but maya harris is about to play a big role in the question of how hillary clinton plots her path to electoral votes. marco rubio drew a direct generational contrast with hillary clinton when he officially joined the gop race and wisconsin gov scott walker also often talks of his hope republicans will look for a nextgeneration leader as their next presidential nominee. so reports aps lisa lerer who was in iowa this past week for the clinton campaign rollout and who analyzes one of the most fascinating balancing acts for the onetime obama rival who of course went on to serve loyally as his secretary of state

@highlight
omalley using youtube to test out attack lines

@highlight
how clintons new hire could help keep the obama coalition together

@highlight
republican concerns about the new primary calendar
