a freshly fallen tree in the roadway was jason warnocks first clue. warnock was driving through a canyon in lewiston idaho on wednesday when he saw the tree then looked up to see an suv dangling over the edge of a cliff. the lewiston police department would like to thank jason warnock for his quick and decisive actions in helping mr sitko and preventing the situation from worsening said roger lanier the interim police chief. the only thing holding the gmc yukon and its terrified driver from a drop was a crumpled chainlink fence still clinging to the earth above bryden canyon road

@highlight
jason warnock rescued a man whose suv was dangling off the edge of a cliff

@highlight
warnock i dont feel like i deserve any credit i just did what anyone would do
