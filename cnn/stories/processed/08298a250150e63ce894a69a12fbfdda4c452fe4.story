it was a typical practice day for the washington university of rowing team but then danger came from beneath. team member devin patel described the moment of terror the fish was flopping on my legs it was so slippery that i couldnt get a grip on it. the scene was creve coeur lake outside of st louis early friday morning. the teams boat got near the dock when suddenly a swarm of asian carp emerged from the water and went on the attack some even going into the boat

@highlight
rowing team at washington university attacked by flying carp

@highlight
member of the team caught the attack on video
