by now you probably have a position regarding the controversy over indianas religious freedom law. as the author of the federal religious freedom restoration act rfra sen chuck schumer is one who can offer clarity over the controversy surrounding indianas version of the law. the federal law was intended to protect individuals religious freedom from government intervention he said the indiana law justifies discrimination in the name of religious freedom he contends. another hypothetical outcome of the new mexico case involving the lesbian couple and the photography studio if new mexico had the same religious freedom law as indiana the case would have gone to trial but new mexico has a nondiscrimination law that protects the lgbt community it and it would have provided a strong counterargument to the religious freedom claim

@highlight
the controversy over indianas religious freedom law is complicated

@highlight
some factors you might have not considered
