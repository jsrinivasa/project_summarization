three people were killed and five others were wounded thursday afternoon when a group of armed assailants stormed into the attorney generals office in balkh province northern afghanistan according to a press release from the provincial governors office. two police officers and a security guard of the provincial attorney generals office were among the dead. although most staff members and civilians have been rescued an exchange of fire between afghan security forces and the assailants is ongoing the statement says. afghan security forces are cautiously making advances in the fight in order to avoid civilian casualties according to the press statement

@highlight
three people killed five wounded in attack on attorney generals office in balkh province

@highlight
staff and civilians have been rescued as gunmen engaged afghan security forces
