a duke student has admitted to hanging a noose made of rope from a tree near a student union university officials said thursday. you came here for the reason that you want to say with me this is no duke we will accept this is no duke we want this is not the duke were here to experience and this is not the duke were here to create duke president richard brodhead told the crowd. the student was identified during an investigation by campus police and the office of student affairs and admitted to placing the noose on the tree early wednesday the university said. the prestigious private school didnt identify the student citing federal privacy laws in a news release it said the student was no longer on campus and will face student conduct review

@highlight
student is no longer on duke university campus and will face disciplinary review

@highlight
school officials identified student during investigation and the person admitted to hanging the noose duke says

@highlight
the noose made of rope was discovered on campus about am
