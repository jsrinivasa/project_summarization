police added attempted murder to the list of charges against the mother of a quadriplegic man who was left in the woods for days philadelphia police spokeswoman christine obrien said tuesday. the montgomery county maryland department of police took parler into custody sunday after philadelphia police reported that she left her son in the woods while she hopped a bus to see her boyfriend in maryland a man walking through the woods found him friday lying in leaves covered in a blanket with a bible and a wheelchair nearby philadelphia police say. for more than four days police say the quadriplegic man who also suffers from cerebral palsy was left lying in the woods of philadelphias cobbs creek park low temperatures reached the during the week and rain was reported in the area wednesday and thursday. the man is unable to communicate how he came to be in the park but philadelphia police lt john walker told reporters that the mans mother left him there the morning of april starks identified the mother as parler on monday

@highlight
philadelphia police add attempted murder to list of charges mom will face

@highlight
mom told police son was with her in maryland but he was found friday alone in woods

@highlight
victim being treated for malnutrition dehydration mother faces host of charges after extradition
