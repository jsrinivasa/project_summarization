if you drove by it you wouldnt even know its there. the ringling bros center for elephant conservation sits on acres of land in rural central florida halfway between orlando and sarasota off a nondescript country road an armed security guard greets you at the entrance. after a short drive down a gravel road you get the sense this is a special place. you can walk around and you dont hear anything said kenneth feld who opened the center in these elephants they have these large feet and they travel silently through the fields i think its very peaceful

@highlight
elephants currently live at the circus sanctuary and more will join by

@highlight
the expansion comes after ringling bros said it would stop using elephants in circus

@highlight
worth of care annually includes pedicures stretching and tons literally of food
