ultrahaptics is a young company with a big dream changing the way we interact with electronic devices. several devices can already be controlled with gestures but ultrahaptics add an extra layer of feedback by generating the sensation of a force field haptics is more than just the sense of touch its really all of the information that you get from the sense of touch what youre feeling what sort of pressure the tactile sensation given by an object or surface you also know where your limbs are and how theyre moving all from the sense of touch its all this information that cues how youre interacting with the world carter said. but even though it is incredibly convincing virtual reality completely bypasses the sense of touch applying ultrahaptics technology to it would allow users to not just see the virtual world projected in front of their eyes but to touch it as well. to create their invisible buttons ultrahaptics use a small collection of ultrasonic speakers concentrating the sound waves to a specific point

@highlight
a company called ultrahaptics has developed a technology to create shapes in midair

@highlight
a tactile sensation is provided by ultrasonic waves which alter air pressure

@highlight
the technology could be applied to electronic devices car dashboards and virtual reality headsets
