getting caught napping on the job is never good getting caught napping on the job in the cargo hold of a plane takes it to a whole different level. the cargo hold is pressurized and temperature controlled the airline said the plane was also only in the air for minutes. alaska airlines flight was just barely on its way to los angeles from seattletacoma international airport on monday afternoon when the pilot reported hearing unusual banging from the cargo hold. the banging in the cargo hold did come from a person and he turned out to be a ramp agent from menzies aviation a contractor for alaska airlines that handles loading the luggage the airline said the man told authorities he had fallen asleep

@highlight
ramp agent tells authorities he fell asleep in cargo hold alaska airlines says

@highlight
the cargo hold is pressurized and temperature controlled
