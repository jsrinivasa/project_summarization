the arizona police officer who intentionally slammed his car into an armed suspect previously faced an excessive force lawsuit in new york according to court documents. rapiejkos actions in marana arizona in which he drove his patrol car into a man who had fired a rifle in the air minutes earlier and before that had pointed the gun at another officer have stirred debate about what type of force police should have used. the lawsuit said luis colon had parked his car and gotten out when rapiejko with his gun aimed at colon ordered him back into the car. officer michael rapiejko was with the new york police department from to two years after he left he was one of the defendants in a suit filed by a man who alleged that rapiejko pointed a gun and threatened to shoot him and handcuffed and choked him in front of his family during a arrest

@highlight
officer michael rapiejko was sued in new york over claims he used excessive force during an arrest

@highlight
the city settled the lawsuit while rapiejko and others admitted no guilt
