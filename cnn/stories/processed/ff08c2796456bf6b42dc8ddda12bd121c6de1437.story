as his military career winds down britains prince harry is going to be spending some time down under. the younger son of prince charles and princess diana harry is known in the british army as captain harry wales from his official title and name his royal highness prince henry of wales. prince harry has trained and served alongside australian armed forces on operational tours to afghanistan he has met them during the invictus games and even trekked to the south pole with a couple of australian soldiers the spokesman said. wounded warriors are a special interest for prince harry he helped spearhead and continues to champion the invictus games a competition for former military personnel who have been wounded in the line of duty

@highlight
prince harry is to begin a monthlong military attachment in australia

@highlight
hell be leaving the british armed forces in june
