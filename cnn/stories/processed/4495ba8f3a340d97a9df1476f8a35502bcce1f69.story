if youve been following the news lately there are certain things you doubtless know about mohammad javad zarif. you may well have read that he is polished and unusually for one burdened with such weighty issues jovial an internet search for mohammad javad zarif and jovial yields thousands of results. the website of the iranian foreign ministry which zarif runs cannot even agree with itself on when he was born the first sentence of his official biography perhaps in a nod to the powers that be in tehran says zarif was born to a religious traditional family in tehran in later on the same page however his date of birth is listed as january and the iranian diplomacy website says he was born in in. early in the iranian revolution zarif was among the students who took over the iranian consulate in san francisco the aim says the website iraniancom which cites zarifs memoirs titled mr ambassador was to expel from the consulate people who were not sufficiently islamic later the website says zarif went to make a similar protest at the iranian mission to the united nations in response the iranian ambassador to the united nations offered him a job

@highlight
mohammad javad zarif has spent more time with john kerry than any other foreign minister

@highlight
he once participated in a takeover of the iranian consulate in san francisco

@highlight
the iranian foreign minister tweets in english
