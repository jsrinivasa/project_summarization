he checked the series of stills on his camera it was then that photographer james oatway realized the entire attack had taken less than two minutes. oatways series of images of the ordeal landed on the front page of south africas sunday times under the headline kill thy neighbor alex attack brings home sas shame. police announced theyve now arrested all four suspects the last caught overnight tuesday with help from oatways photos which is little solace for the photographer who captured a level of depravity rarely seen. mozambican emmanuel sithole was walking down a street when four south africans surrounded him sithole pleaded for mercy but it was already too late the attackers bludgeoned him with a wrench stabbed him with knives all in broad daylight and oatway had captured it all on his camera

@highlight
photographer james oatway captured a violent attack that resulted in death of a mozambican in south africa

@highlight
seven people have been killed in recent violence against poorer immigrants many from south africas neighbors
