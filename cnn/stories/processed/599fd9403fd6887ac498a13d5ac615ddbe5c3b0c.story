nasa says its messenger space probe crashed into mercury on thursday after running out of fuel ending a nearly journey that provided valuable data and thousands of photos. messenger an acronym for mercury surface space environment geochemistry and ranging was launched in and traveled more than years before it started circling mercury on march. the messenger mission is over but scientists say theyll be busy for years studying data from the probe. and while the space probe wont be sending back anymore images you can see mercury with your own eyes its visible in the night sky just before dusk until about the end of may

@highlight
nasas messenger probe smashes into mercury ending mission

@highlight
space probe hit the planets surface at mph
