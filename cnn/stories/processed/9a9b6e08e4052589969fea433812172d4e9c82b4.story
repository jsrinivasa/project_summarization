as nepal grapples with an earthquake that has killed more than people ronen ziv worries about someone he has never met. ziv would prefer shaje fly to israel where hospitals can provide the needed medical help but with only days left until the baby is due ziv said putting shaje on a plane might not be possible. ziv says his baby is in a breech position requiring a cesarean section in the current chaos of kathmandu he worries his surrogate mother nafisa shaje will not get the proper medical attention required for a safe and healthy birth. nepal is a popular place for israeli couples to have surrogate children zivs first child a daughter was born to a surrogate mother in nepal ziv and his partner tom traveled to nepal for the birth they were planning on making the trip once again but the earthquake forced them to change their plans

@highlight
nepal is a popular place for israeli couples to have surrogate children

@highlight
an estimated to surrogate mothers are due to give birth soon in kathmandu
