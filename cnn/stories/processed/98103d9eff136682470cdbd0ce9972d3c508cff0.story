roseanne barr revealed earlier this week that she is going blind in an interview with the daily beast the comic talked about her struggle with macular degeneration and glaucoma two eye diseases that get progressively worse over time and can steal vision. its somewhat unusual that roseanne bar has both but not unheard of explains ophthalmologist steven a shanbom md of shanbom eye specialists in berkley mich though there are some controllable risk factors certain people are genetically predisposed to these diseases so barr may simply be prone to both certainly its sad the combination of the two is terrible macular degeneration takes away her central vision and glaucoma is taking away her peripheral vision dr shanbom adds he is not treating roseanne barr and does not know the specifics of her case. there is no cure for either disease but like those eyedrops there are treatments that may delay the progression of earlystage glaucoma from other drugs to surgery and therapies that might halt further vision loss in advanced cases of macular degeneration including an implantable telescope the future looks brighter however an animal study published this month suggests that an injection of stem cells into the eye might slow or even reverse the effects of earlystage macular degeneration. macular degeneration is a breakdown of the part of the retina that allows us to see fine details in the center of vision while glaucoma damages the nerve that connects the retina to the brain and is often caused by fluid buildup and pressure in the eyes barr said in the interview that she helps relieve the pressure by using marijuana which is known to temporarily lower pressure inside the eye

@highlight
barr has macular degeneration and glaucoma eye diseases that get progressively worse and can steal vision

@highlight
the risk for both diseases goes up for everyone after age

@highlight
sun exposure can up the risk for glaucoma and macular degeneration
