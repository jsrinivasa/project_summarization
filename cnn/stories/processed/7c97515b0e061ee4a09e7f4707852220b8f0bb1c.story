the latest outbreak of bird flu the worst in the us since the is not a likely threat to humans reports the centers for disease control and prevention but as with any potential threat to human health they are preparing for the worst just in case. the news is bad for the birds but not for humans the cdc considers the likelihood of bird to human transmission of the virus low according to dr alicia fry a medical officer with the cdc national center for immunization and respiratory disease influenza division epidemiology and prevention branch. on monday health leaders in iowa said more than million hens would have to be euthanized after bird flu was detected at a commercial laying facility there in the united states some million birds had already been euthanized to prevent the spread of the disease according to the usda. since middecember states have seen bird flu turn up in commercial poultry backyard chickens and in flocks of wild and captive wild birds according to the cdc that number will likely grow as birds with the disease fly from one state to the next

@highlight
the cdc says the risk to humans is low but as always they are preparing for the worst case

@highlight
you cant get bird flu from eating poultry or eggs

@highlight
at least people who worked with the sick birds are being monitored for any sign of sickness

@highlight
so far million birds have been euthanized
