thousands sought refuge in temporary shelters in south africa after mobs with machetes attacked immigrants in durban leaving at least five people dead an aid group said thursday. the attacks in durban killed two immigrants and three south africans including a boy authorities said. it said it hopes the violence is limited to durban but assured immigrants that it has a facility in johannesburg to help those who might need shelter there. there has been an outpouring of support from ordinary south africans who are disgusted with the attacks not only because they are foreign or african but because they are fellow human beings said gift of the givers charity which is helping those seeking refuge

@highlight
a charity group is preparing aid packages for those who want to return home

@highlight
the attacks have left dead two immigrants and three south africans

@highlight
a boy is among those killed after a mob with machetes targeted foreigners
