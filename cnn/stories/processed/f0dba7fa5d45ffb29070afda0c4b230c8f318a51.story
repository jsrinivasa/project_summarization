even from high above flying in an indian air force helicopter it is easy to see that the people of melamchi central nepal are happy to see us. the mission a joint effort between indian air crew and a nepalese army medical team is only the third operation of its kind to reach the village since saturdays massive quake which left more than people dead. local official upendra tamang is there to greet the helicopter as it touches down on a field in front of the village medical clinic and waiting soldiers swing into action to unload the delivery. another cries in pain as she is loaded on to a stretcher from the back of the pickup then awkwardly hoisted on to the helicopter

@highlight
indian air force and nepalese army medical team launch rescue mission to bring injured people to hospitals in kathmandu

@highlight
forshani tamangs family carried her for four hours to reach help after she was wounded when their home was destroyed
