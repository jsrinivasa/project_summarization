he might have just won one of sports most prestigious events but it wasnt long before jordan spieths thoughts turned to his autistic sister in the glow of victory. amid all the pressure of playing professional golf spieths sister provides him with perspective. the unassuming spieths success brings to mind other stars whose supreme ability and selfbelief saw them become major champions in the early days of their careers. at the age of just the prodigy from texas who was named after basketball legend michael jordan had been clear about what he wanted his aim was to take the masters title one day

@highlight
jordan spieth hails autistic younger sibling as his biggest supporter

@highlight
icecool display made him secondyoungest winner behind tiger woods
