at least people were killed sunday and more injured in separate attacks on a police station a checkpoint and along a highway in egypts northern sinai authorities said. six people including one civilian were killed when a car bomb exploded near the police station in alarish capital of north sinai health ministry spokesman hossam abdelghafar told ahram online he said people were injured. ansar beit almaqdis has claimed many attacks against the army and police in sinai. among those being replaced are the generals in charge of military intelligence and egypts second field army which is spearheading the battle against the insurgents in the northern sinai

@highlight
six people including one civilian are killed when a car bomb explodes near a police station

@highlight
six others are killed when their armored vehicle is attacked on a highway in northern sinai

@highlight
ansar beit almaqdis an isis affiliate claims responsibility
