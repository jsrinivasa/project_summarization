a second robotic probe sent into the crippled fukushima nuclear plant has captured images of a strange green glow. tepco said the yellow seen on the images seemed to suggest a discoloration of the grating though the cause was unknown it said the green glow could not be seen when filmed from other angles. it is a great step forward towards the decommissioning work as we can earn necessary data for the next investigation said akira ono the chief of fukushima daiichi plant. tepco called the robotic probe an unprecedented experiment

@highlight
a robotic probe into the fukushima nuclear plant released crucial information on conditions inside the reactor

@highlight
tepco recorded radiation levels and temperatures are lower than expected

@highlight
the robot was sent into the plant after the first one broke down
