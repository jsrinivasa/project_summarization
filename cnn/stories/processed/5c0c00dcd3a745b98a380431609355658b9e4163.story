tornadoes fierce winds and severe thunderstorms with large hail are predicted for the midwest and for the plains from the ozarks eastward to the lower ohio valley on thursday and friday the national weather service said. scattered storms will soak illinois and missouri and wind and hail will continue to be moderate in those states the national weather service said by thursday afternoon storms will hit parts of indiana and kentucky. earlier this week severe weather struck the south alabama arkansas oklahoma and georgia reported large hail. residents in shawnee oklahoma were awakened early wednesday morning by a severe storm producing golf ballsized hail

@highlight
thunderstorms with large hail are predicted for the midwest and the plains

@highlight
tornadoes could strike thursday night and friday
