a columbia university student who was accused of rape is suing the new york city school for allowing his accuser to publicly brand him a serial rapist. the lawsuit alleges the defendants violated nungessers right to an education free of genderbased discrimination by allowing sulkowicz to speak out against him after he had been cleared of wrongdoing during an april news conference at columbia university sen kirsten gillibrand stood alongside sulkowicz as she called nungesser a serial rapist and said she fears for her safety while hes still on campus. the case has produced dueling narratives from both sides in national media outlets the most recent came from nungesser in a daily beast article titled columbia student i didnt rape her in which he shared a long exchange of facebook messages to support his claim that their sex was consensual. columbia university declined to comment on the lawsuit the school also declined to confirm the outcome of nungessers disciplinary proceedings

@highlight
paul nungesser says he was target of genderbased harassment campaign

@highlight
the case drew national attention after his accuser started carrying a mattress around campus
