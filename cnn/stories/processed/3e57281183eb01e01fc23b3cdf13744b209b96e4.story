we are at the beginning of a massive and mounting crisis with no solution in sight perhaps thats incorrect the migrant crisis that has suddenly drawn hundreds of journalists to sicily has been brewing for years but in the past days with as many as deaths in the mediterranean suddenly minds are focused for now. and while this crisis has been brewing for years it is now truly upon us. the fall of libyan leader moammar gadhafis regime which we reporters covered so avidly was followed by chaos which we in the news media largely neglected focused as we journalists were on the next catastrophe the syrian civil war in that chaos the business of human trafficking has boomed. it was late at night in the besieged city of misrata hundreds of african migrants were caught between the libyan civil war back then some optimistically called it a revolution and the deep blue sea they had come to misrata from ghana nigeria and elsewhere hoping to board rickety boats to cross the sea to europe

@highlight
hundreds of desperate migrants have died attempting to cross the mediterranean in recent says

@highlight
and italians are alarmed that this year as many as a million migrants could arrive in europe
