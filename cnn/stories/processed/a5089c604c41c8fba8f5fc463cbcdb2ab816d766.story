no identification no social security card and only a box to live on john helinski was homeless and nameless for three years. he needed to have an identification but we couldnt get an identification without a birth certificate inman said. with those papers squared away he and inman got helinski a drivers license and a social security card. the account was still there and the social security administration hadnt forgotten about him it had kept paying helinski benefits for years and they had stacked up high

@highlight
john helinskis id and social security cards had been stolen

@highlight
his case worker and a cop had to get foreign id papers to get him a drivers license

@highlight
then helinski remembered a bank account he used to have
