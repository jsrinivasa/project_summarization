video may have killed the radio star but in norway its digital thats killing fm radio. on fm norwegians can only find five stations on the digital audio broadcasting network there are four times that number. in two years time the scandinavian nation is slated to become the first in the world to phase out radio entirely. that doesnt mean that norwegians will be left with radio silence theyll merely have to tune in digitally

@highlight
on fm norwegians can only find five stations

@highlight
digitally there are four times that number
