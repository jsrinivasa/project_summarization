supreme court justices appeared divided tuesday during historic arguments over the constitutionality of gay marriage with justice anthony kennedy returning to a familiar role as the courts pivotal vote. it was incredibly moving to gather in the supreme court chamber with their parents and all plaintiffs in these historic cases mary and doug were fantastic making a compelling and to my mind irrefutable case on their behalf lambda legals alphonse gerhardstein said of the lawyers who argued in favor of samesex marriage before the court on tuesday. all eyes were on justice anthony kennedy considered a key vote for challengers to the state bans who has penned three decisions in favor of gay rights over the years. us solicitor general donald verrilli who represented the obama administrations views also presented arguments in favor of samesex marriage focusing on equal protection under the amendment and likening bans on samesex marriage to handing secondclass status to gay americans

@highlight
questions tuesday centered on whether defining marriage should be left to voters in individual states or decided by judicial system

@highlight
chief justice john roberts who shocked conservatives with his swing vote to uphold obamacare seemed to lean conservative

@highlight
eyes on justice anthony kennedy a key vote for challengers to the state bans who has penned decisions in favor of gay rights
