lest we americans forget that in the wellsprings of our nation france bore the torch of liberty alongside us our old ally has launched a reminder from across the atlantics waves. since that gift from france also a reminder of our common bond has been americas quintessential national symbol of freedom. it should be in new york city for the fourth of july possibly sharing independence day fireworks with the statue of liberty. it set sail in france on saturday for virginia to retrace a journey through american history

@highlight
lhermione is a painstaking replica of an century ship of the same name

@highlight
the original fought with american colonists against the british in the revolutionary war
