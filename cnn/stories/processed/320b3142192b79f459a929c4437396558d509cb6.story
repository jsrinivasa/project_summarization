a lamborghini sports car crashed into a guardrail at walt disney world speedway on sunday killing a passenger the florida highway patrol said. petty holdings which operates the exotic driving experience at walt disney world speedway released a statement sunday night about the crash. the passenger gary terry of davenport florida was pronounced dead at the scene florida highway patrol said. the driver tavon watson of kissimmee florida lost control of the vehicle the highway patrol said he was hospitalized with minor injuries

@highlight
authorities identify the deceased passenger as gary terry

@highlight
authorities say the driver tavon watson lost control of a lamborghini

@highlight
the crash occurred at the exotic driving experience at walt disney world speedway
