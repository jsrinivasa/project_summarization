in the first few days after the sewol disappeared beneath the yellow sea divers pulled body after body from the watery wreckage bringing the dead home. parks case is an unsettling one emblematic of what remains unresolved a year after the sewol ferry sank dozens associated with the ferry disaster have been sent to jail on criminal charges but families say the underlying problems that led to the sinking of the sewol are far from resolved. divers had to pluck the bodies from the water onebyone bringing the youngsters back to land in black body bags where they were met with the gutwrenching cries of their families. the divers stopped searching months ago because of the winter and water conditions and the south korean ferry remains on the bottom of the sea floor

@highlight
sewol ferry sank a year ago off the coast of south korea killing people

@highlight
families hold protests vigils say not much has been resolved since sinking

@highlight
government has yet to decide whether to raise the ferry
