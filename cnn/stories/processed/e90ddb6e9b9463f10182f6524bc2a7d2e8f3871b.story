the call from faleh essawi the deputy chief of the provincial council who we were supposed to be meeting up with came just as we were about to hit the bridge the only safe route from baghdad to neighboring anbar province. security is collapsing in the city he screams this is what we warned baghdad would happen where is baghdad where is alabadi. the police chief major aref aljanabi radios to his men to respond aljanabi like so many others is frustrated with the lack of support from baghdad earlier he had taken us to the front lines a long berm that stretches along the northern and western parts of the town that is dotted with fighting positions he says he regularly provides the joint command center with coordinates for isis positions but so far there have been no significant air strikes or reinforcements. she starts to cry harder her husband was killed by us forces in fallujah another city in anbar in her children have all moved away except her youngest who broke his arm in the same attack

@highlight
families flee parts of western iraq amid continuing onslaught from isis fighters

@highlight
officials there say the iraqi government is failing to protect them

@highlight
thousands have been forced to grab what they can and head east toward the capital
