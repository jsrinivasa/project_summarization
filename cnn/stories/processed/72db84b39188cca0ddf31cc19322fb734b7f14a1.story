a fourth man has been indicted in connection with a plot to provide material support to terrorists and for some of the men to join isis according to an indictment from brooklyn federal court released monday. the indictment and a criminal complaint filed last month say juraboev and saidakhmetov planned to join isis and had purchased airline tickets to turkey. dilkhayot kasimov was added to a superseding indictment in which three previously arrested men abdurasul hasanovich juraboev akhror saidakhmetov and abror habibov were charged with two counts of providing support to a foreign terrorist organization those three have pleaded not guilty it is unclear if kasimov has been arrested. in one from november saidakhmetov told juraboev that he wanted to join the us military so he could share information with isis

@highlight
the revised indictment is released monday

@highlight
dilkhayot kasimov is charged with two counts of providing support to a foreign terrorist organization

@highlight
three other men have also been charged in the plot and pleaded not guilty
