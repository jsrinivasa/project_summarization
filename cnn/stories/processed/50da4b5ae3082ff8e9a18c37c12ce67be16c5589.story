south african troops deployed tuesday evening as part of a new government effort to stop deadly antiimmigrant violence. their first target the johannesburg suburb of jeppestown where xenophobic violence broke out on friday south african police raided a jeppestown hostel tuesday while troops secured the perimeter. seven people have been killed in recent violence against poorer immigrants many from south africas neighbors. much of this months violence happened in the port city of durban where at least two foreigners and three south africans were killed after mobs with machetes attacked immigrant shops thousands of people took temporary shelter at refugee centers or police stations as a result according to aid group gift of the givers

@highlight
south african troops help police conduct raids in jeppestown

@highlight
defense minister says police are spread too thin trying to prevent attacks on immigrants

@highlight
seven people have been killed in recent violence against poorer immigrants
