blue bell ice cream has temporarily shut down one of its manufacturing plants over the discovery of listeria contamination in a serving of ice cream originating from that plant. the warning by the centers for disease control and prevention does not affect other blue bell ice cream including other servings not made at the plant but blue bell has recalled other products. public health officials warned consumers friday not to eat any blue bellbranded products made at the companys broken arrow oklahoma plant that includes servings of blue bell ice cream from this plant that went to institutions in containers marked with the letters o p q r s or t behind the coding date. in a statement on its website blue bell said this recall in no way includes blue bell ice cream half gallons pints quarts gallons or other oz cups

@highlight
a test in kansas finds listeria in a blue bell ice cream cup

@highlight
the company announces it is temporarily shutting a plant to check for the source

@highlight
three people in kansas have died from a listeria outbreak
