freddie gray was arrested baltimore police on the morning of april without incident according to police. we want to clear up some of the confusions that may exist baltimore deputy police commissioner jerry rodriguez said we will be looking specifically at our actions from the point that we came into contact with mr gray up until the time we requested medical assistance specifically did we miss any warnings should we have acted sooner should we have acted in any different manner. at the corner of north avenue and mount street in baltimore a police officer makes eye contact with two individuals one of them gray both individuals start running southbound as officers begin pursuing them. when mr gray was placed inside that van he was able to talk he was upset and when mr gray was taken out of that van he could not talk and he could not breath rodriguez said i know mr gray suffered a very traumatic injury but i dont know if it happened prior to him getting into the van or while he was in the van

@highlight
freddie gray died on sunday after slipping into a coma

@highlight
he was arrested a week earlier under murky circumstances
