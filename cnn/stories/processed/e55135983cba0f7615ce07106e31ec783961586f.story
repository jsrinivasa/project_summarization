the people of nepal are still trying to recover from two major earthquakes and a mudslide each day is a struggle in many parts of of the country but there is something you can do to make an impact. we have vetted a list of organizations working in nepal that have created specific funds for relief efforts including. nepal red cross society. actionaid usa

@highlight
aid organizations are still working to help the people of nepal in the wake of two major earthquakes

@highlight
thousands were killed in a magnitude earthquake in nepal on april

@highlight
a second quake rocked the country less than three weeks later
