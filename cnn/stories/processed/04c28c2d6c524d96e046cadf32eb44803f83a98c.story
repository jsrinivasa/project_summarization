police officers saturday accompanied the hearse carrying the body of walter scott to his south carolina funeral service where hundreds of mourners celebrated his life and death as a catalyst for change in america. us rep james clyburn dsouth carolina told reporters outside the service that scott lost a job the first time he was jailed for failing to pay child support. a second video taken from a police dash cam has also emerged from the day scott died it shows moments before the shooting when things seemed to be going smoothly between scott and slager. anthony scott said god had selected his brother as a candidate for change in america

@highlight
police officers escort the funeral procession to the service

@highlight
scotts family did not attend his visitation they need privacy mayor says

@highlight
police meet with the man who was a passenger in his car when it was pulled over
