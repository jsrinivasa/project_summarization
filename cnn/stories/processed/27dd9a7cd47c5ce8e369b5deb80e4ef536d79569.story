cathay pacific was forced to cancel a scheduled flight from london to hong kong after one of the pilots was arrested after trying to board the airliner with knives in his luggage. it apologized to the people on board flight which eventually departed on sunday adding that it assisted passengers with overnight accommodation in london and alternative flight arrangements. they just told us there were crew issues the captain said apologies for everything that has happened he said they tried to get another pilot but they couldnt get there on time one passenger on the flight told the hong kongbased south china morning post. passengers were forced to wait in the seats on board the boeing for more than two hours before the service was canceled

@highlight
pilot stopped during security checks as the flight prepared to depart on saturday night

@highlight
cathay pacific runs regular flights between its hong kong hub and london

@highlight
the male pilot has been bailed pending an investigation
