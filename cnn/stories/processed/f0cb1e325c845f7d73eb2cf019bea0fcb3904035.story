these are fearful times on the highest mountain in the world. they were trying to outrun the avalanche and you cannot said pederson so many people were hit from behind blown off the mountain blown into rocks hit by debris tents were flying off. but concerns were growing for the groups of climbers stuck farther up the mountain in camps and. the avalanche was reported to have trapped them above the icefall area an already treacherous part of the mountain that separates the base camp from camp

@highlight
concerns growing for people trapped higher up the mountain

@highlight
helicopters begin airlifting injured people from the base camp in nepal

@highlight
climber reports at least dead many others injured missing or stuck
