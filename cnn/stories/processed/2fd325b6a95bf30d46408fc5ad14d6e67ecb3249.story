with help from some filmmakers alice barker went back in time. barker was a dancer in such new york nightspots as the cotton club and the cafe zanzibar in the and part of chorus lines that entertained alongside notables including bill bojangles robinson and frank sinatra. there were motion pictures made of barker but she had never seen any of them moreover her photographs and memorabilia had all been lost over the years. mark cantor of jazzonfilmcom and some volunteers put together a video of soundies early music videos and showed them to barker at the nursing home where she lives

@highlight
alice barker was a dancer in the and

@highlight
thanks to filmmakers barker now finally saw herself dance
