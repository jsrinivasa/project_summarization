saudi special forces assisted yemeni fighters targeting houthis and their allies in aden a saudi source told cnn. houthis are battlehardened guerrilla fighters and could cross into saudi arabia theyve already threatened suicide bomb attacks inside saudi arabia. the special forces were on the ground in noncombat roles coordinating and guiding the battle against houthi forces and fighters loyal to former yemeni president ali abdullah saleh the source said they also have helped parachute in weapons and communications equipment. houthi rebels meanwhile withdrew from the presidential palace and other key parts of the southern port city of aden following heavy saudi airstrikes according to the source

@highlight
two humanitarian workers killed

@highlight
two saudi border guards killed in exchange with rebels saudi press agency reports

@highlight
saudi forces in noncombat roles coordinating and guiding fight against houthis source says
