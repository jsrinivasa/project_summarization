the cast of the breakfast crew escaped from principal richard vernon years ago but a draft script of the teen classic has just been found in a filing cabinet in the school district where it was filmed the chicago tribune reported. one day a few weeks ago one of the assistants was going through a filing cabinet and found a file that had a manuscript from the breakfast club dated sept ken wallace superintendent of maine township high school district in suburban chicago told the newspaper. the movie was filmed at the maine north high school building which was auctioned off by the district years ago and is now occupied by the illinois state police according to the tribune the file was discovered at maine south high school as district officials prepared to move to a newly acquired building next door. wallace told the tribune that he would like to find a way to display the script as a piece of film and district history

@highlight
the breakfast club script was found in a high school filing cabinet years later

@highlight
school officials hope to display the draft script
