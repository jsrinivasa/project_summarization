malala yousafzais stellar career has included a nobel peace prize last week she made it into outer space. it took a meticulous medical response to save her life more than two years ago but malala recovered with no serious neurological damage to become a powerhouse for her cause. after reading her story scientist amy mainzer who also consults for pbs on a childrens educational science show decided malala deserved to be immortalized so she attached her name to the heavens. my postdoctoral fellow dr carrie nugent brought to my attention the fact that although many asteroids have been named very few have been named to honor the contributions of women and particularly women of color mainzer wrote in a note to malala

@highlight
astrophysicist amy mainzer says she was was touched by malalas story of determination

@highlight
mainzer also works on educating children about science
