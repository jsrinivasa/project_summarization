on the eve of the oneyear mark since nearly schoolgirls were abducted by boko haram in nigeria malala yousafzai released an open letter to the girls monday. on april islamists with boko haram kidnapped the girls prompting an international campaign for their safe return which used the hashtag bringbackourgirls on monday unicef announced a new campaign for the children who have been displaced in northeast nigeria using the hashtag bringbackourchildhood. last july i spent my birthday in nigeria with some of your parents and five of your classmates who escaped the kidnapping your parents are griefstricken they love you and they miss you my father and i wept and prayed with your parents and they touched our hearts the escapee schoolgirls my father and i met impressed us with their resolve to overcome their challenges and to complete their high school education my father and i promised your parents and the girls who had escaped that we would do all we could to help them i met nigerian president goodluck jonathan and urged him to work harder for your freedom i also asked president jonathan to meet your parents and the girls who escaped the kidnapping which he did a few days later still in my opinion nigerian leaders and the international community have not done enough to help you they must do much more to help secure your release i am among many people pressuring them to make sure you are freed. in the letter she calls on the nigerian government and the international community to do more to rescue the girls nigeria recently held an election

@highlight
malala yousafzai tells the girls she associates with them

@highlight
she writes a message of solidarity love and hope

@highlight
she calls on nigeria and the international community to do more to rescue the girls
