an alaska airlines flight was forced to make an emergency landing monday after its pilot reported hearing unusual banging. hed been on a fourperson team loading baggage onto the flight all ramp employees have security badges and undergo full criminal background checks before being hired said alaska airlines. he appeared to be in ok condition the cargo hold is pressurized and temperature controlled the plane was also only in the air for minutes alaska airlines said. the agent was taken to an area hospital as a precaution he passed a drug test and was discharged alaska airlines said

@highlight
agent was taken to an area hospital as a precaution he passed a drug test and was discharged alaska airlines says

@highlight
the cargo hold is pressurized and temperature controlled
