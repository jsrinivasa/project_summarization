anyone who has given birth or been an observer of the event knows how arduous it can be. in an hourlong labor captured by cameras and streamed live by animal planet katie gave birth to a notsolittle baby about feet tall early friday evening. it wasnt immediately known how many people online saw katie go into labor and give birth but the giraffe definitely did have watchers in the form of fellow giraffes who saw the scene unfold from an abutting barn one of them being katies bff jade. but to do it live on the internet with two hooves sticking out for several minutes in the midst of labor

@highlight
animal planet captures katie the giraffes labor and delivery

@highlight
the new baby wiggles its ears rises tries to nurse from its mom
