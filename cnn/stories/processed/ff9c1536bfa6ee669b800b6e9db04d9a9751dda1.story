lost luggage after a long flight is a common frustrating occurrence of modern air travel and sometimes airlines lose things that are irreplaceable. it isnt the first time a pet has gone missing at new yorks busiest airport in august a cat escaped from its carrier before an american airlines flight from new york to san francisco. that that the loss of pets during air travel is extremely rare the airline shipped more than pets last year. you pay all of this money but for what people assume you pay extra to have your pets taken care of but theyre treated no differently than a free piece of checked luggage stewart said

@highlight
couple spends to ship their cat felix on a flight from the united arab emirates

@highlight
felix went missing somewhere at john f kennedy international airport airline says

@highlight
pets are treated no differently than a free piece of checked luggage jennifer stewart says
