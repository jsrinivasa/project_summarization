larry johnson remembers the fear and feeling of helplessness from being on the skywest airlines flight that made an emergency landing in buffalo new york. the federal aviation administration on wednesday initially reported a pressurization problem with skywest flight and said it would investigate it later issued a statement that did not reference any pressurization issues. according to marissa snow spokeswoman for skywest three passengers reported a loss of consciousness while on the flight. minutes later johnson says the attendant announced there was a pressurization problem and told passengers to prepare for the emergency landing

@highlight
three passengers report a loss of consciousness on skywest flight

@highlight
but officials say there is no evidence of a pressurization problem
