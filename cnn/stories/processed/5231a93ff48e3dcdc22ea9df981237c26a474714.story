a hooded angel with black wings appeared on tuesday near the spot where walter scott was shot and killed by a police officer in north charleston south carolina on saturday since then its been taken up as an icon of the black lives matter movement. after hyman put the piece up on tuesday near where scott was killed he got a call from a local protester with the black lives matter movement which has staged protests around the country in the wake of highprofile deaths at the hands of police the group asked for permission to use his artwork in its demonstrations at the north charleston city hall. creator phillip hyman grew up in the neighborhood where scott an unarmed black man was shot in the back several times by a white police officer on saturday hyman now lives in another part of the city and couldnt stop thinking about it he woke up about am a couple of days after scott was killed and began searching for materials. the figure painted black in mourning for the family has wings because its going to heaven hyman said the man depicted in hymans piece is dressed in a hooded sweatsuit though thats not what scott was wearing when he was killed

@highlight
the walter scott shooting inspired a local artist to create artwork

@highlight
phillip hyman crafted the angelwinged artwork in the middle of the night

@highlight
protesters from the black lives matter movement have started using it as his symbol
