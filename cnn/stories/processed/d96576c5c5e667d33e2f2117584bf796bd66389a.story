chinas state prosecutors on friday formally charged the countrys former security czar with accepting bribes making him the highestranking chinese communist party official ever to face corruption charges. many observers also note zhous patronage of bo xilai a former communist leader sentenced to life in prison for corruption in. as a member of the ruling communist partys politburo standing committee chinas top decisionmaking body zhou was one of nine men who effectively ruled the country of more than billion people he retired in. zhou has not been seen in public since he attended an anniversary event at his alma mater in october he was expelled from the communist party and arrested last december

@highlight
prosecutors formally charged former top official zhou yongkang

@highlight
zhou charged with accepting bribes abuse of power and leaking state secrets

@highlight
former domestic security official is the most senior chinese official to face corruption charges
