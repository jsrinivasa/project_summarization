every day images of war and conflict are splashed across our desktops plastered on our tv screens and scattered through our mobile news feeds because of that ones response to such imagery often becomes calloused or desensitized. thorpe said his project enables the creation of a visual metaphor from which a viewer develops their own emotional physical and political response to war and conflict when faced with the realization that the images do not contain toy soldiers but real soldiers. after the elaborate fiveweek production thorpe hopes his images will raise questions on how images of war will be consumed in the future with everdiminishing attention spans and competition for them. this concept of how we digest images of war through mainstream media outlets was what drove photographer simon brann thorpe to begin his project toy soldiers this idea to create a fresh perspective drove him into the desert of western sahara a longdisputed region of northwestern africa

@highlight
simon brann thorpes project makes reallife soldiers resemble toy soldiers

@highlight
he shot the images in western sahara a disputed region of northwestern africa
