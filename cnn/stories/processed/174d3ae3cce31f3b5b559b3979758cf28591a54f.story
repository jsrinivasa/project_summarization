you can call him joker. weve seen a few teases so far but on friday night director david ayer gave us the first full look at jared leto as the joker. the clown prince of crime will appear in suicide squad the first movie featuring the bestknown comic book villain where as far as we know anyway there is no batman present. the oscar winner cut his hair and shaved his face for the role and appears to have embraced it fully he will be the first actor to play the character on the big screen since the late heath ledger

@highlight
jared leto unveiled as the joker for the first time on twitter

@highlight
leto stars in suicide squad
