no prostitutes. the solicitation of prostitution threatens the core mission of the department holder wrote in a memo to all personnel in the department he heads regardless of whether prostitution is legal or tolerated in a particular jurisdiction soliciting prostitutes creates a greater demand for human trafficking and a consequent increase in the number of minor and adult persons trafficked into commercial sex slavery. the directive comes a few weeks after a justice department inspector general report found dea agents in foreign postings attended sex parties with prostitutes paid for by drug cartels among other indiscretions. no ifs ands or buts and yes that includes when and where prostitution is perfectly legal

@highlight
attorney general holder reiterates justice department policy on prostitutes

@highlight
soliciting prostitutes is banned even in places where its legal holder says

@highlight
his memo comes weeks after a report involving dea agents and prostitutes
