a massachusetts man is facing murder charges authorities said wednesday four days after another mans remains were found in a duffel bag. police were notified saturday morning about a suspicious item along a walkway in cambridge officers arrived at the scene opened a duffel bag and found human remains. a middlesex county judge then revoked bail for colina in another case hes involved in for alleged assault and battery the victim in that case is different from the one whose remains were found in recent days. the middlesex district attorneys office said that carlos colina will be arraigned the morning of april for murder in connection with the remains discovered saturday in cambridge

@highlight
prosecutor carlos colina will be arraigned on the murder charge next week

@highlight
hes already been arraigned for alleged assault and battery improper disposal of a body

@highlight
body parts were found in a duffel bag and a common area of an apartment building
