israeli prime minister benjamin netanyahu will get two extra weeks to form a government israels president said in a news release monday. i wish you success in your work rivlin told the prime minister according to a statement the entire people of israel hope that a government will be established indeed a transition government has not received the confidence of the knesset and is viewed by the public as needing to be dealt with i hope that in the coming days you will succeed in forming a stable government for the state of israel. netanyahu must form his government in less than days according to israeli law. netanyahu made the request at president reuven rivlins jerusalem home monday

@highlight
israeli law says the prime minister must form his government in less than days

@highlight
netanyahu cites government stability and reaching agreement on important issues as reasons he needs additional time
