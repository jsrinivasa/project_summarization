on tuesday a white police officer in north charleston south carolina was charged with murder for shooting an unarmed black man in the back officer michael slager was arrested after raw video surfaced showing him firing numerous shots at walter scott as scott ran away from a traffic stop. we cant bring mr scott back but something like this today can have a bigger precedence than just what happened here with mr scott because what happened today doesnt happen all the time said l chris stewart an attorney for the scott family at a press conference. may walter scott rest in peace the arrest and charging of officer michael slager is a rare event that must be celebrated for the small victory that it is in the midst of unspeakable tragedy but this is by no means over. after watching the video the senseless shooting and taking of walterscotts life was absolutely unnecessary and avoidable tweeted sen tim scott my heart aches for the family and our north charleston community i will be watching this case closely

@highlight
a white police officer in south carolina is charged with killing an unarmed black man in the back

@highlight
david love what happened tells us the epidemic of police deadly force against black people continues
