martin omalley told reporters in iowa on friday that inevitability a term bandied about regarding democratic presidential frontrunner hillary clinton is not unbreakable. orc poll of national democrats only picked omalley in a january poll by bloomberg politics and the des moines register omalley was also at among iowa democrats. omalley had previously dropped the inevitability comment in a television interview last month. history is full of examples where people who are not very well known nationally can be very well known once they are willing to make their case to the people of iowa omalley said

@highlight
he made the statement before in march

@highlight
omalley is low in the polls with democrats but he has been flirting with a presidential run
