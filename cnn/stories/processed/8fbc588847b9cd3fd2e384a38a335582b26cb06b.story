it started with neighbors and local officials digging with their hands through the rubble. the pakistani search team is specialized for rescues in natural disasters officials said and comes equipped with groundpenetrating radar and concrete cutters. as soon as a deadly earthquake stopped rattling a swath of nepal before the scope of the damage was calculated the digging began. the earthquake has killed more than people and the death toll is expected to rise

@highlight
more than deaths reported after nepal earthquake

@highlight
rescue efforts range from digging by hand to military deployments
