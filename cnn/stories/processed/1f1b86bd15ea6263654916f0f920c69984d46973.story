as saudi forces pounded southern yemen with a fresh series of airstrikes wednesday houthi rebels called for peace talks. saudi arabia and its coalition partners launched airstrikes on houthi positions across yemen starting on march hoping to wipe out the iranianallied rebel group that overthrew the government and seized power. the previous round of talks between houthi rebels and the government of yemeni president abdu rabu mansour hadi failed in january after rebels attacked the presidents personal residence and presidential palace in sanaa the yemeni capital. but less than hours later after rebel forces attacked a yemeni government military brigade the airstrikes resumed according to security sources in taiz five airstrikes targeted a weapons depot in the province late wednesday two taiz security officials said explosions lasted for about minutes they said

@highlight
houthis call for halt to fighting and resumption of peace talks

@highlight
the cessation of airstrikes lasted less than hours

@highlight
next phase called operation renewal of hope will focus on political process
