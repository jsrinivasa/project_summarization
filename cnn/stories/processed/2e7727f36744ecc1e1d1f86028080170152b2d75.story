they were sons and daughters brothers and sisters friends and fellow citizens. they were students and dreamers pursuing their ambition for a better life. and on tuesday night kenyans remembered them as innocent victims of a terrorist attack that stunned a nation and left communities heartbroken. the gathering started with quiet chatter among a crowd of hundreds before mourners went silent and moved toward one end of nairobis uhuru park organizers unloaded crosses from a truck and quietly planted them in the ground

@highlight
kenyans gather in nairobi to remember victims of a terrorist attack that stunned a nation

@highlight
the attack at a garissa university last week killed people mostly students
