its the simplest possible assignment but it always teaches a huge lesson every year denver teacher kyle schwartz passes out postit notes to her third grade students and asks them to complete the sentence i wish my teacher knew. schwartz always gives her students the option to write their names on their notes or remain anonymous of sharing them only with her or with the entire class it surprises her how often students stand up and want to read their wishes out loud like the shy student who shared i wish my teacher knew im nervous all the time. schwartz said shes learned not to assume what her students wish although most of the students at her school live in poverty not every message is about a material need. many of the students she teaches at doull elementary just wish they had something in common with her that she knew soccer or video games

@highlight
denver teacher kyle schwartz asked students to share what they wish she knew

@highlight
their honest answers moved schwartz and sparked a discussion online
