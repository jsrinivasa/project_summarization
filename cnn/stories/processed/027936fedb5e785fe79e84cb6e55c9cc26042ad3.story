wanted film director must be eager to shoot footage of golden lassos and invisible jets. maclaren was announced as director of the movie in november. confirms that michelle maclaren is leaving the upcoming wonder woman movie the hollywood reporter first broke the story. obtained a statement from warner bros pictures that says given creative differences warner bros and michelle maclaren have decided not to move forward with plans to develop and direct wonder woman together cnn and warner bros pictures are both owned by time warner

@highlight
michelle maclaren is no longer set to direct the first wonder woman theatrical movie

@highlight
maclaren left the project over creative differences

@highlight
movie is currently set for
