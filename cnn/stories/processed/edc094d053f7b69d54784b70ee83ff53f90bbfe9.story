this article contains language that may be offensive to some. since the news broke last weekend that hillary clinton had declared her candidacy notable among the blitz of news stories are the many that refer to her as the mononymous hillary as if she were a pop star in a pantsuit. the new york post published an item titled lena dunham backs hillary for president while gossip site tmz refers to the former flotus us senator and secretary of state on a firstname basis throughout its news story on the announcement even the new republic posted a piece about the run called theres nothing inevitable about hillary. shes famous enough by now to go by one name but should she

@highlight
peggy drexler media even candidate herself call clinton just hillary this reinforces stereotypes about women needing to be approachable

@highlight
she says especially in global context trust respect important for the potential leader of free world not familiarity just hillary not appropriate
