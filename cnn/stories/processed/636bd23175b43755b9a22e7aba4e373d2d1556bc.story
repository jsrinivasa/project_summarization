a helicopter crash saturday in malaysia killed six people including the nations former ambassador to the united states and a highranking member of the prime ministers staff the malaysian state news agency bernama reported. among the victims were azlin alias who worked on the prime ministers staff and jamaluddin jarjis former malaysian ambassador to the united states and chairman of malaysia bernama said is an organization that develops housing in urban centers. the state news agency called azlin the prime ministers private secretary general but najib referred to him as chief of staff. the news agency said malaysian civil aviation authorities are expected to release a preliminary report on the crash within seven days

@highlight
jamaluddin jarjis former malaysian ambassador to the us among casualties

@highlight
azlin alias a member of the prime ministers staff also dies news agency reports
