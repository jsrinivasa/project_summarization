i would have done anything to get to europe it was worth the risk the bad treatment and the fear hard as that may be to believe simply put i have a better life now than i did before. i didnt know where my fellow travellers were heading but i knew one thing my dream of making it to europe no matter the cost and risk involved had been achieved it was worth it. on the fourth day we started our journey with a mix of excitement and fear fear that this madness often ends in tragedy ends with us as numbers piled on top of all the other unfortunate nameless numbers who never made it to the other side but there was no going back its a oneway ticket. but syrians dont need visas to get into turkey so turkey it was i arrived in the country in december with an old dream in my mind reaching europe

@highlight
moutassem yazbek describes harrowing journey from turkey to italy

@highlight
yazbek a syrian refugee paid a smuggler to get him to italy in december
