governments around the world are using the threat of terrorism real or perceived to advance executions amnesty international alleges in its annual report on the death penalty. in the majority of countries where people were sentenced to death or executed the death penalty was imposed after proceedings that did not meet international fair trial standards the report stated in amnesty international raised particular concerns in relation to court proceedings in afghanistan bangladesh china egypt iran iraq north korea pakistan saudi arabia and sri lanka. the united states has the dubious distinction of being the only country in the americas to conduct executions but the number of convicts put to death here fell slightly from in to in the state of washington also imposed a moratorium on executions last year. amnestys figures do not include statistics on executions carried out in china where information on the practice is regarded as a state secret belarus and vietnam too do not release data on death penalty cases

@highlight
amnestys annual death penalty report catalogs encouraging signs but setbacks in numbers of those sentenced to death

@highlight
organization claims that governments around the world are using the threat of terrorism to advance executions

@highlight
the number of executions worldwide has gone down by almost compared with but death sentences up by
