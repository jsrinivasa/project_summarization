the racist and offensive emails that resulted in three ferguson missouri city employees either resigning or being fired have been released. in a separate report the justice department described what it said was a pattern and practice of discrimination against africanamericans by the ferguson police and municipal courts that discrimination included racist emails ferguson is a town of that is africanamerican. police capt rick henke and sgt william mudd resigned early last month after the emails were discovered as part of the evidence in the justice departments scathing ferguson report. the citys top court clerk mary ann twitty was fired in connection with the emails officials said

@highlight
raciallycharged and offensive emails from ferguson released after public records request

@highlight
two ferguson police officers resigned over racist emails

@highlight
citys top court clerk was fired
