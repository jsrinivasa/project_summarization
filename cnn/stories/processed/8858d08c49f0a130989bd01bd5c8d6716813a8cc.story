congolese immigrant tarsis mboma thale has a small business selling tshirts in johannesburg south africa thales job normally requires him to walk the streets of the city he has called home for the past few years. spoke to several immigrants living and working in south africa about the crisis which has caused south african president jacob zuma to postpone an overseas visit and left authorities scrambling to prevent further clashes. meanwhile eric kalonji left the democratic republic of congo and arrived in the south african capital in working as a waiter until the restaurant he worked at closed in january now devoting himself to his studies in new zealand he feels that the situation in south africa is more complex than a simple case of us versus them he believes the blame lies heavily on what he calls the governments failure to provide its people with jobs and education. he feels the south african government has been far too slow to respond to the violence which has erupted intermittently in various cities over the past few years

@highlight
wave of deadly antiimmigrant violence has caused thousands to flee their homes in south africa

@highlight
immigrants fear further attacks despite clamp down by authorities

@highlight
how am i going to pay the rent and feed my wife says one man
