iraqi and usled coalition forces have successfully ousted isis from the nations largest oil refinery the coalition said sunday. iraqi security forces regained full control of the baiji refinery the combined joint task force said. meanwhile peshmerga forces also with the assistance of coalition strikes cleared square kilometers square miles of isisoccupied territory in iraq on saturday the kurdistan region security council said the peshmerga are the national military force of kurdistan. over the past nine days the coalition conducted airstrikes in the area the statement said

@highlight
iraqi and usled coalition forces say they retook a key refinery from isis

@highlight
peshmerga forces also report retaking terrain from isis
