if you cared deeply about something would you protest make a sign join a demonstration how long could you keep going for in the face of indifference and inaction. but as weeks turned into months there was still no sign of the missing girls the spotlight on the campaign faded people stopped tweeting they stopped marching they stopped pleading with the nigerian government to do more to rescue the young students. when more than nigerian girls were kidnapped from their school a year ago by boko haram militants millions of people around the world joined a social media campaign to plead for their safe return. charles alasholuyi was one of those people from celebrities to world leaders voicing their anger via bringbackourgirls one of the top twitter hashtags of used in more than four million tweets

@highlight
some girls were kidnapped from their school in northeastern nigeria by boko haram a year ago

@highlight
mass abduction prompted global outcry with protesters around the world under the bringbackourgirls banner

@highlight
charles alasholuyi has held up a bringbackourgirls sign almost every day since to keep up awareness
