as ash from chiles calbuco volcano spread east into argentina geologists warned of the potential for more activity friday. the volcano has already erupted twice this week spewing ash to a depth of about inches centimeters in some places according to the ministry of interior and public safety. new advisories say airborne ash could reach an altitude of feet. s shasta darlington reported people were removing salmon a staple of the local economy amid fear of contamination from ash and lava trucks were used to evacuate farm animals and pets

@highlight
volcano already has erupted twice this week

@highlight
it has spewed ash to a depth of about inches in some places chilean officials say

@highlight
authorities issue an alert for two towns and theres a exclusion zone
