a nuclear submarine being repaired at a russian shipyard has caught on fire according to a law enforcement source speaking to russias staterun news agency itartass. russias ria novosti news agency says insulation caught on fire as welding work was being done on the submarine. there are no armaments or chemically active dangerous substances fissionable materials on it gladyshev said to tass the enterprises personnel left the premises when the submarine caught fire no one has been injured the fire presents no threat to people and the shipyard. tass reported that the fire began on a sub in the zvyozdochka shipyard in northwestern russia

@highlight
submarine is in zvyozdochka shipyard in northwestern russia

@highlight
no dangerous substances on the submarine shipyard spokesman told itartass
