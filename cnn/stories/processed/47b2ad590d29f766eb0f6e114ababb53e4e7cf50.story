the night before her daughters first triathlon kate parker could tell the child was nervous. the girls pose for an occasional portrait but most are kidinspired moments shaped by childish wonderment and energy as parents parker and her husband encourage their girls to play outside make new friends and try new things without worrying about grassstained knees and knots in their hair parker said now the girls have the confidence and curiosity to do it on their own. responses to the images are mostly positive parker said but theres the occasional complaint that shes showing just one type of girl its true parker said theyre the ones shes raising the only ones whose adventures she can document she hopes the project inspires parents to find their own creative ways to capture their childrens lives more important she wants kids to see they can be strong in whatever they are and whatever they hope to be. as she pulled the image up on her screen she got chills from her daughters direct stare she looked parker said like a little badass

@highlight
kate parker is the photographer and mother behind strong is the new pretty

@highlight
the photo series shows her messy wild daughters as they are parker said
