every week in the heart of nima a slum in the ghanaian capital of accra families congregate at a local mosque when the time comes young girls say goodbye to their loved ones and part ways filing up concrete steps leading up to the floor above. a predominantly muslim area in a largely christian nation nima has one of the densest populations in accra agyare says life is often challenging for many local girls with some growing up without ever leaving the community. set up by local amadu mohammad the nonprofit organization supports girls between the age of six and priming them for formal education through extracurricular classes in reading math poetry and information technology its goal is to break through social barriers and provide nima with a generation of female role models and so the group provides school funding to help the girls shape their own future. fourteen months after her first visit agyares coding class is well established she has set up a mentor scheme as part of tech needs girls recruiting and training women at university to act as role models for her younger students in nima

@highlight
many girls in nimaone of accras poorest slums receive little or no education

@highlight
achievers ghana is a school funded by the community to give the next generation a better chance of success

@highlight
girls are being taught to code by tech entrepreneur regina agyare who believes her students will go far
