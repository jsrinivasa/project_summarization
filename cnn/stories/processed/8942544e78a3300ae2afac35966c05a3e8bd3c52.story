a new york police department detective apologized friday for an angry exchange with an uber driver that was caught on video and landed him on modified assignment. in the video after the detective steps back to his car the uber driver who moved to america less than two years ago turns to the passenger and thanks him for recording the video. the video picks up seconds after the detective began yelling at the driver and mocking his accent and also shows the unmarked car with lights flashing pulled over behind the uber car. the altercation began monday when the uber driver gestured to a detective in an unmarked car to use his blinker after he was allegedly attempting to park without using it according to sanjay seth a passenger in the car who uploaded the video to youtube

@highlight
detective i sincerely apologize for berating uber driver

@highlight
nypd investigating encounter that was caught on tape by passenger

@highlight
detective placed on modified assignment
