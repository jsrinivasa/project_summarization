at least four people are missing after a severe storm capsized sailboats saturday afternoon during a regatta in mobile bay alabama coast guard spokesman carlos vega said. coast guard sector mobile received a report at approximately pm that a sailing regatta in mobile bay had been struck by severe weather causing several vessels to capsize and leaving a number of people in the water. the coast guard and other agencies were on the scene saturday night vega said. more than sailboats took part in the dauphin island race and as many as people in all were rescued from the water the coast guard said

@highlight
coast guard says about people were rescued from mobile bay

@highlight
more than sailboats took part in the dauphin island race an annual event
