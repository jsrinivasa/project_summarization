eternally blooming in kolkata india along the hooghly river is malik ghat a wholesale flower market that attracts more than sellers each day photographer ken hermann visited the market for his project flower man which is a series of portraits that casts light upon the people behind the petals. hermann was able to spend about days at the market and did not allow any challenges to hinder the completion of flower man he said that in addition to language barriers and the heat of kolkata making communication complicated and shooting at certain times difficult another adversity he faced was taking portraits of the female flower sellers. hermann plans to return to malik ghat and looks forward not only to the opportunity to photograph different kinds of flowers as the range varies based on the seasons but also to the chance to present the flower sellers featured in flower man with their portraits. one of the reasons hermann did not create the flower sellers portraits directly inside the market is because of the hectic atmosphere hermann compares malik ghat to the environments of financial trading and fish markets

@highlight
malik ghat is a wholesale flower market in india that attracts more than sellers each day

@highlight
photographer ken hermann spent days at the market photographing his project flower man
