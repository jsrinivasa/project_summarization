as the missouri national guard prepared to deploy to help quell riots in ferguson missouri that raged sporadically last year the guard used highly militarized words such as enemy forces and adversaries to refer to protesters according to documents obtained by cnn. in addition to analyzing the threat general protesters could pose to soldiers the national guard also briefed its commanders on their intelligence capabilities so they could deny adversaries the ability to identify missouri national guard vulnerabilities upon which threat forces may exploit causing embarrassment or harm to the mong the mission set states. a document titled operation showme protection ii which outlines the missouri national guards mission in ferguson listed players on the ground deemed friendly forces and enemy forces among groups characterized as hate groups were the kkk the rgb black rebels and the new black panther party but also general protesters. the documents reveal that the missouri guard was especially concerned that adversaries might use phone apps and police scanners to expose operational security

@highlight
the national guards language worries those who objected to the tactics used in quelling riots

@highlight
the language is contained in internal mission briefings
