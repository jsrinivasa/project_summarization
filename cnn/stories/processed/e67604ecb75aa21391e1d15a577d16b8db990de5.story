one hundred and fortyseven victims many more families affected even more broken hopes and dreams. as kenyans mourned those killed last week in one of the deadliest terrorist attacks in the nation citizens used social media to share the victims stories hopes and dreams. the posts provided heartwrenching details on the victims including one about an elderly man whose dreams died with his son he had reportedly taken a loan to educate him at the university where he was killed by alshabaab terrorists. using the hashtag a reference to the number of people mostly students killed at garissa university college on thursday kenyans tweeted pictures of the victims in happier times

@highlight
kenyans use hashtag to honor victims of kenya university attack

@highlight
the attack killed students three security officers and two university security personnel
