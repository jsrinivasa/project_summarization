an indonesian court has rejected a bid by two australian drug smugglers members of the bali nine to challenge their planned executions. chan and sukumaran have been jailed since april for a failed bid to smuggle more than kilograms of heroin from bali to australia. indonesia has long taken a hard line on drug smugglers and since assuming office in october widodo has made it clear he intends to be tough on those found guilty of such crimes. opinion why executions wont win indonesias drug war

@highlight
two australian drug traffickers on death row in indonesia have had legal bids rejected

@highlight
the men were seeking to challenge president widodos decision to refuse clemency in their cases

@highlight
andrew chan and myuran sukumaran are members of the bali nine drug syndicate
