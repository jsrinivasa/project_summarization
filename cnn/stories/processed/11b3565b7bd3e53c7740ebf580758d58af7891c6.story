a suburban new york cardiologist has been charged in connection with a failed scheme to have another physician hurt or killed according to prosecutors. moschetto allegedly gave an informant and undercover detective blank prescriptions and cash for the assault and killing of the fellow cardiologist according to prosecutors he also requested that the rivals wife be assaulted if she happened to be present authorities said. dr anthony moschetto pleaded not guilty wednesday to criminal solicitation conspiracy burglary arson criminal prescription sale and weapons charges in connection to what prosecutors called a plot to take out a rival doctor on long island. the fire damaged but did not destroy the office of another cardiologist whose relationship with dr moschetto had soured due to a professional dispute according to the statement from the district attorneys office

@highlight
dr anthony moschetto arrested for selling drugs and weapons prosecutors say

@highlight
authorities allege moschetto hired accomplices to burn down the practice of former associate

@highlight
attorney says client will vigorously defend himself
