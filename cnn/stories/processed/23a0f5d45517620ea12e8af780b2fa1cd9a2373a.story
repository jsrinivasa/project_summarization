they may not be star trektype extraterrestrials but we may be close to finding evidence of alien life a nasa scientist says. i think were going to have strong indications of life beyond earth within a decade and i think were going to have definitive evidence within to years nasa chief scientist ellen stofan said tuesday during a panel discussion on water in the universe. there are some caveats involved of course nasa isnt talking about intelligent alien civilizations from the alpha quadrant its referring to microorganisms. nasa released a graphic noting that scientists have found evidence or indications of water on a number of celestial bodies including the dwarf planet ceres and jupiters moon europa

@highlight
nasa chief scientist ellen stofan believes were close to finding alien life

@highlight
indications within a decade definitive evidence within to years she said

@highlight
finding water on other celestial bodies is key to determination
