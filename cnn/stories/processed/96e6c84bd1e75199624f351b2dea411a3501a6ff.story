duckies still got moves. on tuesday nights late late show on cbs actor jon cryer reprised the characters recordstore dance to otis reddings try a little tenderness right down to the walldancing the counterbashing and of course the trademark white shoes. in the original scene one of the bestloved bits from the john hughes film cryer dances around a record store lipsyncing the song as he tries to win the affection of molly ringwalds andie. in tuesdays recreation he dances in tandem with host james corden who tweeted that hed fulfilled a childhood dream by recreating the scene with cryer who turned on thursday

@highlight
jon cryer revives pretty in pinks duckie dance routine for the late late show

@highlight
host james corden tweets that the bit fulfilled a childhood dream
