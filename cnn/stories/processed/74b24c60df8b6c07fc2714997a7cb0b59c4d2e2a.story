for superhero fans the cup runneth over. with less than a month to go before the movie hits theaters marvel studios put all the speculation to rest with a poster featuring bettany as the heroic android who was a member of the superhero group for many years in the comics. within hours we got yet another indication that the superhero trend isnt going anywhere anytime soon and we didnt even talk about the new photo of ryan reynolds deadpool. meanwhile as many marvel fans know thursday was the eve of the new netflix series daredevil and after a photoshopped first look at charlie coxs iconic red daredevil suit went out marvel put out a video of the real one

@highlight
marvel studios releases first looks at paul bettany as the vision in avengers age of ultron and charlie cox in full daredevil costume

@highlight
jamie bells character of the thing was also unveiled for century foxs marvelbased reboot of fantastic four

@highlight
bryan singer unveiled the first look at xmen apocalypse angel played by ben hardy
