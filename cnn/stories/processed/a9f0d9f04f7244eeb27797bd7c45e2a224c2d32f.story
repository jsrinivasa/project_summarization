remains of up to nearly unaccounted for service members tied to the uss oklahoma at pearl harbor will be exhumed this year the defense department announced tuesday. the uss oklahoma sank when it was hit by torpedoes on december during the japanese attack on pearl harbor a total of sailors and marines on the ship were killed. in five more service members were identified with the help of historical evidence from pearl harbor survivor ray emory. thirtyfive crew members were positively identified and buried in the years immediately after the attack according to the defense department by all unidentified remains were laid to rest as unknowns at the national memorial cemetery of the pacific

@highlight
uss oklahoma was lost during japanese attack on pearl harbor in

@highlight
hundreds of crew members were buried without identification
