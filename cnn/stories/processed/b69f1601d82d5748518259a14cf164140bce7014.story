if one is to believe lawyers for aaron hernandez the former new england patriots star had no conceivable reason to kill a man who was his friend his future brotherinlaw and a reliable purveyor of the marijuana he chainsmoked. the prosecution said wallace and ortiz were longtime friends of hernandez who had complete control of them mccauley reminded the jury of testimony about hernandez and his two friends sunbathing poolside hours after the slaying drinking smoothies and hernandez at times leaving his then child with the two men. the motive for the killing has never been clearly spelled out but prosecutors said lloyd might have done or said something that didnt sit well with hernandez they said hernandez rounded up some friends and orchestrated the killing to settle the score mccauley said a perceived slight that might seem insignificant to someone such as disrespect would easily offend hernandez. why would hernandez leave a marijuana blunt he shared with the victim at the murder scene did those who sultan described as inept and biased police officers and prosecutors simply become fixated with the former tight end with a promising future in the national football league

@highlight
a massachusetts jury is deliberating hernandezs case

@highlight
hernandez is charged with firstdegree murder in the killing of odin lloyd
