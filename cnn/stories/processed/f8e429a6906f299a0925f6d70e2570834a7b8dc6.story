do you remember the talk about plans for iraqiled force to try to take back mosul this spring. in some ways the campaign for mosul has begun according to officials there are no plans for us combat troops involvement in an eventual operation they say but airstrikes have already targeted isis positions in the area. mosul has long been the big prize in the iraqi governments fight aided by a usled military coalition which has carried out airstrikes for months to defeat isis it has also long been a source of embarrassment considering how it fell after iraqi troops dropped their weapons abandoned their posts and ran for their lives when militants arrived last june. still mosul isnt tikrit

@highlight
us official said in february that iraqi troops could go into mosul in april or may

@highlight
officials say now that theres no timetable an invasion could come sooner or later

@highlight
they note that recapturing mosul from isis could be a complicated endeavor
