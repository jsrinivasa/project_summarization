to allay possible concerns boston prosecutors released video friday of the shooting of a police officer last month that resulted in the killing of the gunman. after moynihan was wounded and west killed police and and local leaders sought to allay community concerns at a time when officerinvolved shootings have led to protests throughout the nation. the officer wounded john moynihan is white angelo west the gunman shot to death by officers was black. moynihan is a former us army ranger who was honored at the white house for his heroism in the wake of the boston marathon bombing the top cop helped save a transit officer wounded in a gunbattle with the bombers

@highlight
boston police officer john moynihan is released from the hospital

@highlight
video shows that the man later shot dead by police in boston opened fire first

@highlight
moynihan was shot in the face during a traffic stop
