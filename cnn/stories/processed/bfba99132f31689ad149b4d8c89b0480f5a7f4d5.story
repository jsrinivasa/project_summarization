inside a plane at miami international airport baggage handlers are going on a shopping spree with passengers bags. the problem has been so serious at jfk that in el al airlines set up a hidden camera in a baggage hold the camera showed baggage handlers stealing items on flights bound for israel including a seiko watch iphones an ipad cameras gold rings and cash six of those arrested pleaded guilty to possession of stolen property or petty larceny and the seventh suspects case was sealed according to the queens district attorneys office. in miami which aggressively goes after luggage thieves police have arrested baggage handlers and ramp workers since including six so far this year. then in december seven more jfk baggage handlers were charged with stealing valuables from checked luggage the items were stolen from suitcases of passengers traveling to or from hawaii japan johannesburg london bangkok dubai milan and various us cities

@highlight
tsa received more than claims of missing valuables between

@highlight
most of the missing valuables were packed in checked luggage

@highlight
miamidade police set up hidden cameras as part of sting
