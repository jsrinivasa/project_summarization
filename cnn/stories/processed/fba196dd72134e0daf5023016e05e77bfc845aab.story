nearly million parents in the united states currently are raising kids on their own. farleyberens i grew up with a girl named michelle singleton who was diagnosed with breast cancer at the age of she was a single mother of four children and i was a young mom with two kids and it was scary you just want to be there to watch them grow up. paying the bills cleaning her home making dinner for her kids all of that became a real struggle i wanted to make things as easy as possible for her. farleyberens we focus on daytoday support financial assistance housecleaning prepared meals supplies for the home kids events its that oldfashioned mentality of neighbors helping neighbors

@highlight
jody farleyberens helps single parents who are battling cancer

@highlight
farleyberens saw the need firsthand through her childhood friend

@highlight
heroes
