a connecticut teen who has been forced to have chemotherapy to treat hodgkin lymphoma will remain in temporary custody of the state for the time being according to her attorney josh michtom. cassandra was diagnosed with hodgkin lymphoma in september and medical experts gave her an chance of survival if treated with chemotherapy without it doctors said at the time she was likely to die within two years. in december a judge ordered the young woman to be under the custody of the connecticut department of children and families at that time she was admitted to connecticut childrens medical center in hartford and has remained there since then doctors surgically implanted a port in cassandras chest to administer chemotherapy medications which began in spite of legal maneuvers to halt them. michtom and taylor failed in their effort before the connecticut supreme court to make the case that cassandra was mature enough to make her own medical decisions

@highlight
judge wont allow teen leave hospital before her last chemotherapy treatment

@highlight
attorneys for the teen are deciding whether to appeal

@highlight
cassandra c is now in remission and is no longer opposed to the chemotherapy treatments
