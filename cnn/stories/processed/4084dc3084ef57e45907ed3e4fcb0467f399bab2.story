panic tears fear. all those feelings and more permeated cities villages and camps around nepal on saturday after a massive magnitude earthquake struck around midday. hours later after a wave of relentless aftershocks many people still were too scared to go back inside any buildings others crowded around rubble including men and women racing to rescue those trapped and then there are the hundreds already confirmed dead not to mention the hundreds more who suffered injuries. below are some accounts from witnesses in the mountainous asian nation in their own words

@highlight
massive magnitude earthquake has struck nepal near its capital kathmandu

@highlight
as the death toll rises witnesses describe devastation and panic
