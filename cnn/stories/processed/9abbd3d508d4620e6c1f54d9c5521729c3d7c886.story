a suicide bomber detonated his explosives near a group of protesters in eastern afghanistan on thursday killing people and wounding dozens more police said. the suicide attack hit the group around am local time police said. an afghan lawmaker taking part in the protests in the city of khost was among the people wounded said faizullah ghairat the provincial police chief. humayoon humayoon an afghan member of parliament for khost province and the other protesters were on their way to join a larger rally against the provincial governor according to zahir jan an eyewitness

@highlight
an afghan lawmaker is among people wounded in the attack police say

@highlight
taliban spokesman denies his group was responsible for the attack
