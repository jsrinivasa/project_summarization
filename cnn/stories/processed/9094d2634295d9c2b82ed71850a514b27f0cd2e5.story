every morning sissa abu dahou puts on traditional male dress known as a jalabiya and heads into the streets of the egyptian city of luxor for a day of hard work. even if i die i will not take it off dahou said as she pulled on the male jalabiya in front of a tv camera when i had to dress in a womans jalabiya when i went to cairo i felt suffocated no i thank god i dont want anyone to look at me or look at my daughter. through the years the onetime housewife dreamed of owning her own business one day a small street stand to sell snacks and cigarettes after her interview on cbc television the governor of luxor province offered dahou a kiosk and a cash advance the maverick had one requirement she would only agree to meet with the governor in male attire. a widow at just dahou was forced to fend for herself in egypts patriarchal south where decades ago it was unheard of for women to earn their own living even in recent years women make up barely of egypts workforce according to the world bank

@highlight
sissa abu dahou recently was honored as one of egypts ideal mothers

@highlight
but for years she has dressed as a man so she could work in the conservative country

@highlight
people talked but i said i decided to be a man so i can take care of my small daughter says dahou
