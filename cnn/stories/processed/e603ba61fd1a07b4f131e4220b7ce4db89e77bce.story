this week brought a dramatic turn of events in a small community outside of detroit a man bloodied during a traffic stop was cleared of all charges the police officer who arrested the motorist is charged with beating him and the communitys police chief has stepped down. on monday wayne county prosecutor kym worthy announced charges of assault and mistreatment of a prisoner against william melendez hes the former inkster michigan police officer who is seen in police car dashcam video grabbing the unarmed motorist floyd dent around the neck while he lay on the ground and punching him several times in the head. dent a veteran ford automotive worker was arrested after the january traffic stop on charges of drug possession assaulting an officer and resisting arrest with the court decision wednesday all charges from the arrest have been dropped. on wednesday drug charges against dent were formally dropped in michigan circuit court clearing the motorist of all wrongdoing

@highlight
inkster michigan police chief vicki yost resigns in the wake of charges against one of her former officers

@highlight
william melendez was caught on police car dashcam video in january beating an unarmed black motorist

@highlight
melendez was charged with assault on monday all charges against the motorist have been dropped
