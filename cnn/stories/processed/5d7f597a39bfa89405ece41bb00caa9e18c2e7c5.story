an asiana airlines plane overran a runway while landing at japans hiroshima airport on tuesday evening prompting the airport to temporarily close the japanese transportation ministry said. hiroshima airport closed because of the incident tuesday night while fire department officials worked at the scene. there were passengers and eight crew members including five cabin attendants two pilots and a maintenance official aboard when the flight took off from south koreas incheon international airport at pm local time asiana said in a statement late tuesday. authorities are investigating initial reports that the airbus may have hit an object on the runway during landing causing damage to the rear of its body and the cover of the engine on the left wing the ministry said

@highlight
the plane might have hit an object on the runway the japanese transportation ministry says

@highlight
people have minor injuries officials say

@highlight
the airbus overshot the hiroshima airport runway at pm tuesday officials say
