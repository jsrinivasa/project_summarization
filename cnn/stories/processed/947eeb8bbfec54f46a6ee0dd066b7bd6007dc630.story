police have arrested four employees of a popular indian ethnicwear chain after a minister spotted a security camera overlooking the changing room of one of its stores. they found an overhead camera that the minister had spotted and determined that it was indeed able to take photos of customers using the stores changing room according to kashyap. federal education minister smriti irani was visiting a fabindia outlet in the tourist resort state of goa on friday when she discovered a surveillance camera pointed at the changing room police said. four employees of the store have been arrested but its manager herself a woman was still at large saturday said goa police superintendent kartik kashyap

@highlight
federal education minister smriti irani visited a fabindia store in goa saw cameras

@highlight
authorities discovered the cameras could capture photos from the stores changing room

@highlight
the four store workers arrested could spend years each in prison if convicted
