ciudad juarez mexico was once known as the murder capital of the world back in at the height of cartel violence the city averaged killings per day. but five years later local officials say the city is much safer and plans are underway to lure foreign tourists and investors back to juarez this month the city launched the tourism campaign juarez is waiting for you. as a region el paso and juarez represent of all usmexico trade the binational ties are strong and have remained strong orourke says yes we had a really difficult time for a period juarez was at one time the deadliest city in the world. another factor that may have helped the turf war between the juarez and sinaloa cartels essentially ended with the sinaloa cartel claiming victory in the battle for the trafficking route in juarez

@highlight
cartel violence helped make juarez the murder capital of the world five years ago

@highlight
but the murder rate in the city has declined rapidly since

@highlight
now city leaders are working to bring visitors and foreign investment back to juarez
