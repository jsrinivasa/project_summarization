german police overnight thwarted a terrorist plot by a radicalized couple a plan they suspect involved bombing a bicycle race near frankfurt a german terrorism researcher briefed by investigators told cnn on thursday. the suspected target according to florian flade the terrorism researcher was a race planned for friday the race loops around eshborn and frankfurt on may day each year attracting large crowds of spectators along the cycle route. flade said that according to german police documents german police first became aware of the couple at the end of march when they went to a garden center near frankfurt to purchase hydrogen peroxide he said the store employee contacted police after becoming suspicious for several reasons. furthermore after police thwarted a bomb plot by german extremists trained in the tribal areas of pakistan to kill american servicemen in germany in september with hydrogen peroxidebased bombs the socalled sauerland plot german law had required such stores to report to police significant purchases of hydrogen peroxide

@highlight
german police say they think they have thwarted an islamist attack interior minister for hesse state says

@highlight
german terrorism researcher couple accused of planning bomb attack on bicycle race near frankfurt
