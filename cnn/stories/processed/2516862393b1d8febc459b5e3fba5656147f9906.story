like phone booths and typewriters record stores are a vanishing breed another victim of the digital age. on saturday hundreds of music retailers will hold events to commemorate record store day an annual celebration of well your neighborhood record store many stores will host live performances drawings book signings special sales of rare or autographed vinyl and other happenings some will even serve beer. yes its harder in the spotify era to find a place to go buy physical music but many of the remaining record stores are succeeding even thriving by catering to a passionate core of customers and collectors. corporate america has largely abandoned brickandmortar music retailing to a scattering of independent stores many of them in scruffy urban neighborhoods and thats not necessarily a bad thing

@highlight
saturday is record store day celebrated at music stores around the world

@highlight
many stores will host live performances drawings and special sales of rare vinyl
