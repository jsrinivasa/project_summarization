another controversy has erupted from the press tour for the soontobeblockbuster marvels avengers age of ultron. with one week left until its release the avengers might want to steer clear of controversy from here on out. obtained a statement from renner saying i am sorry that this tasteless joke about a fictional character offended anyone it was not meant to be serious in any way just poking fun during an exhausting and tedious press tour. after robert downey jr walked out on an interview his costars chris evans and jeremy renner found themselves in hot water wednesday over comments they made in a similar junket interview

@highlight
chris evans and jeremy renner get in hot water after a joke made about avengers character black widow

@highlight
renner called scarlett johanssons character a slut and evans referred to her as a whore

@highlight
the actors issued an apology on thursday
