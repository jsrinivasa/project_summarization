with the announcement that e will air a new documentary series this summer about bruce jenners transition from male to female fans are eagerly awaiting bearing witness to the former olympians journey. bruce is incredibly courageous and an inspiration and we are proud to be entrusted with this deeply personal and important story said jeff olde executive vice president of programming and development for e this series will present an unfiltered look as bruce boldly steps into uncharted territory and is true to himself for the first time. fans of keeping up with the kardashians often got to see the sometimes strained relationship between bruce and his wife kris as the kardashian family matriarch who also manages her kids careers kris butted heads with bruce over everything from their children to his desire for more privacy. the pair announced they had filed for divorce in september after some time of living apart there has been plenty of speculation regarding kris jenners feelings about bruces transitioning though she tweeted her support after he made the announcement friday during an interview with abcs diane sawyer

@highlight
e plans to air a new jenner reality show this summer

@highlight
it will follow his transition from male to female
