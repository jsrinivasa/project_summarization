it was a scene worthy of any top cop show on tv bullets flying bangedup cars and the fbi chasing an armed robbery suspect. fbi agents and task force officers were following kevone charleston of austell georgia as he pulled into a cvs pharmacy in forsyth county georgia early saturday charleston is suspected of involvement in commercial robberies dating to november according to fbi officials. the incident all happened around oclock saturday morning said fbi special agent stephen emmett there were multiple agents and officers that were following him based on his prior mo and when they saw he was about to rob another cvs they moved in. there were several fbi vehicles that were rammed or were hit by the suspects vehicle when he was trying to flee one government vehicle sustained heavy damage to its front and side and another government suv ended up on its side thats how the two agents sustained their injuries emmett said

@highlight
fbi agents and a suspected serial robber exchange gunfire in an fbi stakeout

@highlight
two fbi agents are injured and the suspect is shot during the gunfight
