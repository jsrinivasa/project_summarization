the man accused of stabbing us ambassador mark lippert in seoul last month is now charged with attempted murder a south korean court official said wednesday. lippert was stabbed march during an event organized by the korean council for reconciliation and cooperation which advocates peaceful reunification between north and south korea. kim kijong has also been charged with assaulting a foreign envoy and business obstruction the seoul central district court official said. police official yoon myeongseong told reporters that kim had visited north korea seven times between and and that authorities were investigating a possible connection between his visits to the reclusive state and the attack against lippert

@highlight
kim kijong is charged with attempted murder and assaulting a foreign envoy

@highlight
hes accused of stabbing us ambassador mark lippert in the face and arm

@highlight
police said kim opposed the joint ussouth korean military drills
