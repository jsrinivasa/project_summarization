a palestinian teenagers name will be removed from an israeli memorial commemorating fallen soldiers and the victims of terrorism after his family and others complained. indors organization wrote a letter to the national insurance institute of israel the countrys social security administration which maintains the memorial site demanding that abu khdeirs name be removed from the memorial wall indor said if the teenagers name is not taken off members of almagor want their own family members names removed. a picture on the memorial website for abu khdeir shows an israeli flag with two flowers called blood of the maccabees in israel a symbol often used on memorial day when the country honors its soldiers killed in the line of duty and victims of terrorism. but abu khdeirs family objected to his inclusion on the memorial wall his father hussein abu khdeir said no one asked for his permission to put his sons name on the wall i refuse that my sons name will be listed between soldiers of the occupation he said

@highlight
abu khdeirs name is on the memorial wall at jerusalems mount herzl

@highlight
his father and a terror victim advocacy group objected to his being included in the list

@highlight
the palestinian teenager was beaten and burned by three israelis authorities say
