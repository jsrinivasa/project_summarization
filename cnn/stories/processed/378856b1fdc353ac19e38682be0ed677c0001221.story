a dress worn by vivien leigh as she played scarlett ohara in gone with the wind has fetched at an auction. the dress a jacket and full skirt ensemble was worn in several key scenes in the movie including when scarlett ohara encounters butler and when she gets attacked in the shanty town. other standout items include a straw hat worn by leigh in a number of scenes in the movie and a gray wool suit worn by clark gable as his character rhett butler kicks down the door of scarlett oharas boudoir. tumblin said he came across the scarlett ohara dress in the early while doing some research at a costume company

@highlight
collector says he bought the outfit as it was about to be thrown away

@highlight
vivien leigh wore it in several key scenes in the movie
