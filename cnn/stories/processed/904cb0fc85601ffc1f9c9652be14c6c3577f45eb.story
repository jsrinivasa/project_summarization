there is a persian proverb that says a good year is determined by its spring. for many iranians the historic decision by tehran and six world powers the united states britain france germany russia and china to continue nuclear negotiations under a joint comprehensive plan of action is an indication that this years norouz the persian new year that started on march will be a superb one indeed. its early spring and people on the streets are talking about nothing but the current choice it represents the biggest chance of rapprochement between tehran and washington since the iranian revolution in. although edgy hardliners may now try to play games and complain the preliminary agreement is not what they had hoped for many people it shows excellent progress and thats what they want

@highlight
deal between iran and six world powers has given iranians hope writes ghanbar naderi

@highlight
lifting of international sanctions a possibility iranians hope for better living conditions he adds

@highlight
people will likely keep president rouhanis moderate government in power naderi writes
