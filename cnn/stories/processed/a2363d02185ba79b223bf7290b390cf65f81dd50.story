intelligent life that can communicate via radio waves with other intelligent life is less than years old here on earth. are we alone in the cosmos or might there be intelligent life elsewhere. so while planets that develop simple forms of life may be a dime a dozen the number that have sentient beings with whom to converse even assuming they evolved as humans did with ears and spoken language or eyes and written language is likely to be tiny and life that can use radio waves has existed on earth for only of the planets history years out of billion if the half dozen or so rocky earthlike exoplanets now known are similar the odds of discovering humanlike life on them are about the same as well winning your state lottery with one ticket. life on earth developed in its oceans about a billion years after the planet formed that suggests that rocky planets with liquid water on their surfaces might also have developed primitive forms of life

@highlight
nasa scientists discuss steps to discover life elsewhere in the universe over the next two decades

@highlight
meg urry life elsewhere in the universe and even elsewhere in our own milky way galaxy is practically inevitable

@highlight
but the chances that we can communicate with that life are slim she writes
