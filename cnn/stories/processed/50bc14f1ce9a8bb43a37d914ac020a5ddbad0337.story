it seems almost silly to be writing about baseball in the context of recent events except it isnt. politics threaten sports all the time from the demonstrations against the brazilian government before last summers world cup to the massacre of protesting students days before the opening ceremony of the mexico city olympics in sports knows well that it sits within the larger context of the world. last weekend as baltimore reacted to the death of freddie gray the young man who died last week from a spinal cord injury he suffered while in police custody major league baseball had a problem on its hands saturdays game between the orioles and red sox had gone into extra innings in camden yards with plenty of fans for both teams glued to their seats. the riots of baltimore the peaceful marches of baltimore the fury and unrest of baltimore did not seem to have had much to do with baseball but as the alwayswise atlantic magazine writer and baltimore native tanehisi coates take on the situation quickly went viral it became clear that the orioles home stand presented a problem

@highlight
amy bass baltimore rioting caused postponement of two orioleswhite sox games now third game of series will be played to empty stadium

@highlight
she says baseball can bring cities together but with so few black fans players it will be hard for baltimore to gather around this sport to heal
