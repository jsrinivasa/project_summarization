in her years living in rochelle illinois cathy olson had never seen a tornado that big i saw the top of the funnel cloud and it was absolutely massive she said. north of rochelle a tornado took away a local favorite restaurant grubsteakers its kind of one of your little greasy spoon restaurants said eric widick who drove up in his truck to help out. in rochelle the tornado flattened some of olsons friends homes. it could have been worse as severe tornado damage dotted a path not far from the dense populations of chicago and rockford the states third largest city the tornado cut a path through ogle county according to disaster management coordinator tom richter

@highlight
at least one person died as a result of storms in illinois an official says

@highlight
fire department rescuers searching for trapped victims in kirkland illinois
