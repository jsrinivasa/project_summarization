a man charged with planning the deadly mumbai terror attacks in india has been released on bail in pakistan after years of detention prompting sharp criticism from india. lakhvi was charged in pakistan in accused of masterminding the november terror attacks that left more than people dead in mumbai indias most populous city. the country contacted pakistans foreign secretary to underline that this has reinforced the perception that pakistan has a dual policy on dealing with terrorists and those who have carried out attacks or are posing a threat to india are being dealt with differently said syed akbaruddin a spokesman for indias ministry of external affairs. india executed the last surviving gunman from the attacks in other suspects were all killed during the series of attacks which went on for three days

@highlight
the terror attacks in india left more than people dead

@highlight
a court granted the suspect bail last year
