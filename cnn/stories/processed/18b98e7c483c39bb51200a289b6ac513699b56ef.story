five militants from the kurdistan workers party were killed and another was wounded in clashes with turkish armed forces in eastern turkey the countrys military said saturday. last month abdullah ocalan longtime leader of the kurdistan workers party pkk called from his jail cell for the violence to end in a historic letter he urged fighters under his command to lay down their arms stop their war against the turkish state and join a congress to focus on the future. turkish prime minister ahmet davutoglu condemned the violence and said via twitter that the appropriate answer to the heinous attack in agri is being given by the turkish armed forces. four turkish soldiers also were wounded in the fighting that took place in the eastern city of agri the armed forces said in a written statement

@highlight
four turkish troops were wounded in the flight according to the countrys military

@highlight
turkey president recep tayyip erdogan says clashes are attempt to halt a resolution process with kurds

@highlight
violence between kurds and the turkish military has been ongoing for more than three decades
