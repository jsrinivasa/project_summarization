new yorks iconic statue of liberty and liberty island will reopen to the public saturday one day after a bomb threat that led to its evacuation was declared unfounded officials said. shortly after am the national park service was informed that a caller made a threat to blow up the statue of liberty which led to the evacuation according to park service spokeswoman mindi rambo. burke said about people were safely transported from the island and offered full refunds service to liberty island is to resume at am saturday. a sweep by us park police canine units alerted on an area of interest near the lockers at the base of the statue of liberty rambo said

@highlight
statue of liberty to reopen saturday

@highlight
locker thought to have a suspicious package was empty police say

@highlight
statue of liberty evacuated after bomb threat officials say

@highlight
the evacuation came after a phoned threat sources say
