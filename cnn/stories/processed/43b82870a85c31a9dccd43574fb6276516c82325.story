the last time frank jordan spoke with his son louis jordan was fishing on a sailboat a few miles off the south carolina coast. the next time he spoke with him more than two months had passed and the younger jordan was on a germanflagged container ship miles from north carolina just rescued from his disabled boat. the younger jordan said he took his sailboat out to the gulf stream to find some better fishing when it capsized he broke his shoulder when the boat flipped. louis jordan took his sailboat out in late january and hadnt been heard from in days when he was spotted thursday afternoon by the houston express on his ship drifting in the atlantic ocean

@highlight
louis jordan says his sailboat capsized three times

@highlight
he survived by collecting rainwater and eating raw fish

@highlight
his son isnt an experienced sailor but has a strong will
