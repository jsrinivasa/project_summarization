hiho kermit the frog has some competition. a newly discovered species of glassfrog looks an awful lot like the famous muppet. bulging white eyes kelly green skin the works. but the names not nearly so catchy as its famous counterpart hyalinobatrachium dianae or dianes barehearted glassfrog if you have to you can call it h dianae for short

@highlight
the newly discovered species looks a lot like kermit

@highlight
you can see its internal organs through the translucent skin on its belly
