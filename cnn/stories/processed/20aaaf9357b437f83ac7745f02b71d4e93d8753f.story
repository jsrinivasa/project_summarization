the question haunting kiev is this who might be murdering allies of ukraines ousted president viktor yanukovych. on february mikhail chechetov reportedly jumped from the window of his apartment in kiev having left a suicide note he was suspected of having falsified the results of a parliamentary vote in early that essentially prohibited protest just as thousands of people were protesting against the yanukovych government. ukrainian president petro poroshenko who came to office after yanukovych was ousted has demanded an investigation of the killings of kalashnikov and buzyna his media office said. the idea that this might be happening is not entirely new but it muscled its way to the fore again this week with two highprofile shooting deaths in the ukrainian capital one of a former member of parliament with ties to yanukovych the other of a ukrainian journalist known for his prorussian views

@highlight
five recent deaths heighten suspicions on both side of ukraines ethnic divide

@highlight
ukraines president orders an investigation of the recent killings

@highlight
the opposition calls the killings oppression but the government says moscow may be to blame
