nine of ten mostly foreign prisoners on death row in indonesia have been given hours notice ahead of their execution according to a spokesperson for the attorney generals office. the secretarygeneral appeals to the government of indonesia to refrain from carrying out the execution as announced of prisoners on death row for alleged drugrelated crimes a spokesman for ban said according to various reports. the ten on death row which also include two australian citizens andrew chan and myuran sukumaran members of the socalled bali nine smuggling ring as well as prisoners from ghana brazil nigeria and indonesia had their petitions for clemency denied by president joko widodo in late. the two australians convicted for their role in a failed heroin smuggling plot tried to challenge the presidents decision earlier this month but lost an appeal for the state administrative court to hear their case their lawyers have since filed another review at the constitutional court the attorney generals office has said they would respect all ongoing court proceedings but insisted the inmates have exhausted all their legal options

@highlight
australian and philippines governments given notice their nationals will be executed in hours

@highlight
they include philippines maid mary jane velos and australians andrew chan and myuran sukumaran

@highlight
australia has repeatedly appealed for clemency for the pair
