the united states has seemingly erupted this week about what it means to live your religion especially in indiana where its new religious freedom restoration act faces a firestorm from critics who say it uses faith as a pretext to discriminate against gay people. soto and russell sued the federal government but a federal district court ruled in favor of the government rejecting the two mens first amendment assertions and their claims under the federal religious freedom restoration act the same statute that indiana legislators used in developing their new state law. such state laws have been growing ever since the us religious freedom restoration act became law in designed to prohibit the federal government from substantially burdening a persons exercise of religion. they were a legitimate religion and this was a legitimate ritual of the religion and congress wanted to make sure it was protected toobin said of peyote and the law

@highlight
a native american from a tribe not recognized by the feds wins the return of his eagle feathers

@highlight
an irs accountant is fired for insisting on carrying a symbolic sikh knife to work

@highlight
a group of chicago pastors takes on city hall over its permits for new churches and loses
