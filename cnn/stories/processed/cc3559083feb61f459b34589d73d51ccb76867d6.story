two delaware boys are in a coma and their father still is unable to talk or move two weeks after they became sick perhaps from pesticide exposure federal officials say during a trip to the us virgin islands their lawyer said saturday. the family was airlifted to hospitals in the united states the boys and were in critical condition at a philadelphia hospital on saturday the familys lawyer james maron of delaware said. esmond was found unconscious the boys and their mother were having seizures maron said the lawyer did not say who called the paramedics. the us environmental protection agency said friday that the presence of a pesticide at the rented villa in st john may have caused the illnesses which were reported to the epa on march

@highlight
a delaware family becomes ill at the sirenusa resort in the us virgin islands

@highlight
preliminary epa results find methyl bromide was present in the unit where they stayed

@highlight
the us justice department begins a criminal investigation into the matter
