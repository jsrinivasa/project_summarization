the mother of a quadriplegic man who police say was left in the woods for days cannot be extradited to face charges in philadelphia until she completes an unspecified treatment maryland police said monday. the montgomery county maryland department of police took nyia parler into custody sunday after philadelphia police reported that she left her son in the woods while she hopped a bus to see her boyfriend in maryland a man walking through the woods found him friday lying in leaves covered in a blanket with a bible and a wheelchair nearby philadelphia police say. the man is unable to communicate how he came to be in the park but philadelphia police lt john walker told reporters that the mans mother left him there the morning of april starks identified the mother as parler on monday. for more than four days police say the quadriplegic man who also suffers from cerebral palsy was left lying in the woods of philadelphias cobbs creek park low temperatures reached the during the week and rain was reported in the area wednesday and thursday

@highlight
mother must complete treatment before she can be extradited maryland police say

@highlight
mom told police son was with her in maryland but he was found friday alone in woods

@highlight
victim being treated for malnutrition dehydration mother faces host of charges after extradition
