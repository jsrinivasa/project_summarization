when the bomb went off steve woolfenden thought he was still standing that was because as he lay on the ground he was still holding the handles of his sons stroller. dr king told the court that martin was especially vulnerable to the blast because he was so small and close to the ground meaning the shrapnel more easily reached his head and torso its highly unlikely the boy died instantly king said. he pulled back the strollers cover and saw that his son leo was conscious but bleeding from the left side of his head woolfenden checked leo for other injuries and thought lets get out of here. that was before he noticed his achilles tendon which resembled transparent tape covered in blood and his left tibia protruding from his boot

@highlight
tsarnaev family members arrive in boston but its not clear if theyll testify

@highlight
a woman testifies that she had to choose whether to keep her leg some other victims had no choice

@highlight
starting monday the defense is expected to call witnesses to explain dzhokhar tsarnaevs difficult upbringing
