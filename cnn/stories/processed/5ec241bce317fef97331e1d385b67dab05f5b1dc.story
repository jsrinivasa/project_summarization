deputies rushed kenneth morgan stancil iii from court thursday after the murder suspect swore at a judge and tried to flip over a table. just a few minutes into thursdays hearing on the firstdegree murder charge he faces stancil snapped back at the judge after he was offered a courtappointed lawyer. superior court judge arnold o jones interjected pointing out that the maximum sentence stancil faces is the death penalty. i dont give a f what you want stancil said lunging forward and lifting up the table in front of him

@highlight
kenneth morgan stancil charged with firstdegree murder swears at the judge

@highlight
deputies escort him from court after he tries to flip over a table

@highlight
stancil is accused of killing an employee at wayne community college
