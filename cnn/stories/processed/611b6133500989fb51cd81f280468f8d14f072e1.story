a door bearing a graffiti drawing by british artist banksy was seized by police in gaza on thursday after a dispute over its sale a gaza police official told cnn on thursday. banksys graffiti in gaza has become an attraction after he visited in february and painted a series of political messages his works adorn walls and homes turning rubble into riches some of banksys art has sold for hundreds of thousands of dollars banksy has never revealed his identity but he is an english graffiti artist who began displaying his work in the early his street art often portrays political and social messages. darduna says he was duped into believing the door was a normal door when it was really a canvas for banksy one of the worlds most famous graffiti artists who had painted a greek goddess with her head in her hand the door was likely worth a small fortune a fortune darduna gave away for a fraction of its value. the iron door will remain in the possession of the khan younis police in southern gaza until a court hearing at a date yet to be determined

@highlight
rabea dardunas gaza home was destroyed last year he sold his door to bring in some money

@highlight
on thursday gaza police seized the door which had originally been sold for us

@highlight
some of banksys art has sold for hundreds of thousands of dollars
