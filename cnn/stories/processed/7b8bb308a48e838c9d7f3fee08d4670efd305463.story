the nations top stories will be unfolding tuesday in courthouses and political arenas across the country. on monday survivors and victims families wept and tsarnaev fidgeted at a defense table as jurors heard a prosecutor allege that the brought terrorism into the backyards and main streets the jury on tuesday morning began what is expected to be a lengthy deliberation process on total charges before the socalled penalty phase should he be found guilty on any counts. closing arguments are scheduled for tuesday and the jury will begin deliberations soon thereafter jurors in fall river massachusetts will be asked to decide if hernandez is culpable in the shooting death of lloyd whose body was found in a massachusetts industrial park in the summer of much of the evidence against hernandez is circumstantial and among the facts the jury will be asked to take into consideration are new england patriots owner robert krafts testimony the testimony of hernandezs fiancee some grainy footage from hernandezs home security system and a footprint left by a nike air jordan shoe. ok sure no one was floored when the kentucky senator announced his bid for the oval office but of course it was news when he made it official tuesday paul is expected to hit the campaign trail visiting the allimportant early voters in new hampshire south carolina iowa and nevada

@highlight
the trials of dzhokhar tsarnaev and aaron hernandez are coming to a close

@highlight
voting has put rahm emanuel and ferguson missouri back in the headlines

@highlight
rand paul has announced his bid for the presidency
