hundreds of additional iraqi troops are being sent to reinforce colleagues who are trying to fend off isis attempt to overrun iraqs largest oil refinery a key paramilitary force said tuesday. isis launched an assault on the baiji oil refinery late saturday by sunday isis said its fighters were inside the refinery and controlled several buildings but iraqi government security officials denied that claim and insisted that iraqi forces remained in full control. the reinforcements come four days after isis began attacking northern iraqs baiji oil refinery a key strategic resource that has long been a target because the facility refines much of the fuel used by iraqis domestically. the hasd alshaabi media office said tuesday that iraqi troops already at the refinery were holding their ground preparing to push isis out of the facility entirely

@highlight
isis attacked the baiji oil refinery saturday

@highlight
the refinery iraqs largest has long been a lucrative target for militants
