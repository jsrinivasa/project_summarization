when the earthquake hit many of nepals most renowned pagodas in and around kathmandu crumbled into rubblecovered stumps others were smothered under splintered handcarved wooden beams or multilevel rooftops. but the kathmandu valleys other pagodas stupas and shrines also built mostly of red brick hundreds of years ago suffered surprisingly little damage and remained standing next to structures which disappeared. from the late onwards western hippy backpackers who traveled the legendary overland route to kathmandu would climb the shiva pagodas wide ninestep plinth sit in the shade under the triplelayered roofs smoke hashish and enjoy the lofty view. on kathmandus outskirts the town of patan suffered terrible damage when its central durbar square lost several pagodas as structures pancaked straight down while others merely shook and cracked

@highlight
several of nepals best known landmarks have been destroyed by the earthquake of april

@highlight
but outside the capital kathmandu there is hope that many have survived
