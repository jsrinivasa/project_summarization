after more than nine years of traveling through the solar system nasas new horizons spacecraft has sent back its first color image of pluto. by discovering more about pluto and its moons new horizons will shed light on a littleknown third zone of the solar system beyond the rocky planets and the gas giants. discovered in pluto was once considered to be the smallest planet in the solar system but scientists have since revised that view because of plutos size and location demoting it to the status of dwarf planet a planet thats too small to clear other objects out of its way. pluto is miles wide roughly half the width of the continental united states at billion miles out in the solar system its about times as far from the sun as earth is

@highlight
the new horizons spacecraft captures image of pluto and its largest moon

@highlight
its set to reveal new details as it nears the remote area of the solar system
