australian prime minister tony abbott has been caught on camera guzzling a glass of beer in seven seconds amid raucous cheers from onlookers. they said his effort was a pale imitation of the legendary drinking exploits of former prime minister bob hawke who is credited with drinking two and a half pints in seconds. video of abbott making short work of the beer as the crowd around him chants skol skol drew plenty of attention on social media. the prime minister accepted and even gave a short impromptu speech one of the football coaches simon carrodus told the australian womans weekly

@highlight
some observers applaud abbotts beer swilling in a pub full of sportsmen

@highlight
the prime minister last year criticized binge drinking culture in australia
