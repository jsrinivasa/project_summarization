decision time for gop operatives another controversial foreign policy choice for president obama a ripple effect from the robert menendez indictment and two insights into hillary clintons campaign launch those stories filled our sunday trip around the inside politics table. much of the media attention on the corruption indictment against democratic sen robert menendez of new jersey has been on its allegations of luxury hotel stays with girlfriends. others will soon follow and as things get more official pressure is mounting on gop operatives to choose sides. i was talking to some campaign finance watchdogs this week and they say if the federal election commission really starts to look into this theyre actually going to find some impropriety with other lawmakers much much farther than menendez said kucinich so watch for that if it starts happening

@highlight
white house weighing whether obama should meet with raul castro

@highlight
a serious congressional ripple effect from the menendez indictment

@highlight
its decision time for gop operatives as the get ready to launch
