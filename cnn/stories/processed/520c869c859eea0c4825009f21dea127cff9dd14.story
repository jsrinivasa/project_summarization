working as a warden in rwandas volcanoes national park home to the endangered mountain gorilla edwin sabuhoro was determined to do whatever was necessary to protect the animals he was committed to defend including putting himself in harms way. watch this meeting the men who poach rwandas gorillas. but sabuhoro wasnt finished while talking to tourists who came to visit the volcanoes sabuhoro came up with an idea to capitalize on their interest in the area and provide work for former poachers. following a series of incidents in where wildlife had been lost sabuhoro volunteered to infiltrate poachers on their own turf by disguising as a potential buyer for a baby gorilla the mission was successful and the culprits were put to prison

@highlight
conservationist edwin sabuhoro founded a cultural village in rwanda

@highlight
the village provides work for poachers and unemployed youth
