the lawyer for a baltimore community activist whose arrest was broadcast live on cnn said thursday morning that his client had been released from jail. in november the baltimore city paper ran a story about kent then and his participation in baltimore protests over a grand jurys decision not to indict a police officer in the august shooting of teenager michael brown in ferguson missouri he helped lead protesters who walked through morgan states campus and eventually to city hall on november the newspaper reported. investigators made arrests in baltimore on tuesday night city police commissioner anthony batts said seven were for curfew violations he said. complete coverage on the baltimore protests

@highlight
joseph kents attorney says his client was released from jail

@highlight
police in baltimore detained kent on live tv after start of curfew

@highlight
that triggered a wave of interest on social media
