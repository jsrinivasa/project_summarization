since the headlinegrabbing murder of american journalist james foley by isis militants eight months ago the world has been regularly confronted with a modern form of an ancient primally horrifying method of execution. reports of beheadings also emerge from saudi arabia where it is a legal method of execution under the countrys judicial code and mexico and brazil where it is typically the work of criminal gangs. in australia and the philippines jihadrelated threats or plots to decapitate have allegedly been made in the past seven months recently a muslim convert in london was found guilty of a plot to behead a british soldier inspired by the rigby murder. whether or not nonjihadist beheadings are on the increase this brutal method of killing has inarguably come to occupy a larger part of the public consciousness as isiss beheadings have grabbed international headlines and the terror groups call on supporters to attack disbelievers has reverberated worldwide

@highlight
the wave of isis beheadings has horrified people all over the world

@highlight
it may also have contributed to isolated beheading incidents by nonjihadists says an academic

@highlight
professor arie w kruglanski says exposure to the videos could help prime some to emulate them
