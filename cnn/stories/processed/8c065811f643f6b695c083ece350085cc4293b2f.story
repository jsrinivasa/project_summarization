a white police officer claims he feared for his life and is justified in killing an unarmed black man a police chief supports the police officer who is ultimately exonerated and a predominantly black community seethes with rage because it knows that an injustice was done. in ferguson the video we have in the death of michael brown is of press conferences with police chief tom jackson who refused to release the police officers name but did release a video that appeared to show michael brown stealing cigars we have the video of the military weapons deployed by the police in ferguson that were trained on its residents and the press the world seethed. we watched in horror as we saw slager shoot scott in the back multiple times then we saw slager pick up something from one location and place it near scotts lifeless body on tuesday the officer was arrested on murder charges north charleston police chief eddie driggers told reporters i have watched the video and i was sickened by what i saw apparently so was slagers attorney who announced after the video was made public that he was no longer representing the officer. north charlestons police force is about white with a population of black and white in the city fergusons police force is white only three of the police officers are black and the city is black and white

@highlight
dorothy brown shooting by cop might have followed usual narrative of blaming black suspects

@highlight
but video in walter scotts fatal shooting showed the truth brown says

@highlight
with hindsight from michael brown case north charleston did the right thing with arrest
