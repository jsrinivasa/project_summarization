when she was growing up mui thomas wanted to be a fashion model not an unusual aspiration for a young girl. however mui began to thrive in a loving family environment and tina and rog formally adopted mui when she was three years old. after she was abandoned at birth longtime hong kong expats tina and rog thomas began fostering mui when she was just one and a half years old they were told she didnt have long to live. but mui suffers from a rare genetic condition that leaves the skin on her face and body red raw and open to infection

@highlight
mui thomas has a rare genetic condition that leaves her skin raw and open to infection

@highlight
abandoned at birth tina and rog thomas adopted mui

@highlight
shes now a rugby referee and an inspirational speaker
