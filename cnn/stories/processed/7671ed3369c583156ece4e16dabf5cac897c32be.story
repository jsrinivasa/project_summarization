a gunman walked into a building on the campus of wayne community college in goldsboro north carolina on monday and shot the schools print shop operator killing him authorities said. this is indeed a sad day for wayne community college and this close family and community school president kay albertson said. the school was placed on lockdown and the gunman remains at large all buildings have been cleared maj tom effler of the wayne county sheriffs office said. the shooting took place on the third floor of a campus building despite earlier reports the victim was not killed inside the library albertson said

@highlight
school print shop operator ron lane was killed college president says

@highlight
the man believed to be the gunman is identified as former student kenneth stancil

@highlight
the two knew each other authorities say
