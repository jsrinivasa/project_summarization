film director david lynch has confirmed he will no longer direct the revival of twin peaks a cult television show that was set to return in. showtime also loves the world of twin peaks and we continue to hold out hope that we can bring it back in all its glory with both of its extraordinary creators david lynch and mark frost at its helm. the offbeat tv series created by lynch and mark frost featured a quirky fbi agent who went to the pacific northwest town of twin peaks to investigate the mysterious murder of a high school girl named laura palmer. lynch also wrote that he had personally called the actors over the weekend to let them know he would no longer be directing

@highlight
david lynch says he wont be directing new episodes of twin peaks

@highlight
showtime saddened over decision which involved a dispute over money
