henol and mebratu emerge from their current home a modest structure with plastic sheeting serving as its roof carrying the master folder. i ask henol if he still believes in the legal route i can see now that weve been forgotten by the world he says there is no solution here no solution back home what can we do we are living in limbo. one of the most important documents on the camp its a record of each eritreans name and their case whether theyve been granted refugee status whether theyve had their resettlement interview whether theyve attempted the journey to europe by sea and whether theyve survived it. by the rows of names red dots are marked to signify the dead

@highlight
for years ali addeh refugee camp has been a holding point for those fleeing into djibouti

@highlight
many come from somalia ethiopia and especially eritrea which is ruled by a oneparty state

@highlight
despite the risks eritrean refugees say theyd risk their lives with people smugglers
