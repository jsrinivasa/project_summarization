isis claimed it controlled part of iraqs largest oil refinery sunday posting images online that purported to show the storming of the facility fierce clashes and plumes of smoke rising above the contested site. the group said it launched an assault on the baiji oil refinery late saturday by sunday isis said its fighters were inside the refinery and controlled several buildings but iraqi government security officials denied that claim and insisted iraqi forces remain in full control. couldnt independently verify isis claim it wouldnt be the first time that militants and iraqi forces have battled over the refinery a key strategic resource that has long been a lucrative target because the facility refines much of the fuel used by iraqis domestically. the refinery is just kilometers miles from the northern iraqi city of tikrit which iraqi forces and shiite militias wrested from isis less than two weeks ago

@highlight
isis says it controls several buildings at the baiji oil refinery

@highlight
iraqi government security officials say iraqi forces remain in full control

@highlight
the refinery iraqs largest has long been a lucrative target for militants
