seven people including illinois state university associate mens basketball coach torrey ward and deputy athletic director aaron leetch died when their small plane crashed while heading back from the ncaa tournament final. leetch left the illinois school to serve as director of athletics at whitworth university in spokane washington then came back in june as isus deputy director of athletics he was in charge of the redbirds athletics communications and video production units and had a handson role in its football mens basketball golf and baseball programs. he was part of the staffs at jacksonsville state university the university of mississippi and for one year in china before coming to illinois state as an assistant prior to the season he was promoted to associate head coach in may according to his official bio. the plane was coming back from the ncaa final four championship game in indianapolis according to illinois state athletics spokesman john twork

@highlight
the crashed plane was a cessna national transportation safety board reports

@highlight
coach torrey ward administrator aaron leetch among the killed in the crash

@highlight
the plane crashed while coming back from the ncaa title game in indianapolis
