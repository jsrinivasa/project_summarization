ending a decadeslong standstill in uscuba relations president barack obama met for an hour saturday with his cuban counterpart raul castro the first time the two nations top leaders have sat down for substantive talks in more than years. his meeting with castro on saturday isnt being billed as a formal bilateral session but obamas aides are still characterizing the event as the highestlevel engagement with the cuban government since thenvice president richard nixon met with fidel castro in. after the presidents remarks mccain tweeted so pres obama goes to panama meets with castro and attacks me im sure raul is pleased. on friday night obama and castro greeted each other courteously amid an explosion of camera flashes shaking hands before dining at the inaugural session of the conference the two sat at the same table but not directly next to one another

@highlight
the cold war has been over for a long time president obama says

@highlight
the thaw in ties has dominated discussion at the summit of the americas in panama

@highlight
the top leaders from the united states and cuba havent met for substantive talks in more than years
