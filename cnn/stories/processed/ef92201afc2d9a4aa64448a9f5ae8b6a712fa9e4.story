the graffiti written in a french chalk quarry and dating back almost years is plain and stark. the graffiti looks like it was written yesterday he added. for gusky the graffiti provides a human connection with men who lived a century ago in many cases they just wanted to be remembered he said. the land was privately owned for many years and generally offlimits to outsiders said gusky but it changed hands in the rights to operate it were purchased by a consortium of villages that wanted to promote awareness of the areas history he said

@highlight
world war i graffiti is discovered in an underground quarry

@highlight
the writings are generally plain with listings of names and places

@highlight
photographer graffiti a human connection to the past
