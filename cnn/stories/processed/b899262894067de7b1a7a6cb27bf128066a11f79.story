a tulsa county reserve deputy is on administrative leave after inadvertently shooting a suspect with his gun. deputy robert bates whos been placed on administrative leave during the investigation received his reserve status from the tulsa county sheriffs office in and was assigned to the violent crime task force he had also served as a tulsa police officer. when asked if another gun was found on harris shannon clark of the tulsa county sheriffs office says the suspect was placed in the ambulance and transported so quickly i have not been told there was a second weapon found on him yet. the shooting happened after an apparent drug and gun selling operation by the tulsa violent crimes task force thursday bates a member of the task force was part of a group of deputies trying to arrest eric courtney harris in the parking lot of a dollar general store

@highlight
police say robert bates thought he pulled out his taser during an arrest

@highlight
instead he shot the suspect who later died at a local hospital
