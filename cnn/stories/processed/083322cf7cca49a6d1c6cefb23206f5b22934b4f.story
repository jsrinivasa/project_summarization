initial tests on the flight data recorder recovered from downed germanwings flight show that copilot andreas lubitz purposely used the controls to speed up the planes descent according to the french air accident investigation agency the bea. the initial readout shows that the pilot present in the cockpit used the autopilot to put the airplane into a descent towards an altitude of feet then on several occasions during the descent the pilot modified the autopilot setting to increase the speed of the airplane in descent it said. its already emerged that lubitz had battled depression years before he took the controls of flight and that he had concealed from his employer recent medical leave notes saying he was unfit for work. the flight data recorder or black box was found thursday by recovery teams that have spent days since the march crash scouring the mountainside in the french alps where the plane went down

@highlight
french investigators flight data recorder reveals andreas lubitz acted deliberately to crash plane

@highlight
he used autopilot to set altitude at feet and then used the controls to speed up the descent
