when chinas biggest auto show opens in shanghai this week the only models on display will be the ones with four wheels. however the show comes at a turning point for chinas auto market which is facing a second year of slower growth in after a decadelong sales and production frenzy. nissan will use auto shanghai to unveil the lannia midsize sedan which it says has been specially created for the rising young chinese generation. it sells several hundred of its superluxury vehicles in china each year and in shanghai this week will launch its latest model the phantom limelight

@highlight
organizers want to ban scantilyclad models at car show

@highlight
the shanghai auto show is a key event for global automakers

@highlight
cars are no longer the status symbol they once were in china
