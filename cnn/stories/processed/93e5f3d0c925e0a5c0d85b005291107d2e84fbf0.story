we might never truly comprehend what drove copilot andreas lubitz to crash germanwings flight into the french alps on march killing everyone on board the latest report shows that he sped up the descent of the plane to its doom its terrifying. we wont pretend that this is an easy problem to solve but awful as germanwings flight tragedy was the case may galvanize a discussion and reexamination that is long overdue we need to build awareness and support for those who suffer from mental illness. as the investigation unfolds we will learn more about to what extent lubitz kept his mental illness secret or how much help he sought how much did his battle with depression affect his fitness to fly should he have walked away from his job should his doctors have sounded alarm bells. his case raises larger and important issues about people who are burdened with mental illness and the pressure of its stigma

@highlight
andreas lubitz the copilot who crashed the germanwings flight battled with depression

@highlight
jay ruderman and jo ann simons society must talk about mental illness to help people cope with it better
