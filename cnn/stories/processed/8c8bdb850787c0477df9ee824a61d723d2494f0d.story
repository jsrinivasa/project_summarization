two years ago the storied boston marathon ended in terror and altered the lives of runners spectators and those who tried to come to their rescue. the marathon historically happens on a monday this year runners will take on the mile challenge april. boston university graduate student lingzi lu also was killed in one of the two horrific blasts that brought chaos to the competitors and spectators near the races finish line on april. but the tsarnaevs were not on the minds of most people in boston on wednesday

@highlight
citizens gather to honor victims on one boston day two years after the marathon bombings
