brazilian supermodel gisele bundchen sashayed down the catwalk at sao paulo fashion week on wednesday night in an emotional farewell to the runway. bundchen announced over the weekend that she would be retiring from the catwalk though not the fashion industry. bundchen wrote about her fashion career on her instagram account i am grateful that at i was given the opportunity to start this journey today after years in the industry it is a privilege to be doing my last fashion show by choice and yet still be working in other facets of the business. on wednesday night brady had a frontrow seat at what was hailed as a historic moment in brazils fashion world

@highlight
gisele bundchen walked the runway for the last time wednesday night in brazil

@highlight
the supermodel announced her retirement from runway modeling over the weekend

@highlight
she plans to continue working in other facets of the industry
