on the surface the mixup seems incomprehensible how can a volunteer sheriffs deputy accidentally fire a handgun instead of a taser killing a man. tasers training calls for the stun gun to be placed on an officers nondominant side as law enforcement experts say and its training suggests that officers shout aloud taser taser taser as they prepare to deploy it. so how easy or hard is it to draw and fire a handgun mistakenly instead of a taser here are some factors to consider. he said that he saw the laser sight on the shoulder assumed it was the taser brewster said both the gun and the taser have a laser sight and he just made a mistake

@highlight
attorney robert bates assumed the gun was a taser because he saw a laser sight on it

@highlight
harris family lawyers say there are stark differences between the gun and taser used

@highlight
in an officer in california also said he mistakenly used his gun instead of a taser
