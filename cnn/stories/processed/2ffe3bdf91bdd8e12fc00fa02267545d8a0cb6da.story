the pentagon released a map this week showing coalition forces have taken back of iraqi territory seized by isis. the gains made in the fight against the terror group by iraqi security forces and coalition air power certainly look impressive although as the us department of defense acknowledges its a dynamic conflict and territory can change hands depending on daily fluctuations in the battle lines. aa its very telling there are losses but most of the losses are around the edges of their territory and what that means is a very conventional push forward by the iraqi forces its a push against the front line of isis rather than being brave and creative and going in behind isiss lines and breaking it up. what this isnt is using maneuverist warfare which is a military philosophy that exploits the capabilities of conventional forces to project power by using air forces to take land along main supply routes and put friendly forces on that land to cut land into chunks which causes massive disruption to command and control and their supply chains which can cause forces to collapse much more rapidly than a frontal push

@highlight
pentagon releases map showing coalition forces have taken back of iraq territory from isis

@highlight
counterinsurgency specialist afzal ashraf on what new data tells us about fight

@highlight
ashraf where it counts isis is not standing and fighting
