the streets were empty but not quiet shelling began to boom through the yemeni city of aden on thursday afternoon as we hurried back to board the boat that had brought us here from djibouti. saudi arabia began airstrikes on houthi rebels in yemen three weeks ago thursday but aden remains a city not fully in the hands either of houthi rebels or forces loyal to the ousted government of president abdu rabu mansour hadi. everyone we spoke to thursday told us the same thing living in aden these days is terrifying. aden is a city gripped by fear desperation and want people line up for bread they line up for cooking fuel and the electricity only works a few hours a day

@highlight
control of strategic seaport of aden divided between houthi rebels and government loyalists

@highlight
some saudi arms are falling into rebel hands

@highlight
terrified residents line up for bread and fuel and try to stay indoors
