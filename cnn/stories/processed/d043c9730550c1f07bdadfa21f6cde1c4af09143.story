pope francis has accepted the resignation of bishop robert finn who remained on the job for years after becoming the highestranking us catholic official convicted in connection with the churchs longrunning sex abuse scandal the vatican announced tuesday. and finn kept his job as bishop even after his conviction the official website of the catholic diocese of kansas cityst joseph still listed him as its bishop tuesday morning. the pope must show that this decision represents a meaningful shift in papal practice that it signals a new era in bishop accountability doyle said what no pope has done to date is publicly confirm that he removed a culpable bishop because of his failure to make childrens safety his first priority we urge pope francis to issue such a statement immediately. it kind of shook francis reputation said moss having this resignation and putting right one of the more visible injustices on this especially in the us i think this is a typical francis way to reinstall confidence

@highlight
expert decision doesnt look very urgent but it appears welltimed for pope francis

@highlight
robert finn remained a bishop after a conviction for failure to report abuse

@highlight
leader of watchdog group calls the popes decision a good step but just the beginning
