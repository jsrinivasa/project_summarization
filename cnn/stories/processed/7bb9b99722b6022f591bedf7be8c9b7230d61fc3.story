civil unions between people of the same sex will soon be recognized in chile. a number of south american nations have moved to allow samesex civil unions in recent years but marriage between people of the same sex is legal only in argentina brazil and uruguay. the new law will take effect in six months it will give legal weight to cohabiting relationships between two people of the same sex and between a man and a woman. the country joined several of its south american neighbors in allowing the unions when president michelle bachelet enacted a new law on monday

@highlight
president michelle bachelet signs law that will take effect in six months

@highlight
chile joins several other south american nations that allow the unions
