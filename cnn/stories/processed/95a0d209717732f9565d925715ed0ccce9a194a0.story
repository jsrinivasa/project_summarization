chinese police have arrested more than people and seized tons of illegal narcotics during a sixmonth antidrug campaign the countrys ministry of public security has announced. however the police also paid a price liu said in quotes carried by the staterun xinhua news agency nine police officers died and another were wounded in the mission with severely wounded the ministry rewarded units and people. he said the ministry had launched a threemonth online campaign starting in april targeting people engaged in drugrelated internet crimes. authorities also handled drugrelated crimes such as robbery and cases of drug use during the nationwide campaign to ban drugs in hundreds of cities liu yuejin assistant minister of public security said wednesday

@highlight
police arrest people and seize tons of narcotics in a sixmonth period

@highlight
china launches a new online campaign to crack down on online drug crimes

@highlight
celebrities have been embroiled in the nations intensifying antidrug campaign
