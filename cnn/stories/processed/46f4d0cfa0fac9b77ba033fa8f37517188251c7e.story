izzat ibrahim aldouri a former top deputy to saddam hussein and more recently a key figure in sunni extremist groups battling the iraqi government has been killed in a security operation in that country iraqi staterun television reported friday. husseins regime fell during a usled invasion in the us military had said that after the iraqi leaders fall aldouri helped finance a sunni insurgency with money he transferred to syria before the government collapsed. in subsequent years several claims asserted that aldouri was either killed or captured during the war and its aftermath at the same time a man claiming to be aldouri released a number of audio messages over the years taunting iraqi and us officials. aldouri was killed in an operation by iraqi security forces and shia militia members in the hamrin mountains between tikrit and kirkuk iraq shia militia commander hadi alameri said

@highlight
us military doesnt have further information to evaluate the iraqi media reports

@highlight
aldouris body arrives in baghdad where dna samples are taken

@highlight
izzat ibrahim aldouri was the highestranking member of iraqi president saddam husseins regime to evade capture
