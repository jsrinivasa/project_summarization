criminal justice reform is rapidly becoming one of the few bipartisan issues of our time its about time. its time for policymakers to address this criminal justice crisis head on we must change the dismal status quo we must start by asking a simple question why are so many americans criminals look no further than washington which has spent the past century devising the most complicated and nonsensical criminal code known to man. thankfully there is a bipartisan consensus in washington that something needs to be done and fast on the left civil rights groups and their allies in congress have been demanding that the criminal system be fixed for years on the right politicians from paul ryan to rand paul are now recommending the same thing. if politicians are serious they should consider three specific areas for reform

@highlight
america has the highest incarceration rate in the world holding of the worlds prisoners

@highlight
evan feinberg we must change the dismal status quo with specific solutions
