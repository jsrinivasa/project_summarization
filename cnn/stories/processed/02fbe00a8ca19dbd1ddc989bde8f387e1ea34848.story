nearly in americans say that businesses that provide weddingrelated services should be required to provide those services to samesex couples in the same way they would all other customers even if they have religious objections. looking at republicans and independents who lean toward the republican party in that group say weddingrelated businesses should be allowed to refuse services to samesex couples but there are sharp divides within that group by age and ideology moderate and liberal republicans and republicanleaners broadly say weddingrelated businesses should be required to serve all couples the same way while threequarters of conservative republicans favor allowing a caterer or florist to refuse service for religious reasons among republicans and republicanleaning independents under age say weddingrelated businesses should be required to serve samesex and differentsex couples the same way while among those age or older think they should not be required to do so. orc poll most democrats and independents say weddingrelated businesses should be required to provide services to samesex couples as they would differentsex couples while republicans break broadly the other way say religious reasons are a valid justification for refusing service. overall white evangelicals are broadly in favor of allowing businesses to refuse service for religious reasons say they should be able to but among whites who are not evangelicals say such businesses should be required to provide services to all couples the same way

@highlight
most americans say businesses should not discriminate against samesex weddings

@highlight
public opinion has shifted on the issue since last fall

@highlight
indiana passed and later changed its religious freedom law after public outcry
