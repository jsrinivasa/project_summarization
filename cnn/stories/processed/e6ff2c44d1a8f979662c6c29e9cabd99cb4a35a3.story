this is the time of the year when christians the world over more than billion of us reflect upon the crucifixion and resurrection of our lord. in light of the tragic massacre of christian college students in kenya on thursday and the ongoing threat against christians in other nations this holy week we are calling upon christians to also reflect upon the crucifixion beheading stoning enforced slavery sexual abuse human trafficking harassment bombing and displacement of hundreds of thousands of christians and others whose faith alone has made them a target of religious extremists. in a letter to christians living in another dangerous place in another persecuted time he wrote pray that we may be delivered from wicked and evil people. on march in a presentation to the un security council chaldean catholic patriarch louis raphael sako referred to the present reality of his fellow iraqi christians as a catastrophic situation

@highlight
rarely since the first century have christians faced persecution on this scale say dolan downey and burnett

@highlight
crisis escalated substantially as isis has swept through iraqs nineveh province the authors write
