irans president on friday hailed the proposed international deal on his countrys nuclear program vowing that iran will stick to its promises and assuming other countries live up to their end of the bargain become a more active engaged player in world affairs. even as he applauded what he called a good deal president barack obama pointed out thursday that if iran cheats the world will know it his government is also being pressed by israeli prime minister benjamin netanyahu who blasted the agreement friday as posing a grave danger by legitimizing irans nuclear program and making it easier for iran to develop nuclear weapons. what should happen assuming theres a common view that tehran is doing as required is that countries will end their sanctions that was nonnegotiable for iran with rouhani saying the deal shows his governments commitment to removing a major obstacle for business by addressing the worlds worries about its nuclear program. those promises include reducing irans stockpile of lowenriched uranium by for years and significantly scaling back its number of installed centrifuges still while it will be shrunken and centralized irans nuclear program wont go away

@highlight
iranian president says a nuclear deal would remove a major obstacle for business

@highlight
we can cooperate with the world president hassan rouhani insists

@highlight
he says we do not lie and iran will abide by its promises on nuclear deal
