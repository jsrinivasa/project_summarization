many australians are understandably appalled by the brutal and pointless executions of andrew chan and myuran sukumaran. are these cases comparable yes and no widodos executions were coldblooded unnecessary and highly political obama clearly was not intending to kill americans and this has only become an issue because one of the hostages actually was. the death penalty looks anachronistic and ineffective at the best of times but to kill two people who had clearly made the most of their long periods of incarceration to transform themselves and make amends for their actions looks gratuitous and cruel. consequently indonesias actions raise more general questions about the powers we give to states or more accurately to those who control the coercive apparatus of the state at any particular moment as german sociologist max weber pointed out one of the key features of an effective state is that it has a monopoly over the legitimate use of violence

@highlight
indonesia executed eight prisoners including two australians on wednesday

@highlight
two of bali nine were killed despite australias pleas for mercy

@highlight
all around the world innocent people being killed by the state in our name writes mark beeson
