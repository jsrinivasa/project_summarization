the attorney for a suburban new york cardiologist charged in what authorities say was a failed scheme to have another physician hurt or killed is calling the allegations against his client completely unsubstantiated. moschetto allegedly gave an informant and undercover detective blank prescriptions and cash for the assault and killing of the fellow cardiologist according to prosecutors he also requested that the rivals wife be assaulted if she happened to be present authorities said. a requests for comment from an attorney representing chmela was not returned its unclear whether kalamaras has retained an attorney. the fire damaged but did not destroy the office of another cardiologist whose relationship with dr moschetto had soured due to a professional dispute according to the statement from the district attorneys office

@highlight
a lawyer for dr anthony moschetto says the charges against him are baseless

@highlight
moschetto was arrested for selling drugs and weapons prosecutors say

@highlight
authorities allege moschetto hired accomplices to burn down the practice of former associate
