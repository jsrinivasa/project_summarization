anthony ray hinton is thankful to be free after nearly years on alabamas death row for murders he says he didnt commit. race poverty inadequate legal assistance and prosecutorial indifference to innocence conspired to create a textbook example of injustice bryan stevenson the groups executive director and hintons lead attorney said of his africanamerican client i cant think of a case that more urgently dramatizes the need for reform than what has happened to anthony ray hinton. for all of us that say that we believe in justice this is the case to start showing because i shouldnt have sat on death row for years he said. woman who spent years on death row has case tossed

@highlight
anthony ray hinton goes free friday decades after conviction for two murders

@highlight
court ordered new trial in years after gun experts testified on his behalf

@highlight
prosecution moved to dismiss charges this year
