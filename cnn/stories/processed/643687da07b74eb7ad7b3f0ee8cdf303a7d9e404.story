marine life seen swimming in unusual places water temperatures warmer than they should be no snow where there should be feet of it. gov jerry brown in announcing water restrictions the same day stood on a patch of dry brown grass in the sierra nevada mountains that is usually blanketed by up to feet of snow. the warmer temperatures we see now arent due to more heating but less winter cooling a recent news release from the university of washington announcing the studies said the university has worked with noaa on the research. according to new scientist magazine some marine species are exploring the warmer waters leading some fish to migrate hundreds of miles from their normal habitats

@highlight
waters in a huge area of the pacific are running degrees warmer than normal

@highlight
marine life that likes cooler water has moved and others that like warm seas are seen in new places

@highlight
the blob might be having an effect on rain and snow and the west coast drought
