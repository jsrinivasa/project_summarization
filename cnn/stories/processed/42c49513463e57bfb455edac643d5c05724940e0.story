lauren hill who took her inspirational fight against brain cancer onto the basketball court and into the hearts of many has died at age. lauren captured the hearts of people worldwide with her tenacity and determination to play in her first collegiate basketball game with her mount st joseph university team the group said on facebook. hill was diagnosed with diffuse intrinsic pontine glioma in when she was a senior in high school the rare brain tumor was inoperable but hill persisted in playing on her high school team despite chemotherapy treatments. its not often you get to celebrate a loss he told the crowd as he struggled to hold in his tears but today we celebrate a victory on how to live a life through lauren hill no you will be missed and remembered by so many

@highlight
lauren hills coach says she was an unselfish angel

@highlight
after playing for her college lauren hill helped raise money for cancer research

@highlight
ncaa president says she achieved a lasting and meaningful legacy
