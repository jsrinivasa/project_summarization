the flame of remembrance burns in jerusalem and a song of memory haunts valerie braham as it never has before this year israels memorial day commemoration is for bereaved family members such as braham. the latest news is a painful reminder of brahams recent loss braham now lives for her young children she has two daughters and one son braham tells them stories of their father to keep his memory alive and to keep herself strong she pauses as she speaks finding the right words to describe the love of her life who was taken from her. her husband philippe braham was one of people killed in januarys terror attacks in paris he was in a kosher supermarket when a gunman stormed in killing four people all of them jewish the terrorist amedy coulibaly recorded the attack on camera philippe braham was laid to rest in jerusalems givat shaul cemetery after the attacks not far from where the jewish agency held a memorial ceremony to mourn victims of terror. like valerie braham uzans parents attended the memorial service in jerusalem the recent attacks were on the nations collective mind as mourners gathered in groups to commemorate fallen soldiers and victims of terror

@highlight
we all share the same pain valerie braham tells memorial day crowd in israel

@highlight
her husband philippe braham was among killed in januarys terror attacks in paris

@highlight
french authorities foil a new terror plot a painful reminder of widows recent loss
