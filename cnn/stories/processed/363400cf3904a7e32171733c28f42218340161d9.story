its a warm afternoon in miami and emanuel vega has come to baptist health primary care for a physical exam dr mark caruso shakes his hand with a welcoming smile. caruso listens to vegas heart and lungs checks his pulse in his ankles and feels around his lymph nodes he also asks vega about his exercise and sleeping schedule and orders blood and urine tests as long as everything checks out caruso asks vega to return for another exam in a year vega says he definitely will. what if mr vega had had a lump or bump that wasnt right caruso asks what if when he had his shirt off mr vega said oh yeah i forgot to mention this spot on my chest and it ended up being a melanoma we discovered early. but the evidence is not on their side i would argue that we should move forward with the elimination of the annual physical says dr ateev mehrotra a primary care physician and a professor of health policy at harvard medical school

@highlight
percent of americans say it is important to get an annual headtotoe physical exam

@highlight
randomized trials going back to the just dont support that belief
