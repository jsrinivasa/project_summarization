police in india are putting aside their batons in favor of an overhead solution to angry and unruly crowds pepperspraying drones. protests are a common occurrence in india a country with a population of billion lucknow the capital of the northern state of uttar pradesh also used drone cameras to monitor crowds at a recent religious festival. the drones have been tested in controlled conditions he said they have been very successful and will be used by the lucknow police whenever there are violent protests or mob attacks. the citys force has bought four drones and is in the process of purchasing one more

@highlight
police in lucknow northern india have bought four drones to help control crowds

@highlight
the unmanned aerial vehicles are being fitted with cameras and pepper spray to subdue angry protesters

@highlight
some indians have questioned why police are resorting to authoritarian and forceful methods
