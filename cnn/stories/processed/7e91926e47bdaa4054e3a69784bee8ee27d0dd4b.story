i remember traveling one day in the local train in mumbai with my mother my younger sister and brother. this isnt all simply anecdotal a study by we the people found that of women in mumbai had been street harassed primarily in crowded areas like trains and railway platforms. i never told anyone about that day until recently. the reality is that sexual harassment in india is pervasive in all aspects of life it hits you in the face every day when you walk down the street take local transport go about your daily routine or at the workplace

@highlight
women in india are street harassed primarily in crowded areas like trains and railway platforms

@highlight
elsa marie dsilva its time we speak up we cannot accept harassment as part of our daily routine
