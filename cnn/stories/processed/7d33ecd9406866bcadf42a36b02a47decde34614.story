an egyptian court sentenced the leader of the muslim brotherhood mohamed badie on saturday to death by hanging along with members of his group. the egyptian news outlet al ahram reported that badie had been sentenced to death twice before but an appeals court overturned one verdict and egypts grand mufti disapproved of the other. the presiding judge for badie soltan and the other defendants was mohamed nagy shehata who is known for his harsh verdicts shehata has sentenced more than people to death and was the original judge in a highprofile case case involving al jazeera journalists. badie had been sentenced to death before on a conviction related to a deadly attack on a police station he has also been sentenced to life in prison for inciting violence during unrest

@highlight
the death sentences will be appealed

@highlight
mohamed soltan a usegyptian activist on a hunger strike is sentenced to life in prison

@highlight
letter from soltans sister your face with its beautiful smile now looks permanently in pain
