thousands of syrian and palestinian refugees trapped in the yarmouk refugee camp have suffered what can only be described as untold indignities but while the story is in itself tragic it is the individual lives at the heart of the camp that make the imperative for humanitarian action so compelling. when isis islamic state of iraq and syria entered the camp and tensions heightened his mother nadia fled in search of safety her only thoughts were to save the life of her newborn son yet she has not lost hope in the possibility of a dignified future she hopes that if and when life returns to normal she will be able to live once more with her husband and son in the family home in yarmouk. jihad yaqoub the youngest palestinian refugee to flee yarmouk was born on march his mother said fatima never imagined bringing a child into this world could be so tough. i encountered two such individuals on my mission to damascus jihad and mohammad tiny vulnerable infants who were taken from yarmouk in recent days a place that was described last week by the un secretarygeneral ban kimoon as the deepest circle of hell the fact that they are alive truly make them miracle children

@highlight
yarmouk is a refugee camp near damascus in warravaged syria

@highlight
pierre krähenbühl of the united nations individual lives underscore need for humanitarian action
