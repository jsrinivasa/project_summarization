would you build a house with water. we now mix the water with natural solvents that do not cause pollution but lower the freezing temperature to an acceptable level this practically means that even if the reheating technology fails the water cannot freeze. hungarian architect matyas gutai believes that water is the perfect material for keeping a house at a comfortable temperature. panels some of steel and some of glass make up the structure of the house and a sheet of water is trapped between the inner layers which equalizes the temperature across the building

@highlight
matyas gutai is pioneering the use of water as an insulator for sustainable architecture

@highlight
reacting to its surroundings the water keeps the house at a comfortable temperature
