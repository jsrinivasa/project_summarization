on the stage of a tv studio in phnom penh cambodianamerican ly sivhong is telling an engrossed audience a tragic but familiar story. on april years ago today life as ly knew it was shattered when her hometown the cambodian capital of phnom penh fell to the genocidal khmer rouge regime of the communist party of kampuchea. ly then was separated from her mother and two of her sisters who along with virtually the entire population of phnom penh about two million people were sent on a forced march into the countryside to work. on the stage of the its not a dream studio as ly hugs her longlost sister footage of an even older woman is projected on a screen

@highlight
phnom penh the cambodian capital fell to the genocidal khmer rouge years ago today

@highlight
at least million people were killed in the subsequent four years before the regime was driven out

@highlight
decades on the country is still struggling to gain justice for victims and heal from the genocide
