a third person has been arrested in the case of an alleged spring break gang rape that was videotaped on a crowded stretch of panama city beach the bay county florida sheriffs office said wednesday. about spring break revelers come to the beach community every year this year the bay county sheriffs office made more than arrests for various crimes about triple the number of arrests made in the same period last year. investigators discovered that kennedy had family in dekalb county georgia and reached out to the sheriffs office there deputies in dekalb in the atlanta area tracked down kennedy and arrested him on a charge of sexual assault by multiple perpetrators the bay county sheriffs office said. after interviewing witnesses bay county investigators determined the alleged rape took place between march and march behind spinnaker beach club a popular bar and dance club for spring breakers

@highlight
third suspect identified as george davon kennedy of murfreesboro tennessee

@highlight
young woman was raped on a crowded beach in broad daylight police say

@highlight
some bystanders saw what was happening and didnt stop it authorities say
