a top al qaeda in the arabian peninsula leader who a few years ago was in a us detention facility was among five killed in an airstrike in yemen the terror group said showing the organization is vulnerable even as yemen appears close to civil war. meanwhile yemen has been awash in violence and chaos which in some ways has been good for groups such as aqap a prison break earlier this month freed prisoners including some senior aqap figures according to a senior defense ministry official and the united states pulled the last of its special operations forces out of yemen last month which some say makes things easier for aqap. he was eventually released as part of saudi arabias program for rehabilitating jihadist terrorists a program that us sen jeff sessions ralabama characterized as a failure in december sessions listed alrubaish among those on the virtual whos who of al qaeda terrorists on the arabian peninsula who have either graduated or escaped from the program en route to terrorist acts. the united states has been active in yemen working closely with governments there to go after aqap leaders like alrubaish while it was not immediately clear how he died drone strikes have killed many other members of the terrorist group

@highlight
aqap says a crusader airstrike killed ibrahim alrubaish

@highlight
alrubaish was once detained by the united states in guantanamo
