in a broad bipartisan vote the senate on tuesday gave final approval to a medicare reform bill that includes a permanent solution to the doc fix a method the government has used to ensure payments to medicare providers will keep up with inflation. the issue of payments to medicare providers has been a thorny issue for years senate finance committee chairman orrin hatch of utah called passage of the bill a major major accomplishment. unless the senate passes the housepassed doc fix significant cuts to physicians payments will begin tomorrow boehner said we urge the senate to approve the housepassed bill without delay. the house approved the same bill overwhelmingly more than two weeks ago and president barack obama is expected to sign it senate passage came just hours before cuts to physicians would have taken place since the last temporary doc fix had already expired

@highlight
bill passes to passage came just hours before cuts to physicians would have taken place

@highlight
gop presidential candidates ted cruz and marco rubio vote against the bill candidate rand paul votes for it
