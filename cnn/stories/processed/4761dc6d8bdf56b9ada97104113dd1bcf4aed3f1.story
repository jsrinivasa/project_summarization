a north pacific gray whale has earned a spot in the record books after completing the longest migration of a mammal ever recorded. varvara was thought to be an endangered western whale but her ability to navigate across open water over tremendously long distances is impressive he said in the release which could mean that some western gray whales are actually eastern grays with only western gray whales believed to be in existence that number might be even lower. varvaras journey surpassed a record listed on the guinness worlds records website it said the previous record was set by a humpback whale that swam a mere round trip between the warm breeding waters near the equator and the colder foodrich waters of the arctic and antarctic regions. during her journey varvara visited three major breeding areas for eastern gray whales which was a surprise to mate who is also the director of the marine mammal institute at oregon state university

@highlight
the whale varvara swam a round trip from russia to mexico nearly miles

@highlight
the previous record was set by a humpback whale that migrated more than miles
