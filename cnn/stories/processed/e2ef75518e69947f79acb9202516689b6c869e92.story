new york state authorities have issued a health alert following a dramatic spike in hospital visits for synthetic marijuanarelated emergencies. since the exact compounds contained in synthetic cannabinoid products change so frequently its often impossible for users to know exactly what they are putting in their body acting new york state health commissioner dr howard zucker said. symptoms after use have a wide range of severity from confusion drowsiness and headaches to increased heart rate seizures and loss of consciousness according to the new york state department of health. young people may be fooled into thinking that these substances are safe because they are sold over the counter or are in colorful packaging but they are not made for human consumption new york alcohol and substance abuse service s commissioner arlene gonzalez sanchez said they are dangerous and can have significant longterm effects on the brain

@highlight
new york reports hospitalizations related to synthetic marijuana

@highlight
gov andrew cuomo issued a health alert
