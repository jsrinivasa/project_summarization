the officer charged with murder in the shooting death of an unarmed black man in south carolina has been fired as anger continues to build around his case. scotts shooting stirred memories of the michael brown case in ferguson missouri where an unarmed black teenager was killed by a white police officer a grand jury declined to indict the officer in that case but not everyone agreed that scotts case is like browns or that race was a factor. people are upset people are pointing out how wrong the officer was for gunning down mr scott south carolina state rep justin bamberg said as he stood alongside anthony scott on wednesday. the north charleston police department was not legally obligated to but chose to hand the case over to the south carolina law enforcement division according to a news release from scarlett a wilson the ninth judicial circuit solicitor

@highlight
witness who took the video says mr scott didnt deserve this

@highlight
north charleston police officer michael slager is fired

@highlight
the city orders an additional body cameras
