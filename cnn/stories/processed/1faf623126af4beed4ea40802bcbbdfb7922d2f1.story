are you smarter than a really smart singapore high school student. see if you can figure out cheryls birthday the singapore logic problem thats got the internet twisted into knots some are even saying its the math equivalent of the what color is the dress debate. the puzzling problem went viral after singapore television host kenneth kong posted it to facebook. cheryls birthday challenge was meant to test the better highschool students competing in the singapore and asian schools math olympiad held april

@highlight
a logic question about cheryls birthday goes viral

@highlight
the clues give just enough information to eliminate most possibilities

@highlight
it spread after a singapore television host posted it to facebook
