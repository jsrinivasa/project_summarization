hillary clinton is finally announcing her candidacy for the presidential election although she has watched her standing in the polls sag in recent months there is likely to be a boost in the days that follow the announcement. as she learned in bill clinton is not always easy to control when he speaks his mind as he did in dismissive comments about obamas candidacy it can often work against her the fundraising records of the clinton foundation will also raise questions about conflict of interest and ongoing stories about his personal life as was the case when monica lewinsky returned to the media a few months ago could reemerge on the campaign trail whether that is fair or not is beside the point everything is fair game on the modern campaign trail. during the next few months clinton will also have to connect with her partys base the ongoing speculation about sen elizabeth warren of massachusetts has suggested that the most active part of the democratic party is not that enthused with clintons candidacy while they will probably vote for her they are not very motivated and dont trust that she will stand for democratic values. their greatest virtue their immense skills as politicians has often come back to haunt them bill clinton was attacked as slick willie by members of both parties for the perception that he would say anything to win and hillary clinton has faced similar criticism

@highlight
julian zelizer hillary clinton has immense political and governmental experience

@highlight
he says she needs to make stronger connection to her partys base

@highlight
clinton also needs to convince voters of her authenticity zelizer says
