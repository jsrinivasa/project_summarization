its time for liberals to look local with washington gridlocked over almost everything and congressional republicans standing firm against any further expansion of domestic policy the odds of congress passing another new deal or great society are minimal it will take a lot of work by democratic voters and activists to change the numbers on capitol hill so that liberal ideas stand a chance of passing. for the time being liberals need to abandon that bias even if all the fears are warranted right now there are enough benefits to justify more local programs the most important obviously is simply practical this is the only opportunity that liberals have right now of seriously moving forward with new ideas. this is just the tip of the iceberg at the annual conference for new america in washington last week the focus was on innovation the most exciting ideas are taking hold at the local level as the atlantics james fallows explained to the audience when one moves beneath the gridlock of washington and down to the towns and communities of america it is quickly possible to see the functionality of politics where partisanship does not trump the need to solve problems. when franklin roosevelt came to office in the he looked to these programs for inspiration about what to do at the national level the same was true in the when lyndon johnson and the democratic congress put together civil rights antipoverty and urban development programs that had been implemented at the state level it was easier for federal officials to build support for their proposals when they could point to successful experiments voters could see how they could work and how some of the worst predictions of opponents had not come true

@highlight
julian zelizer washington is gridlocked and leans conservative

@highlight
but liberals can launch social programs at lower levels zelizer says

@highlight
trying programs out locally can set groundwork for washington action in coming years he says
