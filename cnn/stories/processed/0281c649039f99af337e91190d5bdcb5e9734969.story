british police investigating a spectacular heist in the heart of londons jewelry district said friday they knew a burglar alarm went off but didnt respond. southern monitoring alarm company called the metropolitan police service also known as scotland yard at am april to report that the burglar alarm had been activated at hatton garden safe deposit ltd mps said in a prepared statement. they have not released any video of the heist when asked about the video published by the daily mirror police said they could not confirm that it was footage from the hatton garden robbery and that officers have not seen that particular video. the heist would not be reported to police for two more days on tuesday morning when employees of the company arrived for work

@highlight
british tabloid releases video it says shows the robbery being carried out

@highlight
british police say they didnt respond to a burglar alarm in jewelry district

@highlight
police give no value of the amount taken in the heist in londons jewelry district
