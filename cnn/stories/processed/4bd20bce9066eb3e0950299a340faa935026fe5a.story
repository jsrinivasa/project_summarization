emy afalava is a loyal american and decorated veteran he was born in american samoa a us territory since he has been subject to american law his whole life and thinks he should be a citizen. in a puerto rican woman named isabel gonzalez sailed for new york because puerto rico was us territory she believed herself to be a us citizen but officials at ellis island labeled her an undesirable alien and prevented her from entering the mainland she sued with some reason to hope for a favorable ruling. in the gonzalez case the justices agreed unanimously that puerto ricans were not aliens and thus not subject to immigration laws but they declined to decide whether or not gonzalez was a citizen though preoccupied by fears that islanders were savages and racially unfit for citizenship they were unwilling to violate the constitution. yet afalava has been denied the right to vote because the federal government insists that he is no citizen how can it be in the century that americans born on us soil are denied the rights of citizenship

@highlight
emy afalava is a loyal american and decorated veteran hes also an american samoan

@highlight
sam erman and nathan perlrosenthal it is outrageous that he and others like him are denied citizenship
