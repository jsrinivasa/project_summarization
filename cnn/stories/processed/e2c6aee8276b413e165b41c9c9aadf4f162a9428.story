it wasnt until her that fethiye cetin discovered her armenian ancestry her grandmother years old at the time told cetin that her real name was heranus like many other survivors of heranus assimilated and kept her identity hidden many feared a repeat of the horrors they witnessed and barely escaped. the shocking discovery of her true heritage would change cetins life she tells heranus story in an evocative memoir titled my grandmother heranus and her family were among a massive stream of women and children being forcibly marched by ottoman soldiers not knowing where they were going or why they were torn away from their male relatives echoing throughout the procession were morbid whispers that the men and teenage boys had all been killed. in a crowded reception before a memorial concert in istanbul this week people rushed to greet fethiye cetin a strong softspoken woman now in her cetin is a prominent lawyer who represented armenianturkish journalist hrant dink dink was a strong proponent of reconciliation between turks and armenians who was tried for insulting turkishness he was assassinated in. heranus and her brother were scooped up onto the officers horse and taken to a garden packed with other children and fed the first warm meal they had had in days but soon reality set in and heranus began to cry and beg to see her mother

@highlight
fethiye cetin learned of her armenian heritage from her grandmother

@highlight
the grandmother survived the killings assimilated then kept her real identity hidden

@highlight
cetin others want turkey to recognize the killings as a genocide the government has refused

@highlight
in the last decade a more public dialogue on the subject has begun in turkey
