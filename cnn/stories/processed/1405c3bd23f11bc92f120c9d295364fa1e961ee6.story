an oklahoma reserve sheriffs deputy accused of fatally shooting a man he says he meant to subdue with a taser pleaded not guilty tuesday to a charge of seconddegree manslaughter. bates was working as a reserve deputy for the tulsa county sheriffs office on april when he was involved the arrest of eric harris in a weapons sting operation. bates claims he meant to stun harris with a taser after harris fled from officers but mistakenly shot harris with a gun instead. the tulsa world newspaper reported some supervisors in the tulsa county sheriffs office were told to forge bates records and were reassigned when they refused

@highlight
robert bates said he meant to subdue a suspect with a taser but accidentally shot him

@highlight
the preliminary hearing is scheduled for july

@highlight
the judge said bates was free to travel to the bahamas for a family vacation
