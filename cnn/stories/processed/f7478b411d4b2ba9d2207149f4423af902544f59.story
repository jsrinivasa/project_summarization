have you ever found the creative inspiration you were seeking at the most unexpected time or thought you were having that longawaited problemsolving epiphany just as you nodded off to sleep. we all know how important sleep is to our cognitive thinking the sleepdeprived mind is more likely to fixate on small matters an absolute killer to creativity ample sleep also helps foster the discovery of hidden connections between ideas. perhaps the single most important thing to remember is that your mental state can change it takes a while to sink into an insightful mindset try and schedule uninterrupted blocks of time for relaxed freewheeling creative thought turn off your phone get rid of the clock let abstract ideas and vague impressions flow where they will. static surroundings encourage static thinking dont be predictable you should sometimes change everyday routines such as where you go for coffee or your route to work rearrange your furniture and decor from time to time at home and in your workplace hold meetings in a variety of places

@highlight
two neuroscientists have conducted brain imaging to examine moments of clarity

@highlight
sudden insights are otherwise known as eureka or aha moments

@highlight
we can increase our chance of these insights with a variety of daily changes
