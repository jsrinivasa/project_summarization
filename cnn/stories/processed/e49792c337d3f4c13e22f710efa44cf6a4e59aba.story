the parents of the youngest victim of the boston marathon bombings are making an emotional passionate plea to take the death penalty off the table for the man convicted in the case. we are in favor of and would support the department of justice in taking the death penalty off the table in exchange for the defendant spending the rest of his life in prison without any possibility of release and waiving all of his rights to appeal they wrote. in a frontpage opinion piece in the boston globe bill and denise richard wrote about the toll taken on their family after the death of their son martin. our family has grieved buried our young son battled injuries and endured numerous surgeries all while trying to rebuild lives that will never be the same they said in the globe column titled to end the anguish drop the death penalty

@highlight
parents of martin richard argue against death penalty for dzhokhar tsarnaev

@highlight
the boy was youngest of victims in the boston marathon bombings

@highlight
sentencing phase for tsarnaev begins next week
