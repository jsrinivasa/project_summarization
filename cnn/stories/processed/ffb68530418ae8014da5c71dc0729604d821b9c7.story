south americas atacama desert one of the driest places on earth resembles some of the faraway planets monitored by giant telescopes there. taking along his mobile lighting studio figueroa photographed a series of religious festivals that take place every july in the atacama in his dancers of the deserts series figueroa chronicles these festivals which attract about people to some otherwise quiet mining towns in chile. i have always wanted to see the desert even though i am not very religious ive always been curious about their traditions said figueroa who is from the chilean capital of santiago. photographing adobe walls desert landscapes and the ubiquitous camping sites where pilgrims come to gather figueroa said each character is perched in his or her own context

@highlight
photographer andres figueroa spent a week in one of the driest places on earth

@highlight
he took portraits of chileans who dress up in costume for popular religious festivals
