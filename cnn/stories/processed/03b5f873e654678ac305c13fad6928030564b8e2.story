james holmes made his introduction to the world in a colorado cinema filled with spectators watching a midnight showing of the new batman movie the dark knight rises in june. holmes psychiatrist contacted several members of a behavioral evaluation and threat assessment team to say holmes could be a danger to others the station reported at issue was whether to order holmes held for hours to be evaluated by mental health professionals the station reported. holmes appeared like a comic book character he resembled the joker with redorange hair similar to the late actor heath ledgers portrayal of the villain in an earlier batman movie authorities said. raised in central coastal california and in san diego james eagan holmes is the son of a mathematician father noted for his work at the fico firm that provides credit scores and a registered nurse mother according to the ut san diego newspaper holmes also has a sister chris a musician whos five years younger the newspaper said

@highlight
opening statements are scheduled monday in the trial of james holmes

@highlight
jury selection took three months

@highlight
holmes faces counts in the movie theater massacre that killed people
