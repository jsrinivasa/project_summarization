the complete skeleton of a camel thought to to have been used by ottoman troops besieging vienna in the century has been found beneath a cellar in austria. it is the first complete camel skeleton found in central europe and central european territories under the control of the ottoman empire apart from the complete skeleton of a dromedary recovered from the sediments of the theodosius harbor on the european part of istanbul. the dismemberment of the carcasses certainly is a reason for the scarce preservation of camel finds in general and is indicated by bones with butchering marks in particular however the citizens buried this camel in a typical postmortal position and together with rubbish in the remnants of a cellar that was leveled researchers said. the austrians unfamiliarity with such a beast may have led to the skeletons preservation as opposed to the ottoman troops who would likely have eaten the camels flesh the residents of tulln apparently buried the camel whole

@highlight
archaeologists have found the skeleton of a camel below a cellar in an austrian village

@highlight
they believe the camel was from the century osmanichabsburg war

@highlight
ottoman troops used camels as troops during the conflict
