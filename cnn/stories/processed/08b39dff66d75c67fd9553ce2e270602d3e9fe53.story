al qaeda fighters attacked a prison in the coastal yemeni city of al mukallah early thursday freeing at least prisoners a third of whom have al qaeda links a senior defense ministry official has told cnn. government troops arrived early thursday and clashed with the al qaeda fighters and most of the militants fled the officials said. khaled batarfi a senior al qaeda figure was among the escapees officials said. but little is simple in the middle east and while the conflict between the houthis and forces loyal to hadi rages in the western part of the country where it has caused hundreds of civilian deaths al qaeda in the arabian peninsula or aqap controls parts of eastern yemen

@highlight
al qaeda fighters attack a prison and other government buildings freeing many prisoners

@highlight
government troops clash with the fighters most of whom flee

@highlight
yemen is descending into chaos as a shiasunni conflict draws in regional rivals saudi arabia and iran
