kenya froze dozens of accounts linked to suspected terror supporters after militants massacred people last week at a university in garissa. the terror group has intensified attacks in kenya since the country sent troops to somalia four years ago to help battle the militants. kenyan authorities had prior intelligence that a university in garissa could be attacked yet the countrys rapid response team was stuck in nairobi for hours after the massacre awaiting transport a police source said monday. alshabaab is based in somalia and its violence has spread to kenya before in militants attacked nairobis upscale westgate mall leaving people dead

@highlight
the attack at a garissa university last week killed people mostly students

@highlight
the government is tracking the finances of people suspected of ties to alshabaab
