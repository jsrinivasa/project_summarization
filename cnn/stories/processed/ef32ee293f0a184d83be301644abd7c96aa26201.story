canadian actor jonathan crombie who costarred in the anne of green gables tv movies died this week at age. based on canadian author lucy maud montgomerys childrens books anne of green gables debuted in canada on cbc tv in and became a cultural touchstone the plot focused on the adventures of fiery orphan anne shirley played by megan follows who is sent to live on a farm in prince edward island. crombie played gilbert blythe who evolves over time from annes pigtailtugging tormentor to friend to husband follows and crombie reprised the roles in the sequels anne of avonlea and anne of green gables the continuing story. crombie died wednesday from complications of a brain hemorrhage anne of green gables producer kevin sullivan said

@highlight
jonathan crombie is best known for playing gilbert blythe in anne of green gables

@highlight
book movies about girl sent to live on canadian farm
