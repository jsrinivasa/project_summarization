britains prince harry arrived monday in australia where hell be spending four weeks with the countrys military. the younger son of prince charles and princess diana harry is known in the british army as captain harry wales from his official title and name his royal highness prince henry of wales. prince harry has trained and served alongside australian armed forces on operational tours to afghanistan he has met them during the invictus games and even trekked to the south pole with a couple of australian soldiers the spokesman said ahead of the visit. wounded warriors are a special interest for prince harry he helped spearhead and continues to champion the invictus games a competition for former military personnel who have been wounded in the line of duty

@highlight
prince harry pays his respects at the tomb of the unknown australian soldier

@highlight
he is starting a fourweek attachment with the australian military
