russian president vladimir putin shrugged off repeated questions about the impact of western sanctions on his nation during a nationally broadcast annual qa session. sanctions are sanctions he said as far as sanctions are concerned theyre about the need to constrain our development not just about ukraine and crimea. last year there was a surprise appearance by nsa whistleblower edward snowden who was granted asylum in russia he addressed putin by video link quizzing putin about moscows own surveillance practices. putin predicted the sanctions would not end soon

@highlight
putin has spent hours fielding questions from the general public on live television

@highlight
sanctions and russias deep economic crisis are a major theme
