ten people have been sentenced to life in prison for their roles in the attack on nobel peace prizewinning activist malala yousafzai a judge announced thursday. she not only survived that attack but went on to become an even more vocal international activist in fact her efforts helped earn her the nobel peace prize which she shared with indias kailash satyarthi last year. that was nearly two years after the then yousafzai who was despised by taliban militants for her outspoken support of girls right to an education was shot as she was traveling home on a school bus. malala at un the taliban failed to silence us

@highlight
the sentences came after a trial in pakistan a judge says

@highlight
malala yousafzai is an outspoken advocate for the education of girls

@highlight
she was attacked in pakistan in
