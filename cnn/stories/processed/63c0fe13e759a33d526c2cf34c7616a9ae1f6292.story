the streets of baltimore are calm once again for the second night in a row protesters peacefully dispersed wednesday night after a pm curfew meant to prevent riots that tore up the city two days earlier. the relative calm that took over baltimore can be credited in part to peaceful protesters who formed human barricades between hottempered demonstrators and police day and night. some national guardsmen and more than police officers from across maryland and neighboring states were assigned to the streets of baltimore on tuesday night maryland gov larry hogan said. more than people were arrested in new york during a nyc rise up shut it down with baltimore rally wednesday night new york police said

@highlight
protests spread to new york and denver with more scheduled for other cities

@highlight
more than people arrested in baltimore this week are released
