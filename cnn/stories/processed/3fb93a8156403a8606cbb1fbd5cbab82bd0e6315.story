the littleknown star of this weeks no car chase movie furious isnt a car its an airplane. inside the planes cockpit lockheed test pilot steve knoblock said hes done a lot of airdrop testing while flying weve dropped some crazy things out knoblock said air dropping a car out of an airplane is easy whatever we can fit inside the cargo hold we can drop. we have to give time for the car to clear the airplane let the parachutes deploy and then actually open he said one tricky decision involves how high the plane drops its cargo. im told that the version of this aircraft is now on the assembly line inside this building deal said it transports it becomes a war airplane as necessary and it is recognized as the true workhorse

@highlight
the type of plane used for a jawdropping stunt in furious is years old

@highlight
lockheeds hercules is the longest continuously produced military plane in history

@highlight
factory in georgia celebrates the flight of the first production model
