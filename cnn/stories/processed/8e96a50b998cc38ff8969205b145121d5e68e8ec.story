the tulsa county deputy who shot and killed a man instead of using his taser now faces a manslaughter charge. in a written statement tulsa county district attorney stephen a kunzweiler said bates is charged with seconddegree manslaughter involving culpable negligence its a felony charge that could land the volunteer deputy in prison for up to four years if hes found guilty. we believe the video itself proves that it was an accident of misfortune that occurred while deputy bates was fulfilling his duties as a reserve deputy wood said he is not guilty of seconddegree manslaughter. wood said his client who had donated cars and video equipment to the sheriffs office had undergone all the required training and had participated in more than operations with the task force he was working with the day he shot harris but hed never been the main deputy in charge of arresting a suspect wood said but was thrust into the situation because harris ran from officers during the arrest

@highlight
harris family attorney says volunteer deputy was a donor who paid to play a cop

@highlight
an attorney representing reserve deputy robert bates says it was an excusable homicide

@highlight
eric harris brother says the shooting was simply evil accuses investigators of trying to cover it up
