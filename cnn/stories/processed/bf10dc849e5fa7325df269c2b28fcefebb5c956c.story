tobacco companies including philip morris and rj reynolds filed suit this week against the food and drug administration alleging that the fda is violating the companies free speech rights. the suit filed in us district court in washington argues that those guidelines go too far and are too vague they violate the first amendment because they preemptively restrict free speech and exceed the scope of the tobacco control act the companies claim. the plaintiffs also include us smokeless tobacco co american snuff co santa fe natural tobacco co and lorillard tobacco co. in march the fda issued guidance that if significant changes are made to a products label like color or a logo the product requires new approval from the administration this holds true even if the product was previously approved

@highlight
companies including philip morris and rj reynolds in suit alleging violation of free speech

@highlight
in march the fda issued guidance about changes to tobacco product labels

@highlight
if significant changes are made to a products label like color or a logo the product requires new approval
