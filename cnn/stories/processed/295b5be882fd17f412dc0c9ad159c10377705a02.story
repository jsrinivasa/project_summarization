times running out for britons to decide who theyll back as polling booths prepare to open across the country in an election that could change the political landscape. the election could result in the handing of power from david camerons conservative government to the labour party led by ed miliband or a frenzy of wrangling from leaders as they attempt to forge alliances with smaller parties. visit hampstead heath a beautiful north london green space that lies partly within the hampstead and kilburn constituency likely to be one of the most hotly contested of the election south hill park gardens london. the centrist liberal democrats for instance had million votes of the total at the last election but won just seats the conservatives claimed with just million votes

@highlight
everything you need to know about how the uk election really works

@highlight
things like the candidates the issues whos likely to win the importance of bacon sandwiches

@highlight
plus if the queen isnt in charge what does she do
