if i had to describe the usiranian relationship in one word it would be overmatched. the usiranian relationship is not symmetrical its not as if we both are doing terrible things and are looking for a fair and equitable compromise to stop our respective bad behaviors iran is about to try a us citizen and washington post reporter and we have made a judgment that even while we protest we will keep the nuclear issue separated not just from this case but from irans serial abuse of human rights including the behavior of its shia militias in iraq i can only hope there is a carefully orchestrated behindthescenes plan to have iran release jason rezaian. were playing checkers on the middle east game board and tehrans playing threedimensional chess iran has no problem reconciling its bad and contradictory behavior while we twist ourselves into knots over our tough choices all the while convincing ourselves that americas policy on the nuclear issue is on the right track. iran isnt feet tall in this region but by making the nuclear issue the beall and endall that is supposed to reduce irans power the united states is only making tehran taller consider the following

@highlight
miller while the us entangles itself in the nuclear negotiations iran is gaining a freer hand to assert its regional influence

@highlight
he says united states is being outfoxed not outgunned
