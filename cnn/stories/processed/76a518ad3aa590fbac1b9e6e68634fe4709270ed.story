a frenchlanguage global television network regained control of one of its channels thursday after a cyberattack a day earlier crippled its broadcasts and social media accounts. television network was gradually regaining control of its channels and social media outlets after suffering what the networks director called an extremely powerful cyberattack. in addition to its channels lost control of its social media outlets and its websites director yves bigot said in a video message posted later on facebook. on a mobile site which was still active the network said it was hacked by an islamist group isis logos and markings appeared on social media accounts

@highlight
network regains control of facebook page and one of its channels

@highlight
isis logos displayed but no claim of responsibility made by any group

@highlight
network reaches million homes worldwide
