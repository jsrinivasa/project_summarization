their eyes reflect childhoods marked by tragedy their faces show wrinkles made deeper by pain and the passage of time. fearing for their safety they couldnt tell their stories publicly until the fall of the soviet union now that theyre in their time is running out for them to document their struggles. tomasz lazar spent hours photographing and interviewing adults who were ripped from their homes as children in the and forced to live thousands of miles away in siberia. for me those faces are like maps lazar said the more you look at them the more you are discovering

@highlight
the soviets invaded poland in world war ii and deported hundreds of thousands of people

@highlight
tomasz lazar photographed some of these poles and listened to their stories
