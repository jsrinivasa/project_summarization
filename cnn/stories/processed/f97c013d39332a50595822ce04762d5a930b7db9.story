by the time kim kardashian set out to break the internet in november last year a photo by conor mcdonnell had already got there with a little help from kims music superstar husband kanye west. mcdonnell is the selftaught photography star behind instagrams most liked photo showing the couples embrace at their may wedding which has earned more than milllion likes to date. fresh from accompanying calvin harris on tour in south america mcdonnell shares his five top tips for anyone who wants to grow their instagram fan base and take great photos on the go. ill upload a photo to an app called snapseed edit the brightness and contrast in that a little bit export it and then open in another app called vsco cam which is my favorite app on my whole phone

@highlight
conor mcdonnell is the young photographer behind instagrams most liked photo

@highlight
has snapped the likes of calvin harris drake and justin bieber
