at least three people were shot in separate incidents in ferguson missouri on late tuesday and early wednesday as hundreds of demonstrators gathered in support of protests in baltimore a city spokesman said. two people were shot in the neck and another was shot in the leg spokesman jeff small said there is a suspect in custody in the latter case a male from st louis county. the renewed tensions in ferguson follow rioting in baltimore after the death of freddie gray the was arrested by police on april and died one week later from a fatal spinal cord injury. demonstrators set a portable toilet on fire one person can be seen squirting what appears to be lighter fluid on it

@highlight
three people shot one man in custody city spokesman says

@highlight
hundreds of demonstrators gathered in support of protests in baltimore

@highlight
police not sure if shootings are related to protests spokesman says
