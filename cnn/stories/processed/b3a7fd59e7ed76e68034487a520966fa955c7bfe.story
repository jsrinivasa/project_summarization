the jailing of four blackwater security guards eight years after they killed iraqi civilians in a shooting in baghdad is a positive step for justice but is also not enough. the civilians killed by the blackwater guards like the abu ghraib prisoners were both sunni and shiite but the repackaging of history by isis in which the saddam hussein regime is reinvented as a sunni regime that tried to stand up to the united states and its shiite allies glosses over those nuances. the united states was also sloppy in relying on private security firms like blackwater without implementing rigorous measures to regulate their behavior it also turned a blind eye to the way its own troops were treating iraqis all those factors contributed to a rising sense of injustice that is now being conveniently packaged by isis to push its own version of iraqi history. to balance out this shiite involvement the us and iraqi governments are counting on the establishment of a crosssectarian iraqi national guard and hoping to resurrect the awakening to reengage and unify the sunnis under a nationalist umbrella

@highlight
isis is using past western transgressions in iraq to justify its brutality

@highlight
lack of accountability following invasion paved way for abuse and for sectarian tensions
