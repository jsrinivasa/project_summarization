the boat that sank in the mediterranean over the weekend with hundreds of migrants on board may have capsized after being touched or swamped by a cargo ship that came to its aid a un official said. the account she offered differs from that provided by italian authorities on sunday they said that as the cargo ship king jacob approached late saturday migrants on the smaller boat moved to one side hoping to be saved and caused the vessel to capsize. mark clark a communications executive representing osm maritime group the company that manages the king jacob denied that the cargo ship caused the migrant boat to capsize he said he also believed that people on the migrant boat rushed to one side causing many to fall off. the likely toll makes the sinking the deadliest known disaster involving migrants crossing the mediterranean from north africa

@highlight
shipping company representative denies cargo ship caused the capsizing

@highlight
that a cargo ship may have touched the migrant boat

@highlight
italian authorities have arrested two survivors on suspicion of human trafficking
