the questions turks asked on tuesday were tinged with fear. she was one of millions of turks left confused and concerned by the worst power outage to grip the country in more than a decade. meanwhile mysterious blackouts are a sore spot for some turks after a surreal incident on election night allegedly involving a feline saboteur that is a cat that allegedly wandered into a power transformer. the ruling justice and development party ended up winning by a comfortable margin but few turks were reassured by the energy ministers explanation that the voting day blackouts were caused by a cat getting lost

@highlight
this week turkey was gripped by a massive power outage and a deadly hostage crisis

@highlight
reactions reveal contemporary turkey is tense and confused after years of political crises

@highlight
censorship has pushed critics to fringes in country cited as democratic model for mideast
