when melissa atkins wardy author of redefining girly and a passionate advocate for fighting gender stereotypes heard from a frustrated mom on facebook she knew she needed to do something so she shared the moms story. wardy said theres no doubt that companies are in business to protect their bottom line and that will always be a motivator but she said these backtoback incidents show what can happen when parents make it clear they wont buy products from companies that sell gender stereotypes. first the rest of the story about springs creative and the big hero fabric after sharing veronicas story with her readers and her twitter followers wardy encouraged people to give springs creative a piece of their mind she also invited them to tweet their own stories with the hashtag includethegirls. theres no question that raising awareness about stereotypes helps said wardy

@highlight
a mom of two got upset when big hero fabric didnt include the two female characters

@highlight
another mom called attention to gender stereotypes on a toms web page

@highlight
in both cases the companies responded quickly to answer parents concerns
