the arrest and death of freddie gray in baltimore has sparked protests and accusations of police brutality but its unclear how gray who was arrested on a weapons charge april suffered a severe spinal cord injury that led to his death seven days later. what we know gray was arrested on a weapons charge in a highcrime area of baltimore known for drugs he gave up without the use of force baltimore deputy police commissioner jerry rodriguez said last week. the demonstrators are pushing to get answers about grays death and for justice as they define it similar protests were held in ferguson missouri following michael browns death and in new york after the death of eric garner other small protests have sprung up in other cities in the past week. what we dont know its unknown what caused the spinal cord injury that led to his death a week after the arrest and its also unknown what if anything happened inside the van

@highlight
freddie gray was arrested on a weapons charge april he was dead seven days later

@highlight
he was put in a police van after his arrest its unclear what happened inside the van

@highlight
gray has a criminal history but its not known if that had anything to do with his arrest or death
