a grand jury in dallas county texas has decided not to indict two officers in the fatal shooting of jason harrison a schizophrenic man whose mother had called police for help getting him to the hospital. video from one officers body camera fades to black as harrisons mother wails oh they killed my son oh they killed my son the officers continue to tell harrison to drop the weapon. within five seconds of that first command the schizophrenic man is shot five times including twice in the back as he crashes headlong into the homes garage door just a few feet from his mother. the grand jurys decision not to indict means officers john rogers and andrew hutchins wont face criminal prosecution in the case but the officers are still facing a wrongful death lawsuit from harrisons family

@highlight
police in dallas shot and killed jason harrison last year

@highlight
a grand jury has decided not to indict the officers

@highlight
the officers are still facing a civil lawsuit filed by harrisons family
