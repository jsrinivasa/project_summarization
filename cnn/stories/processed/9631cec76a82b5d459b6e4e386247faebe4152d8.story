it didnt look like much but wed been told it was typical of the kind of craft ferrying the route between djibouti and aden. wed been given a secure route charted for us by the djiboutian coast guard it meant hugging the coastline trying to stay as much as possible out of the deeper water in the middle of the gulf of aden where the worlds navies seemed to be squaring off. we were told wed be at the port in aden by the next morning but as the hours ticked by it soon became apparent that that was wildly optimistic. because wed brought refugees back with us we were asked by the djibouti coast guard to dock in the countrys north in obok so they could be given the proper status while they wait to see what happens back home

@highlight
s nima elbagir describes the boat journey from djbouti to aden

@highlight
vessel returned with refugees desperate to flee fighting in yemen
