chris copeland of the indiana pacers was stabbed after leaving a trendy new york nightclub early wednesday and two atlanta hawks who had just finished a home game hours before the incident were among those arrested according to police and cnn affiliates. we are aware that chris copeland was injured early this morning in new york city we are still gathering information and will update when we know more our thoughts are with chris and those injured larry bird the pacers president of basketball operations said in a statement. the pair apparently had only recently arrived in new york prior to their arrests as both were on the court for the hawks win over the phoenix suns in atlanta on tuesday night antic played minutes and sefolosha played the game ended around pm. the hawks are preparing for a historic playoff run after clinching the no seed in the nbas eastern conference tuesdays win over the suns marked a franchisebest wins in a season for the club the pacers sit in the conferences spot but are only one game out of playoff contention

@highlight
hawks say neither thabo sefolosha nor pero antic will play wednesday against brooklyn

@highlight
chris copeland left bloody trail of handprints as he returned to club seeking help club says

@highlight
suspect in custody police say adding they will release his name once charges are filed
