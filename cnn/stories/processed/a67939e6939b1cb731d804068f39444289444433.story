wednesdays game between the baltimore orioles and chicago white sox will be closed to the public the orioles announced tuesday. the orioles said tickets from mondays postponed game can be used for admittance on may fans with tickets for tuesdays game must exchange them for tickets for any remaining home game including may. the office of new commissioner rob manfred said the league and orioles will keep an eye on the situation in baltimore. in riots in detroit prompted baseball officials to move games between the tigers and the orioles to baltimore opening day of was postponed for two days after the killing of the rev martin luther king jr

@highlight
white sox hall of famer tweets the closeddoor game also should be postponed

@highlight
baltimore unrest leads to postponement of two baseball games

@highlight
third game in series will be first ever played without spectators
