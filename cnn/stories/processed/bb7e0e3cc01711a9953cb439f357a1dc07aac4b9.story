the third blood moon in a fourpart series was the shortest eclipse of the bunch but still a sweet treat for early risers in north america. the moon slipped fully into earths shadow at am pacific time am et saturday starting a total lunar eclipse for nearly five minutes what nasa says will be the shortest such eclipse of the century. the celestial body took on a burntorange tint in the minutes before during and after the total eclipse giving the moon the appearance that earns total eclipses the blood moon nickname. watchers in the eastern half of north america caught only a partial eclipse and in some places an orange one before the moon set below the horizon

@highlight
the total eclipse lasted minutes and seconds

@highlight
people west of the mississippi river had the best view in the us

@highlight
parts of south america india china and russia were able to see the eclipse
