laying down tracks for their debut album in the recording studio in los angeles iman hashi and her sister siham could not be further from their hometown of mogadishu the sisters were born in the somali capital but were forced to flee after war broke out in. s african voices caught up with the sister act known collectively as faarrow combining the translation of their names into english iman means faith and siham means arrow to talk about music aspirations and somalia. siham were actually in the mixing process right now we still have a few tracks to finish up but the majority of the album is pretty much done we want to turn it in as soon as possible so they can put together a rollout plan and get ready for the first single to drop. siham the entire album was pretty much written and produced by me my sister and elijah and when we signed we already had a lot of those songs already done warner brothers records is really great in that way that they already loved what we were doing and let us do our own thing

@highlight
somali sisters iman and siham hashi make up faarrow

@highlight
a fusion of hiphop world pop and afrobeats they are currently finishing debut album
