the cracker or the bite of ice cream brynn duncan still isnt sure which one sent her into anaphylactic shock that day her food allergies change so frequently keeping track is almost pointless. fruit vegetables milk soy nuts smoke perfume the sun you name it brynn is allergic to it but its not really about the specifics the allergens change depending on how angry her mast cells are that day she says on good days she can eat small amounts of plain meat or mashed potatoes on bad days even using her feeding tube causes her extreme pain. new day new crisis brynn quips as she tells the story as if its about her first day of college or a shopping trip gone wrong it might as well be when youre allergic to life a neardeath experience is no big deal. it was just another day with another massive allergic reaction

@highlight
brynn duncan has mast cell disease which causes her to be allergic to almost everything

@highlight
duncan has a feeding tube and is on constant doses of antihistamine
