on july excited moviegoers gathered for a midnight screening of the dark knight rises at the century aurora multiplex theater in aurora colorado. bailey no i dont forgive him he took so many peoples lives and he hurt so many other people that werent even in the theater he hurt families and friends and just everybody in aurora honestly. eighteen minutes into the show and shortly after midnight a gunman opened fire on the audience twelve people were killed and were injured. kaylan bailey attended the movie with her cousin jamison his girlfriend ashley and their daughter veronica kaylan just at the time babysat veronica often she had watched her earlier in the day when ashley who was pregnant had an appointment for an ultrasound when she heard gunfire kaylan made a heartrending call ashley jamison and veronica had all been shot veronica was the youngest to die in the theater ashley was paralyzed and later suffered a miscarriage because of the trauma she suffered jamison was shot in the head and survived

@highlight
trial for aurora theater shooting suspect begins monday

@highlight
survivors say the shooting changed their lives but doesnt define it
