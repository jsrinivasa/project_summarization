when photographer johan bavman became a father for the first time he took more than a passing wonder about how his native sweden is said to be the most generous nation on earth for parental leave. johan bavman is a freelance photographer based in malmo sweden from he worked as a staff photographer at sydsvenskan one of swedens largest newspapers. get this sweden grants a total of calendar days of parental leave with of them paid at of income with a maximum of euros a month or the remaining days are paid at a flatrate benefit of euros a day or. being home nine months they get time to think about their life the photographer said

@highlight
johan bavman photographed fathers in sweden which has generous parental leave

@highlight
swedens policies encourage fathers to take just as much leave as mothers
