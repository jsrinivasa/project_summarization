three british citizens arrested in the united arab emirates after they were found plane spotting near fujairah airport are to be released monday their lawyer nasser alhashem tells cnn the three have been in jail since february. as a hobby plane spotters view and photograph aircraft around the world. we made our defense and the judge made the decision to drop the case alhashem said no charges were filed there will be no travel ban and the men will not face deportation he said. conrad clitheroes and gary cooper were on a fiveday visit to the uae from manchester when they were arrested the third man neil munro is a british national who lives in the uae

@highlight
three british men wont be charged or deported their lawyer says

@highlight
they were arrested after plane spotting near fujairah airport and have been in jail since february
