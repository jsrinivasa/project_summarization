oklahoma gov mary fallin signed a bill on friday that would allow the state to perform executions with nitrogen gas if lethal injection is ruled unconstitutional or becomes unavailable. oklahoma executes murderers whose crimes are especially heinous fallin said i support that policy and i believe capital punishment must be performed effectively and without cruelty the bill i signed today gives the state of oklahoma another death penalty option that meets that standard. the governors office said the first alternative for execution is lethal injection followed by nitrogen gas the electric chair and the firing squad. oklahomas executions have been put on hold while the us supreme court reviews its use of lethal injections last year the state came under scrutiny when it took minutes to kill convicted killer clayton lockett

@highlight
nitrogen gas causes a quick loss of consciousness and then death from lack of oxygen oklahoma says

@highlight
the states executions are on hold while the us supreme court reviews the states use of lethal injections
