a delaware father is in stable condition and improving as his two boys remain in critical condition after they became sick perhaps from pesticide exposure federal officials say during a trip to the us virgin islands. many questions remain why an odorless pesticide of this level of toxicity could be manufactured distributed and applied in a residential area resulting in this familys injuries maron said. the us environmental protection agency said friday that the presence of a pesticide at the rented villa in st john may have caused the illnesses which were reported to the epa on march. the use of the pesticide is restricted in the united states because of its acute toxicity its not allowed to be used indoors only certified professionals are permitted to use it in certain agricultural settings

@highlight
chemical damages ozone and is being phased out though its used in strawberry fields epa says

@highlight
a delaware family becomes ill at a resort in the us virgin islands

@highlight
preliminary epa results find methyl bromide was present in the unit where they stayed
