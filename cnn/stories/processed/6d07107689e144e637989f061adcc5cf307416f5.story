the story of anthony stokes was supposed to have a happy ending instead it ended tuesday police say with the teen heart transplant recipient carjacking someone burglarizing a home shooting at an elderly woman leading police on a high speed chase and then dying after his car hit a pole. on tuesday stokes carjacked someone at a mall kicked in the door of a home in roswell georgia and fired a shot at an elderly woman who called said roswell police spokeswoman lisa holland. anthony is currently not a transplant candidate due to having a history of noncompliance which is one of our centers contraindications to listing for heart transplant it read. stokes lost control of the car hit a pedestrian and then a pole holland said the vehicle was nearly halved she said

@highlight
in anthony stokes family said a hospital refused him a heart due to his history of noncompliance

@highlight
hospital eventually gave stokes a heart on tuesday he carjacked someone burglarized a home police said

@highlight
stokes shot at an elderly woman hit a pedestrian with a stolen car and died in a police chase authorities said
