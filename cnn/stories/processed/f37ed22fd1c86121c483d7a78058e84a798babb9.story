is there anything laser cant do. but his laser technology can look way beyond the clouds there are potential applications in the biomedical field by changing the color of the laser we could identify and selectively kill cancer cells with little or no collateral damage. from cutting diamonds to preserving endangered sites all the way to building terrifying weapons and turning your eyes from brown to blue there is apparently no end to the list of applications for laser. swiss physicist jeanpierre wolf is working on yet another impressive addition to that list using focused laser beams to affect the weather

@highlight
swiss professor jeanpierre wolf is pioneering the use of lasers to affect the weather

@highlight
he suggests lasers could also be used to limit the impact of climate change
