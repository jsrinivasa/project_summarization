many jetblue customers fly the airline only once or twice per year making it hard to accumulate miles an airline spokesman told the magazine by not allowing miles to expire anymore the airline says customers will be able to eventually redeem them. think its hard to redeem your miles for an airline award ticket. remember that award tickets arent actually free the cost of miles is built into everything you buy thats earning you miles and the airlines profit from you not using your miles at all so it behooves consumers to book award travel carefully. while many us fliers redeemed miles on american airlines flights from los angeles to san francisco the cheapest average fare on that route was just over not worth the to miles needed for an award ticket consumer reports says

@highlight
southwest airlines tops consumer reports survey with the most seats available

@highlight
jetblue is at the bottom of the list but ranks high in customer satisfaction
