a video shoot in nepal for an internet comedy series took a serious turn on saturday as the earth began rumbling. the nepali pranksters were in the middle of shooting an episode for their hidden camera series when the earthquake broke out the team kept the camera rolling as they moved through the crowded streets surveying destruction to homes and historic sites and capturing scenes of heroism and chaos. the nepali pranksters videos show peoples reactions to various pranks that challenge cultural norms one video shows the pranksters walking up to strangers and taking their hands for a long awkward handshake another shows them complimenting men and women on their clothes and appearance with mixed results. the town in lalitpur district is home to nepals famed central zoo

@highlight
nepali pranksters make hidden camera videos of awkward social situations

@highlight
the threeperson team was filming as the nepal earthquake began
