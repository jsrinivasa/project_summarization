whether a patient is in the hospital for an organ transplant an appendectomy or to have a baby one complaint is common the gown. the new gown was emblematicof an attitude that was conveyed to me at the hospital that they cared about me as a whole human being not just the part they were operating on said dale milford who received a liver transplant during the time the redesign was being tested that was the subtext of that whole thing was that they were caring about me as a person and what it meant for me to be comfortable. hospital gowns have gotten a facelift after some help from fashion designers like these from patient style and the henry ford innovation institute. the cleveland clinic was an early trendsetter in it introduced new gowns after being prompted by the ceo who often heard patient complaints when he was a practicing heart surgeon that feedback led to a search for something new said adrienne boissy chief experience officer at the hospital system

@highlight
hospital gowns have gotten a facelift with help from fashion designers such as diane von furstenberg

@highlight
what patients wear needs to be comfortable yet allow health professionals access during exams

@highlight
patient satisfaction is linked to the size of medicare payments hospitals get
