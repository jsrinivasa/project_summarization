the next time you fly to see grandma in florida look down. this millionacre river of grass is not only the best form of hurricane protection it also supports the multibilliondollar fishing shrimping and crabbing industries around the florida keys what was once a slowmoving river of ginclear water became so sluggish and toxic in the that most of the life in florida bay was wiped out and america woke up. and so a century ago some american dreamers decided to drain the swamp they decided to conquer that uninhabitable frontier known as florida and long before air conditioning bug spray and social security helped seal that vision the army corps of engineers blasted and dug miles of dams and dikes ditches and pipes. but what they didnt know is that without this swamp there can be no good life in florida there can be no life

@highlight
the everglades were drained a century ago and are now being restored

@highlight
the wonder list season finale takes places in the everglades
