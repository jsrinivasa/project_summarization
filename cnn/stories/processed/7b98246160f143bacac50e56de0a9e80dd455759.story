al qaeda has fired a verbal salvo in a multifaction battle over yemen saying its offering kilograms of gold to anyone who kills or captures two prominent shia muslim opponents including the leader of the rebels who overtook yemens capital. yemenbased al qaeda in the arabian peninsula in a news release and wanted poster distributed online offers the reward for the death or capture of houthi leader abdelmalik bedrudin alhouthi and former yemeni president ali abdullah saleh. aqap is one of several factions fighting to control yemen with sunni islamic roots aqap is a bitter enemy of the houthi faction which is shia and widely believed to be supported by iran. yemen has been descending into chaos in the weeks since houthi rebels minority shiites who have long complained of being marginalized in the majority sunni country forced yemeni president abdu rabu mansour hadi from power in january

@highlight
the value of the bounty in american dollars is about

@highlight
al qaeda in the arabian peninsula wants a houthi leader and a former yemeni president killed or captured
