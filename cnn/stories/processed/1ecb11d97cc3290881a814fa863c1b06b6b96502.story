the new emojis are here on thursday apple released a new version of its mobile operating system that includes more diversity than ever when it comes to the race ethnicity and sexual orientation of its emojis those cute little images that users can insert into text messages or emails when words alone just wont cut it. its not yet clear if a person who likes to use samesex kissing emoji couples can be denied service by a person who objects on grounds of religious liberty but it would be interesting to hear what any of the gop presidential candidates might have to say about gay emojis and i would predict some conservative will claim that the kissing gay emojis will turn children gay. that the company finally did is a step in the right direction americas demographics are changing so our representations of who we are even representations as tiny as emojis should reflect this apple has evolved in showing diversity from brown people to samesex couples maybe religious liberty conservatives who discriminate will follow. even expanding the flags represented by emojis as apple has done comes at some peril apparently canada is overjoyed that finally apple has included it but armenians are not happy they were left out

@highlight
dean obeidallah apples new emoji lineup is diverse in race ethnicity and sexual orientation

@highlight
its just like america but what took apple so long obeidallah asks

@highlight
he says change may rankle or win over conservatives who discriminate against people because of their sexual orientation
