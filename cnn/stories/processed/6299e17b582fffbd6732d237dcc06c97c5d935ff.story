jamal allabani had hoped to bring his pregnant wife and back to the united states from wartorn yemen. he is believed to be the first us citizen killed in the current violence in yemen. violence quickly escalated in yemen soon afer allabani arrived in february. yemen has been rocked by violence and political turmoil for months houthi rebels minority shiites who have long complained of being marginalized in the majority sunni country forced yemeni president abdu rabu mansour hadi from power in january placing him under house arrest and taking over sanaa the countrys capital

@highlight
jamal allabani is believed to be the first american killed in current violence in yemen

@highlight
he was on his way back from mosque prayers when he was hit in the back by shrapnel his family said

@highlight
he went to yemen in february in hopes of bringing his wife and baby back to the us
