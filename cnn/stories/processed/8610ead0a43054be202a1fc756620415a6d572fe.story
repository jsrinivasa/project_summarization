recently a new york judge issued an opinion authorizing service of divorce papers on a husband completely via facebook. at first blush the idea of service by facebook seems to offend traditional notions of ensuring notification of a defendant of a case against him when it comes to serving papers however traditional doesnt necessarily mean good service by publication or nailing paper to the door of an empty apartment is hardly reliable its just service of last resort. while the older forms of alternate service were public most electronic service takes the form of email where email isnt available it is facebook private messaging which should be as private as email thats the form of service authorized by the court here so for now were not quite putting lawsuits on instagram but i wouldnt rule it out in the future. one form of alternate service is nail and mail service this means that you take a hammer and nail and nail the papers to the defendants front door the problem with that is that many defendants are nomadic by nature just because you find a house that a defendant stayed at doesnt mean hell be back there anytime soon

@highlight
a court allowed a wife to serve divorce papers via facebook

@highlight
danny cevallos why not let people be found via social media
