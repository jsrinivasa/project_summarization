justice may be blind but its easy to see that marvels daredevil is already a hit with fans. marvels daredevil netflixs latest offering is a wellscripted beautifully acted superhero saga that is surprisingly impressive said the philadelphia inquirers tirdad derakhshani. charlie cox is perfectly cast as blind attorney matt murdock whose nights are consumed with cleaning up the new york neighborhood of hells kitchen while dressed in a black ninjaesque outfit. the series stays incredibly faithful to daredevils pulp roots and does something delightfully unexpected trust its fans enough to spare us a long drawnout origin story said sadie gennis of tv guide

@highlight
marvels longawaited show daredevil began streaming early friday

@highlight
bingewatchers are already giving the series high marks
