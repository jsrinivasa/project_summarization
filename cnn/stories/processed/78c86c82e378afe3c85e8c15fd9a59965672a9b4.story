a sweatsmothered man in a widebrimmed hat kneehigh leather boots and a khaki uniform machetes his way through lush jungle foliage. the sprightly šprajc wears the weathered face of a man who has spent much of his years beneath a hot sun at excavations or hacking his way through dense jungle. as šprajc likes to say we can survive without computers but not without machetes. when a primal jungle is allowed to grow rampant for centuries it can indeed swallow entire cities

@highlight
slovenian archaeologist ivan šprajc discovers ancient mayan cities in the jungles of mexico

@highlight
his discoveries could help explain why so many mayan cities were abandoned before the arrival of the spaniards
