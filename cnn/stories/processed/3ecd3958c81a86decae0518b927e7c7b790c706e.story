five years ago rebecca francis posed for a photo while lying next to a dead giraffe. the locals showed me this beautiful old bull giraffe that was wandering all alone he had been kicked out of the herd by a younger and stronger bull he was past his breeding years and very close to death she said they asked me if i would preserve this giraffe by providing all the locals with food and other means of survival i chose to honor his life by providing others with his uses and i do not regret it for one second the locals did not waste a single part of him i am grateful to be a part of something so good. in the past three days his tweet has been retweeted almost times a number of people insulted and threatened francis in response to the giraffe photo and others featuring her. according to the giraffe conservation foundation there are about giraffes in africa a decline of in the past years

@highlight
rebecca francis photo with a giraffe was shared by ricky gervais

@highlight
francis was threatened on twitter for the picture

@highlight
francis a hunter said the giraffe was close to death and became food for locals
