tuesday is equal pay day the fictitious holiday marked by progressive womens groups as the point in the year women would have to work to make up for lost wages as a result of the socalled wage gap. the independent womens forum conducted a randomized controlled experiment on the issue of the wage gap and we found that not surprisingly the progressive message in favor of the paycheck fairness act a legislative solution to close the pay gap increased support for the bill but surprisingly was not effective at increasing support for democrats in short if the right is silent on the issue the left has the potential to win the battle but not the war. when we do control for these variables a much smaller wage gap persists of about cents some of which may be the result of gender discrimination but also is likely a function of womens choices and different behavior such as not negotiating as often as men do factors for which economists simply cant control. in the wake of hillary clintons presidential announcement the holiday has special meaning clintons election will no doubt center on women voters and the democratic womens agenda centers on pay equity and fairness in the workplace

@highlight
sabrina schaeffer tuesday is equal pay day a fictitious holiday marked by progressive women

@highlight
she says the wage gap between men and women is grossly overstated
