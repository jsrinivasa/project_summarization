im haunted by the video of officer michael slager firing eight shots at walter scott as he fled his encounter with north charleston police his back turned to the officer what i find more disturbing is how the officer cuffs the fallen scott and allows him to die facedown in the dirt while slager appears to plant an item next to his body. throughout the entire encounter with scott its clear slager had no idea someone was filming him had he known there would be video of his every move would he have drawn his weapon on a fleeing man would he have fired eight times would he have misrepresented the encounter on his police report. body cameras are expensive to deploy sure and storing the massive amounts of data that body cameras create costs even more that cost however if were talking the monetary kind may be eclipsed by the punitive damages delivered to scotts family in an inevitable civil suit against the north charleston police department most importantly we have to ask ourselves this whats the value of a human life certainly its worth the price of some mass data storage. of course not if slager had been wearing a body camera scott would probably still be alive and slager wouldnt be facing the possibility of life in prison or a possible death sentence

@highlight
mark omara video captured michael slager shooting walter scott if cop had been wearing a body camera he probably wouldnt have fired

@highlight
omara says such cameras are expensive but cheaper than wrongful death payouts and the cost of a human life

@highlight
the underlying problem is racial bias in policing until thats solved body cameras are a good interim solution
