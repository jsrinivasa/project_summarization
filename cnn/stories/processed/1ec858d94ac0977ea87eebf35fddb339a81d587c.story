for five decades bikram choudhury built an empire. civil lawsuits filed in los angeles superior court tell the story of a different bikram choudhury describing him as someone who preyed on young women who looked to him for guidance. we know for a fact that these claims allegedly occurred years and years and years ago and nobody ever came forward he said and yet after this lawyer sends out these kinds of blasts in social media asking people if theyve been a victim of bikram choudhury all of a sudden these people come forward and all have very similar claims. but now the bikram brand is in jeopardy with some yoga studios dropping his name after the guru was accused of rape or sexual assault by six of his former students

@highlight
yoga guru bikram choudhury denies sexual assault allegations

@highlight
his accusers he says were manipulated to lie about him

@highlight
a former student says he uses his yoga accomplishments to hide the harm hes caused
