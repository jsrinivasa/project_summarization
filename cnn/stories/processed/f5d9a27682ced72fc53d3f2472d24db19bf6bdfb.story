the coxes can rest more comfortably living in georgia now that their daughter can get the marijuana extract she needs. with medical marijuana legal in nearly half the states doctors are increasingly studying what effect the drug has on various ailments while georgias law is specific to a handful of conditions medical marijuana laws in states such as california permit marijuana use for an array of ailments. but as states rewrite their regulations federal law remains the same marijuana is illegal to grow sell or use for any purpose under the controlled substances act marijuana is listed on schedule meaning it has no currently accepted medical use and a high potential for abuse. cox had heard that a form of medical marijuana might help but it wasnt available in georgia so a week after hearing a doctors diagnosis that haleigh might not live another three months she and haleigh packed up and moved to colorado springs colorado

@highlight
georgia gov nathan deal signs a medical marijuana bill

@highlight
the bill is inspired by haleigh cox a whose seizures threatened her life
