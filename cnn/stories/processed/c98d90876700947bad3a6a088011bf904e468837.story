jason rezaian has sat in jail in iran for nearly nine months the washington posts bureau chief in tehran was arrested in july on unspecified allegations it took more than four months for a judge to hear charges against him. rezaian was denied bail and for months he was denied access to proper legal representation his family has said. boxing great muhammad ali also an american muslim appealed to tehran last month to give rezaian full access to legal representation and free him on bail. since officers picked up rezaian and his wife yeganeh salehi on july at their home the post the state department and rezaians family have protested and called for his release salehi was released on bail in october

@highlight
officers arrested jason rezaian and his wife in july on unspecified allegations

@highlight
it took months to charge him charges were made public last week

@highlight
the washington post and the state department find the charges absurd
