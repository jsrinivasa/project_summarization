the rev robert h schuller california televangelist and founder of the television ministry hour of power died thursday according to his family. in schuller began broadcasting hour of power believed to be one of the first if not the very first sunday service to be shown regularly on television with his genial smile priestly robes and gray hair he looked and talked like a guy who wanted nothing more than to see his flock succeed the show which ran for decades reached millions making schuller a televangelist before the term became tarnished by the sins of his many successors. schuller also the founder of crystal cathedral megachurch had been diagnosed with esophageal cancer in august a release from hour of power said. eventually schullers grandson also named bobby took over hour of power though at a different church in a statement on thursday the younger schuller recalled standing atop crystal cathedrals tower of hope with his grandfather as they surveyed the surrounding landscape

@highlight
the rev robert schuller had been diagnosed with esophageal cancer in

@highlight
his tv show hour of power was enormously popular in the and
