now the real boston marathon trial can begin. the outcome of this first phase may have been preordained but nearly two years after the bombing the trial has held boston and the region in thrall more so than i might have imagined the case regularly lands on the front pages of our two daily newspapers the globe and the herald and often leads the local television newscasts the twitter feeds of reporters covering the trial are avidly followed. a federal jurys decision to convict dzhokhar tsarnaev of charges related to the boston marathon bombings was the most anticlimactic of anticlimaxes the lawyers admitted from the beginning that their client had participated in the horrific terrorist attack which both scarred and strengthened this city. the boston marathon will take place in less than two weeks on monday april thousands of runners will clog the route and tens of thousands will cheer them on as they did last year proving to the world that we will not be intimidated and tsarnaevs lawyers will still be fighting for their clients life

@highlight
dan kennedy after boston marathon bombing guilty verdict now real trial the sentencing can begin what justice should tsarnaev get

@highlight
he says a plea might have been better to keep bomber out of the news and let him fade into obscurity in maximum security cell

@highlight
kennedy the people who deserve to be remembered are victims and mit officer who was killed
