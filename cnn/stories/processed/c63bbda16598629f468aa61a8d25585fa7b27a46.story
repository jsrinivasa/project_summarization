everybody loves a good comeback story especially one thats dinosized. after its name was booted from science books for more than a century a new study suggests that the brontosaurus belongs to its own genera and therefore deserves its own name. oc marsh first named the brontosaurus in after he received crates of bones discovered at como bluff wyoming according to the yale peabody museum of natural history similar to though not as large as the apatosaurus discovered a couple of years prior marsh named the dinosaur brontosaurus or thunder lizard. apatosaurus had three sacral vertebrae in its hip region and brontosaurus had five according to the museums website so marsh gave the dinosaurs two different names later it was discovered that the number of sacral vertebrae is related to age as the animal gets older two of the vertebrae fuse to the sacrum

@highlight
scientist fossils once renamed should again be classified as brontosaurus

@highlight
study took five years and involved visits to museums worldwide
