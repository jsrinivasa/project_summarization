world heavyweight boxing champion wladimir klitschko has an important title defense coming up but his thoughts continue to be dominated by the ongoing fight for democracy in ukraine. but after leaving almost all his boxing opponents battered and bruised the ukrainian is seeking an impressive consecutive title defense against jennings klitschko is keen to carry on fighting his own and his countrys corner in the opposite way outside the ring. klitschko is the reigning ibf wba wbo and ibo champion and has alongside older brother vitali dominated the heavyweight division in the century. people are dying in ukraine every single day klitschko said i do not want to see it nobody wants to see it its hard to believe these days something like that in europe and ukraine is europe can happen

@highlight
reigning world heavyweight champion discusses ukraine crisis

@highlight
klitschko faces american challenger bryant jennings in new york on april

@highlight
ukraine is looking forward to becoming a democratic country klitschko says

@highlight
klitschkos older brother vitali a prominent figure in ukraine democracy movement
