how can more than nigerian schoolgirls simply disappear and how can the nigerian government and the rest of the world have allowed this to happen. malala offers solidarity love hope to abducted schoolgirls. caught in the blinding glare of global attention and facing a tide of questions about their bungled response to the kidnappings the nigerian government felt the weight of accountability and was spurred to take greater action offers of assistance from the likes of the us uk france and china were accepted. promise after promise was made by nigerian government officials that the girls would come home so where are they and where is the global outrage over these broken promises and broken dreams

@highlight
some girls were kidnapped from their school in northeastern nigeria by boko haram a year ago

@highlight
despite a global outcry one year on only a handful have escaped and returned home

@highlight
isha sesay we should all feel shame that our collective attention span is so fleeting
