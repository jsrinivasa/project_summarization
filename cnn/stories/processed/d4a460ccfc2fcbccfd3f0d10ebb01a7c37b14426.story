the listeria outbreak that prompted blue bell creameries to recall their entire product line dates to according to the centers for disease control. in a separate outbreak in texas three patients were infected with listeria between and tests of those listeria strains were nearly identical to listeria strains isolated from ice cream produced at the blue bell creameries oklahoma facility the cdc said. four of them drank milkshakes at the hospital made with blue bell ice cream the centers for disease control and prevention said its not clear whether the fifth patient at the kansas hospital had also consumed blue bell ice cream. dewaal said the listeria probably wasnt linked to blue bell in because one case wouldnt spark a full investigation other culprits such as cheeses and deli meats would be considered before ice cream products because listeria cant grow in frozen temperatures she said

@highlight
new this is a multistate outbreak occurring over several years the cdc says

@highlight
cdc says people died from bacteria believed to have come from blue bell

@highlight
we are heartbroken about this situation blue bell ceo and president says
