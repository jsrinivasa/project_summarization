an internal inquiry by the tulsa county sheriffs office in concluded that reserve deputy robert bates was shown special treatment and that training policies were violated regarding his role with the agency. one deputy reported that he was tasked with providing field training for bates the written policy is that a reserve deputy is required to have hours of training but that huckeby and albin pressured the trainer to write that bates was qualified after only hours. bates was working as a reserve deputy for the tulsa county sheriffs office on april when he was involved the arrest of harris in a weapons sting operation. later the memo was amended by his superiors to read in part that bates was capable of performing the functions of a patrol deputy the training deputy said he initialed the changes even though he didnt think bates was properly trained

@highlight
documents show that officers thought robert bates got special treatment

@highlight
the reserve deputy has pleaded not guilty to charge of seconddegree manslaughter

@highlight
bates says meant to use his taser but shot eric harris by mistake
