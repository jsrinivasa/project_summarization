kim bokdong is now and is going blind and deaf she knows her health is fading and she can no longer walk unassisted but her eyes burn bright with a passion borne of redressing her suffering of a lifetime ago. kim estimates each japanese soldier took around three minutes they usually kept their boots and leg wraps on hurriedly finishing so the next solider could have his turn kim says it was dehumanizing exhausting and often excruciating. the nightmares from five years as a sex slave of the japanese army from onwards are still crystal clear kim is determined to share her story with anyone who will listen until shes no longer physically able. my only wish is to set the record straight about the past before i die kim says

@highlight
kim bokdong is determined to share her story of sexual slavery until shes no longer physically able

@highlight
kim was held prisoner by the japanese military in a comfort station for five years raped ceaselessly

@highlight
she says she wont rest until she receives a formal apology from the japanese government
