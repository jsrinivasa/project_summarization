they are superstars flown in from across the globe and they probably had more legroom than you. the worlds leading showjumping and dressage horses have reached las vegas for this weeks world cup finals. the man who arranged their travel says for horses its business class all the way. there are two horses per box explains tim dutta who oversaw the loading of more than million in equine talent onto a qatar airways flight at amsterdams schiphol airport earlier this month

@highlight
horses complete transatlantic trip to las vegas in business class luxury

@highlight
fans expected as organizers spend bringing horses back to vegas

@highlight
celebrity chefs and legends of sport will mix with top jumping and dressage riders

@highlight
world cup final trophies to be won some of the most prestigious in the sport
