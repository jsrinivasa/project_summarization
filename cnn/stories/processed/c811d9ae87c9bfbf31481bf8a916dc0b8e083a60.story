there was a larger message in the article about a purported gang rape that rolling stone retracted on sunday night a part of the story that was never disputed the university of virginia is under continuing investigation over how it handles sexual assault on campus. the alleged gang rape at a fraternity house was in and jackie told the university about it the next spring she started telling her story very publicly including at a take back the night rally. that includes how uva officials handled the allegations in the discredited rolling stone article by a student the magazine called jackie especially since the school knew about the allegations for more than a year before the article came out. rolling stone had a line in its original story uvas emphasis on honor is so pronounced that since people have been expelled for honorcode violations such as cheating on exams and yet paradoxically not a single student at uva has ever been expelled for sexual assault

@highlight
university of virginia is under continuing investigation over how it handles sexual assault on campus

@highlight
some fear retraction of rolling stone story about one case takes focus off the broader issue

@highlight
after the story came out uva instituted a zerotolerance policy on sexual assault going forward
