the overturned convertible was smoldering when police pulled up to it on a new jersey roadway the driver was pinned inside and unconscious. kinnelon police sgt chris carbone told wpix that they released the video to show that the driver may have died if others had not informed the police. the woman identified by the kinnelon police department as dawn milosky of beachwood new jersey was airlifted to morristown medical center and survived. the rescue started when someone reported an erratic driver on thursday and the officers sped off to investigate

@highlight
the overturned car was smoldering on a new jersey roadway when police arrived

@highlight
it burst into flames shortly after they pulled out the unconscious driver
