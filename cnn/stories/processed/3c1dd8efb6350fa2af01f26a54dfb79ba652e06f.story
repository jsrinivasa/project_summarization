job one for newly appointed attorney general loretta lynch is to create a muscular federal response to months of national unrest over controversial police killings like that of freddie gray grays death last week from a spinal cord injury he suffered while in police custody has touched off days of protests and rioting in baltimore. that kind of behavior helped set the stage for the riots and looting we now see lynch has already launched a probe of the death of freddie gray and the justice department is also investigating the recent videotaped police killing of walter scott in north charleston south carolina where an officer has been arrested and charged with murder. but as events in baltimore demonstrate too many local departments arent getting the message lynch will need to put down the carrots pick up the stick and make clear that the justice department intends to crack down on police abuse by using one of its most potent weapons the power to withhold federal funds from local departments. a key part of that vow investigating wrongdoing must include a close look at the baltimore police department which has been the subject of bitter complaints of brutality according to a major investigation by the baltimore sun published last fall the city has paid out million in court judgments or settlements to more than people since in connection with allegations of brutality andor violations of civil rights

@highlight
errol louis new ag loretta lynch will try to get cops to improve community relations end abusive practices

@highlight
he says baltimore case shows too many local departments not getting message

@highlight
lynch will have to apply range of tough measures to fix this louis says
