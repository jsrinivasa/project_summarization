whether quaffing artisanal cocktails at hipster bars or knocking back noname beers on the couch more americans are drinking heavily and engaging in episodes of bingedrinking concludes a major study of alcohol use. alcohol control policies such as limits on when and where alcohol can be sold and how long bars can stay open have weakened in past decades greenfield said that may partly explain rising consumption nationwide particularly in some states where blue laws once prohibited alcohol sales on sundays or in supermarkets. heavy drinking among americans rose percent between and largely due to rising rates among women according to the study by the institute for health metrics and evaluation at the university of washington published thursday in the american journal of public health. the study is the first to track adult drinking patterns at the county level in percent of americans were considered heavy drinkers and percent were binge drinkers

@highlight
heavy drinking among americans rose percent between and

@highlight
the increase is driven largely by womens drinking habits

@highlight
its now more acceptable for women to drink the way men traditionally have says one expert
