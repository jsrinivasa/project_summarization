an impressive art collection assembled by the late actress and hollywood icon lauren bacall has officially been offered for purchase. the wideranging collection was expected to appeal to a variety of collectors fine arts and antiques dealers fans of lauren bacall and humphrey bogart as well as memorabilia enthusiasts. king who got to know bacall in her late years said the collection reflects the actress eclectic taste and there was only one principle she applied to collecting. i dont remember what awards lauren bacall won i just remember her saying you know how to whistle dont you steve you just put your lips together and blow said clooney referring to one of her most legendary lines from her first movie

@highlight
a collection of items belonging to legendary actress lauren bacall has been auctioned off at bonhams in new york

@highlight
highlights from the lot which fetched million include bronze sculptures jewelry and a number of decorative arts and paintings
