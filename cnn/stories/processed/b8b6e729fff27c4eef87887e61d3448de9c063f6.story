spacex on tuesday launched a twostage falcon rocket carrying an uncrewed cargo spacecraft called dragon on a flight from cape canaveral florida to the international space station. the space station crew will spend about five weeks unpacking the dragon theyll then stuff it with over pounds of science experiments trash and other stuff to send back to earth when theyre done dragon will leave the space station and mission controllers will guide it to splashdown in the pacific ocean off california. what about the rest of the rocket and the dragon the smaller top part of the rocket will carry the dragon into orbit and then break away from the cargo ship and burn up in earths atmosphere. spacex founder elon musk tweeted ascent successful dragon enroute to space station rocket landed on droneship but too hard for survival

@highlight
spacex founder elon musk rocket landed on droneship but too hard for survival

@highlight
this was the second attempt at historic rocket booster barge landing

@highlight
dragon spacecraft will head toward international space station on resupply mission
