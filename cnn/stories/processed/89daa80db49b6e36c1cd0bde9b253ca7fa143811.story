a new kansas law banning a common secondterm abortion procedure is the first of its kind in the united states. the law does not spell out a specific time frame that limits when an abortion can occur but it bans the dilation and evacuation abortion procedure commonly used during the second trimester of pregnancy the law allows for the procedure if necessary to protect the life or health of the mother according to a statement on brownbacks website. the law signed by kansas gov sam brownback on tuesday bans what it describes as dismemberment abortion and defines as knowingly dismembering a living unborn child and extracting such unborn child one piece at a time from the uterus. kansas is now not only the sole state with this atrocious law it also now has more restrictions on abortion than any state in the us the advocacy group said in a facebook post

@highlight
a new kansas law bans what it describes as dismemberment abortion

@highlight
supporters say its a groundbreaking step

@highlight
opponents say its dangerous and politically motivated
