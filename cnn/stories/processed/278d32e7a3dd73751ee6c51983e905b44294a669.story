about a quarter of a million australian homes and businesses have no power after a once in a decade storm battered sydney and nearby areas. devastating winds lashed cars and homes the storm system also brought destructive flooding that washed away houses and brought down trees onto streets and buildings. samantha mancuso was driving in her neighborhood of liverpool a suburb in sydney on wednesday when she noticed gallons of water pouring into the streets. the powerful storm has already claimed four lives according to new south wales police

@highlight
quarter million people without power in sydney and nearby areas

@highlight
a large storm system brought damaging winds and flooding to parts of australia

@highlight
the flooding is affecting public transportation services residential and coastal areas
