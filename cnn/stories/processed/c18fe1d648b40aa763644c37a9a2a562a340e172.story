phil rudd the drummer for legendary hard rock band acdc has pleaded guilty to charges of threatening to kill and possession of drugs in a new zealand court. rudd made several calls to his personal assistant over the following days but he terminated the calls after realizing it was rudd on the phone. rudd could face up to seven years in prison for the charge of threatening to kill he is currently released on bail until the sentencing hearing on june. according to a court summary rudd fired several employees because the release of his solo album in august had flopped in the charts a month later he called an associate and said he wanted his personal assistant taken out

@highlight
acdc drummer phil rudd pleads guilty to threatening to kill and drug charges

@highlight
court summary revealed that rudd had ordered for his personal assistant to be taken out

@highlight
police found methamphetamine and cannabis in his new zealand home in november
