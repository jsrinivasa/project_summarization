five americans who were monitored for three weeks at an omaha nebraska hospital after being exposed to ebola in west africa have been released a nebraska medicine spokesman said in an email wednesday. more than people have died in a west african epidemic of ebola that dates to december according to the world health organization almost all the deaths have been in guinea liberia and sierra leone. they were exposed to ebola in sierra leone in march but none developed the deadly virus. ebola is spread by direct contact with the bodily fluids of an infected person

@highlight
americans were exposed to the ebola virus while in sierra leone in march

@highlight
another person was diagnosed with the disease and taken to hospital in maryland

@highlight
national institutes of health says the patient is in fair condition after weeks of treatment
