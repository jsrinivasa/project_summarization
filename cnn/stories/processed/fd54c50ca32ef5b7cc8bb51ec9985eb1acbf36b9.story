the head of the libyan army has rejected the possibility of cooperating with any eu military intervention in his country intended to stem the flow of undocumented migrants trying to reach europe. s becky anderson libyan army head gen khalifa haftar said libyan authorities had not been consulted and in any event military action would not solve the problem. military action against libyan territory is an unwise decision he said you need to deal with the libyan crisis as a whole we are a sovereign country that needs to be respected despite what we are going through right now. if they take the right approach we will certainly cooperate he said referring to eu authorities the appropriate approach will benefit libya and its fight against terrorism and i repeat that means lifting sanctions against libya specifically those against the army

@highlight
libyan authorities have not been consulted

@highlight
gen khalifa haftar says libya will look after its interests

@highlight
solution to migration problem requires lifting of sanctions general says
