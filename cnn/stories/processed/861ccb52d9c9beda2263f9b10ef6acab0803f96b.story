the un security council voted tuesday in favor of an arms embargo on houthis the minority group that has taken over large swaths of yemen including its capital sanaa and supporters of former yemeni president ali abdullah saleh. the houthis forced president abdu rabu mansour hadi from power in january though hadi still claims he is yemens legitimate leader and is working with the saudis and other allies to return to yemen. in addition to the arms embargo it also demands that the shiite group pull back and refrain from more violence and includes sanctions aimed at controlling the spread of terrorism according to grant. those allied with hadi have accused the iranian government of supporting the houthis in their uprising in yemen

@highlight
egypt saudi arabia to launch joint military maneuvers inside saudi borders

@highlight
the arms embargo applies to the houthis and backers of expresident saleh

@highlight
russia abstains from the un security council vote over the inclusion of sanctions
