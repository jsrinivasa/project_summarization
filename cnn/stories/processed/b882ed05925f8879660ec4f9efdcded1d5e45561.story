one year after it was perpetrated the kidnapping of nearly schoolgirls by a jihadist group in nigeria remains a crime almost too horrifying to comprehend hundreds of teenaged girls just finishing school destined perhaps for significant achievement kidnapped never to be seen again. the government girls secondary school had been closed for a month because of the danger posed by boko haram militants who are opposed to western education particularly for girls but students from several schools had been called in to take a final exam in physics. the militants stormed the school arriving in a convoy of trucks and buses and engaging in a gun battle with school security guards then they forced the girls from their dormitories loaded them into trucks and drove them into the forest most have never been seen since except in a photograph in which they sat on the ground in a semicircle clad in islamic dress. one year later a few things have changed each of the missing girls has had a birthday in captivity each is now a year older

@highlight
nigerias presidentelect sends nations prayers to families of girls

@highlight
world still expresses hope that the girls will return

@highlight
boko haram controls a portion of northeastern nigeria
