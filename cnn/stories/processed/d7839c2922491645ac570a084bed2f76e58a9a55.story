discovery of the body of a young girl who may have been dead for weeks has led to the arrest of her teen sister herself a mother and a search for the girls parents who may be in california with five other children police in north las vegas nevada said. a day later a search warrant was served at the familys home thats when police found the body of the girl who had apparently been dead for at least a few weeks according to police. the parents left the home several months ago with the other five children ranging in age from to years old but according to patty they left the child and the sister at the residence alone. police arrested the teen mother on one count of child abuse with substantial bodily harm she was taken to a juvenile facility

@highlight
parents wanted for questioning after girls body found at home

@highlight
north las vegas police had been dead for at least a few weeks

@highlight
sibling is held in case
