she could stand on a strand of human hair with room to spare. a lot of the artistic expression that i bring to the world represents the absolute current moment in human development whether its printing technology or science i love to represent the now. the microscopic polymer statue by south african sculptor jonty hurwitz entitled trust measured just of a centimeter and had been called the smallest sculpture ever made. but the worlds tiniest woman suddenly disappeared to an unknown location likely never to be seen again

@highlight
londonbased artist jonty hurwitz creates sculptures that are smaller than a human hair

@highlight
theyre made using ultraviolet light and resin and then photographed with an electron microscope
