when singer avril lavigne went missing from the music scene there was tons of speculation was she pregnant in rehab going through a split from her husband nickelback front man chad kroeger. focus on the mystery intensified in december after a fan twitter account posted a direct message from lavigne when she solicited prayers saying she was having some health issues. lavigne believes that she was bitten by a tick last spring what followed was months of lightheadedness and lethargy that doctors were initially unable to diagnose. after her direct message about her health went viral lavigne was inundated with concern from fans

@highlight
the singer had been off the scene for a while

@highlight
she says she was bedridden for months

@highlight
lavigne was sometimes too weak to shower
