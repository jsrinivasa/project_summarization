charlotte dujardin says she is living the dream as she cements her status as a dressage legend. dujardin and valegro won their their second consecutive reem acra fei world cup dressage title in las vegas at the weekend a trophy to sit alongside olympic gold and the world and european titles. valegro just loves his job said dujardin. i dont have to force him to do anything i just sit there and steer and off i go theres no sweating no pushing no pulling he knows his job and its just fantastic added dujardin who couldnt stop smiling as she was pictured next to an elvis impersonator after her win

@highlight
olympic champion charlotte dujardin remains worlds leading dressage rider after vegas contest

@highlight
swiss olympic showjumping champion steve guerdat finally wins world cup jumping title at attempt
