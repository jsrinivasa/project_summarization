canadian fighter jets have carried out their first airstrike against isis in syria hitting one of the sunni militant groups garrisons. canadian forces are part of the usled coalition trying to stem the extremist groups bloody advances in iraq and syria canadian warplanes have conducted dozens of strikes against isis targets in iraq since november. the canadian aircraft and their crews safely returned to base the military said it wasnt immediately clear how many casualties the airstrike had caused. canadian prime minister stephen harper announced plans last month to expand the airstrikes into syria

@highlight
hornets bomb a garrison near isis de facto capital of raqqa canada says

@highlight
the canadian military has conducted dozens of strikes against isis in iraq
