do you use toilet paper. but she stayed strong and a year later she is at the point where she composts makes her own shampoo toothpaste and even uses reusable feminine products and yes she does use toilet paper the kind thats quickly biodegradable. in the first week she filled up half the mason jar with paper towels after grabbing them to dry her hands just out of habit it also took a while to feel comfortable with the funny stares she would get after politely asking food vendors to put her sandwiches and salads into her metal tin instead of paper and plastic containers. the small amount of waste shreeves does create goes straight into a mason jar that sits threequarters full right next to her kitchen sink its contents include produce stickers some paper tea bag wrappers and a long twisted piece of cotton that went around her toes for a recent pedicure

@highlight
atlanta woman uses a mason jar as a trash can

@highlight
she has produced about as much waste in six months as an average person does in less than a day

@highlight
she strives to live a zero waste lifestyle
