greenpeace activists have climbed aboard a shell oil rig to protest the companys plans to drill in the arctic near alaska. greenpeace is furious over a decision last week by us authorities to lift the suspensions on leases to drill for oil and gas in the chukchi sea which lies between northern alaska and russia shell and several other oil companies bought exploration leases for the sea in. in the current episode the activists pursued the polar pioneer which shell is leasing from transocean as it traveled thousands of miles aboard a transport vessel from malaysia greenpeace said. the rig the polar pioneer is on its way to the arctic via seattle the environmental activists caught up with it about miles northwest of hawaii greenpeace said

@highlight
six protesters scale the polar pioneer hundreds of miles northwest of hawaii

@highlight
greenpeace opposes shells plans to drill for oil in the arctic
