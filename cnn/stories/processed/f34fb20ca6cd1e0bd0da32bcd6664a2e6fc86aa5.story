a mammoth wave of snow darkens the sky over everest base camp appearing like a white mushroom cloud roaring over the climbers they scurry as their tents flap like feathers in the wind then panic hits. helicopters brought stranded climbers off the mountain monday amid growing concern for the groups stuck around feet meters high in camps and the climbers who were higher up everest appeared to have avoided the deadly avalanche that struck base camp but many estimated to be in the hundreds could not descend on their own. bottom line the icefall has been deemed impassable at this point said alan arnette a climber and everest blogger who was at camp when the avalanche struck he said that climbers at camp and others higher up would descend to camp elevation feet to await helicopters. several everest climbing teams have confirmed deaths five nepali staff members were killed at everest base camp and camp according to adventure consultants it did not identify the staffers

@highlight
a youtube video shows the scale of an avalanche on mount everest on saturday

@highlight
eight nepalis are dead at everest but not identified three americans are also dead

@highlight
helicopter rescues are underway to retrieve climbers stranded on everest
