boston native mark wahlberg will star in a film about the boston marathon bombing and the manhunt that followed deadline reported wednesday. patriots day is the second film related to the boston bombing to be announced fox announced in november that it will be making a film called boston strong about the event. according to deadline wahlberg is hoping to play boston police commissioner ed davis who retired after the attack in the film will be told from davis point of view. wahlberg is also a producer of the film

@highlight
mark wahlberg is planning to appear in patriots day

@highlight
the film will be about events surrounding the boston marathon bombing

@highlight
another film boston strong is also in the works
