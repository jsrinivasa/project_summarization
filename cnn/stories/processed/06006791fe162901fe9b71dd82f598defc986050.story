much like the ardent young royalwatchers of today enamored by the duchess of cambridges very being i was similarly captivated by diana princess of wales when i was a youngster. in a recent today show poll of americans predicted the name was a shooin and in the uk the bookies odds of a baby named after her late grandmother change almost daily as diana becomes an increasingly popular choice. traditionally they pick dynastic names and there are plenty to choose from elizabeth alice victoria and charlotte have all been frontrunners but the sentimental favorite among punters remains diana. that said in the event the couple do welcome a baby girl i would hope that they do not opt to name her diana

@highlight
as william and kate await the arrival of their second child speculation is rife as to what he or she will be named

@highlight
royal expert victoria arbiter argues that naming a newborn princess after diana would put too much pressure on her
