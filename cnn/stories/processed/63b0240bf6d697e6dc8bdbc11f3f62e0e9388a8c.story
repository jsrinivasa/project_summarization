the investigation into the crash of germanwings flight has not revealed evidence of the copilot andreas lubitzs motive but he suffered from suicidal tendencies at some point before his aviation career a spokesman for the prosecutors office in dusseldorf germany said monday. the official said he is not aware of any suicidal tendencies reported by lubitz to the doctors but that investigators believe he was suicidal. about german investigators some specializing in homicide cases and others in identifying remains are in the french alps at the site of the crash dusseldorf police said monday meanwhile police continue to examine evidence collected from the apartment of lubitz and from his parents home police said. kumpa emphasized theres no evidence suggesting lubitz was suicidal or acting aggressively before the crash

@highlight
european pilots must fill out forms that ask about mental and physical illnesses

@highlight
road to crash site is almost finished says mayor of le vernet france

@highlight
german newspaper bild releases a timeline of the flights final moments
