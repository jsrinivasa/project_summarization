ursula ward kept repeating her sons name odin. odin lloyd was her first born her only son odin was the backbone of the family odin was the man of the house odin was his sisters keeper ward told judge susan garsh before garsh sentenced the former profootball player. the day i laid my son odin to rest she continued pausing to maintain her composure i think my heart stopped beating for a moment i felt like i wanted to go into that hole with my son odin. odin was my first best gift i will ever receive his mother said i thank god for every second and every day of my sons life that i spent with him

@highlight
ursula ward talks about the shock and pain of her sons murder

@highlight
odin lloyds sister said her brothers death has felt like a bad dream
