the leader of yemens houthi rebels vowed not to back down on sunday as a top saudi military official claimed weeks of airstrikes had significantly weakened the shiite group. meanwhile saudi brig gen ahmed asiri said sunday that airstrikes had decimated the houthis central command by targeting their communications the rebels he said are now holding a defensive stance in besieged areas. his comments came after more than three weeks of saudiled coalition bombings aimed at pushing back the houthis who surged into the capital of sanaa in january and ousted president abdu rabu mansour hadi from power hadi still claims hes yemens legitimate leader and is working with the saudis and other allies to return to his country. since it began the campaign known as operation decisive storm on march the saudiled coalition has launched airstrikes asiri said

@highlight
abdulmalik alhouthi says in a televised address that fighters will not pull out of major cities

@highlight
a top military leader pledges allegiance to yemens ousted president
