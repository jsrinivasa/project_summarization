i usually think of april as tax month but it seems to be morphing into national get tested month dallas mavericks owner mark cuban advised twitterers to have their blood tested for everything available and to do so every three months. but there are many lab tests to order on yourself medicares clinical diagnostic laboratory fee schedule lists over a thousand they are not all blood tests but a lot of them are and since blood tests require blood you would surely develop at least one medical problem if you actually followed mr cubans advice to get them all anemia. of course there have been genuine advances in diagnostic testing diagnostic tests can be extremely useful in sorting out acute medical problems but if you feel well dont think that testing will make you feel better. following her mothers cancer diagnosis singer taylor swift urged her fans to remind their parents to get screening tests and arizona gov doug ducey signed legislation to allow arizonans to get any lab test without a doctors order

@highlight
mark cuban said people should have their blood tested every quarter

@highlight
gilbert welch giving people more tests will increase health spending but it wont make us healthier
