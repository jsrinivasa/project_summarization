one former employee of the private blackwater worldwide security company was sentenced monday to life in prison and three others to years each behind bars for their roles in a mass shooting in baghdad that left people dead. senior us district court judge royce lamberth sentenced blackwater sniper nicholas slatten to a term of life in prison mandatory for his firstdegree murder conviction blackwater workers paul slough evan liberty and dustin heard were sentenced to year each plus one day. according to prosecutors the four were among seven blackwater employees who opened fire in the nusoor square traffic circle in baghdad killing people. ali razzaqs father looked at the defendants and yelled in broken english if i kill anyone in his family what he do today we will see who will win the law or blackwater he said blackwater killed my son

@highlight
blackwater sniper nicholas slatten is sentenced to life in prison mandatory for his firstdegree murder conviction

@highlight
three others get years plus one day in the shooting in baghdad that left dead
