eleven channels associated with the frenchlanguage global television network went black late wednesday due to an extremely powerful cyberattack the networks director said. in addition to its channels also temporarily lost control of its social media outlets and its websites director yves bigot said in a video message posted later on facebook. on a mobile site which was still active the network said it was hacked by an islamist group isis logos and markings could be seen on some social media accounts. the outage began around pm paris time pm et and network teams were still working to restore service more than five hours later

@highlight
went black late wednesday and was still out hours later

@highlight
the network blames an islamist group theres no claim of responsibility
