so youd like a full house reunion and spinoff you got it dude. costar john stamos announced monday night on jimmy kimmel live that netflix has ordered up a reunion special followed by a spinoff series called fuller house. its sort of a role reversal and we turn the house over to her stamos told kimmel. as big fans of the original full house we are thrilled to be able to introduce fuller houses new narrative to existing fans worldwide who grew up on the original as well as a new generation of global viewers that have grown up with the tanners in syndication netflix vice president of original content cindy holland said in a statement

@highlight
show will return with a onehour special followed by spinoff star john stamos says

@highlight
he announced the show monday night on jimmy kimmel live
