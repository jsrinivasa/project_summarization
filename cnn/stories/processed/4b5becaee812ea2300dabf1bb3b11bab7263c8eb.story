after deliberating for more than hours over parts of seven days listening intently to the testimony of more than witnesses and reviewing more than pieces of evidence the tearyeyed men and women of the jury exchanged embraces. jon carlson said he was struck by testimony and video evidence that hernandez and two codefendants were sunbathing poolside hours after the slaying drinking smoothies hernandez at times left his daughter with the two men. the jurors declined to talk about the dynamics inside the jury room choosing instead to keep the focus on the evidence presented during trial they conveyed a sense of gravity about their task. s anderson cooper thursday in the first nationally televised interview with members of the jury

@highlight
were all equal and we all deserve the same fair trial says one juror

@highlight
the monthslong murder trial of aaron hernandez brought jurors together

@highlight
foreperson its been an incredibly emotional toll on all of us
