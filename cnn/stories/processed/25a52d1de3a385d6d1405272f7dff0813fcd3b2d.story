this is the end beautiful friend the end. for the the end arrived with depending on your ideals and your tribe either the rolling stones altamont fiasco in december the kent state shootings in may or richard nixons reelection. for mad men the end of an era as its slogan has it begins sunday. the end of a tv series brings with it some risk the sopranos mad men creator matthew weiners former employer divided fans with its famous cuttoblack finale on the other hand mad mens former amc stablemate breaking bad was saluted for an almost perfect landing

@highlight
mad mens final seven episodes begin airing april

@highlight
the show has never had high ratings but is considered one of the great tv series

@highlight
its unknown what will happen to characters but we can always guess
