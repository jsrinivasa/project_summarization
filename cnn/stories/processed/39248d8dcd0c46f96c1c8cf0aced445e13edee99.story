a crowd gathers near the entrance of tokyos upscale mitsukoshi department store which traces its roots to a kimono shop in the late century. fitting with the stores history the new greeter wears a traditional japanese kimono while delivering information to the growing crowd whose expressions vary from amusement to bewilderment. aiko chihira is an android manufactured by toshiba designed to look and move like a real person it was put on temporary display at the department store. its hard to imagine the stores founders in the late could have imagined this kind of employee

@highlight
toshiba tests robotic greeter at upscale tokyo department store

@highlight
more japanese businesses are testing out robots as possible solution to japans shrinking workforce
