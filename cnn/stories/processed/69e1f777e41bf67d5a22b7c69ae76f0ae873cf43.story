what was supposed to be a fantasy sports car ride at walt disney world speedway turned deadly when a lamborghini crashed into a guardrail. petty holdings which operates the exotic driving experience at walt disney world speedway released a statement sunday night about the crash. petty holdings also operates the richard petty driving experience a chance to drive or ride in nascar race cars named for the winningest driver in the sports history. the driver of the lamborghini tavon watson of kissimmee florida lost control of the vehicle the highway patrol said he was hospitalized with minor injuries

@highlight
the crash occurred at the exotic driving experience at walt disney world speedway

@highlight
officials say the driver tavon watson lost control of a lamborghini

@highlight
passenger gary terry died at the scene
