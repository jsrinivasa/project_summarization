the university of michigan has decided to proceed with a screening of the film american sniper despite objections from some students. on wednesday e royster harper university of michigans vice president for student life said in a statement that it was a mistake to cancel the showing of the movie american sniper on campus as part of a social event for students and that the show will go on. the initial decision to cancel the movie was not consistent with the high value the university of michigan places on freedom of expression and our respect for the right of students to make their own choices in such matters the statement said. more than students signed a petition asking the school not to show the movie as part of umix a series of social events the university stages for students

@highlight
some complained about the films depiction of the iraq war

@highlight
a petition asked the university not to show the bradley cooper film
