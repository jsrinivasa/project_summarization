changing the way ferguson polices its people was no on many a voters agenda even the candidates who take issue with the department of justice report on ferguson agreed that the city needed change when it came to policing. one other seat was up for grabs in ferguson that was won by brian fletcher a former mayor who launched the i love ferguson campaign to raise money for momandpop businesses that were hurt by the violence and vandalism during the protests last fall fletcher beat his opponent bob hudgins with of the vote. change has come to ferguson. i wanted change said ellory glenn who is black his wife is white he said the couple moved to ferguson after he retired from the marine corps in because they felt it was a racially welcoming place

@highlight
for the first time ever fergusons city council will be half black

@highlight
the two winning black candidates vowed to bring reform

@highlight
the election was a critical test in this beleaguered city
