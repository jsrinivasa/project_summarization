the italian navy has boarded and retaken control of a fishing boat that had been seized hours earlier by gunmen off the coast of sicily the italian military said friday. on monday gunmen on a speedboat fired shots in the air and sped away with a wooden boat that was being used to transport migrants according to frontex the european unions border management agency that incident occurred nautical miles from the libyan coast the migrants on board had already been transferred on to a different boat frontex said on its website. an italian naval unit boarded the fishing boat and took custody of a libyan soldier on the vessel according to a statement by the italian navy it said during the operation rifle shots were accidentally fired and one of the seven fisherman on board was slightly injured. at a certain point a tug boat came up and flanked the fish boat and some libyans came on board mazzarino said quoting the captain alberto figuccia

@highlight
italian navy retakes fishing boat seized by smugglers

@highlight
boat was being steered towards libyan port of misrata

@highlight
italian navy says shots were fired accidentally one fisherman injured
