the flag is crude handmade but the message is clear allegiance to isis in afghanistan and the timing with america withdrawing the taliban fractured young men disillusioned and angry could not be worse. it is often said that rivalry between the nascent isis presence and the taliban who remain the big guns in afghanistan is fierce enough to mean the isis fighters could be killed for brandishing the flag. ghazni deputy governor mohammad ali ahmadi says there are extaliban fighters operating under the name of isis in ghazni province at the moment who have changed their flag from white to black there have been armed clashes between newlyconverted isis members and taliban fighters who should be in control of certain places. wardak mp shir wali wardak says i dont think isis fighters from syria and iraq have come here to afghanistan but hardcore taliban members who have understood that the taliban name is dying have changed the color of their flags from white to black in order to stay alive i know that some black flags have been seen in wardak province raised by extaliban fighters

@highlight
cameraman parading isis flags

@highlight
us official isis militants have no military capability at present but are trying to recruit disillusioned taliban in several areas

@highlight
rivalry between isis and the taliban in afghanistan is fierce enough to mean the isis fighters could be killed for brandishing the flag
