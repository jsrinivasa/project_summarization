i dont always talk about news events with my daughters but there was something about the story of espn reporter britt mchenry and the wildly offensive way she spoke to that towing company employee that made me bring it up. said shannon also on twittter i had no idea who britt mchenry was before but now i know shes the kind of woman i hope ive taught my daughters not to be. i see this britt mchenry video and think its not just about how a celebrity shouldnt act in public its how all of us shouldnt act wrote cait on twitter. so britt mchenry check out these examples of how the rich famous and powerful handled stressful situations with grace and dignity

@highlight
espn reporter britt mchenry caught on video berating a towing company employee

@highlight
s kelly wallace used the story as a teachable moment for her daughters

@highlight
wallace mchenry could learn from other celebrities who responded gracefully in stressful situations
