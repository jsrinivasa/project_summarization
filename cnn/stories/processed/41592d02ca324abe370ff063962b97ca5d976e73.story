sell all that you own and distribute the money to the poor and you will have treasure in heaven then come follow me jesus tells the rich man in one of his bestknown parables. mary magdalene was free to travel the country with jesus and his disciples so was unlikely to have a husband and children waiting for her at home and in finding jesus we examine the gnostic gospel of mary magdalene and explore the argument that jesus was in fact her husband she may have simply been an independent woman with her own resources who found a compelling message and messenger. as the gospel of john tells it six days before passover jesus was in bethany at the house of his friend lazarus whom he had raised from the dead a woman named mary takes a jar of costly perfumed oil and anoints the feet of the reclining jesus she dries his feet with her hair an irresistible image for artists and dramatists judas iscariot objected to the act. though denarii was the annual wage of a laborer jesus told judas to leave her alone and foreshadowing his fate said the anointing would be useful for his burial and besides you always have the poor with you but jesus would not always be there

@highlight
some of jesus most important financial backers were women historians say

@highlight
joseph of arimathea and nicodemus both men of stature and wealth chipped in to help fund jesus ministry
