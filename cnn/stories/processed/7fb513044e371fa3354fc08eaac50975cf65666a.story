the presence of a harmful pesticide at a luxury villa in the us virgin islands may have resulted in the illness of a delaware family the us environmental protection agency said friday. paramedics were called last week to a rented villa at the sirenusa resort in st john after the family of four fell ill they had rented the villa from march to march and were later hospitalized the illness was reported to the epa on march. depending on the season the luxury villa where the family stayed rents between and per night. our preliminary results do show that there was a presence of methyl bromide in the unit where the family was staying said elias rodriguez an epa spokesman

@highlight
delaware family becomes ill at the sirenusa resort in the us virgin islands

@highlight
preliminary epa results find methyl bromide was present in unit where family stayed

@highlight
us justice department has initiated a criminal investigation into the matter
