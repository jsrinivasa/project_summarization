a california woman who was recording police activity said she was terrified when a deputy us marshal walked toward her grabbed her cell phone out of her hands and smashed it with his foot. as he gets close the marshal then runs a few steps toward her and wrestles the phone from her hands. paez said she was a few homes away from the center of the police activity. beatriz paez filed a complaint wednesday with police in south gate just south of los angeles however the police dont have authority over marshals paez is also considering a lawsuit

@highlight
beatriz paez was walking sunday when she saw police activity

@highlight
she says marshals told her to stop recording but she refused

@highlight
marshals service said it is looking into incident after man took her phone and smashed it
