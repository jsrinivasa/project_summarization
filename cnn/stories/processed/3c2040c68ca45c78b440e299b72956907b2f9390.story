an unmanned russian spacecraft originally bound for the international space station will reenter the earths atmosphere after flight controllers lost contact with it american astronaut scott kelly said wednesday. the spacecraft that lost contact with flight controllers will reenter the earths atmosphere in about a week roscosmos the russian federal space agency said wednesday. roscosmos the russian federal space agency announced that the progress will not be docking and will reenter the earths atmosphere kelly said from the space station. even if russia hadnt lost contact with the craft the original plan was for progress to burn up reentering earths atmosphere albeit with garbage rather than a full load of equipment for the space station

@highlight
progress spacecraft will reenter earths atmosphere in a week russia space agency says

@highlight
nasa russian flight controllers have been trying to make contact with the unmanned space freighter

@highlight
space station crew can manage without supplies carried by the spacecraft nasa says
