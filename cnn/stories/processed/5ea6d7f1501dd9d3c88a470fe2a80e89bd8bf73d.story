with a wellpaid job in finance and his own apartment li lifei is living the chinese dream but theres one missing ingredient his own car. each month there are around license plates available at the most recent auction on saturday li said the final price was yuan around three times the price of a cheap chinese car and a third of what li plans to spend on his suv. to register for a license plate auction prospective car buyers like li must put down a deposit in exchange for disc containing software they can use to bid online the auctions take place once a month on a saturday morning. i dont want a hybrid car i thought maybe a tesla but its too expensive he said referring to the hyper luxury electric sports car developed by elon musk that has seen weak sales in china

@highlight
many large chinese cities ration license plates as they look for a solution to gridlocked roads and pollution

@highlight
it means many prospective car owners have to bid in license auctions

@highlight
but hybrid vehicles automatically qualify for a license plate
