chiles calbuco volcano erupted twice in hours the countrys national geology and mining service said early thursday. another person said it was impressive to see an enormous mushroom cloud with the immense force of the volcano and to see the ashes at that point there was a lot of panic lots of chaos traffic jams people going to supermarkets everyone looking for water trying to take out money from the atms. alejandro verges regional director at the ministry of interior and public safety said thursday afternoon that officials are concerned there might be a third eruption. the agency said it was evaluating the spectacular nighttime eruption but indicated it was stronger than the first one

@highlight
almost feet of ash fell in some areas

@highlight
authorities evacuate people

@highlight
the last time calbuco erupted was
