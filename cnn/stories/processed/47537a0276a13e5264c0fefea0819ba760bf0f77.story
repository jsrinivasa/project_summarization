a car bomb exploded at a restaurant near the presidential palace in the heart of somalias capital tuesday killing at least people including a woman and a boy police said. somali police spokesman qasim mohamed roble told reporters that the car bomb killed no government official or soldier among the dead he said was a boy who shined shoes. on february militants attacked the central hotel blowing up a car bomb outside before shooting people and detonating another bomb inside alshabaab claimed responsibility for the attack. the restaurant is across the street from the central hotel where alshabaab killed at least people in a bombing and gun attack in february

@highlight
islamist militant group alshabaab claims responsibility for the attack

@highlight
the explosion happened across the street from a hotel that was attacked two months ago

@highlight
mogadishu has been the site of frequent attacks by alshabaab
