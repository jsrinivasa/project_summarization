the hollywood reporterthe author of a novel has accused the avengers director and cabin director drew goddard of stealing his idea. whedon produced and cowrote the script for cabin with director drew goddard a writer on whedons buffy the vampire slayer and a fanboy favorite in his own right with credits that include netflixs daredevil and reportedly may soon include sonys upcoming spiderman projects whedon and goddard are named as defendants along with lionsgate and whedons mutant enemy production company in the complaint filed monday in california federal court. in the complaint peter gallagher no not that peter gallagher claims whedon and goddard took the idea for the cabin in the woods from his novel the little white trip a night in the pines hes suing for copyright infringement and wants million in damages. with just weeks until his boxoffice victory lap for avengers age of ultron joss whedon is now facing a lawsuit accusing him of stealing the idea for the metahorror movie the cabin in the woods

@highlight
an author says avengers director joss whedon and cabin director drew goddard stole his idea

@highlight
peter gallagher alleges similarities to his the little white trip a night in the pines from
