if youre hunting for the earliest galaxies and clues about potential life on other planets you are going to need a very big mirror and a golf ball of gold. nasa says that the project has four main goals namely to search for the first galaxies formed after the big bang find out how galaxies evolved observe the birth of stars and planets and investigate the potential for life on other planets. the new telescope requires a huge mirror of square meters about square feet and a golf ball of gold about grams or ounces to optimize it for infrared light it is then coated with glass. named after james e webb a former nasa leader jwst is being designed to study the first stars and galaxies that formed in the early universe

@highlight
hubble has helped make major discoveries but there are limits to how far it can see into space

@highlight
the james webb space telescope will work in the infrared and be able to see objects that formed billion years ago

@highlight
scientists also believe the new telescope will be able to detect planets around nearby stars
