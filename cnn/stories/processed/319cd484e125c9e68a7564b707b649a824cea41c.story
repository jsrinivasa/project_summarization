the sign painted onto the school entrance wall reads youth is a mistake adulthood is a struggle old age is a regret. the black humor of the message provides some light relief for a school that lives in constant fear of a terrorist attack because of what is taught inside its walls. the students aged to at the ibnusiina school in northern kenya are getting a socalled western education taking lessons in subjects like mathematics science and english. but its an education that alshabaab the somalibased terror group is trying to prevent the children from obtaining according to the head of the school

@highlight
looming threat of alshabaab has terrified students and teachers in kenya

@highlight
terror group massacred at kenyan university last week
