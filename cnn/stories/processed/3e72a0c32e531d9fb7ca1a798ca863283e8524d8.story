beer and ice cream it doesnt exactly spring to mind when you think of classic food pairings old friends such as bacon and eggs or steak and cabernet. but colorados new belgium brewery and the folks at ben jerrys are teaming up on a beer inspired by ice cream salted caramel brownie ice cream to be precise. but sadly for beer fans theres no talk of a beerflavored ice cream not yet anyway. last month ben jerrys cofounder ben cohen said hed be open to the idea of a marijuanainfused ice cream someday news that set pot fans ablaze

@highlight
new belgium brewery will make a beer inspired by ben jerrys ice cream

@highlight
it will be called salted caramel brownie
