there might be terrorism attempts in the future at the olympics and summit using drones suga said so we need to examine and review continuously the way small unmanned vehicles like drones should be operated and how to cope with the threat of terrorism from drones the government will do all that we can to prevent terrorism. a bizarre and alarming discovery is raising concerns in japan about the potential for terrorism involving drones. chief cabinet secretary yoshihide suga said the discovery is raising concerns about terrorism. the discovery came on the same day a japanese court approved a government plan to restart two reactors at the sendai nuclear power plant in kagoshima prefecture more than four years after the fukushima daiichi nuclear disaster

@highlight
the drone is sparking terrorism concerns authorities say

@highlight
it was equipped with a bottle containing radioactive material

@highlight
it was discovered as a court approved a plan to restart two japanese nuclear reactors
