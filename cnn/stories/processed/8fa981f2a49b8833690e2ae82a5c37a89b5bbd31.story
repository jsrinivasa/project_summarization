the onslaught on houthis rebels in yemen continued tuesday with the saudiled coalition asserting increasing control while locals fled the chaos and casualties piled up dozens of civilians among them. the burgeoning conflict threatened to open a wider rift not just in the region but in the muslim world thats because the saudis have painted the houthis as tools of irans government an accusation that also reflects the fact saudi arabia and their allies in the coalition are predominantly sunni muslims while iran and the houthis are shiites. things finally came to a head last week with hadi who claims to still be president even though the houthis control government institutions left yemen at roughly the same time saudi arabia and its allies came in with force to support hadi who they say remains yemens legitimate leader. the instability escalated as the houthis a minority group long marginalized in yemen increasingly challenged the government of president abdu rabu mansour hadi who in took over for saleh who had been in power for years

@highlight
saudi minister if wars drums are beaten we are ready for them

@highlight
un official at least killed in the past week including civilians
