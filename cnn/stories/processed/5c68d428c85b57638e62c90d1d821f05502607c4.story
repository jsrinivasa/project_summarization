the clamor and chaos of the previous day has dissipated by the time we arrive at kathmandus only airport the mad rush of hours previously in those first confusing cacophonous hours following the earthquake near the nepali capital had died down now families sit camped out silent and patiently waiting but for now abandoned. a day after the earthquake struck they found a woman under the rubble unhurt in shock but alive it is this hope that keeps narayan gurung going the belief that his wife and are still alive i raced here after the earthquake i havent slept for days he says workers dig painstakingly slowly removing piles of stone and debris they spot someones hair but cant yet reach the body or tell if its male or female. stepping out of the terminal building the devastation is apparent it is an overwhelming introduction to this city that less than hours ago was hit by the worst earthquake this country has experienced in years. follow the latest coverage of nepal earthquake

@highlight
tremors subside finally in kathmandu but aftereffects of saturdays staggering tragedy will be felt for years

@highlight
arwa damon and gul tuysuz take tour of devastated city as locals struggle to cope

@highlight
workers dig painstakingly slowly removing piles of stone and debris
