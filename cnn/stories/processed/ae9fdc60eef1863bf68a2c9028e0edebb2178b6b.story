dan swangard knows what death looks like. thats one of the reasons swangard joined a california lawsuit last month seeking to let doctors prescribe lethal medications to certain patients who want to hasten death if he were given only months to live swangard said he cant say for certain whether he would take them. to remove the cancer surgeons took out parts of his pancreas and liver as well as his entire spleen and gallbladder the operation was successful but swangard knows theres a strong chance the disease will return and if he gets to a point where theres nothing more medicine can do he wants to be able to control when and how his life ends. swangard completed his medical residency in san francisco in the middle of the aids crisis young men were dying all around him throughout his career as an internal medicine doctor a hospice volunteer and now an anesthesiologist he has become frustrated with how the medical system handles death doctors spend so much time trying to extend life that few focus on what patients want in their last days he said

@highlight
dan swangard a physician wants to be able to control when and how his life ends

@highlight
a recent survey reveals percent of american doctors support assisted suicide
