minnesota vikings running back adrian peterson will be reinstated as an active player by the nfl on friday the league said. initially charged with felony child abuse peterson pleaded no contest to misdemeanor reckless assault in november in texas the nfl then suspended him without pay and he lost his appeal of that sanction the next month in february a minnesota district court judge vacated the decision that upheld his suspension making peterson eligible for reinstatement. in a letter nfl commissioner roger goodell told peterson that his continued participation in the league was contingent on a number of requirements including that he maintain an ongoing program of counseling and treatment as recommended by medical advisers the nfl said thursday. the next vikings organized team activities begin in late may it is unclear whether peterson will attend he has been unhappy with how the vikings have handled the matter

@highlight
adrian peterson had been suspended after pleading guilty to misdemeanor reckless assault

@highlight
nfl commissioner roger goodell requires him to keep going to counseling other treatment

@highlight
minnesota vikings last season say they look forward to him rejoining the team
