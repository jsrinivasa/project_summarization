at first police in marana arizona thought the shoplifted gun mario valencia held as he walked through a busy office park was locked and unable to fire. that february morning police have said valencia committed several crimes in nearby tucson before stealing a car and driving to the walmart in marana there he went to the sporting goods department asked to see a rifle then told an employee he wanted the ammunition. marana police on thursday said the cable gun lock was still on the rifle when it was recovered. valencia took the gun and ammo and fled into a nearby business park where he encountered an officer in a slowmoving patrol car

@highlight
before he was slammed into by a police car mario valencia fired a rifle with a loosened lock

@highlight
he shoplifted the gun and ammo from a walmart where a saleswoman who showed him the weapon alerted security

@highlight
walmart says the lock was properly installed but police say it was loose when it was found
