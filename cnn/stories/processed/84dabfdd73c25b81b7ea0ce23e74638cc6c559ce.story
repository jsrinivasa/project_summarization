an amnesty international report is calling for authorities to address the number of attacks on womens rights activists in afghanistan. selay ghaffer is a womens rights activist and spokesperson for the solidarity party of afghanistan a small but outspoken political party based in kabul and twenty provinces that fights for issues such as democracy social justice and womens rights. in the un assistance mission in afghanistan unama released statistics that showed the number of women killed in the country had increased by from the previous year although the number of civilian victims had decreased said amnesty in the report. womens activism in afghanistan is nothing new the womens rights movement has grown substantially since and has fought for and achieved some very significant gains

@highlight
an amnesty international report calls for attacks on womens rights activists in afghanistan to be investigated

@highlight
the report examines the persecution of activists not only by the taliban and tribal warlords but also by government officials

@highlight
some activists continue their work despite their lives being at risk
