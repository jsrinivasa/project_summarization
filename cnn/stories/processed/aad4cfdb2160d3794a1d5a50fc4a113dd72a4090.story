the united states is urging china to release five young feminists who face years in prison over their campaign for gender equality. campaign group amnesty international said the new charge was less serious but still carried a maximum prison term of five years. protesters in several cities have called for their release and taken to social media with the phrase free the five as a hashtag. the women were doing nothing wrong nothing illegal they were simply calling for an end to sexual harassment said william nee china researcher at amnesty international

@highlight
the international community is calling for the release of the five women

@highlight
chinese authorities detained them last month over their campaign for gender equality
