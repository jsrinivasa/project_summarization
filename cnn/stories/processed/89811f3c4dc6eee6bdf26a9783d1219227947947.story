a former us navy aircraft carrier that survived a japanese torpedo strike and was a massive guinea pig for two atomic bomb blasts looks remarkably intact at the bottom of the pacific according to federal researchers who surveyed the wreck last month with an underwater drone. noaas survey of the carrier was conducted by the echo ranger an autonomous underwater vehicle provided by the boeing co the echo ranger traveled miles from its base in half moon bay california and hovered above the carrier which lies feet below the surface of the pacific ocean the drone used a threedimensional sonar system provided by coda octopus to get images that showed how well the warship has weathered years in the deep. after the war independence became part of a fleet used to measure the effects of atomic bomb tests at bikini atoll in the pacific on july it sat just yards from ground zero in the first test a air blast of a fission bomb similar to the one used over nagasaki japan a year earlier according to the comprehensive nucleartestban treaty organization twentyfour days later independence was yards from the center of a second atomic blast also a device but an underwater detonation. independence was seriously damaged by japanese torpedo planes during the battle of tarawa in late the ship returned to california for repairs and made it back across the pacific by july to participate in the battle of the sibuyan sea and the sinking of one of the japanese imperial navys biggest warships the battleship musashi later in the battle of cape engano planes from the independence were involved in the sinking of four japanese aircraft carriers

@highlight
uss independence was sunk in after weapons tests

@highlight
carrier was closein guinea pig to two atomic bomb tests

@highlight
agency ship looks remarkably intact feet below surface of the pacific ocean
