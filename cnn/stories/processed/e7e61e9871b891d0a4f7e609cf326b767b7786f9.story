would a taste of the finest swiss chocolate make you happy or soaking in the warm icelandic springs perhaps the great jazz and rock music scene in denmark is for you. looking at all that danes have to be happy about you wont notice the slight dip while youre enjoying jazz at the copenhagen jazz festival in july hanging out with the hipsters at queen louises bridge taking a canal tour or playing beach volleyball in front of the royal danish playhouse. switzerland took the top spot from denmark in rising from third to first place in this years list of the worlds happiest countries. denmark

@highlight
the world happiness report highlights the happiest countries

@highlight
people live longer and experience more generosity and social support in these counties

@highlight
the united nations first declared a world happiness day in
