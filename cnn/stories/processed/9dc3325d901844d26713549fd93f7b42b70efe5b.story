hiphop star nelly has been arrested on drug charges in tennessee after a state trooper pulled over the private bus in which he was traveling authorities said. the state trooper stopped the bus carrying nelly and five other people on interstate in putnam county on saturday because it wasnt displaying us department of transportation and international fuel tax association stickers according to tennessee authorities. the trooper was about to conduct an inspection of the bus a prevost motor coach when he noticed an odor of marijuana emitting from the vehicle authorities said in a statement. the rapper from st louis who shot to fame years ago with the track country grammar has been charged with felony possession of drugs simple possession of marijuana and possession of drug paraphernalia the tennessee department of safety and homeland security said

@highlight
state troopers say they found methamphetamine and marijuana on a bus carrying nelly and five others

@highlight
nelly has been charged with felony possession of drugs
