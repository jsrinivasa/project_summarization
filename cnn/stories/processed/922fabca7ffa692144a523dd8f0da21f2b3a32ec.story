its a bird its a plane its an insanely fast japanese bullet train. unlike traditional trains maglev trains work by using magnets to push the train away from the tracks and drive the train forward. a japan railway maglev train hit kilometers per hour miles per hour on an experimental track in yamanashi tuesday setting a decisive new world record. a spokesperson said the train spent seconds traveling above kilometers per hour during which it covered kilometers miles

@highlight
japanese maglev train sets new speed record kilometers per hour

@highlight
the train is planned to begin service in
