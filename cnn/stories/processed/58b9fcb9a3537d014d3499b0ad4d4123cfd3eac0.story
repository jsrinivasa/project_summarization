after years of making the case that the education of athletes is paramount the ncaa now says it has no legal responsibility to make sure education is actually delivered. even with pages of online information about academic standards and even though the ncaa has established a system of academic eligibility and accountability that it boasts of regularly ncaa attorneys wrote in this court filing that the ncaa did not assume a duty to ensure the quality of the education of studentathletes and the ncaa does not have direct daytoday operational control over member institutions like unc. theres nothing left the ncaa can claim it does that is beneficial to college athletes or society one has to wonder what does the ncaa do if it doesnt protect players if it doesnt play a role in the education of college athletics it begs the question of why does the ncaa exist and why does it have a tax exemption. for example before it lost a case filed by former ucla player ed obannon suing for the right of athletes to make money off their images and likenesses the ncaa stood on the pillar of amateurism insisting that college athletes are paid with an education

@highlight
in response to lawsuit ncaa says it doesnt control quality of education for studentathletes

@highlight
but its website emphasizes importance of education opportunities to learn

@highlight
lawsuit claims students didnt get an education because of academic fraud at unc
