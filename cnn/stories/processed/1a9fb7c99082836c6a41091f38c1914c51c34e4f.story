a skywest airlines flight made an emergency landing in buffalo new york on wednesday after a passenger lost consciousness officials said. the passenger received medical attention before being released according to marissa snow spokeswoman for skywest she said the airliner expects to accommodate the passengers on another aircraft to their original destination hartford connecticut later wednesday afternoon. skywest also said there was no problem with the planes door which some media initially reported. flight was originally scheduled to fly from chicago to hartford the plane descended feet in three minutes

@highlight
faa backtracks on saying crew reported a pressurization problem

@highlight
one passenger lost consciousness

@highlight
the plane descended feet in three minutes
