it lays claim to being the most isolated human settlement on earth a volcanic archipelago in the south atlantic ocean home to just under people. mitcham explained that most existing homes and public buildings on tristan da cunha are small bungalowtype structures made from made from a mix of concrete and local volcanic rock. that modernization happened roughly a decade after the island was evacuated temporarily due to ongoing volcanic activity before that period the main way for the islanders to communicate with the outside world was by morse code. now the tiny settlement of tristan da cunha is seeking the help of architects and designers the world over to secure its future well into the century

@highlight
tristan da cunha holds competition for architects and designers

@highlight
islands are most isolated inhabited archipelago on earth
