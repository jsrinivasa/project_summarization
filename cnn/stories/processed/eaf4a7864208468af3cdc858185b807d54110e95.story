five young chinese feminists whose detention has provoked an international outcry may face up to five years in prison over their campaign for gender equality. campaign group amnesty international said the new charge was less serious but still carried a maximum jail term of five years. wang said that wei had been subject to lengthy cross examinations during her detention but was well the last time they met on march two of the women are said to be in poor health. the detention of wei along with wu rongrong li tingting wang man and zheng churan has drawn harsh criticism from the international community

@highlight
five young women have been detained by china since early march

@highlight
they campaigned against sexual harassment

@highlight
their detention has attracted international criticism
