gunmen stormed the headquarters of somalias education ministry in the countrys capital on tuesday after a suicide car bombing a twopronged attack that killed at least people and injured others officials said. the attack began when two suicide bombers detonated their car at the entrance of the twostory building housing the ministry of education culture higher education somali national security ministry spokesman mohamed yusuf said. late last month alshabaab militants detonated a bomb and sprayed bullets at a mogadishu hotel leaving at least people dead in an attack that lasted a number of hours among those killed was yusuf mohamed ismail baribari somalias permanent representative to the united nations in geneva switzerland. somaliabased alshabaab has battled the countrys government for years aiming to overthrow it and turn the nation into a fundamentalist islamic state in recent years the group also launched terror attacks beyond somalias borders sometimes targeting nonmuslims

@highlight
alshabaab claims responsibility for the attack

@highlight
a car bomb explodes outside the front gate of the education ministry building in mogadishu

@highlight
assailants storm the building and engage in a gunbattle with guards
