ive been a chef in some form or another since i was years old but i didnt become a food activist until much later it was and a young girl my wife mentored was found rooting through the trash just to find something to eat that inspired me to found food policy action and become politically active in a range of food issues from hunger to factory farms. the rise in consumer knowledge has led to an increased demand for sustainably sourced food that is healthier and tastes better grace has relaunched the eat well guide an online directory of sustainable farms restaurants food coops and farmers markets that allows consumers to make better choices about the food they eat and provide for their families and we at food policy action have produced a legislative scorecard. to be sure americas food system is complex and some may argue that industrial farming has been a necessity to meet our countrys food needs however the quality of our food has been sacrificed at this supposed altar of necessity as industrial farms reap enormous profits that they direct toward lobbying rather than improving the quality of food they produce. we need to ask members of congress to promote sustainable farming not factory farms we need them to support sensible food policies that ensure that everyone has access to food and water congress should vote against the dark act which would block any federal or state action that required labels for foods made with genetically engineered ingredients even at the expense of the environment public health and local economies

@highlight
tom colicchio the meatrix relaunched is an important benchmark of the evolution of sustainable food movement

@highlight
but factory farms continued to reap large profits while producing subpar meat polluting nature and damaging our health

@highlight
colicchio we need to ask members of congress to promote sustainable farming
