this was the handshake that shook the western hemisphere. the meeting was so important that bernadette meehan national security council spokesperson issued a statement at the summit of the americas this evening president obama and president castro greeted each other and shook hands. president obama briefly met his cuban counterpart raul castro on friday night at a dinner for the dozens of latin american leaders convening in panama city for the summit of the americas. this was historic the two nations have barely been on speaking terms officially for more than years

@highlight
us president obama cuban president raul castro meet in panama city

@highlight
the two nations only miles apart have been at odds for more than years
