relations between iran and saudi arabia have always been thorny but rarely has the state of affairs been as venomous as it is today. iranian foreign minister javad zarif has provided a fourpoint plan to get a ceasefire to encourage the provision of humanitarian aid to promote political dialogue among warring yemeni parties and to achieve the formation of an inclusive government but the iranian proposal also asks for an end to saudi airstrikes as zarif put it iran and saudi arabia need to talk but we cannot talk to determine the future of yemen. iran provides financial support weapons training and intelligence to houthis gerald feierstein a us state department official and former yemen ambassador told a congressional hearing last week we believe that iran sees opportunities with the houthis to expand its influence in yemen and threaten saudi and gulf arab interests. three days after khameneis speech iran suspended religious pilgrimages to mecca this came as news broke about two iranian teenage boys who had reportedly been sexually assaulted by the police while visiting saudi arabia meanwhile antisaudi protests have been staged in a number of iranian cities

@highlight
vatanka tensions between iran and saudi arabia are at an unprecedented level

@highlight
iran has proposed a fourpoint plan for yemen but saudis have ignored it

@highlight
vatanka saudis have tried to muster a ground invasion coalition but have failed
