on march a cyberattack brought chaos to several banks and media outlets in south korea. but many experts say north korea appears to be investing more in cyberwarfare because it is cheaper than spending on conventional weapons and can cause significant economic damage to its southern rival indeed south koreas defense ministry estimates that north korea is operating a cyberarmy of workers as it focuses on strengthening its asymmetrical warfare capability. many in south korea believe not enough effort is being put into defending against cyberattacks a report by the korea institute for industrial economics and trade a governmentfunded think tank estimates that dark seoul caused about million worth of damage. the malicious codes used in the attack were same in composition and working methods as kimsuky codes known to be used by north korea the prosecutors office that leads other government agencies and internet companies in the investigation said in the statement in march

@highlight
south korean investigators say they have proof that north korea is launching cyberattacks

@highlight
reports say the north is investing heavily in digital warfare

@highlight
december attack on banks in south korea caused about million worth of damage a report says
