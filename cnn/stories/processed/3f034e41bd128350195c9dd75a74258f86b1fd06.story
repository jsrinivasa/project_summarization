former rap mogul marion suge knight was ordered thursday to stand trial for murder and other charges stemming from a deadly hitandrun confrontation on the movie set of the biopic straight outta compton earlier this year. the deadly incident happened on january after a flareup on the set of the biopic straight outta compton a film about the highly influential and controversial rap group nwa the alleged argument spilled over to the parking lot of tams burgers in compton. in all knight will stand trial on one count of murder one count of attempted murder and one count of hitandrun the judge ruled after holding a twoday preliminary hearing this week that ended thursday. the judge dismissed the other hitandrun count because california law says no more than one charge of hitandrun should be brought against a defendant when the same weapon in this case the vehicle knight was driving is used against several people

@highlight
former rap mogul marion suge knight will be tried for murder in a videotaped hitandrun

@highlight
his bail is reduced to million from million

@highlight
a judge dismisses one of four charges against knight
