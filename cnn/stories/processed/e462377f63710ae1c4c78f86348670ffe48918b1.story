in the end it played out like a movie a tense heartbreaking story and then a surprise twist at the end. as eight of mary jane velosos fellow death row inmates mostly foreigners like her were put to death by firing squad early wednesday in a wooded grove on the indonesian island of nusa kambangan the filipina maid and mother of two was spared at least for now. her family was returning from what they thought was their final visit to the prison on socalled execution island when a philippine tv crew flagged their bus down to tell them of the decision to postpone her execution. we are so happy so happy i thought i had lost my daughter already but god is so good thank you to everyone who helped us

@highlight
indonesia executed eight drug smugglers wednesday morning

@highlight
mary jane veloso was meant to be the ninth but was given reprieve

@highlight
supporters family overjoyed indonesia stresses its just a delay
