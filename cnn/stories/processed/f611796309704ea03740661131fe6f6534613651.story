about hours after the boston marathon started monday many of the cheering crowds had dispersed and the streets were cleared but one man despite the odds against him crossed the finish line. his perseverance was celebrated by crowds at the marathon finish line tuesday morning and also by fans online. melamed is one of about participants who raced in this years marathon which comes less than two weeks after a jury found dzhokhar tsarnaev guilty of all charges related to the boston bombings. friends who were waiting for melamed to cross the finish line said the university professor and motivational speaker is dedicated and motivated

@highlight
maickel melamed who has muscular dystrophy took part in the boston marathon

@highlight
he completed the race hours after the start

@highlight
despite rainy weather fans and friends cheered for the
