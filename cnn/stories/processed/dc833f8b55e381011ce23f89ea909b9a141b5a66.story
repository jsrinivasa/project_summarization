as goes walmart so goes the nation. but it was the opposition from walmart the ubiquitous retailer that dots the american landscape that perhaps resonated most deeply providing the latest evidence of growing support for gay rights in the heartland. walmart which employs more than people in arkansas emerged victorious on wednesday hours after the companys ceo doug mcmillon called on republican gov asa hutchinson to veto the bill the governor held a news conference and announced he would not sign the legislation unless its language was fixed. in the walmart pac gave around million to republicans versus less than to democrats according to data from the center for responsive politics that gap has grown less pronounced in recent years in the pac spent about million to support republicans and around for democrats

@highlight
while republican gov asa hutchinson was weighing an arkansas religious freedom bill walmart voiced its opposition

@highlight
walmart and other highprofile businesses are showing their support for gay and lesbian rights

@highlight
their stance puts them in conflict with socially conservative republicans traditionally seen as allies
