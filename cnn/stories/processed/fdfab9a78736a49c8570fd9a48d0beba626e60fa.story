the reserve deputy who shot a suspect with his firearm rather than his stun gun and another deputy who can be heard cursing at the suspect after he was shot were not in their normal states of mind because of the elevated stress of apprehending the suspect according to a tulsa oklahoma investigator. one deputy thought he was going to have to shoot this person at the arrest site its very upsetting when you think you are going to have to take someones life and this deputy one of the involved deputies was upset he said secondly this is total stress they are going after a dangerous suspect that they have no idea whether or not this person is armed. tulsa police sgt jim clark who has been brought in to review the case said tulsa county reserve deputy robert bates inadvertently shot eric courtney harris after harris a possibly pcpaddled felon who had days prior sold methamphetamine to an undercover officer ran from authorities after trying to sell an illegal handgun during an undercover sting. reserve deputy bates did not commit a crime reserve deputy bates was a victim a true victim of slip and capture he said theres no other determination i could come to

@highlight
slip and capture explains why deputy shot suspect investigator says

@highlight
sheriffs office says a reserve deputy thought he had pulled out a taser

@highlight
instead he shot the suspect who later died at a local hospital
