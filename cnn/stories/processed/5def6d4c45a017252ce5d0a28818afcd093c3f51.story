despite the glitz and glamour miami is known for the odds for some children growing up there are bleak. his program operates in two of miamis poorest communities north miami and allapattah it pairs middleschool students with professional musicians providing free instrument instruction and mentorship. as professional musicians most of us know what kind of power music has in terms of dealing with the things that are going on in your life and the reasons that we target the neighborhoods that we have the program in are because these kids are facing the most challenges with being successful in and out of school a lot of times these kids only see to the end of their block and back we like to bring them to studios and also to other places in miami because we want them to experience something outside of their own neighborhood. those atrisk children are ones that chad bernstein is trying to help through his nonprofit guitars over guns

@highlight
hero chad bernstein started music program that helps atrisk middle school students

@highlight
nonprofit group guitars over guns pairs miamiarea kids with professional musician mentors

@highlight
heroes
