a former us army enlistee who posted on facebook about the adrenaline rush of dying in jihad was arrested friday and charged with trying to detonate a car bomb at fort riley military base in kansas authorities said. booker acquired components for a bomb and rented a storage locker to store the components the complaint said the plan was for confidential informants to build a bomb and for booker to drive to fort riley and detonate it the complaint said but the bomb was built with inert parts and would never explode the complaint said. booker enlisted in the army last year and was due to ship out to basic training april said army spokesman wayne hall the criminal complaint said the fbi questioned him march about comments posted on facebook such as getting ready to be killed in jihad is a huge adrenaline rush i am so nervous not because im scare to die but i am eager to meet my lord. the fbi said agents interviewed blair after bookers arrest blair said he shared some of bookers views knew of his plans to detonate a vehicle bomb at fort riley and loaned him money to rent storage space according to the fbis criminal complaint he said he thought booker would carry out his plan but did not contact authorities the complaint said

@highlight
alexander blair of topeka accused of knowing about bomb plot but not contacting authorities

@highlight
fort rileys security was never breached and the device was inert and not a threat authorities say

@highlight
john t booker jr of topeka had acquired bomb parts and made a propaganda video the justice department says
