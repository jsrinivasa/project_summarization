robert lewis burns jr the original drummer in southern rock band lynyrd skynyrd died friday night in a car crash a georgia state patrol spokesman said. lynyrd skynyrd changed members over the years as it produced rock anthems including sweet home alabama and freebird burns left the band before its third studio album nuthin fancy in exhausted by touring according to the bands rock and roll hall of fame biography. the band was inducted into the rock and roll hall of fame in lynyrd skynyrd still tours with rossington the only original member still in the band. burns died after his car hit a mailbox and a tree in cartersville spokesman james tallent said

@highlight
robert lewis burns jr was part of lynyrd skynyrds original lineup

@highlight
his car hit a mailbox and a tree just before midnight
