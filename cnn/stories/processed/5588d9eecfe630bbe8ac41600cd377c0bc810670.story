the hollywood reporterstan freberg whose freewheeling comic career in advertising garnered him worldwide acclaim and whose satirical entertainments abounded on tv the radio and on records has died he was. the los angeles native had hit records of his own including st george and the dragonet a sendup of the series dragnet his recordings were so popular that he landed his own radio program in thats rich three years later he presented the stan freberg show on cbs radio where he regularly mocked commercials by advertising bogus products. the godfather of humorous and irreverent commercials freberg lampooned cultural institutions and described himself as a guerilla satirist the new york times dubbed him the che guevara of advertising and years later weird al yankovic called him a major influence on his career. freberg died of natural causes at a santa monica hospital his son and daughter donavan and donna freberg confirmed to the hollywood reporter

@highlight
stan freberg was famed comedian song parodist

@highlight
he later became adman did a number of outrageous commercials

@highlight
weird al yankovic a legend an inspiration and a friend
