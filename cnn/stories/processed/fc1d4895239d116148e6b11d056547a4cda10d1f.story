robert bates shot and killed a man while playing cops and robbers with real police. from a policing perspective there wasnt even good reason to use a taser against harris cops were on scene harris wasnt getting the upper hand he wasnt going anywhere and despite what bates would later claim harris was not running like a man with a gun in fact harris was running fast and his arms were pumping very much like a man who is not protecting a gun in his waistband. he had taken part in more than operations with the violent crimes task force according to his lawyer on april bates thought he was going to use a taser on eric harris who deputies had just tackled after he sold an undercover officer a lugar pistol and then took off running but bates wasnt holding a taser he was holding his gun he fired one shot and killed harris. he was too old to be policing the streets tulsa police said that bates had served a year in as a police officer most police departments have mandatory retirement ages federal lawenforcement officers for instance retire at

@highlight
peter moskos reserve cop meant to use a taser on a man but shot him dead instead why was a volunteer cop witha gun in a violent crimes unit

@highlight
he says the man may have bought his way in with donations to police cops are and should be wary of those a little too eager to be police

@highlight
moskos right approach is unarmed auxiliary cops like in nyc volunteering as a way to connect public to police
