the desks of the small madrassa are empty its students all male are staying home after kenyan president uhuru kenyatta announced three days of national mourning following last weeks deadly attack at a nearby university. the kenyan interior ministry has said at least one of the four gunmen who carried out the attack on the university was also kenyan abdirahim abdullahi was in his and the son of a government chief his father says he lost contact with his son in shortly after he left university the kenyan government is concerned that alshabaab is recruiting disaffected youth from inside the country. our task of countering terrorism has been made all the more difficult by the fact that the planners and financiers of this brutality are deeply embedded in our communities president kenyatta said during an address to the nation in the aftermath of the massacre. he was someone who was very quiet he didnt like too much talk recalls sheikh khalif abdi hussein the principal at the madrassa he says he also taught with mohamud for two years

@highlight
the attack at a garissa university last week killed people mostly students

@highlight
mohamed mohamud taught at a madrassa in the kenyan town

@highlight
authorities fear the rise of homegrown terrorists in the african country
