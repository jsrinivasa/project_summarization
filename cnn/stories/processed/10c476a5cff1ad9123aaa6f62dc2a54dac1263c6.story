the greater adjutant stork is a majestic bird. for the past seven years bouldry has traveled the world photographing landfills hes visited places such as haiti venezuela and colombia the greater adjutant stork initially drew bouldry to boragaon but he became connected with the people. the dirty wet conditions of the landfill attracted the endangered stork and the stork attracted bouldry through a series of photos taken within a day he captures what its like to live inside one of the largest dumping grounds in india. standing about feet tall with an average wingspan of feet it soars over the boragaon landfill like a great protector it knows the residents and shies away from strangers

@highlight
photographer timothy bouldry spent time at a massive landfill in guwahati india

@highlight
about families live inside the boragaon landfill but bouldry said they are content
