americans on the united states nofly list will now be privy to information about why they have been banned from commercial flights and be given the opportunity to dispute their status according to court documents filed by the justice department this week. in cases in which travelers included on the list request to receive or submit more information about their status the government will provide a second more detailed response identifying specific criterion under which the individual has been placed on the no fly list according to the court documents. those who appear on the nofly list will then have further opportunity to dispute their status in writing with supporting materials or exhibits and will receive a final written decision from the transportation security administration. before the change american citizens and permanent residents who inquired with the government about being denied aircraft boarding received a letter that neither confirmed nor denied their inclusion on the nofly list now theyll be made aware of their status if they apply for redress with an option to request further information

@highlight
americans on the nofly list will now get info about why theyve been banned from flights

@highlight
aclu says the policy still denies meaningful notice evidence and a hearing
