ever written yourself a note stuffed it in your pocket and headed out into the world only to discover that somewhere along your journey the piece of paper has disappeared. if you live in the uk theres a small chance that note has found its way into the hands of daisy bentley for the past six years the londonbased artist has scoured the streets of cities and towns looking for those odd bits of paper that flutter to the ground when their owners arent looking. i always naturally collected things as many artists do and it got to the point that i was picking up every one i saw now i can barely walk down a street without picking up a scrap of paper. bentley began collecting them six years ago after a note caught her eye one rainy night on a walk in her home town of norwich england

@highlight
a selection of notes from british artists collection goes on display in london

@highlight
artist spent six years trawling streets finding scraps of paper detailing peoples lives

@highlight
in era of smartphones and social media notes provide reminder of power of handwritten word
