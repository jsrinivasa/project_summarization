indiana gov mike pence issued an executive order monday to extend a public health emergency in his state in response to a rampant hiv outbreak that first began in middecember. the emergency order was first issued last month and set to expire friday but now will be in place until may it calls on multiple state agencies to coordinate a response to the unprecedented outbreak and provides additional resources law enforcement emergency agencies and health officials are working together most notably a temporary needle exchange program that began april as of tuesday more than clean syringes had been distributed and more than used needles had been turned in according to the indiana department of health. pence issued an order in march for scott county which then had confirmed cases of hiv since middecember. in extending the public health emergency declaration the governor said while weve made progress in identifying and treating those affected by this heartbreaking epidemic the public health emergency continues and so must our efforts to fight it

@highlight
gov mike pence extends public health emergency by days

@highlight
cases of hiv have been confirmed since middecember

@highlight
more than needles have been distributed through temporary needle exchange program
