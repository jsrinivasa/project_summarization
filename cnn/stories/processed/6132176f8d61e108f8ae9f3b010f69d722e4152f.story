the mass killings of armenians in the ottoman empire which began years ago friday is said by some scholars and others to have been the first genocide of the century even though the word genocide did not exist at the time. also a year ago on the eve of the anniversary of red sunday thenturkish prime minister nowpresident recep tayyip erdogan offered condolences for the mass killings which he said had inhumane consequences while turkey vehemently continues to reject the word genocide his remarks went further than those of any previous turkish leader in acknowledging the suffering of armenians. the ottoman turks having recently entered world war i on the side of germany and the austrohungarian empire were worried that armenians living in the ottoman empire would offer wartime assistance to russia russia had long coveted control of constantinople now istanbul which controlled access to the black sea and therefore access to russias only yearround seaports. the word genocide was invented in by a polish lawyer named raphael lemkin to describe the nazis systematic attempt to eradicate jews from europe he formed the word by combining the greek word for race with the latin word for killing

@highlight
the anniversary of the start of the mass killings will be commemorated friday

@highlight
turkey and others reject the use of the word genocide

@highlight
most estimates of the deaths fall between and million
