former rap mogul marion suge knight pleaded not guilty thursday to murder and all other charges related to a fatal hitandrun incident in january in compton california. the deadly incident occurred about miles south of downtown los angeles on january after a flareup on the set of the biopic straight outta compton a film about the highly influential and controversial rap group nwa at the time knight was out on bail in a separate robbery case. knight faces one count of murder for the death of terry carter one count of attempted murder in the case of cle bone sloan who was maimed in the incident and one count of hitandrun. the alleged argument spilled over to the parking lot of tams burgers in compton the hitandrun was captured on videotape which shows knight inside a red truck

@highlight
former rap mogul marion suge knight is accused of murder in a videotaped hitandrun

@highlight
judge declines to reduce his bail from million
