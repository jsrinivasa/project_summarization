silently moving deep beneath the oceans surface combat submarines can employ the element of surprise to carry out devastating attacks on naval fleets and land targets. mulloy who is deputy chief of naval operations for capabilities and resources says chinese submarines are still technologically inferior to those used by the united states but that margin of difference is shrinking. concern that china could match us underwater capabilities in the near future has encouraged the development of an unmanned drone ship to independently track enemy ultraquiet diesel electric submarines over thousands of miles to limit their tactical capacity for surprise. however officials say china and other nations are rapidly expanding the size and scope of their own submarine forces and according to a report by the center for strategic and budgetary assessments the us must rethink the role of manned submarines and prioritize new underwater detection techniques

@highlight
us navy is developing an unmanned drone ship to track enemy submarines to limit their tactical capacity for surprise

@highlight
the vessel would be able to operate under with little supervisory control

@highlight
advances are necessary to maintain technological edge on russia and china admiral tells house panel
