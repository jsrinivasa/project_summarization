the american pharmacists association is discouraging its members from participating in executions on monday the group voted at its annual meeting to adopt a ban as an official policy stating that such activities are fundamentally contrary to the role of pharmacists as healthcare providers. the new declaration by the american pharmacists association aligns with positions held by other professional medical organizations such as the american medical association the american nurses association and the american board of anesthesiology. the group acted this week because of increased public attention on lethal injection said michelle spinnler spokeswoman for the american pharmacists association. pharmacists should not be involved in preparation of these products or involved in executions in any other way she says

@highlight
the american pharmacists association passed a new policy banning members from participating in lethal injections

@highlight
pharmacists say role as health care providers conflicts with participation in lethal injection

@highlight
the pharmacy association first adopted a policy against lethal injection in
