film critic richard corliss whose populist passion for all genres of movies illuminated time magazines coverage of cinema for years died thursday night in new york city he was. born in philadelphia corliss moved to new york after college and began writing film reviews for a variety of publications before joining time in he served as editor of film comment the movie journal of the film society of lincoln center for years was a frequent guest on charlie roses talk show and made annual pilgrimages to film festivals in cannes toronto and venice. unlike some critics corliss appreciated all kinds of movies from the arty drama of ingmar bergman to the epic fantasy of the lord of the rings his alltime top movies list which he compiled with fellow time critic richard schickel contained everything from pulp fiction to finding nemo to jackie chans drunken master ii. corliss died a week after suffering a major stroke according to a tribute on times website by colleague richard zoglin who called him perhaps the magazines most quoted writer of all time

@highlight
veteran time magazine film critic richard corliss died thursday night in new york city

@highlight
corliss reviewed more than movies and authored four books on film
