universals furious continues to build momentum at the friday box office for a weekend debut in the million range the largest opening in north america since fall. the final film featuring the late paul walker furious is opening around the globe this weekend and earned a recordbreaking million internationally on wednesday and thursday for a possible worldwide debut approaching or crossing million by the end of easter sunday. furious is getting the widest release in universals history domestically it will be playing in theaters by good friday internationally it has booked more than screens in territories although it wont open in china japan and russia until later. furious is likewise poised to nab the biggest opening of to date and it will easily beat the million launch of the hunger games mockingjay part in november making it the largest threeday opening since the hunger games catching fire million in november

@highlight
the final film featuring the late paul walker furious is opening around the globe this weekend

@highlight
its worldwide debut may approach or cross million by the end of easter sunday
