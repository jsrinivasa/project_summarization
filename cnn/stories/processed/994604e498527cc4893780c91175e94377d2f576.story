hundreds of thousands of brazilians are taking to the streets in protests across the country lashing out against president dilma rousseff as she struggles with an economic downturn and a massive bribery scandal the demonstrators have called for the president to be impeached on the other side rousseffs base is holding rallies in her support. but with the petrobras scandal growing and the economy sinking the protests have gotten bigger and broader with many demonstrators saying they initially voted for rousseff protesters say rousseff should be impeached for failing to halt the corruption at petrobras. she says shes given prosecutors and the federal police free rein to investigate the petrobas scandal some of her allies who have been implicated in the investigation think rousseff should do more to protect them this scandal has been known publicly for at least a year and during rousseffs election campaign she said she would root out corruption. both sides are the country was already sharply divided during presidential elections in october roughly half of the voting population didnt vote for rousseff and many of those same people joined protests immediately after elections

@highlight
protesters angry over bribery scandal involving staterun oil company petrobras

@highlight
brazilian president dilma rousseff also is struggling with an economic downturn
