adrianne hasletdavis and her husband adam davis were standing near the finish line on the day of the boston marathon bombing. it was the second day the boston jury heard a procession of heartbreaking loss the survivors and families of those killed when twin bombs planted by the pair of brothers named tsarnaev exploded near the finish line. i dont care if its illegal i need to talk to you because these might be our last words i said i was in a terrorist attack at the boston marathon and adam is dead and this might be it for me. hasletdavis and davis who had just returned from a tour in afghanistan with the air force were steps away from the explosion near the forum restaurant

@highlight
jurors in sentencing phase in dzhokhar tsarnaevs trial hear of loss

@highlight
victims testify about the impact of the bombing on their lives
