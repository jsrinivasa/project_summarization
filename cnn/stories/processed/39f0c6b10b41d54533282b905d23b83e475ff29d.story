as a boat packed with hundreds of migrants capsized in mediterranean waters many were trapped inside behind locked doors. as rescuers approached authorities say migrants on the boat moved to one side hoping to be saved their movement caused the large multilevel boat to capsize about kilometers almost miles north of libya sending the desperate crowd plunging into the sea their chance of survival slim. it was the latest in a series of dangerous voyages for hundreds of men women and children who boarded the boat in libya hoping to make it safely to europe passengers on the boat were from a number of nations including algeria egypt somalia niger senegal mali zambia bangladesh and ghana prosecutors said. united nations high commissioner for refugees antonio guterres said that the incident could be worse than an incident last week in which refugees and migrants died in the mediterranean

@highlight
a survivor tells authorities that migrants were trapped behind locked doors

@highlight
rescuers say they have found scores of bodies in the waters off libya
