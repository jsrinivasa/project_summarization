on the floor of roppongi hills an upscale tokyo highrise full of offices restaurants and residences two actors in kimonos sit on a tatami mat and glare at each other as artificial smoke billows around them. in youtube is striving to improve the quality of its content by investing in youtube spaces like the one in tokyo where creators like tokyo independent filmmaker jr lipartito have access to professional studios training and many resources once out of reach for low budget productions. theyre building sets we have high quality cameras high quality sets high quality actors says bob werley one of the actors in the crawler in the dark. the tokyo space is one of five worldwide available for free to youtube partners who create content and share ad revenue with the website

@highlight
april marks years since first video me at the zoo was uploaded to youtube

@highlight
site gets billions of views every day hours of uploads every minute

@highlight
new studios being opened for budget filmmakers to improve quality of output
