editors note july questions have arisen about the identity of the girl who dr sanjay gupta helped operate on during a week in nepal in the aftermath of a devastating earthquake cnn is looking into those questions and will update our coverage as warranted gupta helped doctors at bir hospital in kathmandu perform a craniotomy in a makeshift operating room on a young patient as described in this story it is the identity of the patient that is in question. bir hospital a government facility is one of the busiest in kathmandu. the girl has received some treatment at another hospital but has been brought to bir in the hopes her life can be saved. many of the wounded are now across the road from the hospital at the nepal army pavilion a huge open space in central kathmandu and tarps have been erected at the front of the hospital for people to have shelter

@highlight
patients flood hospitals in nepalese capital after devastating earthquake

@highlight
s dr sanjay gupta helps with girls operation at nepalese medical teams request

@highlight
this is as bad a situation as ive ever seen gupta says
