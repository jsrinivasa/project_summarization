a natural gas line explosion at a law enforcement shooting range in fresno california injured people including some inmates who were on a work detail there. the most serious injuries were suffered by a group of inmates who were assigned to maintenance and cleaning work at the sheriffs firing range. there were inmates near the blast site who were also injured officials said three other inmates at the site were not hurt earlier the sheriffs office included them in the injury count. others being treated include a county road worker and two sheriffs deputies fresno county sheriff margaret mims said

@highlight
the cause of a gas line explosion in fresno california is unknown

@highlight
two of the injured were undergoing emergency surgery
