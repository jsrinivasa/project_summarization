falih essawi shouted on the phone as he described his situation from his point of view isis militants might be just hours away from taking the key iraqi city of ramadi. essawi said isis militants made significant advances wednesday in three areas east of ramadi albu soda albu ghanem and parts of soufia which leads to central ramadi later he said that militants were rolling into the center of ramadi. the politician said he was on a front line himself armed with a machine gun security was collapsing rapidly in the city and he begged the iraqi government for reinforcements and the usled coalition against isis for air support he stressed that urgent support from the military and security forces is needed to save the city. the extremist groups offensive in ramadi shows its resilience despite months of usled airstrikes and its recent defeat by iraqi forces in the northern city of tikrit

@highlight
the fall of ramadi is not imminent

@highlight
official in ramadi says its unclear how long government forces can hold out there

@highlight
he begs the iraqi government for reinforcements and the usled coalition for airstrikes
