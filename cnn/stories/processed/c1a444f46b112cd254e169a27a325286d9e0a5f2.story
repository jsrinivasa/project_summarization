thousands remain missing in nepal after a devastating earthquake struck the region on saturday a majority of them are nepalese indian and chinese residents but a handful are adventurers trekkers and vacationers who have not been heard from since the catastrophe. tashnova was with a group of friends at the last resort a spalike resort near the border with china when the earthquake struck she was getting ready for a group excursion a canyon swing. see scenes from nepal after the earthquake. dr carol pineda and her husband michael macdonald of massachusetts were vacationing in nepal when the quake struck her brother james pineda got news of the disaster from a friend

@highlight
thousands remain missing in nepal after a magnitude earthquake struck

@highlight
social media has helped people overseas tracked down their loved ones in nepal

@highlight
technology has helped those stranded after the quake reach out for help
