a new jersey auction house has removed items from its april event after an uproar from the public. there is an essential discussion to be had about the sale of historical items that are a legacy of mans inhumanity to man it extends beyond what is legal it is something auction houses galleries and dealers are faced with regularly the auction house said we hope this controversy will be the beginning of a discourse on this issue. miriam tucker a partner with the auction house said it had hoped the items would go to someone who cared about their historical meaning. a grassroots campaign of a changeorg petition a facebook page and mediation by star trek actor george takei has resulted in rago arts and auction center agreeing to pull the items from the sale

@highlight
affiliate reports

@highlight
auctioneer hoped they would be bought by museum or someone who would donate them for historical appreciation

@highlight
japaneseamericans were furious about items from family members others being sold
