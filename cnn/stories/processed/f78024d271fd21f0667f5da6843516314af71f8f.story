standing up for what you believe what does it cost you what do you gain. faced with threats against business theyre still weighing the cost. memories pizza in the indiana town of walkerton is finding out. the familyrun restaurant finds itself at the center of the debate over the states religious freedom restoration act after its owners said theyd refuse to cater a samesex couples wedding

@highlight
indiana towns memories pizza is shut down after online threat

@highlight
its owners say theyd refuse to cater a samesex couples wedding
