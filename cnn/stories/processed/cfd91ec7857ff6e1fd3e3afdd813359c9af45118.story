a brutal raid on the garissa university college in kenya has left nearly people dead including students and dozens more wounded. but that didnt mean alshabaab was calling it quits in january french forces attempted to rescue a french intelligence commando held hostage in somalia by the group the raid left the soldier dead another soldier missing and islamist fighters dead. it has a history of striking abroad too before admitting to the kenya quarry attack alshabaab claimed responsibility for the july suicide bombings in kampala uganda that killed more than people including a us citizen who had gathered at different locations to watch the broadcast of the world cup final soccer match. in after attacks on tourist destinations in northern kenya blamed on alshabaab the kenyan government ordered a crossborder incursion aimed at creating a security buffer zone in southern somalia

@highlight
alshabaab is an alqaedalinked militant group based in somalia

@highlight
it claimed responsibility for the deadly attack at a kenyan mall in september

@highlight
the group has recruited some americans
