erika langhart had a zest for life by the time she finished college she had already visited countries after graduating from college she was working in washington and thinking about going to law school her life was full of promise but all that ended suddenly when she was just years old. they had determined that erika had no brain activity and that because of her heart attacks they basically told us that she was brain dead and thats it rick langhart said fighting back tears. with groceries in hand erikas boyfriend sean coakley had arrived at her apartment to make dinner and found erika collapsed on the floor the fire department and paramedics were already on the scene the attendant at the front desk had heard erika screaming for help and called. and thinking about what happened to her classmate erika henry said i think if i knew what i know now and you know if erika had known that a number of people i think that they would have made a slightly different choice

@highlight
the nuvaring is one of the most popular birth control products on the market

@highlight
lawsuit cites a heightened risk of blood clots associated with the use of nuvaring

@highlight
maker there is substantial evidence to support the safety and efficacy of nuvaring
