indonesia has a tough stance on drug smugglers and since assuming office in october president joko widodo has made it clear he intends to show no mercy toward those found guilty of such crimes. that tough stance casts a further pall on two australian drug smugglers part of the socalled bali nine. tan duc thanh nguyen from brisbane who is now was one of four drug couriers in the case he was found in a hotel room on balis kuta beach with a small amount of heroin and drug paraphernalia. indonesian prosecutors asked for and received a sentence of life in prison for several of the bali nine who were identified as drug couriers in the operation that includes scott rush of brisbane australia rush was when he was captured in indonesia he was arrested at denpasar airport with more than kilogram of heroin strapped to his body

@highlight
two australian drug smugglers part of the bali nine await word whether theyll face a firing squad

@highlight
less is known about the seven other members of the group
