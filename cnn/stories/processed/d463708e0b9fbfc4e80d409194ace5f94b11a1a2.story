a day after the chinese government released five young feminists on bail their families and supporters expressed mixed emotions on the unexpected development. kerrys predecessor hillary clinton a longtime champion of womens rights who just announced her bid for the us presidency called the jailing of the feminists inexcusable on twitter joining the free the five hashtag campaign. i cant hide my happiness for the women but being released on bail is not the end of their ordeal echoed lis lawyer yan xin on chinese social media without closing their cases they still cant live without shackles well have our work cut out for us. supporters of the woman activists however sense the chilling effect of their ordeal on chinas nascent civil society as the ruling communist party under president xi jinping continues to tighten its grip over the country

@highlight
wei tingting wang man zheng churan li tingting and wu rongrong freed

@highlight
theyre still considered suspects in an ongoing criminal investigation may face charges in the future

@highlight
they will be under surveillance for a year with their movements and activities restricted
