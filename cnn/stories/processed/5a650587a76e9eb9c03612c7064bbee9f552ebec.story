the vii summit of the americas was supposed to be all about the symbolic handshake between the united states and cuba but insert venezuela into the mix and panama city panama quickly turns into a triangle of tension. heads of state from countries in the western hemisphere have met every three years to discuss economic social or political issues since the creation of the summit in cuba has historically been the wrench in the diplomatic machinery with some latin american leaders threatening not to attend the summit of the americas if the united states and canada didnt agree to invite president raul castro. maduro didnt stop there he has been rallying other latin american leaders including bolivian president evo morales ecuadors rafael correa and nicaraguas daniel ortega but perhaps most damning for the united states and creating the triangle of tension at the summit cuban foreign minister bruno rodriguez has sided publicly with maduro. while the world watches for the photoop of obama and castro its unclear if more latin american diplomats will side with maduro and for america the vii summit of the americas could go from mi casa es su casa to a walk into the lions den

@highlight
us venezuelan relations threaten to overshadow obama castro meeting

@highlight
venezuelan president says united states moved to oust him he has the support of the cuban foreign minister
