dzhokhar tsarnaevs bombs tore through their bodies singeing flesh shattering bones shredding muscles and severing limbs but on tuesday jurors also began to hear about the holes his bombs left in the hearts of the survivors and the families of the dead. to understand the toll tsarnaevs bombs took jurors must know the stories of his victims the prosecutor said. by contrast jurors also were left with an indelible image of tsarnaev taken when he was in a holding cell in the very courthouse where the trial is being held it is dated july the day of his arraignment on charges he deliberately set off the deadly bombs at the boston marathon. the people who were maimed by tsarnaevs bombs are also testifying during the sentencing phase of the trial as prosecutors try to show the impact on victims of the attacks

@highlight
the sentencing phase in dzhokhar tsarnaevs trial begins in a federal court in boston

@highlight
prosecutor shows pictures of the four victims and tsarnaev flipping his middle finger

@highlight
victims testify about the impact of the bombing on their lives
