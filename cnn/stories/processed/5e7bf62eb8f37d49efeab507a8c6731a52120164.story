a bus collided with a fuel tanker in southern morocco on friday a fiery crash that killed at least people most of them children state and local media reported. the crash happened near the city of tantan just before am the maghreb arabe presse state news agency reported it said a number of people were also injured. the accident caused a fire that hollowed out the bus leaving little more than its frame debris scattered across the road while smoke rose into the sky videos posted to social media show. after the crash photos appeared on social media of young smiling boys in soccer uniforms along with condolence messages such as may god have mercy on them and oh no they are so young

@highlight
most of the victims were children according to reports

@highlight
condolence messages appear online with images of boys in soccer uniforms

@highlight
the bus collided with a fuel tanker near the southern moroccan city of tantan
