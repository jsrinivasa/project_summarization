australia an important ally of the united states has agreed to share some of its intelligence with iran. during my discussions with the national leadership here it was agreed that we could share intelligence particularly on the foreign terrorist fighters from australia who are taking part in this conflict in iraq she said the iranians were very agreeable to share that information with us she added. iran has a long standing relationship with iraq and as you point out they have a very strong military presence there they also have an influence over the shia militia who are operating within iraq she said so they are in iraq in places that we are not they also have a very sophisticated intelligence network and they have a lot of information that theyve been gathering. like many western nations australia is grappling with the problem of citizens who are choosing to travel to iraq and syria to join isis which calls itself the islamic state

@highlight
australian foreign minister says deal is to focus on tracking citizens who join isis

@highlight
but one lawmaker describes it as dancing with the devil
