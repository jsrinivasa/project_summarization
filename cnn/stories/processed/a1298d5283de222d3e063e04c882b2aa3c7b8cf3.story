the children laugh and shriek as some of them seem to always have the capacity to do no matter how depressing the circumstances. they left at midnight ahlam cradled the baby as her two other children ages and years old clutched at her clothes she prayed the baby wouldnt cry that the children could keep walking. mahmoud was out running errands when isis fighters arrived taking his wife ahlam their three children the youngest of which was just a month old and his elderly parents. mahmoud gently caressing his daughters palm says he could hardly believe that the woman whose stunning eyes and gentle words he had fallen in love with and their three children were by his side again it had been eight agonizing months

@highlight
the shariya refugee camp opened around six months ago made up of tents and counting

@highlight
the vast majority of the camps occupants are from the town of sinjar and fled an isis assault

@highlight
but ahlam her children and their grandparents were taken captive
