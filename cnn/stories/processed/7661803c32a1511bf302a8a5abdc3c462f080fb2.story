the stark video of a south carolina officer gunning down an apparently unarmed black man as he ran away with his back to police has prompted an equally fastmoving reaction by officials and the public alike. later in the video when the officer approaches scotts body he drops a dark object next to the man its not clear whether it is the taser. north charleston police arent involved in the investigation into the shooting and have turned the matter over to the south carolina law enforcement division. the video is being dissected frame by frame by authorities and media outlets all in an effort to reconstruct what exactly happened between north charleston police officer michael slager a fiveyear employee of that force and walter scott

@highlight
victims brother says he felt anger and happy at the same time upon seeing video

@highlight
officer michael slager pulls over scott at am saturday

@highlight
video shows the officer firing eight times as scott runs away with his back to police
