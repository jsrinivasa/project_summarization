actress mindy kalings brother says that he posed as a black man years ago to get into medical school and that the experience opened his eyes to what he calls the hypocrisy of affirmative action. whatever you feel about affirmative action lets consider that one persons experience over a decade and a half ago an experience that ultimately didnt yield any deluge in acceptance letters anyway is not really indicative of the current state of college admissions wrote salons mary elizabeth williams. affirmative action has been in the news a lot the past few years with a supreme court ruling that tightened how affirmative action admissions programs have to be structured and a ruling that upheld the university of michigans ban on the use of race in admissions. so i shaved my head trimmed my long indian eyelashes and applied to medical school as a black man he wrote on the website my change in appearance was so startling that my own fraternity brothers didnt recognize me at first

@highlight
vijay chokalingam says he pretended to be black to get into medical school

@highlight
he says the experience showed him that affirmative action is a flawed system
