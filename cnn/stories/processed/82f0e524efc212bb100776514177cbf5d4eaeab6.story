from late january when new england was living through one of its bleakest and snowiest winters to a warm and sunny afternoon in april the jurors in the firstdegree murder trial of former nfl star aaron hernandez have considered how a promising young athlete who earned millions came to take the life of his onetime friend and future brotherinlaw odin lloyd. the families of de abreu and furtado filed civil suits against hernandez and a judge froze his million in assets pending the outcome of the doublemurder trial the freeze includes the disputed million signing bonus payment hernandez claims he is owed by the new england patriots. to this day we just went through a threemonth trial and this is now two years later we still dont know the exact time of odins murder a male juror said so i dont know how aaron would have had that information two years ago. the jury of seven women and five men listened to more than witnesses and reviewed more than pieces of evidence over the monthslong trial on wednesday they convicted hernandez who was sentenced to life in prison without the possibility of parole after deliberating more than hours over parts of seven days

@highlight
female juror everyones life changed because of this

@highlight
the jurors said they didnt learn of the other charges against hernandez until after the verdict

@highlight
for these jurors the system worked its designed to be fair to both sides
