it wasnt messrs clooney pitt and their nine accomplices who sailed down an elevator shaft and cracked open dozens of safety deposit boxes at a london vault during the easter weekend. the brinks mat robbery in a case which i was involved in at the time saw a criminal gang escape with gold bullion worth more than million around million or adjusting for inflation from a warehouse at londons heathrow airport the knightsbridge safety deposit robbery of saw a gang make off with tens of millions of pounds in cash and valuables from an upscale london neighbourhood the true amount will never be known both were made possible by inside information its an angle that london detectives investigating last weekends heist will be looking at very closely. such robberies are rare the gang didnt follow the current criminal trend of manipulating digits in cyber space but instead went back to basics and committed their burglary in a way not seen in london for more than years in september the staff of a bank in baker street central london arrived at work to find that thieves had dug a tunnel from a shop they had rented hauled in a thermic lance and explosives and opened the strong room the gang got away with a haul worth around million the incident later formed the basis of the movie the bank job. the planning behind the hatton garden raid will have been meticulous the target will have been observed perhaps for months and the thieves will have decided on the right time to commit the crime a long weekend or a public holiday are occasions when more time may be available also perhaps when regular staff are away

@highlight
police in london are trying to catch the gang which staged a multimillion heist during the easter vacation

@highlight
former police commander such crimes require meticulous planning and use of information by criminals

@highlight
the masterminds behind such complicated crimes carefully assemble their gangs with men they can trust
