chinas cybercensors have long used a great firewall to block its citizens from reading critical articles from western news websites or consuming other content it disapproves of. by triggering attacks and analyzing them the researchers concluded that beijing has developed a tool distinctly different from the great firewall they are confident it is also in china and say it is technically similar to the great firewall. not only does the great firewall monitor tons of traffic but its systems have to do a lot of processing to discern what to block and what not so its workintensive. one of the great cannons targets that the researchers studied was an obvious one greatfireorg run by chinese expats bent on fighting beijings censorship they monitor chinese citizens access to international news sites such as german news service deutsche welle or the tibet post

@highlight
chinas cybercensors have developed a new it weapon and have attacked servers outside their borders

@highlight
attacks by the great cannon are in the open and could draw international ire the authors of the study say
