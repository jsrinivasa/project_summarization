the fbi has confirmed that one of its most wanted terrorists the malaysian bomb maker known as marwan was killed in an otherwise disastrous raid in the philippines in january. the fbi said in february that a dna sample understood to be from a severed finger taken from a man killed in a raid in the southern philippines showed a link with a known relative of marwan. but in january it launched a surprise raid in pursuit of marwan at mamapasono in the southern province of maguindanao. marwan had previously been falsely reported dead after a raid by philippine security forces in

@highlight
a man killed in a raid in the philippines in january was a most wanted terrorist the fbi says

@highlight
marwan was a malaysian believed to have provided support to islamist terror groups

@highlight
elite philippine commandos were killed in the raid on his hideout last month
