on the morning of april a man parked a rental truck packed with explosives in front of the alfred p murrah federal building in oklahoma city at am the explosives detonated killing people including children. according to the fbi explosives had been placed in a stolen truck three blocks from the building. it was classified as the largest act of domestic terror until the oklahoma city bombing the explosion killed a physics researcher severely damaged a building at the university and damaged others. william guillermo morales was an explosives expert who allegedly made bombs for an extremist organization violently advocating for puerto rican independence the faln translated as the armed forces of national liberation claimed responsibility for or were blamed for bombings most of them in new york and chicago in the and early that caused numerous deaths injuries and millions of dollars in damage

@highlight
april marks years since the oklahoma city bombing

@highlight
the bombing was carried out by domestic terrorists

@highlight
todays domestic terror threats range from ecoterrorists to antigovernment extremists
