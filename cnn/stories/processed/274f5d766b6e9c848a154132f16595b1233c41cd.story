yemeni officials said saudi airstrikes targeting a military base on tuesday hit a nearby school injuring at least a half dozen students. the officials from ibbs governors office said the al hamza military base was targeted because houthis have been sending reinforcements from ibb to nearby provinces there were no casualties on the base the officials said but it was heavily damaged. the school which is about meters onethird of a mile from the base was not the main target the officials said schoolchildren were heading to their lunch break when the attacks took place the officials said. separately saudi airstrikes wiped out about a fifth of the armored vehicles recently captured by southern separatists opposing the houthis near aden according to a senior official in the separatist movement

@highlight
saudi military official accuses iran of training and arming rebels

@highlight
yemeni officials say school hit by airstrikes one source says three students killed

@highlight
noncombatants are caught up in yemens fighting
