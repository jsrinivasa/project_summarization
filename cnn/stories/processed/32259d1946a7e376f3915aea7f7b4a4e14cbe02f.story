after weeks of dramatic and emotionally wrenching testimony in the boston marathon bombing trial jurors deliberated for more than seven hours tuesday. tsarnaevs defense attorney judy clarke tried to persuade jurors that her clients older brother tamerlan tsarnaev who died in a shootout with police days after the terror attack was the instigator of the marathon plot the younger man clarke said was only following his older brother. the defendant brought terrorism into the backyards and main streets assistant us attorney aloke chakravarty said the defendant thought that his values were more important than the people around him he wanted to awake the mujahedeen the holy warriors so he chose patriots day marathon monday a time for families to gather and watch the marathon. according to testimony tamerlan tsarnaev set off a bomb made from a pressure cooker explosive powder from fireworks duct tape nails and bbs on boylston street near the finish line that bomb which exploded near marathon sports claimed the life of krystle campbell a restaurant manager

@highlight
court has adjourned for the day after more than seven hours of deliberations

@highlight
jurors sent out two questions that are set to be addressed wednesday

@highlight
if dzhokhar tsarnaev is found guilty of at least one capital count trial will go to penalty phase
