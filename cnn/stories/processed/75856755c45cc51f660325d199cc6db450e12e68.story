huge military exercises are underway off and around britains coast but nato insists they are not a deliberate response to the russian militarys increasingly brazen behavior. commanding officer rear adm brad williamson says russias behavior is an added motivation to do well in these exercises. so if joint warrior is not a deliberate response its certainly a timely one. for more than a year nato has been condemning russias actions in ukraine as well as its frequent large scale snap military drills and those longrange bear bombers repeatedly flying very close to nato airspace

@highlight
nato is holding huge war games exercises off the coast of scotland

@highlight
alliance says planning started long before russia renewed its status as the alliances chief adversary

@highlight
but commanding officer says russias behavior is an added motivation to do well in these exercises
