brazilian police have arrested the treasurer of the ruling workers party bringing the bribery investigation at the staterun oil company petrobras a step closer to president dilma rousseff. vaccari is the closest political figure to rousseff so far implicated in the investigation rousseff herself has not been implicated although she was the chairwoman of petrobras when much of the alleged corruption took place. vaccari faces charges of corruption and money laundering as part of the broader probe into corruption at petrobras former executives who have turned states evidence claim that construction companies paid large sums under the table to petrobras officials and politicians in order to secure lucrative contracts with the oil giant. rousseff has insisted she supports the probe and has not in any way interfered with the investigation

@highlight
a top official with president dilma rousseffs ruling party is arrested in bribery probe

@highlight
joao vaccari neto denies wrongdoing says all donations were legal

@highlight
hundreds of thousands of brazilians have protested against rousseff in the last few months
