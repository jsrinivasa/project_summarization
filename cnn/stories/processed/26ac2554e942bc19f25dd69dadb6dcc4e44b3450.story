the israeli military conducted airstrikes sunday night in the area between israel and syria targeting a group of militants allegedly trying to plant a bomb on the israeli border. the israel defense forces said the airstrikes were carried out in the occupied golan heights against four militants who crossed into the area from syria. the golan is regarded internationally as occupied territory despite israeli governmental control it is home to residents including jews druze and alawites israel seized the territory from syria during the israelarab war and it was eventually annexed. a group of armed terrorists approached the border with an explosive device which was intended to be detonated against idf forces the israeli military said

@highlight
the israeli military says the militants were trying to plant a bomb

@highlight
the men crossed from syria israel says
