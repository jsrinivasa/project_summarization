when the earthquake struck we huddled under a concrete beam and prayed. in another room my grandfather could not comprehend what was happening and instead of seeking cover drifted towards the window. outside a brown dustcloud rose from the ruins of cottages that had dotted the next hill. i was at my uncles place in ramkot west kathmandu some kilometers miles east from my family home

@highlight
journalist sunir pandey was visiting relatives with nepals magnitude quake struck

@highlight
he says they ran to shelter under a concrete beam and prayed as dust rose from the rubble
