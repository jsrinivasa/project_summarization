the golden state could soon refer to the hue of californias lawns. replace million square feet of lawns throughout the state with droughttolerant landscaping. the entire state faces at least a moderate drought and more than half of the state faces the worst category of dryness called an exceptional drought according to the us drought monitor. the reduction in water use does not apply to the agriculture industry except for the requirement that it report more information on its groundwater use the exclusion prompted some criticism as agriculture uses about of californias developed water supply

@highlight
pepperdine university seeks ways to meet new usage allowances turns off fountains

@highlight
droughtstricken california for the first time imposes water restrictions

@highlight
executive order demands that cities and towns reduce water usage by
