a saudiled coalition tuesday ended its operation decisive storm its nearly monthlong airstrike campaign in yemen and a new initiative is underway. we promise to restructure the yemen military to ensure that it serves the people of yemen hadi said calling on the houthis to withdraw and saying that he would return to yemen at the right time to rebuild the country. ousted yemen president abdu rabu mansour hadi thanked the saudiled coalition hadi claims hes yemens legitimate leader and is working with the saudis and other allies to return to his country. saudi arabia had launched airstrikes on houthi positions across yemen hoping to wipe out the iranianallied rebel group that has overthrown the government and seized power

@highlight
former yemeni president ali abdullah saleh will leave a source says

@highlight
ousted leader abdu rabu mansour hadi promises to return

@highlight
next phase called operation renewal of hope will focus on political process
