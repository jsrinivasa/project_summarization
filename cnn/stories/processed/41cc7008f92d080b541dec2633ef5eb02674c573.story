at least people have died and others are missing after a russian fishing vessel sank off the kamchatka peninsula according to russias staterun tass news agency. the trawler is also thought to have keeled over as a result of hauling some tons of fish on to the deck the chairman of the emergencies commission in the kamchatka region sergey khabarov told tass. more than fishing vessels are searching for the people still thought to be missing tass said. of the people on board were russians the others were foreign nationals from myanmar ukraine lithuania and vanuatu according to the news agency with the majority coming from myanmar

@highlight
fishing vessels are searching for people still thought to be missing

@highlight
there were people on board the ship of them russians tass news agency says

@highlight
the rest were foreign nationals from myanmar ukraine lithuania and vanuatu it says
