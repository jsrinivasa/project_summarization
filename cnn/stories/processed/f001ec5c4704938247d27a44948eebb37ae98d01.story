the palestinian authority officially became the member of the international criminal court on wednesday a step that gives the court jurisdiction over alleged crimes in palestinian territories. later that month the icc opened a preliminary examination into the situation in palestinian territories paving the way for possible war crimes investigations against israelis as members of the court palestinians may be subject to countercharges as well. the international criminal court was set up in to prosecute genocide crimes against humanity and war crimes. the palestinians signed the iccs founding rome statute in january when they also accepted its jurisdiction over alleged crimes committed in the occupied palestinian territory including east jerusalem since june

@highlight
membership gives the icc jurisdiction over alleged crimes committed in palestinian territories since last june

@highlight
israel and the united states opposed the move which could open the door to war crimes investigations against israelis
