authorities identified and charged a man monday in connection with the discovery of human remains in a duffel bag in cambridge massachusetts over the weekend. police were notified saturday morning about a suspicious item along a walkway in cambridge officers arrived at the scene opened a duffel bag and found human remains. the remains at both locations belonged to the same victim identified monday as jonathan camilien camilien and colina knew each other according to authorities. after that discovery police say a surveillance video led them to an apartment building where more body parts were discovered in a common area that location is near the cambridge police department headquarters

@highlight
carlos colina is arraigned on charges of assault and battery improper disposal of a body

@highlight
body parts were discovered saturday in a duffel bag and a common area of an apartment building

@highlight
the victim in the case is identified as jonathan camilien authorities say he knew colina
