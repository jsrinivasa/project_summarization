university of nairobi students were terrified sunday morning when they heard explosions caused by a faulty electrical cable and believed it was a terror attack the school said. on sunday as many as students from the university of nairobi were admitted to kenyatta national hospital among them at least students have been discharged and at least four are slated for surgery the school said. students on the kikuyu campus stampeded down the halls of the kimberly dormitory and some jumped from its fifth floor the university said hundreds were injured and were taken to hospitals one person died according to the school. almost all of the students being treated at pcea kikuyu hospital have been released the university said

@highlight
students stampeded some jumped from a fifth story at a dorm one student died school officials say

@highlight
the blasts were caused by faulty electrical cable and kenya power is at the school

@highlight
the panic came less than two weeks after terrorists attacked kenyas garissa university
