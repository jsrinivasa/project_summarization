last week california gov jerry brown ordered mandatory statewide restrictions on water use for the first time in the states history his action was driven by a specific crisis unique to california at the moment the severe drought now in its fourth year there but it has significance for the whole nation. is the california drought a consequence of climate change a recent national oceanic and atmospheric administration report for which my colleague richard seager was lead author argued that it isnt though the authors acknowledge that global warming makes the drought worse by increasing evaporation from the soil california and southwestern north america indeed saw worse megadroughts in the precolumbian past long before any humans burned fossil fuels at the same time the latest projections are that the odds of such megadroughts are increasing with warming. i lived in australia for a year in when the country was well into a drought that lasted about a dozen years and that led to water restrictions in nearly all the countrys major cities though the role of greenhouse gases in that drought wasnt clear either the hardship nonetheless made australians much more concerned about climate change than americans were at the time. what california does now has global significance lack of clean fresh water is a serious problem in much of the world on the indian subcontinent where supply cant meet the demands of rapid development groundwater is being depleted much as it is in california and is an equally unsustainable practice in both places

@highlight
adam sobel californias steps against drought are a preview for rest of us and world

@highlight
tying climate change to weather doesnt rest on single extreme event sobel says

@highlight
the big picture should spur us to prepare for new climates by fixing infrastructure he says
