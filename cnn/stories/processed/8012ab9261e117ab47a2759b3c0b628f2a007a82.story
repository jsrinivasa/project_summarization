according to an outside review by columbia journalism school professors an institutional failure at rolling stone resulted in a deeply flawed article about a purported gang rape at the university of virginia. the columbia team concluded that the failure encompassed reporting editing editorial supervision and factchecking. true statements cannot be defamatory neither can pure statements of opinion because they theoretically cannot be either true or false but the rolling stone article certainly purported to be fact and it apparently is not exactly what the law considers true. first lets eliminate uva the university is a public university and therefore it is a governmental entity the supreme court has been clear on the issue of libelous statements about the government the government cannot sue for defamation there is no such cause of action in american jurisprudence

@highlight
an outside review found that a rolling stone article about campus rape was deeply flawed

@highlight
danny cevallos says that there are obstacles to a successful libel case should one be filed
