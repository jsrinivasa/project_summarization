rescuers in nepal have pulled a man from the wreckage of a building where he was stuck for a staggering hours after the devastating earthquake that hit the country saturday. jon keisi was buried for more than hours under the wreckage of a sevenstory building in kathmandu that came tumbling down around him during the quake. tanka maya sitoula a mother of four was at home in kathmandu when the earthquake shook the city bringing the fivestory building down around her groundfloor apartment. a baby was rescued from a destroyed building in the town of bhaktapur at least hours after the quake struck the newspaper kathmandu today reported

@highlight
a french rescue team finds rishi khanal more than three days after the quake

@highlight
a baby is reported to have been rescued after hours in rubble
