launching rockets is a complicated thing landing them upright on a platform floating in the ocean well thats never been done before spacex the private space exploration company founded by billionaire elon musk hopes to give it a try but weather forced it to scrub mondays plan to launch. after the launch spacex will try to guide the bottom stage of the rocket upright onto a platform or what it calls an autonomous spaceport drone ship in the atlantic ocean off florida. spacex said on twitter that tuesday at pm et will be the next opportunity for the company to launch a twostage falcon rocket carrying an uncrewed cargo spacecraft called dragon on a flight from cape canaveral florida to the international space station. this is the sixth spacex mission to the international space station the company was the first private space contractor to dock with the station

@highlight
spacex says weather forced it to delay its rocket launch plan

@highlight
the company plans to launch a twostage rocket to the international space station

@highlight
after the launch spacex will try to guide the bottom stage upright onto the platform
