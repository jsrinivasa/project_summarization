it was right after one of the longest lockouts in nba history jeff green then playing for the boston celtics was looking forward to getting back to the hardwood. greens nba basketball career wasnt over but it was a slow progression back to the court he missed the entire season. green believes the surgery and hard road back to the nba have given him a new perspective and more meaning in his life. that was until a routine physical stopped him in his tracks the physical revealed he had an aortic aneurysm near the left valve of his heart

@highlight
nba player jeff green had open heart surgery to repair an aortic aneurysm in

@highlight
green missed the entire basketball season

@highlight
now he donates time to young cardiovascular patients
