north korean leader kim jong un is continuing to rule with an iron fist having ordered the execution of about senior officials so far this year according to an assessment by south korean intelligence agents a lawmaker who attended a closed briefing said. kim became north koreas supreme commander in december following the death of his father kim jong il according to the national intelligence service he is reported to have executed senior officials in in and in. and in march according to the south korean lawmaker kim executed on charges of espionage four members of the unhasu orchestra including the general director because of a scandal shin said. shin kyungmin a lawmaker with the new politics alliance for democracy told a handful of reporters that he had been given the information by the south korean national intelligence service

@highlight
south korean lawmaker quotes intelligence officials as saying kim jong un countenances no disagreement

@highlight
official reportedly executed for expressing dissatisfaction with forestry program

@highlight
four member of unhasu orchestra also reportedly executed
