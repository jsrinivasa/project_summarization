kathmandu is a city with few good stories right now but tanka maya sitoula has one of them. the motheroffour was at home when saturdays deadly earthquake struck bringing the building down around her ground floor apartment. in the wake of the disaster which has left at least people dead across nepal sitoula endured long hours trapped in a room on the ground floor before she was freed by an indian rescue team. remarkably she escaped without injury apparently protected by a beam

@highlight
tanka maya sitoula was at home in kathmandu nepal when deadly quake struck

@highlight
she was trapped inside the ruins of her wrecked home for hours
