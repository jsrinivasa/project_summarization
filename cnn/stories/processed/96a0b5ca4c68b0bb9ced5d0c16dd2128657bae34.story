a naturalized us citizen pleaded not guilty in ohio friday to federal charges of providing material support to terrorists and lying to the fbi. he became a us citizen in february and submitted a us passport application days later according to the indictment. mohamud traveled to syria in april for the purpose of training and fighting with terrorists prosecutors said in a news release. mohamud was remanded into custody on friday

@highlight
abdirahman sheik mohamud pleads not guilty to charges of providing material support to terrorists and lying to the fbi

@highlight
the columbus ohio resident became a us citizen in february

@highlight
in april he went to syria for terrorism training prosecutors say in a news release
