a teen who has been living at connecticut childrens medical center since december while being forced to have chemotherapy to treat hodgkins lymphoma has completed treatment and left the hospital monday according to her attorney josh michtom. as we do for every patient we care for at connecticut childrens we wish her the best for a happy and healthy future robert fraleigh director of corporate communications for connecticut childrens medical center said in a statement monday. cassandra was diagnosed with hodgkins lymphoma in september and doctors gave her an chance of survival if treated but said she would die within two years if left untreated. she started the treatment in november but ran away after two days according to court documents when she decided she did not want to put the poison of the treatment into her body thats when a judge ordered her into custody of the state attorneys for the teen and her mom have tried to appeal but they did not succeed cassandra remained in the hospital

@highlight
teen allowed to return home now that her chemotherapy is complete

@highlight
cassandra was diagnosed with hodgkins lymphoma in september

@highlight
teen was in temporary custody of the connecticut department of children and families
