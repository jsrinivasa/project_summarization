a man stands on a beach in a distant land waves lap his ankles he wades through the gentle dawn light arms outstretched his head held high he is fully dressed not a tourist but a freedom fighter. the sum total of television programming beer advertising political grandstanding and opportunistic marketing suggests that the historical legacy of australias involvement in the first world war boils down to a simple equation young white man plus distant beach equals sacrifice. lest we also forget that the democratic freedoms we hold dear today freedom of the press freedom of assembly freedom of speech were won in battles fought on home soil by courageous women and men who sacrificed much but are still accorded little recognition. a photograph of this man beamed around the world becomes a universal symbol of the struggle against tyranny and the sweet triumph of liberty it is the man is peter greste

@highlight
april marks the centenary of the start of the gallipoli campaign in turkey during wwi

@highlight
anzac troops stormed the beaches at gallipoli beginning a bloody eightmonth campaign
