rebekah gregory blinked back tears as she thought about the verdict. for gregory and others who lived through the attack wednesdays verdict brought a mix of emotions from triumphant vows to move forward to expressions of gratitude to debate over whether tsarnaev should be sentenced to death. gregory who wrote a widely publicized letter to tsarnaev after testifying said the trial has left her and other victims reeling from a flood of emotions as they relive horrifying memories but its an important step. there were no outbursts inside the federal courthouse in boston in fact there was barely any peripheral noise as people sat on the edges of their seats as tsarnaev fidgeted and scratched the back of his head some survivors and victims family members lowered their heads and dabbed tears

@highlight
survivor jeff bauman stresses we will never replace the lives that were lost

@highlight
a man who was at the finish line is glad dzhokhar tsarnaev is now a convicted killer

@highlight
justice has been served today says a once wounded police officer
