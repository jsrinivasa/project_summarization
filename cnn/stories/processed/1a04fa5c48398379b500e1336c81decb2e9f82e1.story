it was all set for a fairytale ending for record breaking jockey ap mccoy in the end it was a different but familiar name who won the grand national on saturday. yet for much of the grand national arguably the worlds most famous and certainly the sports most prestigious jump race it looked as if ap mccoy was about to write an ending befitting the career of a man who has dominated jump racing for two decades. shutthefrontdoor was heavily backed by the betting public sensing a storybook conclusion to mccoys career uk and irish betting firms even predicted they would lose as much as million if mccoy won. outsider many clouds who had shown little form going into the race won by a length and a half ridden by jockey leighton aspell

@highlight
shot many clouds wins grand national

@highlight
second win a row for jockey leighton aspell

@highlight
first jockey to win two in a row on different horses since
