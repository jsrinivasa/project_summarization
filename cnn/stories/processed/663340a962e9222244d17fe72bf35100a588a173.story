the marriage apocalypse may be coming talk to any millennial and you can envision an america virtually marriagefree with everyone happily single. recently i talked about marriage with a group of journalism students from my alma mater kent state university they came to me for career advice which i gave them but i also picked their brains about politics religion and marriage their views on marriage intrigued me the most cause guess what they dont care what your generation thinks theyll get married if and when they want. which brings me back to that idea of a pending marriage apocalypse would it be so terrible if we all remained single if i had remained single i thought about it so did my husband we didnt plan to have children what was the point. still there are no doubt more than a few parents out there wondering where they went wrong especially in light of a fascinating pew research report on marriage when asked if society is just as well off if people have priorities other than marriage and children of respondents were ok with that and of that were adults between and

@highlight
carol costello talk to any millennial and you can envision an america virtually marriagefree

@highlight
in countries like sweden or denmark people dont feel pressured to marry even if they have kids together
