by some estimates about a third of americans use some form of alternative medicine including homeopathic remedies because they find western medicine inadequate. a mother and editor of an alternative medicine magazine and website peggy omara testified that homeopathic medicine has helped her family over the years. a third of americans use alternative medicine. homeopathy is a medical philosophy that essentially believes your body is the best weapon to fight disease homeopathic medicine is based on the idea that like cures like meaning if something causes a symptom in your body if you take a diluted form it will boost your bodys ability to fight it typically these remedies include a plant or a mineral in a tiny amount

@highlight
the fda may take a more handson approach to regulating homeopathic medicine

@highlight
it does not go through the same approval process as overthecounter drugs

@highlight
some studies suggest homeopathic medicine is no more effective than placebos
