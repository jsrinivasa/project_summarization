jon reiter is no stranger to mount everest its worldrecord height its prestige its challenges. for many mountaineers the draw of everest has long been hard to resist one of them is reiter who has scaled all of the seven summits the highest mountain on each of the seven continents except this one this would be his third straight year trying he turned back in because it didnt feel right and survived last years avalanche according to his wife susan. at least nepalese locals and sherpas were killed in that incident which at the time was the deadliest incident ever around everest the highest singleday death toll before then came in may when eight climbers disappeared during a big storm an episode chronicled in jon krakauers bestselling book into thin air. jim whittaker the first american to reach the summit of mount everest back in is still a mentor to experienced climbers trying to follow in his footsteps

@highlight
family of american medic killed posts message on facebook says she died while at base camp

@highlight
at least die after avalanches at mount everest base camp authorities say climber says dead

@highlight
dining tent at one base camp has been transformed into a hospital another hiker says
