wall street is more than ready for hillary clinton. as clinton sets off onto the campaign trail to reintroduce herself to voters and court donors across the country wall street elites are ready to roll out the red carpet but while the enthusiastic support from the industry will be a financial boon for clintons newly launched campaign it will also pose a delicate balancing act when it comes to appeasing a vocal wing of her party that is antagonistic toward the banking sector. kathy wylde president of the partnership for new york city a prominent business coalition predicted that clinton would be able to maintain her relationship with wall street without alienating the liberal base. we look forward to hillary clinton and other candidates laying out their platforms and hearing whether they embrace the fights that sen warren has spent her life leading said ready for warren campaign manager erica sagrans in the coming days ready for warren will be stepping up our efforts to convince warren to run for president

@highlight
hillary clinton developed a close relationship with the financial world as a new york senator

@highlight
clintons allies there are eager to galvanize a broad network of potential donors

@highlight
her coziness with wall street irritates liberal activists who are a growing influence in the democratic party
