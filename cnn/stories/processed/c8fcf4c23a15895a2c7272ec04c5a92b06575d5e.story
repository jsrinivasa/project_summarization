an international human rights group is calling for an independent investigation of the killings by police of suspected red sandalwood smugglers in southeastern india. there must be a criminal investigation to determine whether the police used excessive force and whether the killings amount to fake encounters or staged extrajudicial executions said abhirr vp of amnesty international india the police are not above the law and must not be treated like they are. forest officials near the town of tirupati spotted hundreds of smugglers cutting trees for red sandalwood rao said tuesday the forest officials who were unarmed called for police. according to amnesty indias national human rights commission has said the incident involved a serious violation of human rights of the individuals and that the opening of firing cannot be justified on the ground of self defense since it resulted in the loss of lives of persons

@highlight
amnesty calls for probe of india police shooting of suspected smugglers

@highlight
police decline comment saying investigation is still going on

@highlight
indias national human rights commission says incident involved serious violation of human rights of the individuals
