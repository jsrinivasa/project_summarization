chiles calbuco volcano erupted again thursday marking the third time since last week the national service of geology and mining said. for the past several days the geology agency has maintained there was a possibility of a third eruption as part of steadily declining seismic activity in the area. gregorio billikopf lives across lake llanquihue from the volcano has been photographing and videotaping the three eruptions and described thursdays event as spectacular but not as severe as the two prior ones. another person said it was impressive to see an enormous mushroom cloud with the immense force of the volcano and to see the ashes at that point there was a lot of panic lots of chaos traffic jams people going to supermarkets everyone looking for water trying to take out money from the atms

@highlight
there is still smoke on and off says resident with distant view

@highlight
the volcano erupts for third time since april

@highlight
chile
