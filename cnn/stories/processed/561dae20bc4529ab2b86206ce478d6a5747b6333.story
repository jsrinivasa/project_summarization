a catandmouse chase spanning from antarctic waters to the coast of west africa had an unlikely end when the crew of an alleged poaching vessel were rescued by conservationists pursuing them. the pursuit ended early monday in the gulf of guinea off the coast of west africa when the bob barker received a distress call from the thunder. the pursuit of the thunder began after the bob barker encountered the vessel off the coast of antarctica said burling. he said he hoped the authorities would work with interpol to prosecute those operating the vessel

@highlight
sea shepherd rescues the crew of an alleged poaching ship it had chased for days

@highlight
the conservationist group had pursued the vessel since it was found illegally fishing off antarctica it says

@highlight
he believes the ship was deliberately sunk to destroy evidence
