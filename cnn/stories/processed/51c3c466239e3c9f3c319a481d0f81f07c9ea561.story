jackson gordon is no ordinary by day he is an industrial design student at philadelphia university but gordon has another side to him a side altogether darker tougher and more enigmatic. gordon who doesnt appear to be related to gotham citys police commissioner james gordon is also an expert in shaolin kung fu he is both brains and brawn a cross between bruce wayne and batsuit designer lucius fox from nolans batman trilogy legendary the production company behind the films has taken note of his design and given it their seal of approval. hanging in his workshop gordon has a full suit of armor plating cape and cowl matte black and built to stop a knife gordon has an alter ego the dark knight himself batman. gordon therefore fired up a kickstarter campaign he didnt really think anyone would fund it or even be interested in it he raised in days it was a little surprising gordon demurs

@highlight
student jackson gordon has designed and built a functional batsuit

@highlight
made with money raised on kickstarter the outfit has received a prestigious endorsement
