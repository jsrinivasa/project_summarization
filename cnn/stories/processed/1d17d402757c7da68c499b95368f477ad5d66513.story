a us army soldier was killed wednesday in an attack in eastern afghanistan by an afghan national army gunman a us military official told cnn shortly after an american official met with a provincial governor. that an afghan national army soldier shot at us soldiers at a provincial governors compound in jalalabad on wednesday. a senior us official had just held a meeting with nangarhars governor at the compound when gunfire erupted a us embassy representative said the embassy representative didnt identify the official but said that all diplomatic personnel had been accounted for after the incident. first there are far fewer us soldiers in afghanistan bergen said second more counterintelligence resources were devoted to countering the threat and third an attempt was made to better vet afghan army recruits

@highlight
gunfire erupts after senior us official meets with afghan governor in jalalabad us embassy says

@highlight
afghan soldier fires at us troops afghan police official says
