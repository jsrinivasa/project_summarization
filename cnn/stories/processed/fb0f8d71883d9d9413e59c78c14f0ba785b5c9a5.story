cspans live telecast of the white house correspondents association dinner on saturday night hosted by cecily strong of saturday night live was not strongs finest hour though the entire affair seemed like five of cspans longest hours. but the seemingly tepid response to strongs routine may have been partly due to the late hour she came on after not only the dinner service and obama but after scholarship awards correspondent awards and tributes and other bits of official business and her routine wasnt over until pm making it a longer tv show than even the emmys. that being said its a very tough room and not just because the washington hilton ballroom is so cavernous more than credentialed white house journalists and their mostly celebrity guests convened for the occasion and convened and convened and convened as time dragged on and dinner was served late president barack obama as strongs warmup act didnt hit the podium until pm strong got her turn at. and then came cecily strong with a sly opening line referring not only to her appearance as one of the few female guest speakers at the correspondents dinner but to the upcoming presidential election

@highlight
david bianculli correspondents dinner and cecily strong as host were mostly weak but obama had some funny zingers

@highlight
he says anger translator bit was funny but crowd was tough on strong as event went on and on
