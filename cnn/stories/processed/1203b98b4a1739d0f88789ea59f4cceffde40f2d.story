joy womack is taking part in her first ballet class of the day at the kremlin ballet theatre kicking her legs up to her head jumping and spinning across the room after class she eats boiled sweets one after another they are a cheap form of energy. when she left the bolshoi in womack joined the kremlin ballet theatre where she still works aged as a principal ballerina dancing close to the russian presidents office next to the cathedrals inside the red walls of the kremlin. the kremlin ballet theatre says womacks salary corresponds to her job title as a principal dancer and that on average the salary principal dancers are paid is significantly higher than a month but that womack could have been paid that equivalent in dollars depending on the exchange rate on the day and depending on how much she danced in productions the previous month. although relations between the us and russia have taken a nosedive since russias annexation of crimea last march womack says she is very loyal to the kremlin ballet theatre

@highlight
american dancer makes a month at kremlin ballet theatre

@highlight
joy womack studied at bolshoi ballet academy but left in cloud of controversy
