president barack obama says he is absolutely committed to making sure israel maintains a military advantage over iran. but what i would say to them is that not only am i absolutely committed to making sure they maintain their qualitative military edge and that they can deter any potential future attacks but what im willing to do is to make the kinds of commitments that would give everybody in the neighborhood including iran a clarity that if israel were to be attacked by any state that we would stand by them obama said. obama said he understands and respects netanyahus stance that israel is particularly vulnerable and doesnt have the luxury of testing these propositions in the deal. but netanyahu and republican critics in congress have complained that iran wont have to shut down its nuclear facilities and that the countrys leadership isnt trustworthy enough for the inspections to be as valuable as obama says they are

@highlight
in an interview with the new york times president obama says he understands israel feels particularly vulnerable

@highlight
obama calls the nuclear deal with iran a onceinalifetime opportunity

@highlight
israeli prime minister benjamin netanyahu and many us republicans warn that iran cannot be trusted
