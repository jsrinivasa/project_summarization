nba player thabo sefolosha says police caused his seasonending leg injury when he was arrested last week after leaving a nightclub in new york. tmz sports released video last week that shows a group of police officers arresting the sefolosha and taking him to the ground it also shows an officer within that group getting out a baton and extending it near him but what may have caused the injury is not clear in the video sefolosha appears to be limping as he is led away by officers. sefolosha suffered a fractured fibula and ligament damage when he and teammate pero antic were arrested near the scene of the stabbing of indiana pacers forward chris copeland and two other women early april police said sefolosha and antic were not involved in the stabbing incident but they were charged with misdemeanors including disorderly conduct and obstructing governmental administration. sefolosha did not specify his injury in his statement tuesday but the hawks said last week that he has a fractured fibula and ligament damage will undergo surgery and will miss the rest of the season including the playoffs which begin this weekend the hawks enter as the top seed in the nbas eastern conference

@highlight
thabo sefolosha says he experienced a significant injury and the injury was caused by the police

@highlight
he and teammate pero antic were arrested near the scene of a stabbing early april

@highlight
they were not involved in the stabbing police said but they were arrested for obstruction other charges
