he was impressively polite and bright in the eyes of his boyhood teachers an encourager of his college friends he was a docile captured killer in the care of paramedics tending to his gunshot wounds. tamerlan dying from gunshot wounds and having been run over by the car his brother was driving as he fled became combative in the ambulance a paramedic testified wednesday that it was common for patients in shock to become agitated and fight back. jahar on the other hand was portrayed in the testimony of five of his former teachers as a smart sweet kid who worked hard and earned good grades. all the teachers loved him said becki norris who had jahar in her seventhgrade class at community charter school a middle school in cambridge known for setting its students on the path to full ticket scholarships to good colleges

@highlight
defense seeks to make case to spare life of convicted boston bomber dzhokhar tsarnaev

@highlight
defense brother tamerlan who was killed during police showdown was obsessed with jihad

@highlight
dzhokhar tsarnaev was wellliked polite and docile according to witnesses
