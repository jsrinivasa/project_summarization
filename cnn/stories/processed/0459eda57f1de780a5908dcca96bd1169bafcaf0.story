six survivors of the paris kosher supermarket siege in january are suing a french media outlet for what they call dangerous live broadcasting during the hostagetaking. affiliate bfmtv is accused of endangering the lives of the hostages who were hiding in a cold room during the attack by broadcasting their location live during the siege. gunman amedy coulibaly also suspected in the slaying of a police officer stormed the hyper cacher jewish supermarket on january killing four people and taking others hostage. the hostagetaking was the culmination of three days of terror in paris that began with the january shooting of people at the offices of french satirical magazine charlie hebdo

@highlight
six people taken hostage in a kosher market siege say media outlet endangered their lives

@highlight
they hid in a cold room during the attack in paris by gunman amedy coulibaly
