rescue crews and residents in nepal early sunday began the desperate search for survivors after a quake near the capital of kathmandu a day earlier flattened homes buildings and temples causing widespread damage across the region and killing more than people. the quake was the strongest in the region in more than years residents are used to earthquakes in nepal and many thought the start of saturdays quake was a tremor until the earth kept shaking and buildings crashed down. the quake struck at am local time am et and was centered less than miles northwest of kathmandu it occurred at a depth of miles which is considered shallow and more damaging than a deeper quake it was reported by people in the area as having lasted a long time one person said he felt as if he were on a ship in rough seas. building codes in kathmandu itself have not been well upheld in recent years he said efforts have been made over the last few years to strengthen these building codes but unfortunately this comes too late for the many thousands of buildings that have gone up across the kathmandu valley over the last years that did not adhere to the building codes

@highlight
ngo official says people will urgently need food water medicine and shelter

@highlight
more than people across nepal confirmed dead official says

@highlight
people treated outside hospitals avalanches reported on everest
