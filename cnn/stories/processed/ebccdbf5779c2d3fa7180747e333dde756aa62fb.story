a detroit mother arrested last month after police discovered the bodies of two of her children inside a freezer was arraigned this week on murder and torture charges. according to detroit police chief james craig the bodies were found wrapped in plastic inside the freezer by a bailiff performing a court ordered eviction on march. in addition to the two counts of premeditated murder each of which carries a mandatory life sentence without the possibility of parole blair has been charged with two counts of torture four counts of felony child abuse and one count of committing child abuse in the presence of another child she has two other children a daughter and an son both of whom are now in in protective custody according to worthy. wayne county prosecutor kym worthy charged mitchelle angela blair on wednesday with killing her then daughter stoni ann blair and her then son stephen gage berry

@highlight
murder and torture charges for detroit mom after bodies found in her freezer

@highlight
prosecutors accuse mitchelle angela blair of killing her daughter and son

@highlight
their bodies were discovered in her freezer as she was evicted from her home last month
