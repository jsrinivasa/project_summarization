five young men were arrested saturday in melbourne australia in what police called a major counterterrorism operation. police said the suspects were targeting a ceremony on anzac day australia and new zealand army corps day which is april and this year is the centennial of the gallipoli campaign in world war i. i think the entire australian community should be concerned about the young age of those particular men gaughan said and this is an issue not just with law enforcement but for the broader community we need to get better in relation to identifying young men and woman involved in this type of behavior at the very early stage. the suspects planned to attack during a major national commemoration in a week prime minister tony abbott said saturday the act that we believe was in preparation involved attacks against police officers he said

@highlight
three of the five teens released

@highlight
one suspect has been charged report says

@highlight
australian police said the suspects were allegedly planning an isisinspired attack
