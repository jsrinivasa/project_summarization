some of the men and women of the indianapolis police force are giving up their blues. as police departments around the country see more protests over the use of lethal force impd officials acknowledge that this is a time of increased scrutiny of police operations and tactics but said the decision to change the uniform for certain ranks within the department is not related to any specific individual incident occurring elsewhere in the united states. beginning friday blue uniform shirts will be traded for white ones for command staff members of the indianapolis metropolitan police department impd. although its not a total uniform makeover police officials said the white shirts will make commandlevel staff immediately identifiable to those who dont recognize the rank badges on officers collars the statement said the new color will be worn by majors district commanders deputy chiefs assistant chiefs and the chief of police

@highlight
beginning friday some ranks of the indianapolis police department will wear white shirts

@highlight
police say the change in attire is not related to any specific incident

@highlight
the new uniform shirt color is aimed at ensuring accountability
