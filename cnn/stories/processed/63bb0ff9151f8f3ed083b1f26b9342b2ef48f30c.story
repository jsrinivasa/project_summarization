we came on a commercial flight to kathmandu blue tarps were visible from the sky for people to hide under signs that there was something wrong. kathmandu isnt the epicenter though no ones sure what its like at the epicenter people havent been able to get to outlying areas we havent been able to corroborate this but the people were talking to here say theres damage to the villages outside kathmandu thousands of houses damaged to the north closer to the epicenter. people are beside themselves in shock their biggest concern now is the structures needing a place for shelter to hide from the elements and sleep food will become the biggest concern in the coming days. there were torrential rainstorms for a couple of hours and with the strong aftershock a couple of hours ago no one wants to go inside the residents of kathmandu sure dont you can see some structural damage to buildings most buildings are not up to high construction standards

@highlight
the earthquake that struck nepal has left thousands of nepalis without shelter

@highlight
torrential rains making situation worse food and drinking water supplies could become a serious issue soon

@highlight
its unclear how bad conditions are closer to the epicenter
