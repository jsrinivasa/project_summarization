before bali nine duo andrew chan and myuran sukumaran the last australian executed for drug offenses abroad was van tuong nguyen by singapore in. while working assiduously behind the scenes to argue for clemency for the bali nine duo abbott nevertheless appeared to accept the inevitability of the executions even as he publicly reiterated australias opposition to capital punishment such a approach was also aided by an australian public that did not seem overly interested in the plight of chan and sukumaran. the executions of chan and sukumaran early wednesday morning will be a different story. all that changed when credible stories began to emerge about the rehabilitation of the two australian prisoners chan becoming a pastor and respected religious example for his fellow prisoners and sukumaran a passionate painter with impressive talent

@highlight
despite pleas for mercy indonesia executed eight prisoners on wednesday

@highlight
included two of the bali nine convicted drug traffickers from australia

@highlight
executions will damage relations between countries but public image will take longer to heal
