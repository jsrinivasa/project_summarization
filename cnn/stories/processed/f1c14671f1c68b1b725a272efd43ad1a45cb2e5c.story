the hollywood reporterandrew lesnie the oscarwinning cinematographer who spent more than a decade collaborating with director peter jackson on the six lord of the ringsand hobbit films has died he was. for the inaugural lord of the rings film jackson reached out to lesnie after seeing his work on babe and its sequel those australian films revolving around a pig and other animals featured impressive visual effects and proved to be big hits at the worldwide box office. id never worked with him or even met him before but hed shot the babe films and i thought they looked amazing the way hed used backlight and the sun and natural light to create a very magical effect jackson said in a interview and babe had that largerthanlife feel about it that i wanted. so when we began looking for dps in early i first decided to get either an australian or new zealand dp as theyd be used to the way we make films jackson continued every country is slightly different in that way and i immediately thought of andrew

@highlight
oscarwinning cinematographer andrew lesnie has died

@highlight
he is best known for lord of the rings the hobbit and babe
