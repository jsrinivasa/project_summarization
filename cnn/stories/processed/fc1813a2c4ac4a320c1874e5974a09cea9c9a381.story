most kids want to go out and play when they finish their homework early but zuriel oduwole isnt most kids when she gets ahead of her work she packs her camera and microphones jumps on a plane and interviews presidents instead. although shes just oduwole who is homeschooled through an online californian system is already a grader two years ahead of the rest of the kids her age. her mother patricia has a fulltime job as a computer engineer whilst her father ademola has taken time off his work in the tourism sector to help organize a lot of what zuriel and her three other siblings are doing there really is a lot happening in our household but somehow we make it work says patricia oduwole. i want to show them there is a lot more to africa than what we see on the news theres dancing music great culture and more

@highlight
zuriel oduwole is a filmmaker

@highlight
to date she has interviewed heads of state
