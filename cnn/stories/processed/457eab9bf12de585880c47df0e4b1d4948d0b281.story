the solar impulse the experimental plane attempting to fly around the world without using a drop of fuel has been grounded by the weather in china. with more than solar cells along the tops of its wings and fuselage the solar impulse stores up energy during the day in order to power the motors that carry it through the night typically at speeds no faster than a car on a highway. but so far the solar impulse has been spending a lot of time on the ground. our boss is the sun says solar impulse spokeswoman claudia durgnat

@highlight
solar plane attempting to be first to circumnavigate world without using fuel is stuck in china

@highlight
solar impulse attempts to prove the power of renewable energy
