washington new york philadelphia havana. the pope is expected to continue his international activism this july with a trip to south america where he will visit ecuador bolivia and paraguay just a few months later in late september francis will visit washington where he will address congress new york where he will address the un general assembly and philadelphia where he will celebrate a public mass thats expected to draw more than million people. s jake tapper on friday archbishop charles chaput the popes host in philadelphia said hed hadnt heard about the potential for a papal visit to cuba until he turned on the morning news. the vatican says pope francis may add another leg to his trip to the united states this september visiting cuba just months after he helped negotiate a diplomatic thaw between the two nations

@highlight
pope francis played key role in reestablishing diplomatic ties between cuba and us

@highlight
contacts with the cuban authorities are still in too early a phase vatican spokesman says
