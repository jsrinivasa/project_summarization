sabra dipping co is recalling cases of hummus due to possible contamination with listeria the us food and drug administration said wednesday. the potential for contamination was discovered when a routine random sample collected at a michigan store on march tested positive for listeria monocytogenes. the nationwide recall is voluntary so far no illnesses caused by the hummus have been reported. listeria monocytogenes can cause serious and sometimes fatal infections in young children frail or elderly people and others with weakened immune systems the fda says

@highlight
a random sample from a michigan store tested positive for listeria monocytogenes

@highlight
no illnesses caused by the hummus have been reported so far
