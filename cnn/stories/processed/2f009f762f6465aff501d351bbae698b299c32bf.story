famed cosmologist stephen hawking has proved his comedy chops on shows like the big bang theory and now hes trying his hand at musicals. galaxy song song was written by python member eric idle along with john du prez and is an intricate and informative lecture on the enormity of the universe fashioned into a bewitching and above all highly amusing pop song according to the comedy troupes site. the scene is derived from a filmed bit that monty python uses during its live shows. its not hawkings first music gig hes also featured on the pink floyd song talkin hawkin

@highlight
stephen hawking is a famed cosmologist and mathematician

@highlight
he sings monty pythons galaxy song in a hilarious new video
