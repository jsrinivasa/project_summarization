amid growing scrutiny over whether a volunteer deputy who killed a suspect during a sting operation was qualified to be policing the streets a new report raises a troubling allegation. i dont put a lot of stock in that report or the credibility of who would further that report brewster said. claims that the volunteer deputys records had been falsified emerged almost immediately from multiple sources after bates killed eric harris on april reporter dylan goforth said bates claims he meant to use his taser but accidentally fired his handgun at harris instead. shooting casts spotlight on volunteer police programs

@highlight
maricopa county sheriffs office in arizona says robert bates never trained with them

@highlight
he met every requirement and all he did was give of himself his attorney says

@highlight
tulsa world newspaper three supervisors who refused to sign forged records on robert bates were reassigned
