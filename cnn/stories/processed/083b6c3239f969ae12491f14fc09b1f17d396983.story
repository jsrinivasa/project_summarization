authorities in south carolina have released dash cam video in connection with the fatal shooting of walter scott but the footage does not show the actual shooting. this dash cam footage does not change the fact that at the moment the officer shot and killed mr scott that shooting was completely unjustified and that is the key point of both the criminal investigation and the civil lawsuit the lawyer said. scott exits his vehicle briefly and slager tells him to stay in the car scott then gets out of the car again and runs away out of the range of the dash cam. but bamberg was adamant the dash cam video does not alter what happened

@highlight
footage shows a traffic stop and early interactions between officer michael slager and walter scott

@highlight
the two men speak and then scott gets out of the car running

@highlight
slager charged with murder was fired from the north charleston police department
