a border guard was killed in a crossboundary fire exchange with militants in yemen this week the staterun saudi press agency reported thursday marking saudi arabias first publicly known military death since it launched airstrikes against rebels inside its southern neighbor. the clash occurred wednesday night at the border in southwestern saudi arabias asir region when militants in a mountainous area on the yemeni side fired on saudi border troops prompting them to return fire spa reported. besides the slain border guard identified as cpl salman ali yahya almaliki others suffered injuries that were not lifethreatening the saudi media outlet said. the offshore area has been a route for weapons smuggling into yemen as well as sudan and gaza so the navy has maintained a regular surveillance presence there the official said that increased fighting in the last several days in southern yemen has resulted in other ships trying to dock to help civilians leave the country

@highlight
militants in yemen fired on saudi border troops in saudi arabias asir region media outlet says

@highlight
rebels have taken yemens presidential palace in aden sources say

@highlight
us warships are patrolling off yemen in search of suspicious shipping a us defense official says
