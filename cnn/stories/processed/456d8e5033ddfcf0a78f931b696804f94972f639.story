be afraid be very afraid this is the warning the world deserves to hear because the leader of the free world refuses to look with clear eyes at the chief security challenges of the century the fruits of radical islam. of course its hardly a surprise president barack obama refuses to acknowledge all this in plain terms the president and his national security advisers have too often proven naïve with a dangerous habit of viewing the world not as it is but as they hope it could be. not only americans but also our allies should be very very afraid indeed president obamas refusal to simply call a problem like radical islam by its name strongly suggests he is unwilling to make the difficult decisions that must be made today if we are to stand a chance of defeating radical islamist groups. despite what the white house wants the world to believe a sober look at the security environment reveals the following key realities

@highlight
authors warn president obama must be clear about radical islam threat

@highlight
al qaeda still a looming threat they say
