wikileaks founder julian assange has agreed to be interviewed by swedish prosecutors in london his lawyer in sweden told cnn. assange has been holed up in the ecuadorian embassy in london since june to avoid extradition to sweden where prosecutors want to question him about allegations that he raped one woman and sexually molested another. the australian national has not been charged and denies the claims assange has said he fears sweden would extradite him to the united states where he could face the death penalty if he is charged and convicted of publishing government secrets through wikileaks. this assessment remains unchanged now that time is of the essence i have viewed it therefore necessary to accept such deficiencies to the investigation and likewise take the risk that the interview does not move the case forward particularly as there are no other measures on offer without assange being present in sweden

@highlight
the wikileaks founder is wanted for questioning over sexual abuse claims he denies the allegations

@highlight
assange has been holed up in the ecuadorian embassy in london since june
