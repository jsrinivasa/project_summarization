arizona investigators have released dramatic video of a walmart parking lot brawl that left a police officer wounded one man dead and reportedly involved members of a christian family band. the police dashcam video released friday shows cottonwood police approaching the group of eight people all identified as members of the gaver family around a large suv in a walmart parking lot on march. officers wanted to question them about the alleged assault of a walmart employee who was going into the store bathroom the police were accompanied by another walmart employee. police charged four members of the family with assaulting an officer and resisting arrest two minors were also taken into custody and are being held at a juvenile detention facility

@highlight
police questioned the group about an alleged assault of a walmart employee

@highlight
a man put a police officer in a headlock and fighting broke out
