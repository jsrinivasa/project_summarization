the last three defendants prosecuted in the hazing death of florida am drum major robert champion were convicted friday of manslaughter and hazing with the result of death reported cnn affiliate wftv. a total of defendants were charged originally but most took plea deals wftv reported. a medical examiner ruled champions death a homicide and said he died within an hour of being beaten champion suffered multiple blunt trauma blows the medical examiner said. champion died in november after a band hazing ritual in which he was beaten aboard a school bus after a football game in orlando florida the initiation required pledges to run down the center of the bus while being punched kicked and assaulted by senior members band members have said

@highlight
florida am drum major robert champion died in after a hazing ritual aboard a bus

@highlight
a jury convicted the last three defendants of manslaughter and hazing with the result of death
