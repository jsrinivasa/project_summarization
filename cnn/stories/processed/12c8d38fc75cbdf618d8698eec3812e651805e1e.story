chief justice john roberts is back in the spotlight. if the windsor majority votes in favor of marriage equality the ruling will be one of the most momentous decisions of the roberts court said judith e schaeffer of the constitutional accountability center which is advocating for samesex marriage will john roberts want to be remembered as having dissented from such a historic decision. for some conservatives a vote in favor of samesex marriage in the case would be a huge disappointment it would be akin to the type of betrayal they felt when justice david souter who was nominated to the bench by george hw bush and who retired in consistently voted with the liberals or when justice anthony kennedy a ronald reagan appointee disappointed them on earlier gay rights cases as well as abortion and the death penalty or when roberts infuriated conservative allies by providing the crucial fifth vote to uphold obamacare on the grounds that the law is a constitutional use of the governments taxing authority. yet both sides see the case as a legacymaking moment for the chief justice and advocates for samesex couples hold out hope that he will emerge as their ally they will be scrutinizing his words and actions tuesday for clues about how hell vote and whether he will upset some conservatives once again

@highlight
john roberts is at judicial crossroads as high court to hear key samesex marriage case

@highlight
case could decide whether samesex couples nationwide have constitutional right to marry

@highlight
chief justice disappointed conservatives earlier when he helped uphold obamacare
