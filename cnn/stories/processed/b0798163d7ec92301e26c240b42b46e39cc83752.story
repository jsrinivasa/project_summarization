the flight voice recorder aboard germanwings flight reportedly captured blaring cockpit alarms warning copilot andreas lubitz to pull up and that terrain was ahead but that was not enough to stop the aircraft from slamming into the french alps. however she adds in the case of the germanwings crash this technology i believe would have saved the flight not only would it have saved this flight and the germanwings passengers it would also save lives in situations where it is not a suicidal homicidal pilot it has implications literally for safer flight across the industry. barton says there are already plenty of safeguards in place in the united states for example he says the federal aviation administration rule that two people must always be in the cockpit during a flight might also have saved the germanwings flight that is a practice that several overseas carriers including germanwings and its parent company lufthansa have now adopted. commercial airline pilot john barton says there are other considerations to take into account as well in systems like this the control cannot be taken back from the aircraft or indiscriminately the aircraft could be taken over on just a normal flight and they cockpit crew wouldnt be able to get control back i think it is going to take a long time to operationally test this kind of technology before it is foolproof

@highlight
autopilot could have taken control of germanwings flight and flown plane to safe altitude

@highlight
but some experts say taking control away from humans could lead to other dangers

@highlight
another concern autopilot might be vulnerable to hackers
