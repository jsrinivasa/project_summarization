her second floor cafe on a dusty industrial road was painted with dashes of psychedelic colors and sabeen mahmud surrounded herself there with books people and discussions on technology human rights and womens entrepreneurship. two gunman shot her dead at pointblank range late friday after she locked up the second floor cafe in karachi for the night police said mahmud died from five bullet wounds. in the province of baluchistan where separatists have fought a virulent insurgency for years people have been disappearing regularly there have been steady allegations of mass abduction the lahore university of management sciences planned to host the discussion on the topic with human rights activist mama qadeer baloch but authorities shut it down. mahmuds killing broke hearts beating for nonviolence and progressive values across the country she freely said what she thought in a place where many people are too afraid to and by doing so spoke for many more people than just herself

@highlight
she hosted a controversial discussion on the disappearance of people

@highlight
she knew what she was doing was dangerous and had received death threats before

@highlight
mahmud loved books jimi hendrix and discussions about human rights
