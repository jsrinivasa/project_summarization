barney frank to say the least knows his way around politics in a chicago appearance recently the retired congressman had the soldout crowd at the center on halsted well entertained as he detailed his journey in public service. barney frank said he believes republicans want the supreme court to rule in favor of samesex marriage to provide political cover in the gop primary that may be true but its doubtful that will allow a candidate to avoid taking a position on the wave of socalled religious freedom bills currently snaking through redstate legislatures. frank youll recall was the first member of congress to marry someone of the samesex while in office and among other things he had some choice words for closeted politicians who vote against lgbt rights. but at the end of the day its about votes frank said when progressives get angry they march in the streets and when conservatives get mad they march to the polls if that holds true in winning is going to feel very strange

@highlight
lz barney frank may say lgbt rights winning but indiana law pushing them back and other states antilgbt moves a bad sign

@highlight
cruz huckabee jindal carson walker and some state judges rulings feel like reviving culture wars he says
