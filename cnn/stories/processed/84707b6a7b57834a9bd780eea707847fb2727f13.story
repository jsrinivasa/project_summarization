this weekend millions of people are expected to tune in to watch two men beat each other up why is this. if we just wanted blood and pain we wouldnt bother with the tame violence of payperview fisticuffs instead wed fire up a web browser and watch isis snuff videos for free but many people who feel no temptation to watch internet snuff feel sorely tempted to watch a big fight whats going on. but why do we like to watch fights in the first place over the past years ive watched boxing and mma in a spirit of nervous fascination watching fighters kick punch and strangle each other id be thinking im a civilized person i appear not to be a sociopath. so should we feel virtuous as we watch mayweather and pacquiaos epic brain damage contest i wouldnt go that far

@highlight
jonathan gottschall millions to tune in to see mayweatherpacquiao fight but this doesnt show resurgence of declining sport of boxing

@highlight
so why will so many watchhe says a fight is metaphor for the whole human condition with everything noble and ugly on display
