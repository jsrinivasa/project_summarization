there will be no hate crime charges for two males arrested in the beating of a man that may have been sparked by a question about the shooting of michael brown. there had been speculation that the suspects might be charged with a hate crime because the victim was white and the people who attacked him were black and because the punches were thrown following the michael brown reference. he declined and the young man asked his opinion about the shooting of michael brown an unarmed teenager killed by a police officer in nearby ferguson missouri last summer the man said that shooting raised questions nationwide about use of deadly force by police. the video showed a male unleashing a barrage of punches at the head of the victim who covered himself with his hand and forearms two other males joined in police said the attackers fled

@highlight
the two males arrested are and years old st louis police say in a tweet

@highlight
a video that went viral showed three black males attacking a white man
