fifteen buffalo were shot and killed on friday after a day on the loose in upstate new york. four men from gem farms in schodack new york from where the buffalo escaped were on the scene by a ravine in coeymans new york to kill the animals a decision heffernan said wasnt made lightly but that was necessary. george mesick the owner of the farm sat in the car listening to the radio as his buffalo were shot. twentytwo buffalo escaped from the farm on thursday half the farms stock including six that were shot thursday night in rensselaer county mesick said

@highlight
buffalo are shot on friday after escaping the day before from a farm in schodack new york

@highlight
police helicopters fly overhead and nearby schools put on alert in the final moments of the chase

@highlight
the herd breaks through three layers of barbed wire fencing and crosses the hudson river during the escape
