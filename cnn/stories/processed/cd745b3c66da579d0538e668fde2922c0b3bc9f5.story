fans of the late actor paul walker knew that watching him in furious would be bittersweet. not gonna lie i shed a few tears at the end of furious the tribute to paul walker was very well done one woman said monday on twitter. hers was just one of a flood of messages on social media from people who said they got choked up during scenes featuring walker who died at in a car crash in november before filming on furious was completed to finish walkers scenes the makers of the movie used body doubles computergenerated images and even the actors brothers. a scene late in the movie shows him and mia playing on a beach with their son while the crew looks on essentially saying goodbye then his longtime buddy dom reminisces about their years together leading to a montage of walker scenes from the first six movies

@highlight
moviegoers are tearing up during the emotional ending of furious

@highlight
the movies end is a tribute of sorts to actor paul walker who died during filming
