much of the world has been stunned by the huge increase of migrant deaths in the mediterranean this year increasing the number of deaths at sea by a factor of compared to the same time last year almost all the deaths have occurred in the perilous central mediterranean crossing from libya to italy. the flows of migrants across the mediterranean are unlikely to stop italian authorities estimate that up to migrants in libya are waiting to cross following refugees and migrants who arrived in italy last year these flows reflect a significant increase in the number of refugees and internally displaced people across the world with a total estimate of million people. since then australias prime minister tony abbott has also suggested that europe adopt a tougher approach saying the only way you can stop the deaths is to stop the people smuggling trade the only way you can stop the deaths is in fact to stop the boats thats why it is so urgent that the countries of europe adopt very strong policies that will end the people smuggling trade across the mediterranean. in italy both mare nostrum and operation triton were stemming an inevitable tide given the political instability in north africa the migrants currently in libya are in a perilous limbo with a growing civil war having displaced more than libyans and with human rights watch noting that the conflict and collapse of government authority has eliminated any semblance of law and order from large parts of libya

@highlight
the european union is trying to stop thousands of migrants from drowning at sea

@highlight
migrants risk their lives by paying people smugglers to get them to europe

@highlight
australia has successfully stopped the flow of migrant boats to its waters
