when cases of pappy van winkle one of the rarest and most expensive bourbons in the world were reported missing from a kentucky distillery in october it was the crime heard round the whiskeydrinking world. however according to melton curtsinger was involved in numerous thefts of pappy van winkle as well as eagle rare bourbon both in bottles and barrels melton said curtsinger distributed the highly coveted bourbon through a network of connections in his softball league. on tuesday a franklin county grand jury indicted nine members of a criminal syndicate that collaborated to promote or engage in the theft and illegal trafficking of liquor from two different kentucky distilleries frankforts buffalo trace makers of pappy and the nearby wild turkey distillery makers of the eponymous bourbon according to the indictment. the alleged ringleader according to assistant commonwealth attorney zach becker is gilbert toby curtsinger a loading dock worker at the buffalo trace distillery curtsinger and his wife julie each face eight charges for allegedly engaging in organized crime

@highlight
indicted on organized crime charges related to bourbon thefts

@highlight
employees at two kentucky distilleries among those indicted
