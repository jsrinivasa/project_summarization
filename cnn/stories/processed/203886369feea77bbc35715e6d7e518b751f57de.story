seventy years ago anne frank died of typhus in a nazi concentration camp at the age of. but new research released by the anne frank house shows that anne and her older sister margot frank died at least a month earlier than previously thought. as the russians advanced further the bergenbelsen concentration camp became even more crowded bringing more disease a deadly typhus outbreak caused thousands to die each day. in anne and seven others hiding in the amsterdam secret annex were arrested and sent to the auschwitzbirkenau concentration camp

@highlight
museum anne frank died earlier than previously believed

@highlight
researchers reexamined archives and testimonies of survivors

@highlight
anne and older sister margot frank are believed to have died in february
