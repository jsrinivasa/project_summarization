freddie gray did not get timely medical care after he was arrested and was not buckled into a seat belt while being transported in a police van baltimore police said friday. batts told reporters that at the third stop an officer saw gray on the floor of the van asking for a medic the officer and the van driver picked him up and put him on the seat the commissioner said. police commissioner anthony batts told reporters there are no excuses for the fact that gray was not buckled in as he was transported to a police station. gray died sunday one week after baltimore police arrested him

@highlight
attorney for the family of freddie gray says developments are step forward but another issue is more important

@highlight
family will have a forensic pathologist do an independent autopsy

@highlight
police say gray should have received medical care at different points before he got to a police station
