as the transport plane comes in to land at sanaa airport the deep scars of the brutal conflict tearing yemen apart are only too clear wrecked aircraft line the runway and nearby buildings lie in ruins. gusts of wind blow dust across the runway as crates and pallets of emergency nutrition and medical equipment pile up rapidly beside the plane. as they work an air india plane is taxiing away from the terminal thousands have fled the country on evacuation flights in recent weeks as the situation in yemen has deteriorated. even before saudi airstrikes most of the million people in yemen required humanitarian assistance to meet their most basic needs according to the united nations

@highlight
almost million people in yemen are in need of humanitarian aid according to un

@highlight
planeload of aid supplies including food and medicine was flown in to sanaa on tuesday

@highlight
a rare ceasefire was negotiated to allow the plane to land briefly
