australia has recalled its ambassador to indonesia for consultations after two australians were among eight drug smugglers executed by firing squad early wednesday. gularte is the second brazilian to be executed in indonesia this year with the first marco archer cardoso moreira prompting the country to recall its ambassador for consultations. foreign minister retno marsudi said the country had no plans to recall its own ambassador in response this is a legal case this is not a political case so at this very stage we do not have any plan to call our ambassador back from canberra he said. the men then aged in their early twenties were arrested in as part of the bali nine a drug smuggling gang that intended to import kilograms pounds of heroin from bali to australia they failed

@highlight
brazil extends deepest sympathy to family of executed brazilian

@highlight
indonesia executed eight death row inmates early wednesday

@highlight
australian pm calls executions cruel and unnecessary
