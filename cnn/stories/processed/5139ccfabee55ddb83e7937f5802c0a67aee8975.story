after a russian fighter jet intercepted a us reconnaissance plane in an unsafe and unprofessional manner earlier this week the united states is complaining to moscow about the incident. russian state news agency sputnik reported the us plane was flying toward the russian border with its transponder switched off according to a defense ministry spokesman maj gen igor konashenkov said the russian jet flew around the us plane several times to identify it and get its tail number. this is not the first time the us has complained about an incident involving a and a a year ago a russian jet flew within feet of a over the sea of okhotsk in the western pacific according to us officials who called it one of the most dangerous close passes in decades the pentagon complained to the russia military about that incident. on tuesday a us was flying over the baltic sea when it was intercepted by a russian flanker the pentagon said the incident occurred in international airspace north of poland

@highlight
the incident occurred on april north of poland in the baltic sea

@highlight
us says plane was in international airspace

@highlight
russia says it had transponder turned off and was flying toward russia
