a year after its cannes debut and finally seeing a theatrical release lost river is still causing quite a stir. lost river receives a limited theatrical release in the us and uk on april. gosling said that lost river began as a collection of speculative shots of the brewsterdouglass projects the first black social housing development in america and a place motown legends the supremes and boxer joe louis once called home. as much as the visual content of lost river revolves around a maudlin preoccupation with dereliction and perhaps plays on the outside worlds perception of detroit aspects of the narrative suggest hope and the possibility of reincarnation for the city

@highlight
ryan goslings directorial debut lost river is set in the city of detroit
