an elite police commando unit waited hours for transport from nairobi kenya to garissa where alshabaab terrorists had taken over a university on april according to one of the commandos who participated in the response operation. mbithi conceded that one of the two aircraft ultimately used to transport the commandos to garissa left nairobi at am to fly to mombasa on a scheduled flight hours after the garissa terror attack began. we have a military garrison in garissa and the work began immediately after the attack was reported and continued for a number of hours until we were able to rescue students of the students that had been taken hostage by these terrorists so the response was adequate she said. the police assault to retake the university compound began around pm and lasted about minutes one commando was killed in the assault all four alshabaab attackers were killed

@highlight
commando says air transport was delayed for hours as alshabaab forces slaughtered students

@highlight
on april were killed at a university in garissa kenya most were students
