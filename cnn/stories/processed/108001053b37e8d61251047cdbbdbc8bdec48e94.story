she predicted events that unfolded in the middle east well before they happened and her book on iraq became required reading for many military leaders trying to understand the country. in the us military actually flew mackey to iraq to teach commanders from the armys infantry division while the war was still raging her book on iraq became required reading for many military officers. her book on iraq was published one year before the usled invasion of the country in the book forewarned of the consequences of such action. her book the reckoning iraq and the legacy of saddam hussein portended some of the outcomes of the war in iraq but also drew some sharp criticism

@highlight
mackey predicted what would happen to iraq if the us invaded and deposed saddam hussein

@highlight
she also wrote a book credited with helping bridge gap between arabs and americans
