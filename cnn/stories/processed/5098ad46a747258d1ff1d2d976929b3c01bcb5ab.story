as the earth shook in nepal tremors were felt over the border in india as well. by monday india was in fullscale crisis mode from airports across india planes flew in loaded with aid and trucks made the trip by land from indias east aiming for more remote areas. it was a call to action within hours of the first magnitude quake india began planning a massive crossborder aid mission. with each hour the scale of the devastation in nepal became clearer some people are known to have died so far and plans in india where people were also killed got bigger

@highlight
india has launched a massive aid mission to its earthquakehit neighbor nepal

@highlight
disaster relief troops and tons of food water and medicine have been flown in
