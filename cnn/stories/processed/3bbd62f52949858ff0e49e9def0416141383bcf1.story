when etan patz went missing in new york city at age hardly anyone in america could help but see his face at their breakfast table. i think anyone who sees these confessions will understand that when the police were finished mr hernandez believed he had killed etan patz but that doesnt mean he actually did and thats the whole point of this case fishbein has said. another mans name has also hung over the patz case for years jose antonio ramos a convicted child molester acquainted with etans babysitter etans parents stan and julia patz sued ramos in the boy was officially declared dead as part of that lawsuit. hernandez told police in a taped statement that he lured patz into a basement as the boy was on his way to a bus stop in lower manhattan he said he killed the boy and threw his body away in a plastic bag

@highlight
the young boys face appeared on milk cartons all across the united states

@highlight
patzs case marked a time of heightened awareness of crimes against children

@highlight
pedro hernandez confessed three years ago to the killing in
