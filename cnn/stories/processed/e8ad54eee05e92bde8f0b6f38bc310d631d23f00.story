shes one of the hottest and most successful latinas in hollywood but now sofia vergara is playing defense in a legal battle initiated by her exfiance he wants to keep the two frozen embryos from their relationship both female. these embryos are not inside her body he said they are in a nitrogen tank in a clinic in beverly hills so there the planned parenthood of central missouri vs danforth case cannot be used by the sofia vergara team to allege that the embryos would present some danger to her health as they did in that case. usually when embryos are created whether the couple is married or just consenting adults theres usually a power of attorney that is described to these embryos if they are frozen for future use he said. loeb is suing the colombianborn actress in los angeles to prevent vergara from destroying their two embryos conceived through in vitro fertilization in november according to published reports by new york daily news and in touch magazine

@highlight
loeb says he filed the lawsuit and doesnt want want money from his ex

@highlight
nick loeb reportedly wants to prevent vergara from destroying the embryos

@highlight
vergara spoke of freezing embryos with loeb in a interview
