jodi arias was sentenced to life in prison monday for the gruesome murder of her exboyfriend travis alexander. arias was found guilty of firstdegree murder in may the jury that convicted her found the murder was especially cruel making arias eligible for the death penalty however that same jury was unable to reach a unanimous decision on whether she should live or die. earlier travis alexanders sisters gave their victim impact statements hillary alexander said shes trying to block her brother from her life. maricopa county judge sherry stephens could have sentenced arias to life with the possibility of early release after years but decided the convicted killer should spend the rest of her life behind bars

@highlight
jodi arias is sentenced to life in prison with no possibility for parole

@highlight
arias expressed remorse for her actions
