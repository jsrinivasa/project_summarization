an improvised bomb exploded near a un vehicle traveling near the northeastern somali city of garowe on monday morning killing six people including four aid workers for the international childrens agency unicef. unicef the un aid agency focused on children said it employed four of the dead four other workers were in serious condition the agency said. the bomb went off as a vehicle was taking workers from their guest house to their office the local police chief said the bomber was inside the vehicle among the un staff. this attack is not just targeted at the united nations but in attacking unicef alshabaab has also attacked somali children it is an attack against the future of our country and i condemn it in the strongest possible terms he said

@highlight
the bombing is an attack against the future of our country somalias president says

@highlight
unicef says the staff members vehicle was hit by an explosion on its way to their office

@highlight
four wounded staff members are in serious condition the agency says
