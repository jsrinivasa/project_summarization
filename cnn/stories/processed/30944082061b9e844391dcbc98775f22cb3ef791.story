in this holy week let us be reminded of what the word of god says about fair and living wages. today thousands of mcdonalds workers live below the federal poverty line many get help from government programs especially food stamps to make it week to week whether we like it or not our tax dollars help mcdonalds keep wages artificially low. the mcdonalds announcement that the company is going to raise wages for of its employees is a significant victory for fastfood cooks and cashiers and those of us who support them by standing up together fastfood workers are making it less acceptable for profitable companies like mcdonalds to pay wages so low that its workers are boxed into poverty. mcdonalds took a small step forward by raising wages for a small minority of the people who run their stores its time for the corporation to find a way to raise wages for everyone working in its restaurants so they can pay their bills put some money back into their community and expand prosperity and opportunity across our land

@highlight
william barber mcdonalds will raise minimum wage for of workers this is a step in right direction but falls short in three ways

@highlight
he says it leaves out of workers is not enough to lift workers from poverty company prevents workers from speaking out in a union
