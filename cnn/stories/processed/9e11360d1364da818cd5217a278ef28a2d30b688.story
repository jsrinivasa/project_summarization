one israeli citizen was killed and another injured in what police are calling a suspected terror attack wednesday night near hebrew university in jerusalem. israel police spokesman micky rosenfeld said a arab motorist from east jerusalem struck two people standing at a bus stop in the french hill section of the city. from the investigation and first findings there is a strong suspicion that were talking about a terror attack rosenfeld said. one victim identified by police as shalom yohai cherki died at the hospital a woman remains in serious condition according to rosenfeld

@highlight
incident occurred wednesday night near hebrew university in jerusalem police say

@highlight
one victim a man has died a woman is in serious condition

@highlight
the suspect is a arab from east jerusalem israeli police say
