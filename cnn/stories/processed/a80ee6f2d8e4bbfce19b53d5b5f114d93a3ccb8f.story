the arrest and death of freddie gray in baltimore has stoked protests and accusations of police brutality but its unclear how gray arrested on a weapons charge april suffered a severe spinal cord injury that led to his death seven days later. what we know gray was arrested on a weapons charge in a highcrime area of baltimore known for drugs he gave up without the use of force according to baltimore deputy police commissioner jerry rodriguez. what we dont know its unknown what caused the spinal cord injury that led to his death a week after the arrest and its also unknown what if anything happened inside the van. at am police called an ambulance for gray police say gray requested medical attention including an inhaler and an ambulance later took him to the university of maryland medical centers shock trauma center

@highlight
gray was arrested on a weapons charge april he was dead seven days later

@highlight
gray was placed inside a police van after his arrest its unclear if anything happened inside the van

@highlight
gray has a criminal history but its unclear whether that had anything to do with his arrest or death
