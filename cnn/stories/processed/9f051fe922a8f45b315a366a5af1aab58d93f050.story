just as mimeograph machines and photocopiers were in their day online activity blogs youtube channels even social media platforms like facebook and twitter have fully emerged as the alternative to traditional mainstream media. the bottom line online journalists operating outside the restraints of mainstream media have become the most vulnerable targets for governments and independent actors where there is the restrictive rule of law journalists are vulnerable to the anger of officialdom where the rule of law is weak they are vulnerable to the attacks of killers who seldom if ever answer to the rule of law. it is not just the low cost of posting online that attracts dissidence though that in itself is liberating it is the lack of access to traditional print and broadcast media in authoritarian countries that is really the driving force leading disaffected voices to post online. but in second place in asia is vietnam where cpjs most recent prison census showed vietnam holding reporters behind bars as of december add one more in late december nguyen dinh ngoc a prominent blogger who was arrested for lawviolating after police searched his home in southern ho chi minh city on december and two more in january nguyen quang lap and hong le tho arrested on antistate charges of abusing democratic freedoms and you can see the pattern because the print and broadcast media are so totally government controlled mainstream journalists seldom go to jail any more in vietnam only two investigative print reporters remain behind bars in vietnam their cases dating back to and both were accused of accepting bribes for dialing back critical news coverage

@highlight
going online has become the path of least resistance if you want to make yourself heard

@highlight
but where there is the restrictive rule of law journalists are vulnerable to the anger of officialdom

@highlight
from china to malaysia journalists and bloggers have been jailed even killed
