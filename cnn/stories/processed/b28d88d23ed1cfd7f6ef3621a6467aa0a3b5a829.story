monday night as unrest raged across baltimores streets amanda rothschild lay awake in her remington home a neighborhood in the northern part of the city thinking about what the next day would be like. i was initially a little concerned that the reaction and the unrest on monday would take away from important issues facing the city duda said but he has seen residents of the city step up to show their support for not only freddie gray but the need to address social and economic disparities in the city. as a coowner of charmingtons a cafe in the intersection of three major baltimore neighborhoods rothschild knew that the cafe and its workers needed to support the city after all the unrest. late monday evening over email rothschild and other workers from charmingtons decided that the cafe would remain closed on tuesday so that staff members could spend the day in the community to help clean up or peacefully protest

@highlight
some local businesses in baltimore are banding together to show support for change

@highlight
business owners have given workers opportunities to peacefully demonstrate

@highlight
several businesses dealing with destruction looting from monday riot
