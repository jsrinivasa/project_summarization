martin omalley and jim webb share little in common. orc poll pdf of national democrats only said omalley and webb were their top choice in a january poll from bloomberg politics and the des moines register pdf omalley was at among iowa democrats while webb found himself at. webb wasnt nearly as active opting instead to stay close to his seat near the front of the venue and chat with a small group of people around him as webb cut into his sizable helping of pork omalley was standing directly behind him shaking hands. asked whether he enjoys the retail politics that is crucial in early voting states like iowa and new hampshire webb smiled skepticism of retail politics is not new for webb as a oneterm democratic senator webb was rumored to loathe the burdens that came with campaigning namely fundraising and retail politics

@highlight
there are few similarities between democrats martin omalley and jim webb

@highlight
but they find themselves in a similar position as longshot presidential hopefuls
