duke university students and faculty members marched wednesday afternoon chanting we are not afraid we stand together after a noose was found hanging from a tree on campus. you came here for the reason that you want to say with me this is no duke we will accept this is no duke we want this is not the duke were here to experience and this is not the duke were here to create duke president richard brodhead told the crowd. a statement issued by duke said there was a previous report of hate speech directed at students on campus. duke officials have asked anyone with information about the rope noose which was found near a student center at am to call campus police

@highlight
the noose made of rope was discovered on campus about am

@highlight
hundreds of people gathered wednesday afternoon to show solidarity against racism

@highlight
duke official says to unknown perpetrator you wanted to create fear but the opposite will happen
