the underwater search area for missing malaysia airlines flight looks set to double in size. ministers remain committed to bring closure and some peace to the families and loved ones of those on board malaysia airlines flight the three countries said in a joint statement. if the searchers havent found anything by the time theyve covered the entire priority zone the search will stretch into a new equally vast area government officials from malaysia australia and china announced thursday. the size of the doubled search area the equivalent of more than square miles would be bigger than the us state of pennsylvania

@highlight
malaysia australia and china announce possible new phase of hunt for missing plane

@highlight
the search of the current priority zone is expected to be completed in may
