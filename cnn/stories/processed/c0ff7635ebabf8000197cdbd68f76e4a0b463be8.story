a longtime friend of joni mitchell has filed a legal petition seeking to be named the singersongwriters conservator. contrary to rumors circulating on the internet today joni is not in a coma joni is still in the hospital but she comprehends shes alert and she has her full senses a full recovery is expected the document obtained by a certain media outlet simply gives her longtime friend leslie morris the authority in the absence of doctor care to make care decisions for joni once she leaves the hospital. in her april petition morris says mitchell is unconscious at this time she mitchell remains unconscious and unable to make any responses and is therefore unable to provide for any of her personal needs. she remains at the hospital according to leslie morris court petition filed april morris is described as mitchells friend of more than years in the legal documents

@highlight
singersongwriter joni mitchell is still hospitalized

@highlight
her longtime friend leslie morris wants to be appointed her conservator
