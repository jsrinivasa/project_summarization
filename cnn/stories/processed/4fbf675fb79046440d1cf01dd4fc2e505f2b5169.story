the terrorist group alshabaab has claimed an attack on garissa university college in eastern kenya in which many people have been killed and still more taken hostage. crossborder raids into kenya by the group however date back to alshabaab incursions triggered a military response by the government in nairobi which sent troops to somalia as part of an african union mission in support of somalias internationally recognized government that had been under pressure from alshabaab and other militants for several years. alshabaab literally the youth split from unity of islam in and merged with another radical islamist group the socalled islamic courts union as their alliance obtained control of somalias capital mogadishu in ethiopia the only majority christian country in the region took military action against the group the offensive weakened alshabaab and pushed it back into the rural areas of central and southern somalia but it failed to defeat it. attacking a university in northern kenya and separating christian from muslim students epitomizes the way alshabaab advances itself by exploiting religious tribal and nationalist identities ultimately though this all comes down to a struggle for control over people over territory and over resources

@highlight
terrorist group alshabaab has attacked a kenyan college killing and taking hostages

@highlight
it is a clear indicator the security situation in east africa is deteriorating says stefan wolff

@highlight
more than military action aloe is needed to combat terrorism in the region he says
