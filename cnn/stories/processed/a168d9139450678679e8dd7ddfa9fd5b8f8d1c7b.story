even in the horror of syrias civil war there are few places that showcase the scale of the destruction and the senselessness of the loss of life more than the yarmouk camp on the outskirts of damascus. i fled palestine when i was seven years old she said but i will not leave the yarmouk camp even if i am or years old yarmouk camp is equal to my soul i built it with my bare hands i carried its stones on my head from a village and laid the foundation to my home block by block i carried them on my head. while the battle for yarmouk is very typical of syrias civil war the conflict here is unique most of those fighting on all sides are palestinians progovernment factions besiege the area from the outside cutting off supplies and aid most of the time the inside is held by antiregime groups some of which are islamists. set up as a refugee camp for palestinians in the it slowly evolved into a neighborhood over the years but since it has been engulfed in the syrian conflict two weeks ago isis fighters stormed yarmouk and that made life for those still inside even worse than it was before

@highlight
isis raided yarmouk refugee camp near damascus on april

@highlight
palestinian refugees remain in the camp cut off from vital aid

@highlight
i will not leave the yarmouk camp even if i am or years old
