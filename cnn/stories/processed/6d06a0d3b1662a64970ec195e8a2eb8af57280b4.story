about years ago give or take a guy fell into a well last month he made science history. the altamura man became the oldest neanderthal to have his dna extracted by researchers it took them more than years to get around to doing it. altamura mans intact skull and jumbled pile of bones made for a great specimen but they were wedged into a panoply of stalactites and stony globules deposited by water dripping over them for tens of thousands of years. calcite pebbles line the neanderthals eye sockets nose bone and an upper jaw like a hundred decorative piercings analysis of the calcite has shown the bones to be to years old

@highlight
scientists in southern italy have known about him since

@highlight
researchers worried that rescuing the bones would shatter them
