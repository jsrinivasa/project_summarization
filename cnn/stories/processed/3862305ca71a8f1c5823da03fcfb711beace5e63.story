for a decade the new south china mall the biggest shopping mall in the world has been an embarrassment for its owners and china. in the mall changed name from south china mall to new south china mall living city and a revitalization plan was drawn up by current owners the founders group a conglomerate set up by peking university and its subsidiary pku founder. the mall spans five million square feet of shopping area making it the largest in the world in terms of leasable space more than twice the size of mall of america the biggest shopping center in the united states. outside the mall a giant egyptian sphinx and a replica of the arc de triomphe were erected alongside fountains and canals complete with venetian gondolas it even boasted an indoor roller coaster

@highlight
for a decade the new south china mall has lain empty

@highlight
it was labeled a ghost mall symbol of chinas runaway speculation on real estate

@highlight
however a recent visit showed it may be springing back to life
