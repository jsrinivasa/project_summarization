ten years ago with her high school diploma and a backpack maggie doyne left her new jersey hometown to travel the world before college. its become so much more than just a little girl with a backpack and a big dream its become a community and i want to teach and have other people take this example and hope this sets a precedent for what our world can be and look like. doyne we started with the home and then school we run the school lunch program then we needed to keep our kids really healthy so we started a small clinic and then a counseling center from there we started getting more sustainable and growing our own food and then from there we decided to start a womens center. maggie doyne its communal living for sure

@highlight
nepal civil war aftermath inspired maggie doyne to help children

@highlight
doynes blinknow foundation supports a home for children and a school that educates hundreds more

@highlight
heroes
