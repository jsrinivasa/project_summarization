the quaint town of dunblane scotland has been set abuzz by the wedding of tennis legend andy murray to his longterm girlfriend kim sears. saturdays event dubbed the royal wedding of scotland took place at dunblane cathedral with cheering crowds spilling onto the streets to support their homegrown talent. the people of dunblane braved wind rain and even snow to catch a glimpse of the happy couple having seen murray grow from a young boy into a british sporting legend. fellow tennis veteran rafael nadal and first minister of scotland nicola sturgeon posted their congratulatory messages on twitter

@highlight
uk tennis star andy murray wed his longterm girlfriend kim sears in dunblane scotland

@highlight
saturdays event has been dubbed the royal wedding of scotland
