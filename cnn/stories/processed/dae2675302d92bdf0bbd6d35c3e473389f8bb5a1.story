two cnn heroes are among the earthquake survivors in kathmandu nepal and they are struggling in the aftermath. anuradha koirala who rescues victims of sex trafficking has a rehabilitation center in kathmandu that is home to young women and girls while her primary facility seems structurally unharmed all of the children have been sleeping outdoors because of aftershocks followed by a second earthquake on may. jake woods disaster relief organization team rubicon has a team of experienced veterans and first responders on the ground in kathmandu the group deployed a medical and assessment team to aid the nepalese people in several remote villages outside of the city. arlene samens group one heart worldwide has set up tented birthing centers where pregnant women can safely deliver their babies especially in hardhit districts robin lim a disaster response midwife who founded bumi sehat joined samen in kathmandu to help the mothers

@highlight
anuradha koirala and young women and girls have been sleeping outdoors because of aftershocks

@highlight
pushpa basnet and children she cares for were forced to evacuate their residence

@highlight
heroes and their organizations now assisting in relief efforts
