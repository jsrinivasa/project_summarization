a long long time ago. those five words when uttered or sung makes baby boomers immediately think of don mcleans pop masterpiece american pie its hard to believe that his phenomenal minute allegory which millions of americans know by heart is years old all sorts of historical crosscurrents play off each other in this timeless song brilliantly gilded with the unforgettable chorus which starts as bye bye miss american pie there is no real way to categorize mcleans american pie for its hybrid of modern poetry and folk ballad beerhall chant and highart rock. on tuesday christies sold the handwritten manuscript of the songs lyrics for million to an unnamed buyer. mclean was a paperboy when on february he saw that buddy holly ritchie valens and jp the big bopper richardson had been tragically killed in an airplane crash in clear lake iowa the next day i went to school in shock and guess what mclean recalled nobody cared rock n roll in those days was sort of like hula hoops and buddy hadnt had a big hit on the charts since by cathartically writing american pie mclean has guaranteed that the memory of those great musicians lives forever

@highlight
manuscript of american pie lyrics is sold to unnamed buyer for million

@highlight
douglas brinkley the song a talisman for its age brings joy to people years later
