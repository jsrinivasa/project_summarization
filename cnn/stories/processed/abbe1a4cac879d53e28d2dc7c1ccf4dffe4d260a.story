sawyer sweeten grew up before the eyes of millions as a child star on the endearing family sitcom everybody loves raymond early thursday he committed suicide his sister madylin sweeten said in a statement. we are devastated to report that our beloved brother son and friend sawyer sweeten took his own life he was weeks away from his birthday at this sensitive time our family requests privacy and we beg of you to reach out to the ones you love. sawyer sweeten was born in may in brownwood texas. this morning a terrible family tragedy has occurred madylin sweeten said in a statement passed on by her manager dino may

@highlight
sawyer sweeten played across from his twin brother and their sister as the children of ray and patricia barone

@highlight
report sweeten was visiting family in texas and is believed to have shot himself
