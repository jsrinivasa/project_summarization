they took yarmouk by storm a sea of masked men flooding into the streets of one the worlds most beleaguered places. the regime now is about to destroy the yarmouk camp he said and isis i dont know they didnt do anything except rename yarmouk camp as yarmouk islamic state. besieged and bombed by syrian forces for more than two years the desperate residents of this palestinian refugee camp near damascus awoke in early april to a new even more terrifying reality isis militants seizing yarmouk after defeating several militia groups operating in the area. an estimated refugees are now trapped inside yarmouk stuck between isis and syrian regime forces in the deepest circle of hell in the words of un secretarygeneral ban kimoon

@highlight
isis has seized control of large parts of the yarmouk palestinian refugee camp in syria

@highlight
an estimated refugees are trapped between militant groups and regime forces

@highlight
un in the horror that is syria the yarmouk refugee camp is the deepest circle of hell
