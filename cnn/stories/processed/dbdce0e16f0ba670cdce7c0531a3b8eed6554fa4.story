they left iran unwillingly often in a hurry it was for the best for these refugees iran is a difficult place to be gay or lesbian. i couldnt understand that in iran homosexuality isnt accepted she said recently by phone from geneva a lot of my friends are gay and for me it was a huge cultural difference between europe and iran. mahmoud ahmadinejad the former president of iran famously said during a trip to the united states in iran we dont have homosexuals like in your country. rasti a photographer for four years used her mamiya to give people a view into the spirit of these refugees despite their status they cannot earn a living in turkey and it take years to getting papers to go to a new country the refugees are still full of hope and love despite their rough lives rasti said

@highlight
homosexuality is illegal in iran

@highlight
denizli turkey is host to hundreds of gays and lesbians from iran

@highlight
photographer laurence rasti traveled to turkey to explore her fascination with identity issues
