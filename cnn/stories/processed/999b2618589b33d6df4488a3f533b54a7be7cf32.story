they were huddled in the back of a tugboat some were without shoes their coats and jackets still wet were piled up in a huge container behind them. the migrants mostly from subsaharan africa arrived in the port of augusta sicily around tuesday after being picked up by the tugboat off the coast of libya. the two boats they had been in were barely seaworthy the tugs montenegrin captain told me the discarded coats he said would be thrown away. we had flown to sicily from rome following news that as many as migrants had been lost at sea the tragedy adds to the mounting death toll among those fleeing war and poverty in africa and the middle east

@highlight
why they fled

@highlight
they were packed onto two barely seaworthy boats tug captain said
