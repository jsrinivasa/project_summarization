real housewives of beverly hills star and former child actress kim richards is accused of kicking a police officer after being arrested thursday morning. ms richards was displaying symptoms of alcohol intoxication including slurred speech and belligerent insolent behavior cursing at the officers and passively resisted arrest police said in a statement after being transported to the station for booking richards kicked one of the officers in the leg however the officer was not injured. kim richards appeared in disneys escape to witch mountain and wonderful world of color as a child and was a frequent guest star on television series though her acting career later stalled she had a significant role in the film black snake moan. richards reportedly entered rehab in for serious issues after what watchers deemed erratic behavior on the reality show which also features her sister kyle richards the richardses are the aunts of former tv star paris hilton

@highlight
real housewives of beverly hills star kim richards was arrested early thursday morning

@highlight
beverly hills police say richards wouldnt leave a hotel when asked and later struck an officer
