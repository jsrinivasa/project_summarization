a judge declaring he wasnt comfortable with sevenyear prison terms given earlier to three educators in the atlanta public schools cheating scandal on thursday reduced their sentences to three years in prison. all defendants sentenced to prison have appealed and are out on bond the lower prison sentences given to other defendants ranging from one to two years have not been reduced. the judge said he was tired of dealing with the atlanta public schools cheating scandal which he referred to as this mess. cotman daviswilliams and pitts all school reform team executive directors got the harshest sentences during an april hearing seven years in prison years of probation and fines

@highlight
i had never seen a judge conduct himself in that way defendants lawyer says

@highlight
a judge reduces prison sentences for educators in the atlanta public schools cheating scandal

@highlight
im not comfortable with it judge jerry baxter said of the original longer sentences
