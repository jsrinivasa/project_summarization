at least people were killed during a shipwreck off the northern coast of haiti the countrys civil protection directorate told cnn on thursday. a small boat carrying about migrants left from the area of le borgne west of caphaitien on wednesday night it began to sail toward the island of providenciales in the turks and caicos when it was caught in bad weather civil protection spokesman joseph edgar celestin said. rescuers most of them volunteers from the town of le borgne and surrounding communities were dispatched to the scene and rescued at least people celestin said. the vessel hit a reef and sank as it tried to return to shore near le borgne

@highlight
a small boat carrying about migrants left from the area of le borgne west of caphaitien wednesday

@highlight
it got caught in bad weather on its way to turks and caicos
