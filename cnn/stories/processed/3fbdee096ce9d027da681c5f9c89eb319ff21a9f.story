all eyes are going to be on the new kid finally allowed to play and the big kid who for so long wanted nothing to do with him cuba and the united states in the same diplomatic playground. its remains to be seen how much cuba will risk its warming relations with the united states to back up ally venezuela. now its time to see how they play and who they play with especially venezuela which often falls out with washington for crushing dissent at home and supplying havana with billions of dollars in oil. cuba pulled off a diplomatic coup by marshaling the support of other regional countries to insist on their attendance at the summit of the americas

@highlight
cuba pulled off a diplomatic coup by gaining attendance at summit of the americas

@highlight
first time since the us has not blocked cubas attempt to join

@highlight
cuba is trying to reestablish itself at the twoday summit in panama
