charlie hill sits in a dark bar on a blindingly sunny havana day. as the interview ends hill lights another cigarette a group of americans touring havana in a classic car pull up to the park they have no idea they are feet away from a wanted fugitive but as hill would say thats cuba. for years cuba has provided refuge for hill from facing charges that he killed a new mexico police officer and hijacked an airliner to havana. hill isnt interested in staying in touch he doesnt have a cell phone too poor he says he wont say exactly where he lives in havana he has to be cautious

@highlight
charlie hill contemplates returning to the united states for family and blackberry pie

@highlight
he would also have to face justice on charges of killing a police officer and hijacking a plane
