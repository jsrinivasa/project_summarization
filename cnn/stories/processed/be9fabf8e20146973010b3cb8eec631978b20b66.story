the abduction of more than schoolgirls a year ago this week captured global attention and inspired the hashtag bringbackourgirls but the horrors for nigerias children are widespread. it also launched a social media campaign using the hashtag bringbackourchildhood the campaign has leading snapchat artists sharing images based on drawings from children in nigeria chad niger and cameroon artwork can also be seen on facebook twitter instagram and tumblr. the number of children running for their lives within nigeria or crossing over the border to chad niger and cameroon has more than doubled in just less than a year. around children have been forced to flee their homes as a result of the conflict in northeast nigeria between boko haram military forces and civilian selfdefense groups unicef said monday

@highlight
more than million people are displaced including children unicef says

@highlight
the kidnappings that inspired bringbackourgirls were a year ago this week

@highlight
unicef is launching a bringbackourchildhood campaign
