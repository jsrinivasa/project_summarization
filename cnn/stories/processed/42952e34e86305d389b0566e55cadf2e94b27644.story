nobel literature laureate guenter grass best known around the world for his novel the tin drum has died his publisher said monday he was. grass novel characters are the forgotten the downtrodden and the weird the nobel committee said and like oskar matzerath the boy in the tin drum they often slip into surreal situations. the tin drum which was published in breaks the bounds of realism by having as its protagonist and narrator an infernal intelligence in the body of a threeyearold a monster who overpowers the fellow human beings he approaches with the help of a toy drum the nobel committee wrote. in his excavation of the past günter grass goes deeper than most and he unearths the intertwined roots of good and evil the nobel committee wrote when it awarded him the literature prize in

@highlight
grass tried in his literature to come to grips with world war ii and the nazi era

@highlight
his characters were the downtrodden and his style slipped into the surreal

@highlight
he stoked controversy with his admission to being a member of the waffen ss
