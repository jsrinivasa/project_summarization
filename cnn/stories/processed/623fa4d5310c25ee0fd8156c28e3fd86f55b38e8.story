a sexual harassment complaint has been filed against new zealand prime minister john key after a waitress complained about him repeatedly pulling her ponytail at an auckland cafe. key publicly apologized to bailey a waitress at his local cafe for repeatedly tugging on her ponytail after she complained about his behavior in a blog post. key who regularly visits the auckland cafe with his wife told reporters that his behavior was in the context of a bit of banter but said that he had apologized when it was clear she had taken offense. the blog post says that the prime minister offered the offended waitress two bottles of his own jk pinot noir wine by way of apology

@highlight
a sexual harassment complaint has been filed against pm john key after a waitress complained about him repeatedly pulling her ponytail

@highlight
kiwi prime minister accused of pulling a waitress hair on several occasions despite her obvious discomfort

@highlight
pm key later apologized but said that he was merely engaging in banter

@highlight
politicians and public figures have condemned his behavior
