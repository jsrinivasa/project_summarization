after months of intensive questioning a jury has finally been picked for the trial of colorado movie theater massacre suspect james holmes. holmes defense attorneys asked for a change of venue after the jury was seated tuesday the judge denied their request noting that a jury had already been seated. holmes is accused of killing people and injuring others when he allegedly opened fire inside a packed theater during the midnight showing of the dark knight rises on july the onetime neuroscience doctoral student faces counts including murder and attempted murder charges now he has pleaded not guilty by reason of insanity if convicted of the most serious charges he could face a death sentence. wearing a gray dress shirt and tan slacks holmes sat quietly in court on tuesday looking relaxed and leaning back in his chair for much of the day as lawyers made their picks from the jury pool he looked down often but smiled occasionally such as when the judge made a joke about a juror needing to share his cheetos and when the district attorney accidentally addressed a male juror as a miss

@highlight
in the murder trial of james holmes jurors and alternates have been selected

@highlight
the mostly middleaged group includes women and five men

@highlight
jury selection started in january opening statements are scheduled to begin on april
