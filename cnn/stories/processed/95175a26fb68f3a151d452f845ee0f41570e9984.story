its only june but may be remembered as the year the term transgender fully entered mainstream consciousness. but violence against trans people especially transgender women of color remains a national plague according to recent statistics from the human rights campaign at least transgender women were slain last year in the united states and at least seven have already been killed this year of those victims all but one were black or latina. the may issue of vogue has a photo spread with transgender model andreja pejic who said on instagram this week that she was told by various people many times over that the chances of me ending up on these pages were slim to none a transgender character had a recurring storyline on the justwrapped final season of glee while transgender activist and youtube star jazz jennings will star in a reality show debuting on tlc this summer. transgender is an umbrella term for people whose gender identity their internal personal sense of being a man or a woman differs from whats typically associated with their sex at birth some transgender people alter their bodies through hormones and or surgery although many dont

@highlight
olympic hero bruce jenner appears on vanity fair cover as caitlyn

@highlight
transgender people in the united states are riding an unprecedented wave of visibility

@highlight
shows such as transparent orange is the new black have raised awareness
