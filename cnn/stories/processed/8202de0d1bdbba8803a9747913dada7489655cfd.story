the announcement this year of a new original dr seuss book sent a wave of nostalgic giddiness across twitter and months before publication the number of preorders for what pet should i get continues to climb. a neverbeforeseen image from the new volume by dr seuss aka theodor geisel it features a brother and sister familiar to seuss fans as they ogle a prim feline in a pet shop window and ponder. when what pet should i get debuts july it will be the first new original dr seuss book since oh the places youll go in it features the spirited siblings from the beloved classic one fish two fish red fish blue fish and is believed to have been written between and. ted loved and had pets himself as a young boy on up through adulthood and that makes the wonderful excitement and buzz for this new book all the more special said susan brandt president licensing and marketing of dr seuss enterprises

@highlight
dr seuss new book what pet should i get will have a first printing of million copies

@highlight
the publisher released a neverbeforeseen image from inside the book by theodor geisel
