facebook may soon need to add just got served divorce papers to its list of relationship statuses now that a new york judge has said the social media site is an acceptable way for a brooklyn woman to serve her husband with a summons for divorce. if blooddzraku refuses the summons spinnell said the judge can move forward with a divorce by default for his client. before cooper agreed to her using facebook baidoo had to prove the facebook account belongs to her husband and that he consistently logs on to the account and would therefore see the summons. ellanora arthur baidoo has been trying to divorce her husband for several years according to her attorney andrew spinnell

@highlight
ellanora arthur baidoo has been trying to divorce her husband for several years

@highlight
husband doesnt have permanent address or permanent employment

@highlight
baidoo is granted permission to send divorce papers via facebook
