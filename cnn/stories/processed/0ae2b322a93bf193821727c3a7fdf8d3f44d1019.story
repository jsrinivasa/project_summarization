protests are gaining steam in baltimore after a man died from a devastating injury he allegedly suffered while in police custody. baltimore police union attorney michael davey told reporters wednesday that officers had every right to chase gray. five of the six officers involved in grays arrest have provided statements to investigators the baltimore police department said wednesday. baltimore police officials say theyre being as transparent as they can about the case while their criminal investigation is ongoing and they say they plan to hand over details from the investigation to the state attorneys office next week

@highlight
freddie grays death has fueled protests in baltimore

@highlight
demonstrators accuse police of using too much force and say officers should face charges
