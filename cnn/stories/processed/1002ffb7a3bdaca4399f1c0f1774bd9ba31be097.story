silent almost shy as she headed into manhattan criminal court ailina tsarnaeva was anything but timid when it came to a perceived rival prosecutors say. not much is known about the two tsarnaeva daughters ailina and bella though their last known address was in north bergen new jersey. along with charges of making bomb threats ailina tsarnaeva has a past record that includes misleading police in a counterfeiting case she pleaded guilty but got no jail time she was also charged with leaving the scene of an accident but that charge was dismissed. though not elaborating on her beliefs ailina tsarnaeva has said she believes as her mother does that her surviving brother dzhokhar and her dead brother tamerlan are innocent

@highlight
dzhokhar tsarnaev is on trial for his alleged role in the boston marathon bombings

@highlight
tsarnaevs sister ailina was in court in december related to aggravated harassment charges

@highlight
tsarnaevs mother is wanted on felony charges of shoplifting and destruction of property
