it took one somali woman seven months and miles to trek to libya from there she hoped to cross the mediterranean sea so her baby could be born in europe she didnt get there. the somali womans baby sabrine was born a week after she was detained. that was the scene in a tent outside the mater dei hospital in valletta malta a chance for citizens and dignitaries to remember of whats thought to be hundreds of migrants killed when their crammed ship sank in the mediterranean sea. the deaths are the latest illustration of the increasing flow of migrants from north africa and the middle east through the mediterranean and into europe assuming they survive the trip

@highlight
migrant women hope to reach europe so their babies will be born there

@highlight
hundreds of arrested migrants are detained in libya while officials try to figure out what to do

@highlight
a funeral is held outside a valletta malta hospital for migrants killed in ships sinking
