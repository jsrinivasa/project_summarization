unicef said friday that an initial shipment of tons of medical supplies meant to help innocents caught up in the havoc of yemen had at last landed in yemens capital sanaa. the humanitarian situation is worsening all the time with increasingly limited access to water basic sanitation and critical health services the statement quoted unicef yemen representative julien harneis speaking from amman jordan as saying the supplies we have managed to bring in today can make the difference between life and death for children and their families but we know they are not enough and we are planning more of these airlifts. unhcr is extremely concerned about the dangers for anyone trying to flee across the red sea and gulf of aden where there are no search and rescue operations the agency said in a statement last year lives were reported lost in sea crossings to yemen unhcr appeals to all ships in the area to be extra vigilant and assist any boats in distress we also ask that countries with vessels in waters near yemen including surveillance and antipiracy vessels instruct their ships to help with rescues. unicef said its cargo included antibiotics bandages syringes iv sets and other medical supplies included as well the agency said were micronutrients for up to children and water storage materials airlifted through djibouti from unicefs supply center in denmark

@highlight
un agency says refugees from yemen have arrived in horn of africa asks ships in area to be vigilant

@highlight
who at least people have been killed more than injured in three weeks

@highlight
unicef aid includes medical supplies for up to people and more airlifts are planned
