the coverup is often worse than the crime. henry louis gates stands accused of scrubbing part of a segment in his pbs documentary series finding your roots because the actor ben affleck put pressure on him afflecks concern was that the segment would have aired his familys dirty laundry which includes a slaveholding ancestor benjamin cole. affleck said in a statement posted on facebook that he didnt want any television show about my family to include a guy who owned slaves i was embarrassed and gates later explained that he subbed that part of the segment for another that made for more compelling television. but providing a window into the importance of slaverys past to americas present should never just be about what makes for good television gates missed an opportunity

@highlight
dorothy brown ben affleck and henry louis gates scrubbed segment about afflecks slaveowning ancestors from tv show

@highlight
she says they two missed a chance to discuss racial issues that still fester in this country
