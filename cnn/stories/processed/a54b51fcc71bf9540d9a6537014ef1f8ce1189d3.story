in russias tightlymanaged democracy where being an opposition politician can seriously damage your health chances to question the countrys leader are rare. critics of the kremlin of course slam this entire event as russias imitation of democracy in action its hard to imagine a truly critical question they say getting aired on national television here in fact its best not to look at this event as an opportunity for russians to question their leader at all. the kremlin says they will have to sift through well over million emails video messages and texts to decide who gets to ask what on the big day a few wouldbe questions released ahead of the event give us a flavor of whats on russias mind. sanctions and russias deep economic crisis which saw the value of the russian currency the ruble plunge in value by against the dollar is set to be a major a theme

@highlight
putin to spend hours fielding questions from the general public on live television

@highlight
sanctions and russias deep economic crisis likely to be a major theme

@highlight
critics of the kremlin slam event as russias imitation of democracy in action
