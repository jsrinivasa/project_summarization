the hollywood reportergeoffrey lewis a prolific character actor who appeared opposite frequent collaborator clint eastwood as his pal orville boggs in every which way but loose and its sequel has died he was. lewis began his long association with eastwood in high plains drifter he also appeared with the actor in thunderbolt and lightfoot bronco billy pink cadillac and midnight in the garden of good and evil. lewis the father of oscarnominated actress juliette lewis died tuesday family friend michael henderson said no other details were immediately available. lewis scored a golden globe nomination for playing bartender earl tucker on the cbs sitcom flo the spinoff of alice that starred polly holliday and he had recurring roles on such series as falcon crest and the syndicated lands end

@highlight
geoffrey lewis appeared in many movies tv shows

@highlight
actor was frequently collaborator with clint eastwood

@highlight
actress juliette lewis his daughter called him my hero
