smugglers lure arab and african migrants by offering discounts to get onto overcrowded ships if people bring more potential passengers a cnn investigation has revealed. the smuggler took her to an unfinished building on the outskirts of tripoli near the citys many ports where the migrants they have already found are kept until the crossing is ready the building could only be reached by walking down a trashlittered alleyway and featured a series of packed rooms separated by curtains where dozens sat well over the migrants she was promised would be in her boat. the conversation recorded using a mobile phone exposes the prices and incentives used to gather as many migrants as possible onto ships. an estimated migrants have died so far this year on the dangerous mediterranean crossing but still more wait to try to reach europe

@highlight
investigation uncovers the business inside a human smuggling ring

@highlight
discount offered for every referral of another paying migrant desperate to reach europe
