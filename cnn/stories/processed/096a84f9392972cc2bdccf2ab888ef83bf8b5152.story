volvo says it will begin exporting vehicles made in a factory in southwest china to the united states next month the first time chinesebuilt passenger cars will roll into american showrooms. geely chairman li shufu said he hoped the company would eventually become a global car brand but said there were no current plans to begin export its homegrown brand to us showrooms. china surpassed the us as the largest market for car sales globally in and most major automakers build cars in china. for many us consumers china is still more closely linked with cheap clothing and electronics than luxury vehicles but samuelsson downplayed any concerns about quality

@highlight
volvo says it will begin exporting chinesebuilt cars to the us in may

@highlight
its the first time made in china cars will be available in us showrooms

@highlight
but its unlikely that chinese car brands will take on developed markets
