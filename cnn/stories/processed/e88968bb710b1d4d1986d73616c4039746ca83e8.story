government officials are trying to track down vacationers who stayed at villas in the virgin islands who may have been exposed to a deadly pesticide. methyl bromide is banned from indoor use and is only approved as an agricultural pesticide other pest control companies on the virgin islands were found in possession of methyl bromide and officials said they are checking records to see whether it was used improperly ken mapp the governor of the virgin islands said it was. authorities are trying to track down anyone who has stayed at the affected villas or who might have been exposed. local officials said methyl bromide is suspected to have been used improperly several times in the us virgin islands in different parts of the island even the governor said his condominium complex was fumigated with it in without his knowledge

@highlight
methyl bromide is suspected to have been used improperly several times in the us virgin islands local officials say

@highlight
teen brothers exposed to the pesticide while on vacation are both in comas parents are recovering
