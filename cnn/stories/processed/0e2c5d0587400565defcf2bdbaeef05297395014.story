traveling to iran these days the mood among many people and the government can probably best be described using two words confident and optimistic. we would like the us to change its rhetoric and tone of voice so that our nation could have more trust in the us military leadership pourdastan continued we trust the american people but the tone of us government and military officials is such that we still consider the us a threat. after a preliminary framework for a possible nuclear deal was reached between iran and world powers many here believe a final agreement is possible and most hope that widespread sanctions relief could be on the horizon. as the us saudi arabia and other countries criticize iran for its alleged involvement in funding and supplying the houthi rebels who have seized power in much of yemen iranian president hassan rouhani has praised the military as peacemakers for the middle east

@highlight
irans military held annual national army day parade over the weekend

@highlight
top military official says he hopes usiranian enmity will fade

@highlight
us has welcomed limited iranian help in fight against isis but neither side plans full coordination
