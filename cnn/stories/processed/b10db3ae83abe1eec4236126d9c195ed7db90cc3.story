one of tokyos most prominent districts has taken a small but potentially significant step to recognizing samesex unions in japan. conservative groups were vocal in their opposition with one known as the network pushing for normalization of education telling the japan times that granting samesex couples the same rights as all other japanese citizens would degrade the familial system and practice that heterosexual unions have long preserved in human history. the government of shibuya ward one of the capitals most famous shopping and trendy entertainment districts passed ordinance on wednesday paving the way for partnership certificates for samesex couples allowing them some of the rights of married heterosexual couples. samesex partners who are registered with the districts ward office will be able to hold visitation rights in hospitals and cosign tenancy agreements

@highlight
shibuya ward in tokyo passes an ordinance that gives samesex couples some of the rights of married heterosexual couples

@highlight
activists welcome the decision hope that it will lead to greater equality for lgbt people in japan

@highlight
recent poll finds most young japanese open to the idea of gay marriage
