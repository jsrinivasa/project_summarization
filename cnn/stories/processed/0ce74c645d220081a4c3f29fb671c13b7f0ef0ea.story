its a case of mistaken identity that reached international proportions. derived on this information and in compliance with international law the judge in charge of the case asked interpol to intervene to make the girl appear at a hearing in which the court would confirm her identity the statement said. once in houston and with questions about her identity being raised by the girls biological parents the mexican consulate in that city ordered dna testing the results confirmed that alondra is not the daughter of the houston woman. a girl was returned to mexico from texas on wednesday after dna tests showed shes not related to a woman who claimed the teenager was her daughter

@highlight
a girl is seized by authorities who thought she was the daughter of a woman in houston

@highlight
dna tests show she is not

@highlight
the mother of alondra luna nuñez says they stole my child
