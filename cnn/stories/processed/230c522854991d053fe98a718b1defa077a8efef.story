never mind cats having nine lives a stray pooch in washington state has used up at least three of her own after being hit by a car apparently whacked on the head with a hammer in a misguided mercy killing and then buried in a field only to survive. a cat in tampa florida found seemingly dead after he was hit by a car in january showed up alive in a neighbors yard five days after he was buried by his owner. thats according to washington state university where the dog a friendly whiteandblack bully breed mix now named theia has been receiving care at the veterinary teaching hospital. she was taken in by moses lake washington resident sara mellado

@highlight
theia a bully breed mix was apparently hit by a car whacked with a hammer and buried in a field

@highlight
shes a true miracle dog and she deserves a good life says sara mellado who is looking for a home for theia
