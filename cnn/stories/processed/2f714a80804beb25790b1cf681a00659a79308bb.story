isis is a problem that is off the charts historically and has sent the united states into uncharted territory when it comes to putting down the terror group the obama administrations point man in the fight recently told cnn. this is a problem that is off the charts historically he said referring to the more than foreign fighters who have gone into syria just put that into perspective its about twice the number that went into afghanistan in the over a period to fight the soviet union and those came really from only a handful of countries. the comments which brett mcgurk made in an exclusive interview were some of the administrations strongest to date in describing the challenge the united states and its allies face in battling isis. the united states has also been stepping up efforts to involve sunni groups in the fight against isis to date that involvement has been extremely limited as sunni tribes see shiite militias many with horrendous human rights records take the lead

@highlight
obamas point man in isis fight doesnt rule out us military action beyond iraq and syria

@highlight
brett mcgurk iraqi leader making progress with sunni tribes in planned anbar offensive
