a new york jury deliberating the fate of the man charged with the killing of etan patz is struggling to reach a verdict the little boys disappearance more than three decades ago sparked an era of heightened awareness of crimes against children. another mans name has also hung over the patz case for years jose antonio ramos a convicted child molester acquainted with etans babysitter etans parents stan and julia patz sued ramos in the boy was officially declared dead as part of that lawsuit. since their young sons disappearance the patzes have worked to keep the case alive and to create awareness of missing children in the united states. i think anyone who sees these confessions will understand that when the police were finished mr hernandez believed he had killed etan patz but that doesnt mean he actually did and thats the whole point of this case fishbein has said

@highlight
etan patz disappeared in his face appeared on milk cartons all across the united states

@highlight
his case marked a time of heightened awareness of crimes against children

@highlight
pedro hernandez confessed three years ago to the killing
