you might call her a watchdog because this boston area doberman really has a thing for timepieces. last thursday jeff courcelle came home from work to find mocha a pure bred fawncolored doberman pincher hovering over a pile of screws metal pieces three watch heads and some chewed leather straps. the doberman whom her owners describe as more goofy than scary had pulled down a basket of wrist wear from a shelf in their bedroom and eaten nearly all the contents. parkinson said friday mocha was her playful energetic curious doberman self

@highlight
a bostonarea dog ate three of her owners wristwatches

@highlight
a veterinarian removed about lb of watch parts from her stomach

@highlight
mocha the doberman is now doing well
