it took prosecutors months to present witnesses to support their claim that former nfl star aaron hernandez killed semipro player odin lloyd. prosecutors have said in pretrial hearings that hernandez may have been mad at himself for possibly showing lloyd the spot where that double murder happened during trial prosecutors suggest a text written by hernandez the day before the murder saying he was buggin for showing lloyd the spot may have played a role in plotting to kill lloyd. theres only one potential problem with that claim the time lloyd was killed hadnt been made public yet by the time hernandez met with kraft so how could hernandez have known when lloyd was killed. prosecutors have said lloyd might have done or said something that didnt sit well with hernandez they claimed hernandez rounded up some friends and orchestrated a hit to settle the score

@highlight
closing arguments in the case are set for tuesday

@highlight
aaron hernandez is charged with firstdegree murder in the killing of odin lloyd

@highlight
his defense lawyers made their case on monday
