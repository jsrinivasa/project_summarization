its the beer so good the danes hate to see it leave or so carlsberg advertisements used to say. carlsberg will remain very much inside carlsberg city making specialty beer and building a tourist center that is expected to attract halfamillion visitors peryear when it opens in. we call it carlsberg brand and experience center said thomas kjelfred communications consultant at visit carlsberg but its not a brand house its a brew house until six years ago we brewed million liters a beer here. the area in question aptly named carlsberg city has been home to the famous carlsberg brewery since and with it a big slice of danish cultural history

@highlight
new neighborhood named carlsberg city set to emerge in copenhagen denmark

@highlight
district has been built on site of beer companys former brewery
