just weeks after marvels daredevil premiered its first season on netflix the company confirmed tuesday that a second season will be coming in. daredevil is just one of four series that the disneyowned marvel has committed to airing on netflix expect to see avengers characters jessica jones iron fist and luke cage in their own upcoming series leading into the defenders miniseries programming event. the show focuses on attorney matt murdock played by charlie cox who was blinded as a child as he fights injustice by day using the law by night he continues the fight becoming the superhero daredevil and using his powers to protect the new york neighborhood of hells kitchen. its gotten rave reviews with tight adherence to its source materials history high production quality and a nononsense dramatic flair daredevil excels as an effective superhero origin story a gritty procedural and an exciting action adventure says review site rotten tomatoes

@highlight
the critically acclaimed daredevil will be back for season

@highlight
charlie cox plays a blind attorney by day who is a superhero by night
