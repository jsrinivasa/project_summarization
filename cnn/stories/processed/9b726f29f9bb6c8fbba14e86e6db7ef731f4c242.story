italians have a saying too much of something cripples it. its also about not having a sense of artistic and cultural attachment the care to cherish a countrys valuable monuments and thats because italians have always lacked a sense of national belonging italy despite its millenary history is one of the worlds most modern states unified in. patriot massimo dazeglio once said now that weve made italy we need to make italians. were overcrowded with so many frescoed churches medieval castles and roman ruins that we simply dont know what to do with them let alone care for a proper upkeep

@highlight
italy boasts the highest number of unesco world heritage sites in the world

@highlight
italy doesnt know how to exploit treasures and appears not to care about them writes silvia marchetti
