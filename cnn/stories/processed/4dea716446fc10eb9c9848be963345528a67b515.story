he wouldnt give his name but the name tattooed on his neck gertrude gave him away. as bukowski said of the gertrude tattoo its not a common name to have around your neck. now that taylor is once again locked up the focus has shifted more to how he left in the first place opining that the escape wouldnt have occurred if everybody had followed protocol bukowski said its believed the inmate wasnt in his cell at lockdown. thats what kankakee county illinois sheriff tim bukowski said saturday about kamron taylor the convicted murderer turned jail escapee until his arrest overnight in chicago

@highlight
the sheriff says a big mistake contributed to the inmates escape

@highlight
months after being convicted of murder kamron taylor escaped an illinois jail

@highlight
he is captured by police miles away in chicago authorities say
