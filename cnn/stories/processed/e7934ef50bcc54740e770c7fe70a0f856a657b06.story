chinese president xi jinping and indian prime minister narendra modi are two dynamic leaders at the helm of the worlds two most populous nations. as pragmatic as president xi is as pragmatic as prime minister modi is economic integration is going to happen regardless of trust or not said haiyan wang. so when modi touches down in beijing next month for his second meeting with xi can both leaders cast aside areas of political tension to get down to business. while modis india is out to put its economy on the fast track chinas economy is slowing after over years of breakneck growth president xi is managing the slowdown with efforts to boost domestic spending while cracking down hard on corruption

@highlight
india predicted to outpace china as as worlds fastestgrowing economy in next year

@highlight
chinas economy is slowing after over years of breakneck growth

@highlight
but experts say india simply cant size up against chinas raw economic might
