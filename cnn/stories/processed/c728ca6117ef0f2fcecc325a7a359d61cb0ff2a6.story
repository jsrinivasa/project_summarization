a lot of questions not too many answers. the questions that many of you have are the same questions that were asking how was mr gray injured were our proper protocols and procedures actually followed what are the next steps to take from here said baltimore mayor stephanie rawlingsblake. affiliate wjz protesters rallied over the weekend in baltimore demanding answers. our hope and goal here is to be as informative as we can without compromising the criminal investigation said kowalczyk were very troubled by this we want to find out the answers as much as the public does

@highlight
an attorney for freddie grays family alleges that police are involved in a coverup

@highlight
there are ongoing administrative and criminal investigations

@highlight
baltimores mayor promises to get the bottom of what happened
