it came a day early baltimore investigators handed their files on freddie grays death over to prosecutors thursday but the public shouldnt expect much. no reports will be made public police commissioner anthony batts said echoing an assertion he made last week that even after the prosecutors receive the files the task force assigned to grays death will continue investigating. baltimore police have established a task force of investigators including members of the force investigation unit and homicide detectives to look into grays death batts said. asked during her thursday interview why no police officers have yet been charged in grays case given that if the case involved civilians instead of officers there would ostensibly be probable cause for arrests rawlingsblake an attorney by trade said the question amounted to speculation and cited her efforts to reform the baltimore police department and repair the communitys mistrust in police

@highlight
prosecutors get investigative report a day early but dont expect immediate word on charges

@highlight
attorney general were continuing careful and deliberate examination of the facts

@highlight
gray family was told answers were not going to come quickly and thats fine attorney says
