a judge this week sentenced a former tsa agent to six months in jail for secretly videotaping a female coworker while she was in the bathroom prosecutors said. boykin was terminated last year when the investigation began tsa holds its employees to the highest ethical standards and has zero tolerance for misconduct in the workplace tsas ross feinstein said in a statement. during the investigation detectives with the metro nashville police department in tennessee also found that the agent daniel boykin entered the womans home multiple times where he took videos photos and other data police found more than videos and photos of the victim on boykins phone and computer. the judge randall wyatt on friday called the invasion of privacy egregious

@highlight
former tsa agent daniel boykin videotaped his female coworker in the restroom authorities say

@highlight
authorities say they found videos and photos of the victim on boykins phone and computer

@highlight
boykin worked in an administrative capacity and didnt do public security screenings tsa official says
