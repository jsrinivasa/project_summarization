filipinos are being warned to be on guard for flash floods and landslides as tropical storm maysak approached the asian island nation saturday. its now classified as a tropical storm according to the philippine national weather service which calls it a different name chedeng it boasts steady winds of more than mph kph and gusts up to mph as of pm am et saturday. ahead of the storm isabela gov faustino dry iii warned saturday that residents should act as if this will be no ordinary typhoon. aldczar aurelio a meteorologist with the philippine atmospheric geophysical and astronomical services administration pagasa said the storm was centered miles southwest of aurora province as of pm am et and heading west at a mph clip

@highlight
once a super typhoon maysak is now a tropical storm with mph winds

@highlight
it could still cause flooding landslides and other problems in the philippines
