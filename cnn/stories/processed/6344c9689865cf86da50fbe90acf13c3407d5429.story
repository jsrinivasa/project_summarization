what began as psychological torment namecalling and humiliation turned into beatings so severe cifuentes feared for her life one day two men sent by her husband showed up at her house armed with a shotgun and orders to kill her they probably would have succeeded but after the first bullet was fired cifuentes two sons dragged her inside still in her deeply conservative community it took neighbors two hours to call for help and cifuentes lost her arm. for years adelma cifuentes felt worthless frightened and alone never knowing when her abusive husband would strike. although the situation for girls and women in guatemala is alarming there are signs the culture of discrimination may be slowly changing with the help of an organization known as cicam or centro de investigación cifuentes was finally able to escape her husband and get the justice she deserved he is now spending years behind bars. but the abuse didnt stop there when she returned home cifuentes husband continued his attacks and threatened to rape their little girl unless she left thats when the nightmare finally ended and her search for justice began

@highlight
genderbased violence is at epidemic levels in guatemala

@highlight
according to the united nations two women are killed in guatemala every day

@highlight
five abuse survivors known as la poderosas have been appearing in a play based on their real life stories
