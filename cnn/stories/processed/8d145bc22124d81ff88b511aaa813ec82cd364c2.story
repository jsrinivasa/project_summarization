a shooting that prompted the lockdown of the us capitol for several hours saturday was a suicide and does not have an apparent connection to terrorism capitol police chief kim dine said in a news conference. members of the capitol police force which responded to the scene did not fire their weapons dine said. the unidentified shooter had a backpack and a rolling suitcase that authorities treated as treated suspicious packages prompting the lockdown as they were investigated capitol police said. dine said there was no nexus to terrorism apparent so far in this incident

@highlight
capitol police said a male shot himself as shocked onlookers watched

@highlight
the incident appears to have no connection to terrorism police said
