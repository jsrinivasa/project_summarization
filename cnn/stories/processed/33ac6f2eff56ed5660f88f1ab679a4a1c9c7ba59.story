pope francis will visit cuba on his way to the united states in september the vatican said wednesday a trip that will come months after he helped negotiate a diplomatic thaw between the two countries. the exact timing of the cuba trip wasnt immediately released but the vatican said the pope would stop in cuba before his planned late september stops in washington new york and philadelphia. francis will be the third consecutive leader of the roman catholic church to visit cuba st john paul ii stopped there for several days in and pope benedict xvi visited for three days in. church officials in havana said that they expect francis visit to be shorter than those of his predecessors officials from the vatican are expected to travel to the island soon to finalize logistics for francis trip the officials said

@highlight
trip will come before pope francis arrives in united states

@highlight
francis played key role in reestablishing diplomatic ties between cuba and us
