the other day i searched through hundreds of photos hoping to find a starting point to write this article looking through old photos is usually an enjoyable experience coming across a wedding or remembrances of happy times with family and friends. on this occasion however i found myself flipping through images from a devastating time we dearly wish had never happened but cannot afford to forget. white sand beaches stained with black sludge oilchoked waterways and wildlife shuttered businesses and frontlawn signs pleading for justice and help. five years after the deepwater horizon oil rig disaster that killed people devastated livelihoods and wreaked havoc on the already fragile natural resources of the gulf of mexico region its time to ask ourselves what have we learned and what are we willing to do make sure it doesnt happen again

@highlight
out of sight out of mind doesnt apply to communities along the gulf of mexico philippe cousteau says

@highlight
we must take the time and effort needed to understand our natural resources he says

@highlight
he says our understanding of how the gulf works remains limited
