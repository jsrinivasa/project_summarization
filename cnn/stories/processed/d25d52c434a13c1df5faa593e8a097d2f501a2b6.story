you know the phrase dodging a bullet forget about it probably not going to happen anymore. the us military said this week it has made great progress in its effort to develop a selfsteering bullet. true to darpas mission exacto has demonstrated what was once thought impossible the continuous guidance of a smallcaliber bullet to target said jerome dunn darpa program manager. it all conjures up images of a cartoon character frantically fleeing a bullet that follows him wherever he goes only these bullets are traveling at hundreds of miles per hour and even the road runner cant run that fast

@highlight
bullets equipped with optical sensors can follow moving targets

@highlight
the smart bullets can help shooters compensate for high winds

@highlight
the goal of the program is to give shooters greater range and make american troops safer
