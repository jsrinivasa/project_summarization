parents who refuse to vaccinate their children can lose up to of welfare benefits a year under a new government policy australian prime minister tony abbott has announced. the choice made by families not to immunize their children is not supported by public policy or medical research nor should such action be supported by taxpayers in the form of child care payments said abbott in a joint statement with social services minister scott morrison. thousands of families could lose out on welfare payments with the australian government estimating more than children under the age of seven have not been vaccinated because of their parents objections. currently parents can choose to opt out of vaccinations for medical or religious reasons or by stating they are conscientious objectors and still receive taxpayer funded child care benefits

@highlight
australia to cut welfare benefits for parents who refuse to vaccinate their children

@highlight
the no jab no pay policy will come into effect in january

@highlight
the australian government estimates more than children who have not been vaccinated
