sleek chassis alluring paintwork and a need for speed but these are no ordinary motors when pedal hits metal an ecofriendly process gets set in motion. at mays shell ecomarathon abucars team will be hoping to race past the sleek autonov iii creation from the university of lagos. other ecofriendly engineers are less about sleek and more focused on comfort. elsewhere engineers are working on green vehicles for the whole family one ghanaian inventor is building suvs with electric motors powered by rechargeable batteries

@highlight
african students and car enthusiasts are creating ecofriendly cars

@highlight
nigerian students will compete in an ecomarathon in may
