anthony doerrs all the light we cannot see a novel centered on the world war ii bombing of stmalo france and two characters on opposite sides of the war won the pulitzer prize for fiction monday. doerrs work was also a finalist for the national book award its his second novel and fourth work of fiction including two short story collections. doerrs novel had received rave reviews upon its release last spring. i must blame anthony doerr for lost sleep because once i started reading his new novel all the light we cannot see there was no putting it down wrote william t vollmann in the new york times book review

@highlight
anthony doerrs all the light we cannot see wins pulitzer for fiction

@highlight
elizabeth kolberts the sixth extinction wins general nonfiction prize
