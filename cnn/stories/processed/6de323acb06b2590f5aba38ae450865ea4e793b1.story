theres a stampede of investigators in california. in the california case the fbi said the results of its investigation will be forwarded to the us attorneys office in the central district of california and justice department in washington to determine whether prosecution is warranted. the aclu of southern california issued a statement friday saying that it was deeply troubled by the images. the fbi said friday that it will investigate whether civil rights were violated during the videotaped beating of a suspect in san bernardino the suspect allegedly fled by car foot and horseback when law enforcement officers tried to arrest him

@highlight
san bernardino sheriff says deputies have been put on leave

@highlight
video from a news helicopter shows deputies punching and kicking a man repeatedly
