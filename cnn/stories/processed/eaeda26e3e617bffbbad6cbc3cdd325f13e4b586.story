georgia southern university was in mourning thursday after five nursing students were killed the day before in a multivehicle wreck near savannah. today should have been a day of celebration for this bright group of students at st josephscandler hospital said in a facebook posting it was their last day of clinical rotations in their first year of nursing school clinicals include handson instruction at a health care facility. fellow nursing students brittney mcdaniel and megan richards were injured as was another person who was not identified by the georgia state patrol. georgia southern flew flags at halfstaff and counseling was offered to students a campuswide vigil was held thursday night

@highlight
georgia state patrol provides more details of crash

@highlight
georgia southern university mourns five nursing students killed in auto accident

@highlight
five cars and two tractortrailers were involved in crash on interstate
