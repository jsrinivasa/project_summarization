a rollercoaster series of talks wrapped up thursday in lausanne as a group of world powers known as the reached a framework agreement with iran over the countrys nuclear program. over the past months since talks began zarif said negotiators have developed personal respect for one another even though serious mistrust still exists between iran and the western powers. tucked amid the swiss alps on the shores of lake geneva lausanne is certainly one of the more scenic places to be trapped for talks a sort of camp david for the rich and famous. shortly after the framework agreement was announced secretary of state john kerry said i think there was a seriousness of purpose in meetings with the iranians

@highlight
this weeks talks on an iranian nuclear deal framework are historic

@highlight
the negotiations demonstrated diplomacy at its best but also at its most hectic

@highlight
reporters resorted to ambushes to talk to officials negotiations were sometimes emotional and confrontational
