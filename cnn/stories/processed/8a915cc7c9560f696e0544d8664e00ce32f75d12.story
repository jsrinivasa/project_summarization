more than suspects have been arrested in south africa in connection with deadly attacks on foreigners that have forced thousands to flee the government said sunday. during the apartheid many south africans fled persecution and death at the hands of the apartheid government it said in its statement africa opened its doors and became a home away from home for many south africans. the attacks in durban killed two immigrants and three south africans including a boy authorities said. there has been an outpouring of support from ordinary south africans who are disgusted with the attacks not only because they are foreign or african but because they are fellow human beings said gift of the givers charity which is helping those seeking refuge

@highlight
south africa is battling xenophobic violence after some said foreigners are taking jobs away

@highlight
a boy is among those killed after a mob with machetes targeted foreigners
