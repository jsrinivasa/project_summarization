japans space agency announced this week that the country would put an unmanned rover on the surface of the moon by joining an elite club of nations who have explored earths satellite. in japan put its selene craft known in japan as kaguya after a japanese moon princess from a century folk tale into orbit around the moon to gather data about its surface the data gathered by the orbiter will also be used to calculate a suitable landing site for the rover. the move could be seen as japans attempt to play catchup to its asian neighbors china and india which have both notched significant extraterrestrial victories in recent years chinas yutu lunar rover outlasted expectations and india successfully put a probe into orbit around mars the first time of asking. if it is approved the agency will reportedly use its epsilon solidfuel rocket technology to carry and deploy a slim probe the acronym stands for smart lander for investigating moon on the surface of the celestial body

@highlight
japan aims to put an unmanned rover on the surface of the moon by

@highlight
the mission is expected to to be used to perfect technologies which could be utilized for future manned space missions
