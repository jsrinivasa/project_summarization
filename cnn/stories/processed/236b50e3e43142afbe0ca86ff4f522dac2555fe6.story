a large storm system dubbed typhoon maysak is churning over the pacific ocean days away from a possible direct hit on the philippines. the storm has weakened and is expected to lose more strength before it reaches the philippines its maximum sustained winds on wednesday were mph making it a super typhoon in the joint typhoon warning centers definition. but that devastation paled in comparison to the havoc wrought by super typhoon haiyan in november which killed more than people and injured more than others that typhoon considered to be among the strongest storms ever to make landfall hit the eastern city of tacloban especially hard. pagasa meteorologist shelley ignacio predicts maysak which is called chedeng in the philippines will bring heavy rains to luzon starting on friday according to the staterun philippines news agency

@highlight
maysak has sustained winds of more than mph but is expected to weaken

@highlight
its forecast to hit the philippines during the easter weekend
