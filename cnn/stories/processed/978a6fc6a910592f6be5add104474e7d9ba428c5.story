an indian software pioneer and nine others have been sentenced to seven years in jail for their role in what has been dubbed indias biggest corporate scandal in memory police said. when the scam made headlines satyam which means truth in sanskrit was indias fourthlargest software services provider. a heavyweight of the nations software industry raju has been in jail for the past months. a special court convicted raju and nine other people of cheating criminal conspiracy breach of public trust and other charges said the central bureau of investigation which looked into the case in the media the case has been compared to the enron corp scandal in which a houston energy companys earnings had been overstated by several hundred million dollars

@highlight
satyam computers services was at the center of a massive billion fraud case in

@highlight
the software services exporters chairman ramalinga raju admitted inflating profits

@highlight
satyam had been indias fourthlargest software services provider
