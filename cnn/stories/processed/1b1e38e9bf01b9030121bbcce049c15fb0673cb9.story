anthony sideri hit rock bottom while wrapped in a dirty blanket on the floor of a jail infirmary bathroom in middleton massachusetts he was shivering sweating throwing up and going through the full withdrawals of heroin. sideri spent the next couple of weeks curled up on the floor of a jail cell withdrawing cold turkey from his years of heroin abuse he describes the withdrawals as like having food poisoning for three days straight combined with a burning tingling sensation in his bones. he had just robbed a bank after shooting up heroin all day that was july the day his life changed forever it was the last time he used drugs the first and only time he was arrested and the first and only time to fully withdrawal from heroin. after graduating from high school in sideri began abusing percocet and oxycontin opiumcontaining painkillers as his tolerance to the pills built up he began snorting the drugs to feel the effects faster then he moved on to snorting heroin

@highlight
strung out on heroin anthony sideri robbed a bank

@highlight
he had to go through withdrawal in a jail cell

@highlight
overcoming addiction is possible he says as hes building a new life as a family man
