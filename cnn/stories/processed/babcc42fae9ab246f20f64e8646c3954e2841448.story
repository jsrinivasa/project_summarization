wealthy nigerians used to travel abroad to get their fix of luxury goods. however in spite of mainstream brands dominance smaller homegrown labels also see the growth in appetite for luxury goods as a valuable opportunity. africa has all the foundations that are needed to create a real vibrant luxury industry says swaady martin leke ivorian entrepreneur and founder of the johannesburgbased luxury tea brand yswara. this emerging class of africas new millionaires has been pushing the demand for luxury products across the continent with sales of highend products growing by a third between and however they are no longer concentrated in southern africa traditionally the wealthiest part of the continent

@highlight
the city with most multimillionaires in africa is johannesburg

@highlight
however a crop of new pretenders have been expanding their millionaire count
