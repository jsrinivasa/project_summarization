the arizona police officer who slammed into an armed suspect with his patrol car told investigators he thought he was too far to take a shot at the man so he chose the other option cnn affiliate kvoa reported wednesday. kvoa obtained police inquiry tapes on which rapiejko tells investigators why he chose his car as a weapon the officer who has been a cop for more than a decade but joined the marana police department in said he was yards away from the suspect and worried a missed shot might hit another officer or bystanders. officer michael rapiejko ran his car into mario valencia in february as the suspect carried a rifle he had just fired in the air rapiejko sped around another officer as valencia walked through a business park hit the man from behind with the left side of his front bumper. video of the car striking valencia sparking nationwide debate on what type of force police should use to go after armed suspects many people commended the officer some people said the police should have set up a perimeter around the man and talked him into surrendering

@highlight
officer michael rapiejko said he needed to use lethal force to stop the suspect

@highlight
mario valencia was carrying a rifle and fired one round into the air

@highlight
rapiejko said two options crossed his mind and it was too far to shoot valencia
