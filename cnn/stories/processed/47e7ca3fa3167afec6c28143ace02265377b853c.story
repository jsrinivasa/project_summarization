it sounds like the plot for a science fiction movie. as crazy as this sounds to put an entire head on a new body a human body italian physician dr sergio canavero says we are approaching heaven an acronym for head anastomosis venture anastomosis is surgically connecting two parts the pieces are coming together but there are still many hurdles to jump. but what about the science is such an idea even plausible he says he has research that supports it. canavero points to dr robert white who transplanted the head of one monkey to the body of another at case western reserve university school of medicine in the monkey died after eight days because the body rejected the new head before ithe monkey died it could not move because the spinal cord of the head and body were not connected the monkey also was unable to breathe on its own the paper in which canavero outlined his procedure references a different experiment white conducted with six monkey heads none of which survived more than hours but canavero says advances in science and medicine since then eliminate the problems white faced

@highlight
dr sergio canavero says he is two years away from performing the first total human head transplant

@highlight
the first patient will be a russian man with a rare genetic muscle wasting disease

@highlight
one ethicist says canavero should be helping paralyzed patients walk before performing body transplants
