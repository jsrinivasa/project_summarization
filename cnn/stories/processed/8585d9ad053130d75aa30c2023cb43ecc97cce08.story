it took more than seven decades but england finally got its delivery of tons of silver coins. under contract to the uk ministry of transport dos recovered several tens of tons of silver coins from a depth of meters the company said the depth is a world record the company claimed. the coins were melted and the silver sold the bbc reported deep ocean search got a percentage of the sale and the uk treasury the rest according to the bbc. in november the unguarded ss city of cairo was sunk by a german uboat while carrying civilians and cargo that included tons of silver

@highlight
the ship was sunk in hundreds of miles of the coast of south america

@highlight
a british company says the salvage operation occurred at a world record depth

@highlight
the torpedoing is the subject of the book goodnight sorry for sinking you
