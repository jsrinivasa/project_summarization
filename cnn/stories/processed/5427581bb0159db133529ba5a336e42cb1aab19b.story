for lt colonel john schwemmer the scenery is all too familiar this is his sixth tour in iraq and hes back doing a job that hes been tasked with before training iraqi soldiers. bradbury set up north american expeditionary forces naef a training body which he says is currently providing material support and training to the peshmerga whose name means those who face death in northern iraq. and increasingly us military training efforts are being supplemented by outside agencies who are working with kurdish government troops and even militia in iraq and syria. many of us do feel that we do have the skills and qualifications that can be used to benefit those in the region said ian bradbury a canadian former soldier who is training kurdish peshmerga forces in northern iraq

@highlight
foreign fighters are increasingly signing up to fight isis on the front lines

@highlight
for some of the jihadist groups foes foreign fighters are not welcome comrades

@highlight
training and logistical support some argue is the best way to support the fight against isis
