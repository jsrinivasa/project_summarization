the hollywood reporterroseanne barr has revealed that she is slowly going blind. the actress attended the tribeca film festival in support of her documentary roseanne for president directed by eric weinrib the film follows barr in her unsuccessful attempt to become the green partys presidential nominee the peace and freedom party eventually made barr its nominee. in an interview with the daily beast barr said she suffers from macular degeneration and glaucoma and told the website my vision is closing in now the comedic actress said smoking marijuana is good medicine for relieving the pressure in her eyes. barr also defended her use of pot saying its expansive it opens your mind and it makes you wonder it doesnt close that down

@highlight
roseanne barr told the daily beast that she is slowly going blind

@highlight
barr said she has macular degeneration and glaucoma
