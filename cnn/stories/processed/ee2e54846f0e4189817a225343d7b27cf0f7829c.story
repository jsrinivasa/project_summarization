too little too late. thats the mayors response to an artists apology and offer to cover the cost of fixing the scary lucy statue that has put the new york town of celoron in the spotlight this week. the bronze figure of comedian and area native lucille ball has elicited comparisons to a walking dead zombie and inspired the facebook campaign we love lucy get rid of this statue the sculpture a gift to the town from its original owners has been on display since. artist dave poulin has had plenty of opportunity to step forward and our last conversation he wanted to celoron mayor scott schrecengost said and then he stated that if we didnt have the funds and we didnt like the statue we should take it down and put it in storage

@highlight
an artist apologizes for his lucille ball statue

@highlight
in a public letter the sculptor offers to pay for fixes to the statue

@highlight
the mayor says hes not interested in having the original artist work on the statue
