north korea accused mexico of illegally holding one of its cargo ships wednesday and demanded the release of the vessel and crew. panama seized the cargo and held onto the ship and its crew for months north korea eventually agreed to pay a fine of for the vessels release. but an myong hun north koreas deputy ambassador to the united nations said there was no reason to hold the mu du bong and accused mexico of violating the crew members human rights by keeping them from their families. mexico defended the move wednesday saying it followed proper protocol because the company that owns the ship north koreas ocean maritime management company has skirted united nations sanctions

@highlight
the mu du bong was detained after it ran aground off mexicos coast in july

@highlight
north korea says theres no reason to hold the ship and accuses mexico of human rights violations

@highlight
mexico says it followed proper protocol because the ships owner skirted un sanctions
