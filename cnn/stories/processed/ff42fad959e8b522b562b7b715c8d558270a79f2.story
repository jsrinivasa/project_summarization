just as the agency begins to recover from a series of highprofile missteps the secret service is facing yet another scandal. an independent report released in december found that the secret service is stretched beyond its limits needing more training more staff and a director from outside its ranks clancy who formally assumed the post in february is a veteran of the agency. a secret service spokesperson confirms that morales was placed on administrative leave and his security clearance was suspended. this incident was first reported on april and secret service director joe clancy was briefed that afternoon

@highlight
secret service says supervisors security clearance has been suspended

@highlight
he is accused of trying to kiss a colleague
