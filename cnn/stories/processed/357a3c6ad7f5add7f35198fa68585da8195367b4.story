residents of central sanaa the yemeni capital have learned the hard way that key strategic bombing targets are located in their neighborhoods detonating ordnance has been shattering their windows and doors. the saudiled coalition smashed parts of yemens defense ministry central command in the capital over the weekend senior yemeni officials said. under the rain of coalition bombs the houthis who are shiites in a majority sunni country still control sanaa but the airstrikes have hurt them and destroyed a lot of infrastructure. medical supplies need to be here yesterday said icrc spokeswoman marieclaire feghali from sanaa we need to save the lives that can be saved

@highlight
bombing of targets in central sanaa smashes residents windows and doors

@highlight
hundreds killed in less than two weeks humanitarian situation desperate agencies say
