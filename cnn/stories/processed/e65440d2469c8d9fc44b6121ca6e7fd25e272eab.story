the united states department of justice has named a new defendant in the war on drugs and the charges are serious indeed. possession is an elusive concept when it comes to drugs the law recognizes two kinds of possession actual and constructive actual possession is when you have physical control over the contraband when you have a gun in your hand or drugs in your pocket you actually possess those things. a indictment filed in federal court in california bristles with accusations of conspiracies transporting prescription pharmaceuticals dispensed with illegal prescriptions violations of the controlled substances act misbranding charges and money laundering charges. obviously the department of justice disagrees which is why it has brought this criminal prosecution

@highlight
justice department prosecuting fedex over unauthorized shipment of drugs

@highlight
danny cevallos fedex has a strong argument that it shouldnt be held responsible
