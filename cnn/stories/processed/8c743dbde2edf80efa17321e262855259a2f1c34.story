on april a final cement seal of an oil well in the gulf of mexico failed causing what has been called the worst environmental disaster in us history and taking the lives of rig workers. ocean conservationist philippe cousteau witnessed much of the spills aftermath in but when he returned to the gulf to dive near an oil rig last month he was astonished by the abundance of amberjacks hammerhead sharks and other marine life he saw. imagining daddy a rig workers daughter and her dreams. bp the company that caused the spill is eager to point out it appears the gulf of mexico is healing itself

@highlight
april marks years since the bp oil spill

@highlight
at the time there were dire predictions for the environment

@highlight
today it is still too soon to know the longterm impact
