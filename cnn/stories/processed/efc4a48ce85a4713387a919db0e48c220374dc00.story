it would be easy to laugh off vin diesels prediction that his film furious will win an oscar next year but not for the reason you might think. in then boston globe film critic wesley morris wrote go on and laugh your benetton kumbaya kashi quinoa laugh but its true the most progressive force in hollywood today is the fast and furious movies. but rather than ignore it because its a glossy blockbuster action film some might argue that the movie goes against type for academy award nominated films because the cast is so diverse. entertainment weekly points out that the film franchise is doing a much better job of reflecting its audience than others in hollywood

@highlight
the films cast is diverse

@highlight
ew points out that hollywood still needs to catch up

@highlight
one of the stars says the franchise has evolved
