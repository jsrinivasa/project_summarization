with the world watching in his historic address to a joint session of the us congress the first ever by a japanese prime minister shinzo abe surprised nobody by missing a great opportunity to dispel worries and tensions he raises with his revisionist views on history. fatalistically abe said history is harsh what is done cannot be undone well yes that is selfevident but this should not mean the door is closed on apologies acts of contrition and atonement and a clear reckoning of the harsh history that still isolates japan in east asia abe is certainly right that the neighbors are playing the history card and relentlessly hammering japan on the anvil of history but they were handed the hammer by japan. in his speech abe also referred to shared values of democracy and freedom this too is welcome news because over the past year abe has presided over an orchestrated attack on the asahi newspaper for its coverage of the comfort women system packed nhks management with likeminded reactionaries and pressured critics out of their jobs indeed the new york times ran an article on april implicating team abe in the axing of a prominent tv pundit who made the mistake of criticizing abe. in japan there has been a lot of pressure on abe to explicitly invoke the language of the murayama statement that stands as the most forthright apology and acknowledgment of war responsibility even the conservative yomiuri newspaper urged him to do so murayama clearly condemned mistaken national policy selfrighteous nationalism colonial rule and aggression but abe has spent his entire political career renouncing this socalled masochistic view of japans wartime history

@highlight
abe did express eternal condolences about the loss of american lives in world war ii

@highlight
but he has been evasive and ambiguous about embracing responsibility for japans wartime actions

@highlight
kingston he is putting his personal agenda on history ahead of the national interest
