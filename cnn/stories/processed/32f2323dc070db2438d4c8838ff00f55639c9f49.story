the hillary clinton presidential campaign surely anticipated the coming wave of inquiries and criticism about conflicts of interest involving big foreign donors to charities run by the clinton family questions set to get a thorough airing in a new book called clinton cash the untold story of how and why foreign governments and businesses helped make bill and hillary rich by peter schweizer coming out may. my guess is that the issues raised by the book will prompt team clinton to put its candidate on the road where she can continue holding loosely scheduled informal meetings with ordinary americans the sorts of people more concerned about local jobs than whether some foreign government or company paid a big speaking fee to bill clinton in hopes of getting special treatment by secretary of state hillary clinton. in a wellknown case of deception a con man named raffaello follieri charmed his way into the foundations good graces earning public praise from bill clinton for promising million to the clinton global initiative money that never materialized. calling extensive exposure of conflicts of interest distractions suggests that clinton knows what the polls suggest that in a nation still struggling to emerge from a long recession voters will likely judge her on something other than the efficiency and ethics of her charities

@highlight
errol louis new book to detail alleged conflicts of interest with foreign donors to clinton family charities

@highlight
he says without smoking gun its likely not election dealbreaker for americans worried about issues like economy jobs schools

@highlight
louis more notable is mismanagement ethical history of clinton charities
