its not easy being the pope not only does he shepherd nearly billion catholic souls he also leads a small but morally significant state with envoys and interests in nearly every country. because he often shines a sympathetic face on the world emphasizing mercy over judgment its easy to miss the bluntness francis brings to the bully pulpit on matters of doctrine and diplomacy he may be carrying on catholic traditions but in his willingness to engage in geopolitics and the tone that engagement often takes this pope is decidedly different. the vatican and the papacy love continuity said the rev thomas reese a vatican analyst for national catholic reporter. this sunday in rome pope francis faced just such a dilemma

@highlight
previous popes had finessed the question of whether the killing of million armenians was genocide

@highlight
because he often shines such a smiley face on the world it can be easy to forget the bluntness francis sometimes brings to the bully pulpit
