sophisticated glamorous and spacious when the superrich go househunting they are searching for something special. real estate in londons swankier suburbs can catch a buyers eye mayfair kensington and chelsea have long been the stomping ground of the elite and are now welcoming a new wave of african investors. the africans who are coming into london now are africans who themselves have worked for their money explains bimpe nkontchou a britishnigerian wealth manager based in london. they have grown in industry and are actually part of the exciting story of the african renaissance she continues its bringing to london the best of the continent

@highlight
wealthy africans are investing in some of londons most upscale real estate

@highlight
some nigerians are spending as much as million on houses

@highlight
property experts say african investment in london is set to grow
