isis fighters seized several districts in the iraqi city of ramadi in an hourslong assault friday that included suicide and car bombs an iraqi provincial official said. iraqi security forces discovered tunnels in february that they say could have led opposition fighters to a central government compound in the city but they didnt find all of them a few weeks later isis detonated hundreds of homemade bombs from a tunnel underneath an army headquarters there according to sabah alkarhout the head of the anbar provincial council more than iraqi soldiers died in that explosion. a us official said in february that up to iraqis troops were expected to return to the key northern iraqi city in april or may but on thursday a senior official in us president barack obamas administration appeared to back away from that timeline saying an iraqiled assault on mosul might be some time from now or might be soon. at least iraqi security forces were killed in the attacks according to faleh alessawi the deputy head of iraqs anbar provincial council and the head of the iraqi military operation in anbar province gen qassim almuhammadi was wounded

@highlight
anbar provincial official suicide and car bombs were part of the isis assault

@highlight
iraqi and allied forces have had recent success but isis remains powerful
