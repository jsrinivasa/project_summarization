ten years ago a prosecutor in centre county pennsylvania took a day off work and vanished. pennsylvania state police continue to chase down new leads and take a fresh look at old leads and we continue to hold out hope that something will break out in this case said centre countys district attorney stacy parks miller everybody regardless of what position they held deserves this kind of attention in any missing persons case hes not the only one we feel discouraged when we cant answer the questions for the family but it doesnt change our dedication to the case. bob buehner a former district attorney in montour county pennsylvania who was gricars friend has never accepted a suicide or walkaway theory he believes his colleague was killed. the case has gotten significant attention on the national level appearing on several truecrime television shows including hlns nancy grace so it was strange to many in pennsylvania that for years a case with such a high profile would be handled by the tiny bellefonte police department where one investigator was assigned to juggle gricars case along with several more

@highlight
prosecutor ray gricar has been missing for years

@highlight
his laptop and hard drive were found too damaged to read

@highlight
gricar has been declared legally dead
