my son served in the army for four years in iraq he served because we love our country as we should now look at us. theyd waited for days but neither her son nor her parents could cross over to the port in al tawahi district too scared to risk missing the boat and endangering the lives of their other three children her husband had convinced her to board when they called to tell her son he also had news for them hed joined the fight against the houthi forces. the first night on board our boat had an almost festive air our new passengers were laughing and sharing cigarettes euphoric at their escape one woman though was sitting alone on deck and i realized she was crying she told me her son was trapped on the other side of one of the many front lines that are now etched into the citys streets. muna mansour is gesturing around her at the slatted cargo hold she and her family all nine of them are trying to get comfortable in theyre squeezed in with two other families on the ground by my feet munas middle grandchild is sleeping curled up beside an oil drum

@highlight
no official way out for americans stranded amid fighting in yemen

@highlight
us deputy chief of mission says situation is very dangerous so no mass evacuation is planned
