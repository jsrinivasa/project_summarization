craig hicks who is charged in the deaths of three muslim college students in chapel hill north carolina earlier this year can face the death penalty a judge ruled monday according to cnn affiliates. it has always been our position that mr hicks should be held responsible for his actions to the full extent of the law his killing of three college students was despicable and now he must face the consequences of his actions said rob maitland an attorney for hicks wife karen and craig hicks are in the process of divorce. superior court judge orlando hudson jr ruled that hicks case is death penalty qualified wral and wtvd reported. the us department of justice issued a statement in february saying the departments civil rights division along with the the us attorneys office for the middle district of north carolina and the fbi have opened a parallel preliminary inquiry to determine whether any federal laws including hate crime laws were violated

@highlight
hicks is charged in the deaths of three muslim college students in chapel hill north carolina

@highlight
victims family members have called on authorities to investigate the slayings as a hate crime
