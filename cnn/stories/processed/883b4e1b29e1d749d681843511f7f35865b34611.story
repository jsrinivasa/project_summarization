lee minbok didnt laugh once when he watched the interview the north korean defector calls the hollywood comedy vulgar admitting he couldnt even watch the whole film. at am lee makes a final check of the wind speed and direction then heads towards the border with north korea he has company the south korean police and military drive closely behind after pyongyang fired on similar propaganda balloons recently they are monitoring launches very closely. at am lee fills the balloons with helium and ties the bundles of dvds dollar bills and political leaflets to the bottom a timer is attached which will release the bundle once safely in north korean territory. the chances of at least some north koreans having watched the film that north korea sees as an act of terrorism is certainly possible

@highlight
defector deploys balloons with the interview to north korea

@highlight
lee minbok says he finds the movie vulgar but sends it anyway
