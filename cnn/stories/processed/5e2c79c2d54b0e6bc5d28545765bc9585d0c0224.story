giovanni lo porto put himself in harms way to help heading to pakistan to work on a much needed reconstruction project following deadly flooding there. at the time he was taken captive lo porto worked with the german aid organization welthungerlife a group dedicated to fighting hunger and poverty worldwide such work means going to places where the need is most places like multan in pakistan. some of those friends in italy england and beyond pressed for lo portos release after he was taken captive urging italys government and newspaper editors to get his story out ansa reported the same story said that al qaeda after first claiming he was being held denied abducting lo porto as did the pakistani taliban. specifically lo porto was a project manager with welthungerlifes clean water and sanitation program working with to fellow international staffers and to locals starting in october according to simone pott a spokeswoman for the aid group

@highlight
ngo where lo porto works describes him as a lively positive man with lots of friends

@highlight
london university says he was a popular student committed to helping others

@highlight
he was killed in a us counterterrorism strike in january authorities say
