a japanese court has issued a landmark injunction halting plans to restart two nuclear reactors in the west of the country citing safety concerns a court official told cnn. but the court ruled that the new safety standards were loose lacked rationality and could not guarantee the safety of the plant an official said. but locals successfully petitioned the court in fukui prefecture where the plant is located raising concerns about whether the reactors would survive a strong earthquake. the nuclear plant operator had argued in court that the plant was safe meeting heightened safety regulations introduced by the nuclear watchdog following the fukushima disaster

@highlight
the reopening of two nuclear reactors has been blocked by a japanese court over safety fears

@highlight
the reactors had previously been cleared to reopen by the countrys nuclear watchdog

@highlight
japans nuclear reactors have been offline in the wake of the fukushima disaster
