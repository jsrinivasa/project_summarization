so now the real trial is underway what does the surviving boston marathon bomber dzhokhar tsarnaev deserve and why whats he likely to get and why. the prosecutions emphasis the killings involved substantial planning and premeditation and a betrayal of the united states the very country that gave tsarnaev shelter and citizenship and then theres the selection of the site the boston marathon an iconic event. and yet if we really commit ourselves to having the punishment fit the crime if we rightly reserve the death penalty for the most heinous crimes and criminals surely the boston marathon bomber stands among the worst of the worst. dzhokhar tsarnaev was the lesser of two evils the defense will insist over and over tamerlan was the source of his younger brothers malicious intent as the prisoners inside washington dcs now defunct lorton central prison once described the street code to me no snitching but if someone dies then the dead guy did everything

@highlight
robert blecker in sentencing phase the prosecution lays out wealth of evidence that dzhokhar tsarnaev deserves penalty reserved for the worst of the worst

@highlight
he predicts most of the jury will vote for a death sentence but it must be unanimous therefore tsarnaev will most likely get life in prison
