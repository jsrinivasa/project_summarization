this week is the anniversary of what many historians acknowledge as the armenian genocide the turkish massacre of an estimated million armenians. on the campaign trail obama promised to use the word genocide to describe the massacre by turks of armenians a pledge he made when seeking armenianamerican votes. no us president has ever made genocide prevention a priority and no us president has ever suffered politically for his indifference to its occurrence it is thus no coincidence that genocide rages on she wrote. things to know about the mass killings of armenians years ago

@highlight
obama promised armenianamericans he would call the atrocity genocide during the campaign

@highlight
the white house views turkey as a more crucial ally than armenia

@highlight
pope francis actor george clooney and even the kardashians have taken the moral position calling it the armenian genocide
