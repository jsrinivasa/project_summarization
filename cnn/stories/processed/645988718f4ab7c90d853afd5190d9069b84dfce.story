mountaineers have returned to mount everest for this years climbing season resuming the quest to summit the worlds highest peak after a deadly season last year. nepal has issued permits this year to climb mount everest with of them from the previously shortened season according to the nepal ministry of tourism its a slight increase from the who were given permission last year. in the nepal climbing season ended after a piece of glacial ice fell unleashing an avalanche that killed nepalis who had just finished their morning prayers the april accident was the single deadliest incident to ever occur on mount everest. i think it will be an hour longer on the icefall said alan arnette who is blogging from everest base camp this season i dont think it will be game changer

@highlight
climbers are returning to everest after season on nepal side was canceled

@highlight
climbing permits increase in tibetan and nepalese side this year

@highlight
nepalis died in khumbu icefall on everest last year
