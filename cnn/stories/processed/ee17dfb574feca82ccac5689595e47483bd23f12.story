a man was charged wednesday with terror offenses after he was arrested as he returned to britain from turkey londons metropolitan police said. rashid is due to appear in westminster magistrates court on wednesday police said. yahya rashid a uk national from northwest london was detained at luton airport on tuesday after he arrived on a flight from istanbul police said. hes been charged with engaging in conduct in preparation of acts of terrorism and with engaging in conduct with the intention of assisting others to commit acts of terrorism both charges relate to the period between november and march

@highlight
londons metropolitan police say the man was arrested at luton airport after landing on a flight from istanbul

@highlight
hes been charged with terror offenses allegedly committed since the start of november
