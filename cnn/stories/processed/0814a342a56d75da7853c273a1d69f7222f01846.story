it is a city transformed swollen in size but shrunken in scope anxiously awaiting what comes next. helicopters still buzz around the capital its population is five times what it was when nato arrived here even by the most conservative estimates and the violence in the provinces means people swell it further still arriving in kabuls dusty mountainous bowl of a city in order to avoid the fighting. on kabuls streets you can easily find the uneasy legacy of americas longest war outside one mosque mixing with other men desperate for a days worth of casual manual labor are five men who months ago had one valuable skill nato depended upon they speak english. now however their world has turned upon them they were each for a different reason each for a reason they do not understand all fired from their jobs and then blacklisted they say meaning they can no longer get work with other government groups or ngos here

@highlight
kabul faces uncertain future as nato presence and the money that came with it fades away

@highlight
interpreters are out of work nato trucks sit idle on roads restaurants are empty
