if newly revised nypd training materials are approved by a federal judge new cadets could be taking courses reminding them not to engage in racial profiling. the current training materials had not undergone a comprehensive review for some time and did not address some important subjects or account for some changes in nypd policies and law zimroth wrote in a cover letter to torres all parties agreed it was essential that the materials be rewritten for the current class to reflect current law and policy. if approved the new training materials will be integrated into the class curriculum for the current class of cadets they are expected to graduate in june. do not engage in racial profiling the training materials read it is against the law it violates fundamental democratic precepts and freedoms it violates this departments policies it is offensive it violates your responsibility to treat people equally it diverts us from catching real criminals it alienates us from people who need us and hurts our ability to do our job you can probably think of other reasons not to do it but the point is that you will not do it

@highlight
the training materials are a result of a ruling declaring stop question and frisk unconstitutional

@highlight
they read that racial profiling is offensive it diverts us from catching real criminals
