star wars fans will get more than they bargained for when the saga comes to digital hd on friday. the collection of the first six star wars movies will also include many special features some of which give fans a rare glimpse behind the scenes of the saga. star wars films available for digital download for first time. one focus of the features will be the sound effects of the movies including that of the insectlike geonosians as seen in star wars episode ii attack of the clones

@highlight
the star wars digital collection is set for release this week

@highlight
special features include behindthescenes stories on the unique alien sounds from the movie
