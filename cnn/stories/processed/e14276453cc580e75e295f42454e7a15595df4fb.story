ive visited nepal at least half a dozen times over the last decade and of the more than countries that save the children serves it is undoubtedly one of my favorites there are the usual things that are said about it the stunning landscape which includes mount everest and the amazing food. in fact even getting food to people will be a logistical nightmare given that the entire country which is about the size of tennessee is served by only two main roads which have likely been badly damaged if not destroyed. in fact i remember a time just last may when i sat with a group of mothers and their tiny babies as they told me how proud they were that they now understood how important it was to make sure they prioritized breastfeeding and nutritious foods they spoke of the wonderful future they were now expecting for their children and they shared with me the big dreams that they had. with that in mind we have set up a fund to help address the immediate needs of children who are always the most vulnerable in an emergency we and other relief organizations will be doing everything we can to help get nepal back on its feet without having to wait for more people to die from diseases that will inevitably come from contaminated water and the like

@highlight
a earthquake struck near kathmandu nepal

@highlight
carolyn miles many survivors will have nowhere to go
