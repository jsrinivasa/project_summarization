a suicide bomber on a motorbike blew himself up in front of the kabul bank in jalalabad early saturday a local government spokesman said. in a statement the group said the bomber was named abu mohammad and he belonged to their ranks he was targeting government workers collecting their pay at the bank the terrorists said. the explosion in jalalabad doesnt have anything to do with us and we condemn it he said. the continuing use of suicide attacks in densely populated areas that are certain to kill and maim large numbers of afghan civilians may amount to a war crime said nicholas haysom head of the united nations assistance mission in afghanistan

@highlight
un says suicide attacks on mass groups of civilians may be labeled as war crimes

@highlight
taliban condemns the attack which isis took credit for

@highlight
the bomber targeted government workers picking up their pay isis said in a statement
