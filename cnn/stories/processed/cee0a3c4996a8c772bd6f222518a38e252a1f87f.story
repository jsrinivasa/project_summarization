pope francis reminded the world of the vaticans status as a state and his role as a moral diplomat in his traditional speech given at the end of easter mass. pope francis ended the address consoling the marginalized the poor the sick and the suffering. and he wished all a happy easter. francis mentioned libya the nuclear talks with iran in lausanne switzerland yemen nigeria south sudan and the shooting at the garissa university college in kenya

@highlight
the pontiff laments the suffering of people in conflicts currently making headlines

@highlight
foremost he asks that bloodshed end in iraq and syria
