the hollywood reporterrichard dysart the emmywinning actor who portrayed the cranky senior partner leland mckenzie in the slick longrunning nbc drama la law has died he was. the acclaimed la law created by steven bochco who eventually handed off the series to david e kelley and terry louise fisher aired for eight seasons from to for playing the founder of the firm mckenzie brackman chaney and kuzak dysart was nominated for the emmy for outstanding supporting actor in a drama series for four straight years finally winning the trophy in. dysart also excelled as cranky coots and shifty sorts he portrayed a motel receptionist in richard lesters petulia was the bad guy who battled clint eastwood in pale rider stood out as a power player in oliver stones wall street and sold barbwire in back to the future iii. dysart and jacobi had a second home in the forests of british columbia he was lured out of retirement for his last onscreen appearance the la law reunion telefilm of

@highlight
richard dysart best known for leland mckenzie in la law

@highlight
dysart had many tv and film roles including spots in being there and the thing

@highlight
actor won drama desk award for performance in theatrical that championship season
