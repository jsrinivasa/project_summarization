its not going to be enough to slake the thirst of the elusive mars bunny but scientists say new research seems to support the theory that what looks like a bonedry red planet during the day could be dotted with tiny puddles of salty water at night. the new study doesnt change the picture for life on mars the researchers say the temperatures they measured are too low and water too scarce to support terrestrial organisms sorry bunny fans. researchers arent saying theyve seen direct evidence of brine hiding out in the martian night but they say the new study based on a full year of monitoring of temperature and humidity conditions by the mars curiosity rover in gale crater does seem to bear the theory out. but scientists say evidence of water ice at the planets poles and now more evidence toward the theory of widespread brines keeps them hoping theyll find evidence that life at least once existed there

@highlight
analysis of martian weather seems to support the idea that the planet could be dotted with salty puddles at night

@highlight
the finding has wider implications for efforts to find evidence of life on mars a researcher says
