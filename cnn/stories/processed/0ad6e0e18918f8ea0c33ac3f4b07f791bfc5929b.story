feidin santana the man who recorded a south carolina police officer fatally shooting a fleeing unarmed man told cnn on thursday night he was told by another cop to stop using his phone to capture the incident. santana told cooper an officer told him to wait where he was but eventually he left the scene to go to work. a police report says a third officer not shown in the video reported seeing an officer administering first aid and that the third officer approached and helped that person with first aid and cpr. santana recalled the moments when he recorded a roughly threeminute video of north charleston police officer michael slager shooting walter scott as scott was running away saturday that evidence led to the officers firing and arrest on a murder charge

@highlight
witness who took video of shooting said when he arrived officer was on top of walter scott

@highlight
feidin santana says walter scott didnt take michael slagers taser

@highlight
santana said he never saw officers perform cpr before he left the scene to go to work
