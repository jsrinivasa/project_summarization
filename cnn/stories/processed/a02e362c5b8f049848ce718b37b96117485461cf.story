a fiery sunset greeted people in washington sunday. the reason people are seeing an intense red sunset is a result of smoke particles filtering out the shorter wavelength colors from the sunlight like greens blues yellows and purples komotv said. as the smoke starts to dissipate air quality will get better and these fiery sunsets will lose their reddish hue. the winds carried the smoke from siberia across the pacific ocean and brought it to the pacific northwest parts of oregon washington and british columbia are seeing the results of the smoke wind and solar light combination

@highlight
smoke from massive fires in siberia created fiery sunsets in the pacific northwest

@highlight
atmospheric winds carried smoke from the wildfires across the pacific ocean

@highlight
smoke particles altered wavelengths from the sun creating a more intense color
