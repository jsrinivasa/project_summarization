late one night on facebook a girl with cystic fibrosis messaged a boy with cystic fibrosis and both their lives were changed forever. cystic fibrosis patients shouldnt be near each other because they can share infections that could cripple their already fragile lungs dr michael anstead at the university of kentucky katies pulmonologist since she was a little girl had lectured her many times that facetoface meetings with other cf patients were a bad idea. the girl katie donovan read that the boy dalton prager was very sick if you ever need a friend to talk to you can reach out to me she wrote. caught in the middle between the hospital and insurance katie tries to stay strong just as she advised her husband to do nearly six years ago in their first facebook conversation skyping with dalton helps and raising money on their facebook page keeps her mind busy

@highlight
katie and dalton met as patients dealing with cystic fibrosis

@highlight
two years later they were married

@highlight
dalton received a lung transplant but katie is still waiting
