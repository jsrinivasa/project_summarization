it all started by looking up. during an expedition through the brittany region on the west coast of france photographer fabien le coq noticed an unusual tree he positioned himself at the base of the tree and turned his camera upward filling the frame with the trunk and its spindly branches. im kind of a walking photographer le coq said i love exploring new places one day i was taking a break during an excursion in the broceliande forest looking for the best place to settle when i discovered a small clearing with a tree without leaves i stayed for hours looking around taking some pictures and i found myself lying down under the tree the trees branches were rising as if to touch the sky. this was the start of the photo series treesome an embodiment of le coqs unique way of interacting with the world around him

@highlight
fabien le coq took photos of trees from the bottom looking up

@highlight
the branches take on their own patterns in the sky each tree has its own personality
