what would you do if a complete stranger asked you for or offered you an apple in a parking lot without explanation. jiang who as a child dreamed of being bill gates and has been viewed million times on youtube has found his entrepreneurial dream in a different role for the moment my goal is to turn rejection into opportunity i always thought it was something to run away from but if we can embrace it we can turn it into a lot more than an obstacle. the fear of rejection holds us back a lot more than actual rejection by putting ourselves out there the world will usually open itself up to you though the world can seem cruel and cold actually humans have a hard time saying no so open yourself up dont be afraid to ask for something if you fail remember its not about you. stand tall and remember rejection is an opinion people are who they are a lot of people will reject you because of their mood their education their upbringing and you cant change who they are but you can stand confidently innate confidence comes across

@highlight
one mans entrepreneurial quest turned into unexpected success

@highlight
days of rejection took jiang out of his comfort zone

@highlight
its the fear of rejection more than rejection itself which holds us back
