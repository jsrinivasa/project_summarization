one of the biggest tv events of all time is being reimagined for new audiences. audiences will once again feel the impact of kunta kintes indomitable spirit. we are proud to bring this saga to fans of the original as well as to a new generation that will experience this powerful and poignant tale for the first time said dirk hoogstra historys executive vice president and general manager. roots the epic miniseries about an africanamerican slave and his descendants had a staggering audience of over million viewers back in now ae networks are remaking the miniseries to air in

@highlight
the ae networks are remaking the blockbuster roots miniseries to air in

@highlight
the epic miniseries about an africanamerican slave had million viewers
