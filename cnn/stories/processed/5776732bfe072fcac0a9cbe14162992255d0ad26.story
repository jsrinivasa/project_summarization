blinky and pinky on the champs elysees inky and clyde running down broadway power pellets on the embarcadero. leave it to google to make april fools day into throwback fun by combining google maps with pacman. the massive tech company is known for its impish april fools day pranks and google maps has been at the center of a few including a pokemon challenge and a treasure map this year the company was a day early to the party rolling out the pacman game tuesday. its easy to play simply pull up google maps on your desktop browser click on the pacman icon on the lower left and your map suddenly becomes a pacman course

@highlight
google maps has a temporary pacman function

@highlight
google has long been fond of april fools day pranks and games

@highlight
many people are turning their cities into pacman courses
