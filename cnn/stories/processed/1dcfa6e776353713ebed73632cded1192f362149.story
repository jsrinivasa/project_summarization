the ride from kathmandu to near the epicenter of nepals devastating earthquake is not for those who get easily carsick steep slopes and small winding roads through deep river valleys greet us as we make our way to a town where nepalese and international medical teams are treating the injured in much less than ideal circumstances. one of the biggest challenges rescue workers are facing in nepal is reaching affected people in remote mountainous areas one of them is gorkha a district northwest of kathmandu the town we were visiting looks intact some houses have cracks and i see a few collapsed brick walls most of the shops and businesses are open. on thursday we wake up to a drizzly morning heavy fog blankets surrounding slopes and mountains all helicopter flights are suspended people in remote areas desperately awaiting help wont get any today but here in gorkha humanitarian teams are at full speed swiss doctors along with their nepalese colleagues perform surgeries the german team heads off in a bus to villages reachable by road. little food less optimism in kathmandu tent city

@highlight
a trip to the town of gorkha shows the human toll of the earthquake

@highlight
international teams are assisting nepalese medical staff officials
