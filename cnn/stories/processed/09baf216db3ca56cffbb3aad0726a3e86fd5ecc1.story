after weeks of dramatic testimony jurors are set to begin deliberations tuesday in the trial of dzhokhar tsarnaev who faces life in prison or the death penalty for working with his brother to explode bombs at the boston marathon. according to testimony tamerlan tsarnaev set off a bomb made from a pressure cooker explosive powder from fireworks duct tape nails and bbs on boylston street near the finish line that bomb which exploded near marathon sports claimed the life of krystle campbell a restaurant manager. the prosecutor showed a picture of dzhokhar tsarnaev and his brother tamerlan in the marathon crowd. dzhokhar tsarnaev chose a day when the eyes of the world would be on boston chakravarty said he chose a day when there would be civilians on the sidewalks and he targeted those civilians men women and children

@highlight
jurors are scheduled to begin deliberations tuesday morning

@highlight
if tsarnaev is found guilty of at least one capital count the trial will go to the penalty phase

@highlight
prosecutor during closing argument tsarnaev wanted to awake the mujahideen the holy warriors
