the operator of the crippled fukushima daiichi nuclear plant has given up trying to recover a robotic probe after it stopped moving inside one of the reactors. tokyo electric power company tepco deployed the remotecontrolled robot on friday inside one of the damaged reactors that had suffered a meltdown following a devastating earthquake and tsunami in. four years after the devastating nuclear crisis the radiation levels inside the three damaged reactors are still extremely high and remain unsafe for people to enter. it was the first time the probe had been used

@highlight
the operator of the fukushima nuclear plant said it has abandoned a robotic probe inside one of the damaged reactors

@highlight
a report stated that a fallen object has left the robot stranded

@highlight
the robot collected data on radiation levels and investigated the spread of debris
