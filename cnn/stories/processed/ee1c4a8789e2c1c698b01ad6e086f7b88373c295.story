the death toll from the devastating earthquake that struck nepal two days ago surged past on monday a government official said the desperate search for survivors from the countrys worst natural disaster in more than years continued. the us agency for international developments disaster response team made up of urban search and rescue specialists from fairfax county virginia and six headed to nepal on sunday on a military transport plane the dogs are trained to find signs of life in rubble after a disaster. the death toll is expected to climb further as officials get information from the rugged countryside that makes up most of nepal. dhakal the government spokesman said monday people were reported to have been injured

@highlight
the death toll in nepal rises to a government official says

@highlight
the number of injured is reported to be more than he says

@highlight
another people are dead in india and in china
