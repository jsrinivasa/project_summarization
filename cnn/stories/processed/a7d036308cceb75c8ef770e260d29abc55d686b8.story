in the hours after the funeral of freddie gray a community center and apartment complex that local leaders expected to serve as a catalyst for the rebuilding of a long blighted east baltimore neighborhood went up in flames. weve been trying to make a major difference trying to transform the community only to discover that something as tragic as this would take place said the rev walden wilson ii pastor of israel baptist church part of the east baltimore ministers community development partnership. young people just needed somebody to sit and talk to them and hear them cry hickman said looking out over the ruins of the community project this is reactionary this is emotional this is frustration this is i dont know what else to do if we can rebuild iraq we can rebuild east baltimore. i think the reason that they burned it is exactly the reason why we needed it hickman said of the community center we were seeking to restore people while we rebuilt properties we wanted to effect change in the human community as well as rebuild properties with affordable housing

@highlight
mary harvin transformation center was to house seniorcitizen apartments community center

@highlight
it burned down during baltimore riots
