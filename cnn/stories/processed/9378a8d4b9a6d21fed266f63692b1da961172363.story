an iranian military observation aircraft flew within yards of an armed us navy helicopter over the persian gulf this month sparking concern that top iranian commanders might not be in full control of local forces cnn has learned. an unarmed iranian observation aircraft approached the iranian aircraft made two passes at the helicopter coming within yards before the helicopter moved off according to the official. this type of iranian observation aircraft generally operates over the gulf several times a month but after the recent incident us naval intelligence did not see it again for two weeks leading to the conclusion that the incident may have been ordered by a local commander who was then reprimanded by higherups. the navy helicopter was in radio contact with the ship during the encounter but there was no contact between the two aircraft and no shots were fired

@highlight
iranian plane came within yards of us navy sea hawk copter

@highlight
navy copter was on patrol in international airspace

@highlight
us official think iranian plane may have been under orders of local commander
