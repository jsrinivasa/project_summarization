ever had a headache so big you felt like drilling a hole in your head to let the pain out. in neolithic times trepanation or drilling a hole into the skull was thought to be a cure for everything from epilepsy to migraines. it could even have been a form of emergency surgery for battle wounds. but while there is still conjecture about the real reasons behind the mysterious procedure what is known is that the implement often used to carry out the primitive surgery was made from one of the sharpest substances found in nature obsidian

@highlight
obsidian can produce cutting edges many times finer than even the best steel scalpels

@highlight
some surgeons still use the blades in procedures today
