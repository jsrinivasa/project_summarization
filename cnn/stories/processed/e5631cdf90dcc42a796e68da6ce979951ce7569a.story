former new england patriots star aaron hernandez looked on impassively wednesday as he was sentenced to life without the possibility of parole a new low for a young man who once enjoyed a million profootball contract and now stands convicted in the murder of onetime friend odin lloyd. odin was my only son his mother ursula ward told the court without looking at hernandez odin was the man of the house odin was his sisters keeper after my daughter olivia had her daughter odin became her keeper too. of the defendant quinn said aaron hernandez may have been a wellknown new england patriots football player however in the end the jury found that he was just a man who committed a brutal murder. judge susan garsh sentenced hernandez to a term of your natural life without the possibility of parole for the firstdegree murder conviction

@highlight
they got it wrong aaron hernandez says as he is transported to prison

@highlight
the jury deliberated for more than hours over parts of seven days

@highlight
mother of murder victim odin lloyd says she forgives those who played a role in her sons death
