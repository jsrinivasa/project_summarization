over the last year more than people a population about the size of key west florida have fought ebola infections. while the intensity of the largest ebola epidemic in history has died down and the initial dire predictions that there would be over a million infections by january never came true dozens are still newly infected each week. taken together these data indicate that though surveillance is improving unknown chains of transmission could be a source of new infections in the coming weeks the latest who report said. more than have not survived but for those who have survived life will never be the same and even for those who did not experience ebola personally the most severe public health emergency seen in modern times showed the world its vulnerability to disease

@highlight
april the who finally started reporting the ebola epidemic was a concern

@highlight
front line health care workers and ebola survivors say the world has to act quicker
