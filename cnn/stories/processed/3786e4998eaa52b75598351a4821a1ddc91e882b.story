the casket is draped with an american flag and walter scott is dressed in a dark suit. moments later scott gets out of his car and bolts a foot chase ensues scott never reappears on the dash cam video but a witness later takes video of the officer shooting scott several times in the back as he is running away. who was walter scott. scott family attorney chris stewart said the man with scott was a coworker and friend but he did not identify the friend by name nor did thom berry a south carolina law enforcement division spokesman who confirmed fridays meeting

@highlight
mourners attend visitation service for walter scott

@highlight
police meet with man who was passenger in his car when it was pulled over

@highlight
michael slagers lawyer says police arent being helpful at this point
