isis claimed responsibility for a suicide car bomb attack friday near the us consulate in the kurdish iraqi city of irbil according to several twitter accounts linked to the terror group. police said the incident began with an explosion of a small improvised bomb in the area after that blast a car moved in the direction of the consulate. the us consulate was the target of the attack isis said. security personnel fired at the car which exploded but did not reach the consulate a police official said it appeared that people inside the car detonated explosives that the vehicle was carrying according to the police official

@highlight
all us consulate personnel safe after blast state department spokeswoman says

@highlight
suicide bombers blow up car near the us consulate in irbil iraq
