the indonesian government has ordered preparations for the execution of inmates on death row including filipino maid mary jane veloso and australians andrew chan and myuran sukumaran. chan and sukumaran members of the socalled bali nine convicted for their role in a failed heroin smuggling plot tried to challenge the presidents decision earlier this month but lost an appeal for the state administrative court to hear their case their lawyers have since filed another review at the constitutional court the attorney generals office has said they would respect all ongoing court proceedings but insisted the inmates have exhausted all their legal options. friday this is not the required notice given to death row convicts before the actual execution but he said the time is approaching. in another sign that the execution date may be announced soon spontana said veloso was moved friday to nusa kambangan which lies off the coast of west java she has been held in a prison in yogyakarta central java since the supreme court rejected her petition for a judicial review in march but her lawyers were still preparing to file a second review on monday

@highlight
indonesia has advised foreign consular officials to travel to indonesias execution island

@highlight
the death row inmates being held have had their legal bids rejected

@highlight
they include australian bali nine members andrew chan and myuran sukumaran
