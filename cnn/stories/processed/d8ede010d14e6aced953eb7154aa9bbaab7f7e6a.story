saudi airstrikes over yemen have resumed once again two days after saudi arabia announced the end of its air campaign. on tuesday saudi arabia announced the end of its operation decisive storm a nearly monthlong air campaign against houthi positions the saudiled coalition said a new initiative was underway operation renewal of hope focused on the political process. saudi arabia and its coalition partners started pounding houthi positions across yemen starting on march hoping to wipe out the iranianallied rebel group that overthrew the yemeni government and seized power. why is saudi arabia bombing yemen

@highlight
after calling off its air campaign saudi arabia resumes airstrikes in yemen

@highlight
no casualties are reported but houthi military compounds were destroyed

@highlight
saudi airstrikes resumed after rebel forces attacked a yemeni military brigade
