the cuba that photographer carolina sandretto captures is a world away from the images of neon american cars and postcardworthy white sand beaches that most visitors to the island bring back home. sandretto said she hopes to continue to document the changes on the island that occur as the united states and cuba work to restore diplomatic relations and an inevitable influx of american visitors arrive. carolina sandretto is an italian photographer based in new york you can follow her on twitter. following fidel castros revolution houses and apartments were redistributed throughout cuba and the government promised that everyone would have a home in the new socialist utopia

@highlight
carolina sandretto focuses on the crumbling buildings many cubans live in together

@highlight
the mazelike solares often include separate families under one roof
