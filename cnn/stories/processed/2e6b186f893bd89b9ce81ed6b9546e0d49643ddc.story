novak djokovic extended his current winning streak to matches after beating thomas berdych in the raininterrupted final of the monte carlo masters. it was a tough match a particular match said djokovic after winning his career title and his second monte carlo masters championship. after winning the australian open back in january djokovic has followed up with masters victories at indian wells and miami he then beat rafa nadal arguably one of the greatest players on clay of all time in the semi finals in monte carlo. but djokovic broke twice early in the third set to surge to a lead and although berdych gamely fought back djokovic served out for the title

@highlight
djokovic wins monte carlo masters

@highlight
defeats berdych

@highlight
djokovic had earlier beaten clay expert nadal in semis
