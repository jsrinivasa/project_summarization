if theres one thing that hurts more than the saddle sores from cycling around china for days its having the bike you did it on stolen just days before completing your epic trip. wang a recent university graduate who embarked on his trip with a budget of little more than had earlier turned down offers from across china to help him get a new bike vowing to walk if he couldnt be reunited with his old lover. now with kilometers under his tires wang can continue the final few days of his epic journey. arriving in the southern city of shenzhen in southern guandong province wang locked up his trusty mountain bike loaded with panniers to check out electronics markets in the citys huaqiangbei area

@highlight
cyclist wang pingan had his cycle stolen just days before completing an epic ride around china

@highlight
locked bike was stolen outside an electronics market in shenzhen guandong province

@highlight
wang had vowed to complete his journey by foot but police managed to recover it against the odds
