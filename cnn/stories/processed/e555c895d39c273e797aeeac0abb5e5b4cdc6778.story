billboardconsidering the academy of country music awards celebrated its anniversary on sunday night at the dallas cowboys stadium it was bound to be bigger than any previous years acms plus as hosts blake shelton and luke bryan were quick to point out everything is bigger in texas. the dallas cowboys qb was understandably a little stiff on the mic athletes usually arent the most charismatic public speakers but the whole gag with shelton asking romo to toss bryan a pass went on waaaaay too long on the plus side bryan caught the pass on the other hand there was a tired play on words about balls. taylor swift was given an extended honor at this years acm awards but some of the milestone awards especially those given to reba mcentire kenny chesney and george strait seemed rushed for the country giants they were saluting its understandable since is the anniversary of the acms but sometimes less recipients is more. plenty of country fans went after tswizzle on twitter berating her for attending the acms after abandoning country music for pop the truth is swift has just as many country classics under her belt as any other artist in her age range she might have moved to pop but dont underplay her importance to the genre that birthed her

@highlight
acms celebrated years sunday night

@highlight
best moments garth brooks reba mcentire taylor swifts mom
