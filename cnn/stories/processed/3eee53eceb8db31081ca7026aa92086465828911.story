just before writing this column i reached into the depths of my wallet and in between the pilot licenses i slid out a postage stampsize certificate issued by the federal aviation administration. aircraft on instrument flight rule flight plans which include all airline operations are not required to comply with sfra restrictions air traffic control assumes the responsibility for the appropriate routing as a matter of standard procedure flights using an instrument flight rule flight plan have specific clearances with specific transponder codes so the authorization for transit through the sfra airspace is already built into the system why do i carry the certificate as an airline pilot i have had occasion to fly my own little airplane through the airspace on a visual flight rule flight plan. the certificate documents my successful completion of the dc special flight rules area or sfra online course the online course verifies that i am knowledgeable to fly a plane under visual flight rules into the most highly restricted us airspace in the country. an airspeed restriction begins at a radius from the center of reagan national airport at the radius all aircraft must file a flight plan that identifies itself to air traffic control via a specific fourdigit transponder code a transponder is an electronic communication device that identifies a specific airplane on an air traffic controllers display screen to indicate authorization for the flight aircraft must enter the sfra through specific flight gates that are displayed on a standard aviation map

@highlight
les abend how did gyrocopter fly on to capitol grounds when faa defense forces keep tight rein on airspace

@highlight
he says gyrocopter may be lightweight and slow enough that it evaded radar

@highlight
he says its unlikely such a flight could pose a serious danger
