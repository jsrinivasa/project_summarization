in a sobering example of life imitating art the chaos sweeping the streets of baltimore may have been partly inspired by a series of actionhorror movies. the baltimore sun reported that a flier circulated widely among city school students via social media touted a purge to begin monday at pm at mondawmin mall and end downtown the flier included an image of protesters smashing the windshield of a police car in baltimore on saturday the sun said. in the movies set in los angeles people barricade themselves in their homes at night while gangs of violent purgers roam the streets the government markets the sanctioned mayhem as a catharsis that reduces crime on the other days of the year when in fact its really a means of population control mostly against people living in poor urban neighborhoods. baltimore police said rioting at a shopping mall and elsewhere monday afternoon started amid rumors spread on social media of a purge led by large groups of marauding high school students

@highlight
some of baltimores unrest may have been inspired by the purge movies

@highlight
movies are about a dystopian america where all crime is temporarily legal
