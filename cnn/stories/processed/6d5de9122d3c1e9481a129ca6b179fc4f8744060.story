police said thursday that there was no sign of forced entry to a building in a spectacular holiday weekend heist of safe deposit boxes in the heart of londons jewelry district. people with knowledge of the area have speculated that cash and jewels were probably taken some jewelry businesses reportedly stored some of their jewels in the boxes rather than leaving them in their stores over the holiday weekend. johnson said he had no figure for the value of what was stolen a former police official in london has speculated that the loss could run to million pounds or million dollars in a remark widely reported by news media and numerous british news organizations put the value of the loss in the hundreds of thousands of pounds but johnson said police were still identifying the owners of the ransacked safe deposit boxes and trying to contact them to learn what had been lost. johnson said the scene in the vault remained chaotic as police continued their forensic examination he said the floor was covered with dust and littered with safe deposit boxes and power tools

@highlight
police say the thieves gained entry through the buildings communal elevator shaft

@highlight
police give no value of the amount taken in the heist in londons jewelry district

@highlight
theres no evidence of forced entry to the building police say
