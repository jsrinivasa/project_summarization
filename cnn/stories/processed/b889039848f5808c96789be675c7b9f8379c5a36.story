by the time you reach the outskirts of nepals capital even the roads are showing signs of the sheer magnitude of this earthquake and the enormity of the task awaiting a country struggling to come to terms with devastation and tragedy. and all this a mere minutes drive from the capital there are parts of nepal so remote it takes days to reach under normal circumstances there are villages here that one can only reach on foot and it is those areas that were hardest hit. it takes an age though picking our way along damaged roads to a small village community ravi opi it is only kilometers miles east of kathmandu but the journey takes almost two hours and the travel times likely to be compounded the farther out from the city people go. throughout this region there have been small landslides and people have been industrious in clearing rubble from the roads there is little sign of aid having made it out here out of necessity people are back working their fields near the road a family makes lunch in the open as their house was destroyed

@highlight
roads out of kathmandu are damaged but passable

@highlight
even close to the capital aid is taking forever to trickle through

@highlight
east of the city the village of ravi opi counts the cost of devastation
