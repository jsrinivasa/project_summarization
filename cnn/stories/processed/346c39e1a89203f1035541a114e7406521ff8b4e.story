the commissionergeneral of the united nations relief and works agency will make an emergency visit to the yarmouk palestinian refugee camp in syria on saturday a spokesman says. yarmouk was formed in to accommodate people displaced by the arabisraeli conflict and is the largest palestinian refugee camp in syria the un relief agency estimates that there were people in the camp when the conflict began in between forces loyal to president bashar alassad and opposition fighters that number has dropped to about according to estimates. the yarmouk refugee camp which sits just miles from central damascus has been engulfed in fighting between the syrian government and armed groups since december. we will not abandon hope gunness said we will not submit to pessimism because to abandon hope would be to abandon the people of yarmouk we cannot abandon the people of yarmouk and we will not hence this mission

@highlight
the united nations relief and works agency chief will visit yarmouk camp saturday

@highlight
pierre krähenbühl will assess the humanitarian situation there

@highlight
yarmouk has been engulfed in fighting since december
