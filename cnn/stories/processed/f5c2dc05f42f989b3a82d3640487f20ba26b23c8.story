at first blush brian rogers and caitlin mcguire seem like any other small business owners polite hardworking and passionate about what they do but theres the potential for things to get a little awkward when the conversation turns from his to highs. as pioneers in a newly legalized business the risks rogers and mcguire face go above and beyond those of your average small business while the sale and private consumption of cannabis is legal in colorado the federal government still considers it a schedule i controlled substance like heroin or lsd a dangerous drug with no currently accepted medical use and a high potential for abuse. and forget about paying for your pot with plastic like most colorado cannabis stores the breckenridge cannabis club is a cash only business without access to banking mcguire and rogers are forced to pay their staff their suppliers and even their taxes in cash. rogers family used to tell his grandmother that he and mcguire ran a ski rental business

@highlight
high profits follows the owners of a recreational marijuana dispensary

@highlight
original series airs sundays at pm et
