mass graves believed to hold iraqi soldiers have been discovered in newly liberated tikrit. a total of bodies have been exhumed from two of the mass graves discovered in tikrit an iraqi government official said tuesday hundreds are believed to have been executed by isis in june. grieving iraqis apparently not related to the soldiers gathered to pray over the bodies when the first three bodies were found iraqi soldiers saluted the dead by firing seven shots into the air the national anthem was played while soldiers wept. up to bodies may be recovered isis claimed to have executed that many soldiers captured in june outside camp speicher a fortified iraqi base near tikrit

@highlight
a total of bodies have been exhumed from two mass graves

@highlight
iraqis find mass graves inside presidential palace compound in tikrit

@highlight
isis claimed to have executed iraqi soldiers captured outside camp speicher
