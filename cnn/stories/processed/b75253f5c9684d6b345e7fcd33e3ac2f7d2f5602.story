the bad news the deadline to agree on what will be the parameters for an endgame pact on irans nuclear program has come and gone with no deal. obama administration officials say that any agreement would involve heavy monitoring of irans nuclear activities they insist no deal would be better than signing a bad deal. whether your glass is halffull or halfempty it doesnt change the fact that its not easy to reach resolutions on complex issues involving nuclear physics and international relations nor does it change the fact that tuesdays selfimposed deadline didnt really matter anyway the date that really counts is june when the parties must figure out a comprehensive deal with all the technical details and diplomatic impasses fully worked out or else everything falls apart. irans power rises with or without deal

@highlight
talks run until early thursday morning expected to resume hours later

@highlight
iranian minister other side must seize the moment not try to pressure iran

@highlight
us official it is still totally unclear when this might happen if it happens at all
