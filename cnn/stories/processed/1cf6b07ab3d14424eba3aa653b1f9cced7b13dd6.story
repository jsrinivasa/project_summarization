amina ali qassim is sitting with her youngest grandchild on her lap wiping away tears with her headscarf. our neighbor shouted to my husband you have to leave theyre coming and we just ran as soon as we left the house the first missile fell right by it and then a second on it it burned everything to the ground qassim tells us. qassim and her family fled birim at first light piling in with three other families twentyfive of them squeezed into one boat setting sail through the bab almandab strait to djibouti. qassim and her family will soon have to move to the plastic tents that have been prepared for them on the dusty outskirts of the town taking with them only the collection of plastic mats and pots neatly stacked in the corner its all that remains of everything they once owned

@highlight
amina ali qassims family sought shelter in a mosque before fleeing yemen

@highlight
thousands like them are boarding boats to sail to djibouti

@highlight
saudi arabia has been pounding yemen in a bid to defeat houthi rebels
