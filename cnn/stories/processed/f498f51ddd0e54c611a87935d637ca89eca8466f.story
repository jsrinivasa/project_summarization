mercedes driver and championship leader lewis hamilton stole pole position for sundays chinese grand prix from teammate and fierce rival nico rosberg in dramatic fashion. kimi raikkonen will join his teammate vettel on the second row and will be looking for a repeat of ferraris performance in malaysia which shocked mercedes and hamilton in particular. hamilton took first place on the front row on the last lap beating rosberg by a slim four hundredths of a second margin. but hamilton has been quickest all weekend and will enjoy a surprising amount of support in shanghai a track the british driver has always thrived on

@highlight
lewis hamilton will start on pole in china

@highlight
pushed teammate rosberg into second on the last lap

@highlight
rosberg refused to shake hamiltons hand afterward
