much of the population of tikrit is like isis sunni muslim and officials fear that reprisals by shiite militias against the sunni population could stoke local anger jeopardizing the governments ability to hold onto tikrit and pull the country together sectarian resentment helped fuel the rise of isis in the first place. isis is gone but the fear remains. the potential booby traps were political as well as physical officials are concerned about the behavior of the conquerors particularly the iranianbacked shiite militiamen who helped iraqi troops officials fear the militiamen might take scorched earth reprisals for the reported massacre of shiite air force cadets by isis fighters in tikrit last year. team also saw a destroyed truck with a large machine gun mounted on the back iraqi forces said they had fired an rpg at the truck killing three isis fighters isis was ejected from the palaces compound in fierce fighting they said adding that there may still be booby traps

@highlight
tikrit is under the control of iraqi forces iraqi prime minister says

@highlight
isis departs leaving city strewn with booby traps explosivefilled vehicles

@highlight
officials hope to avoid shia reprisals for isis slaughter of air force recruits
