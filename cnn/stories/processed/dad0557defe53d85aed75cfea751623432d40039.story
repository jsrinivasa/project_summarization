the white house insists it doesnt need congressional approval for the iran nuclear deal announced this month but while historical precedent suggests the president might indeed have the authority to move forward without congress the obama administration should probably learn another lesson from history getting congress signature might be worth the effort. for a start there is growing pressure on capitol hill from members of both parties to pass legislation that would give congress the right to review the deal and make a decision about lifting sanctions on tuesday a deal was reached on legislation proposed by senate foreign relations committee chairman bob corker that would require president barack obama to submit the final deal to congress giving it days to review and approve the agreement corker told msnbc on tuesday that negotiators had reached a bipartisan agreement that keeps the congressional review process absolutely intact full of integrity. the reality is that the signature of congress is still worth a lot in american politics the ratification process brings legitimacy to a major and controversial agreement and makes it much more difficult for opponents to attack in the future as some power grab by a president congressional support also makes the strength of the treaty greater in the eyes of leaders overseas. true the fight for congressional approval would be politically bruising and consume a huge amount of energy but it would still be a mistake to move forward with the deal as an executivebased agreement rather than obtaining the consent of the legislative branch a diplomatic breakthrough of this magnitude would be far more enduring with the imprimatur of congress

@highlight
framework agreement with iran over its nuclear program was reached this month

@highlight
julian zelizer white house should seek congressional approval for final deal

@highlight
theres a history of congress causing trouble for major international treaties he says
