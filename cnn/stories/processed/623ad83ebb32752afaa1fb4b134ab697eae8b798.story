two deputies involved in the fatal attempt to arrest eric harris in tulsa oklahoma have been reassigned because of threats against them and their families sheriff stanley glanz said monday in a news conference. the deputies were trying to arrest harris when reserve deputy robert bates shot him unlike bates they are not charged with a crime but have come under criticism for pinning harris head to the ground as he said im losing my breath police appear on video saying fck your breath apparently in response. sheriff stanley glanz didnt specify the nature of the threats but said he was very concerned for their safety and that of their families. bates who is free on bond pending trial shot harris with his handgun after calling out taser taser an indication he planned to use a stun gun to subdue harris following a brief foot chase with the other deputies

@highlight
deputies reassigned after threats sheriff says

@highlight
the two deputies pinned eric harris to the ground and one yelled fck your breath at him after he was shot
