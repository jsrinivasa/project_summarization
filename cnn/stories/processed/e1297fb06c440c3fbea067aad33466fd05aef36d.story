most days jason zobott walks into huntley high school in suburban chicago around am like any high schooler might its what he does the rest of the day thats not so typical. huntley high school students and educators often refer to the program simply as blended it allows teachers to write the curriculum with students giving feedback about the focus unlike many traditional online learning programs students have the option of a flexible schedule during allotted blended learning days some days they meet with teachers and some days they work online according to anne pasco who heads the schools educational technology efforts. before blended that had to happen after school or before school or on saturday or maybe not at all because we had too many other activities that encroach upon students academic day pasco said now this student can work oneonone with that teacher during the school day. zobott is enrolled in huntley highs blended learning program which merges internetbased instruction with a more traditional classroom setting onethird of the schools students are enrolled in the school is working toward enrolling the majority of its students

@highlight
huntley high school in huntley illinois offers a blended learning program

@highlight
it allows students to combine online learning with inperson teacher instruction
