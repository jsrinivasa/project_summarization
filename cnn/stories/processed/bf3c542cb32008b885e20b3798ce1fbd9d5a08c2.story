after a weekend shipwreck off the coast of italy that may have killed hundreds of migrants the international organization for migrants said monday that there may be three more migrant boats in distress in international waters. since the beginning of more than refugees and migrants have crossed the mediterranean sea have landed in italy and more than in greece according to the united nations high commission on human rights while those numbers sound high they were even higher the previous year in approximately refugees and migrants sailed across the mediterranean with most having to be rescued by the italian navy coast guard or merchant ships the unchr said it is estimated that people in died at sea. an italian search and rescue program mare nostrum was credited with rescuing more than migrants in the space of a year but it ended in october because of budget constraints and criticism from the european union that the program itself was encouraging migrants to head across the mediterranean. as rescuers approached the boat in response to a distress call saturday night authorities say migrants moved to one side hoping to be saved their movement caused the large multilevel boat to capsize about kilometers miles north of libya sending many passengers plunging into the sea

@highlight
two survivors were arrested on suspicion of human trafficking police say

@highlight
european officials propose a plan meant to address the crisis

@highlight
a survivor tells authorities that migrants were trapped behind locked doors
