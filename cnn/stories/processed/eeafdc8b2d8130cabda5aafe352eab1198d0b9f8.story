if youre famous and performing the american national anthem be prepared to become a national hero or a national disgrace. the late whitney houston set the modern standard for the national anthem at super bowl xxv in the early stages of the gulf war in a patriotic america saluted her performance. one of the most controversial and beloved versions of the starspangled banner comes from guitar slinger jimi hendrix inflamed mainstream america with his psychedelic take on the national anthem to the delight of the woodstock generation. facts are facts just ask vince whitney roseanne jimi and michael

@highlight
singing the national anthem is a risky proposition

@highlight
whitney houston nailed it roseanne barr destroyed it
