thats some rich american pie. don mcleans manuscript of american pie achieved the highest auction price for an american literary manuscript a fitting tribute to one the foremost singersongwriters of his generation christies tom lecky said in a statement. over the years american pie has become one of the most dissected and arguedabout songs in the pop music canon mclean has said that the opening lines were inspired by the death of buddy holly but after that its all been conjecture which hasnt stopped a marching bands worth of analysts from trying to parse the symbols in the opus. is the jester bob dylan the football game vietnam the girl who sang the blues janis joplin one things certain buddy hollys plane was not named american pie

@highlight
don mcleans american pie lyrics auctioned for million

@highlight
the song is dense with symbolism mclean says lyrics notes will reveal meaning

@highlight
pie is mcleans biggest hit was no in
