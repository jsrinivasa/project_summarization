at first glance sudan looks like any other northern white rhino stout and agile with square lips. the northern white rhino cannot mate with a black rhino but there is a chance it could mate with a southern white rhino paul says while southern white rhinos are not endangered ol pejeta has they are a different subspecies from the northern white rhino genetically though the offspring would not be northern white rhino it would be better than nothing experts say. but sudan is not just any rhino hes the last known male northern white rhino left in the entire world. at sudan is elderly in rhino years fatu is a spring chicken while najin is

@highlight
sudan is one of a handful of northern white rhinos left worldwide

@highlight
as the only male the fate of the subspecies rests on his ability to conceive with two females at a conservancy

@highlight
experts are trying various ways including in vitro fertilization
