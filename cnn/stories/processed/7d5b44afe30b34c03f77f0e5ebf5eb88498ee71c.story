tornado sirens blared wednesday night in kansas as several storms brought reports of twisters. kansas wasnt the only state affected by the storms. other reports of tornadoes came in from southwestern kansas according to the storm prediction center three of the sightings were near aetna miles southwest of wichita. spotters reported a tornado about miles northwest of goddard which is less than miles west of wichita that storm moved to the northeast missing the city but posing potential risks to other communities

@highlight
kansas spotters report at least four tornadoes

@highlight
potosi missouri sees wind damage to roofs and some flooding

@highlight
thursdays forecast calls for more storms but to the east
