investigators have collected all the main evidence from the site where germanwings flight crashed a french national police official told cnn on saturday. all the police investigators have left the germanwings crash site he said there is only a private security company ensuring security around the crash site so that no one can go there. investigators are not expected to return to the crash site said capt yves naffrechoux of the high mountain gendarmerie the plane crashed march in rugged terrain of the alps about miles kilometers from the town of seynelesalpes. the flight data recorder or black box was found thursday by a member of the recovery team the cockpit voice recorder was found days after the crash

@highlight
all the police investigators have left the germanwings crash site a police official says

@highlight
private security company is ensuring no one goes on the site official says

@highlight
authorities say copilot andreas lubitz deliberately crashed the plane killing all on board
