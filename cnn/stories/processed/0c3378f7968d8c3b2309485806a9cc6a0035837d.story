a year after isis became a household name in america using brutality and savvy propaganda to challenge al qaeda and its affiliates for jihadist adherents us prosecutions of wouldbe recruits have exploded. at first most of the recruits were selfstarters people radicalized on their own from consuming isis propaganda from youtube videos and other social media much of the propaganda comes in the form of slick movie trailerstyle videos some glorifying brutal practices such as the beheading of anyone who isis leaders decide doesnt comport with their medieval brand of islam. like a new rock band storming the music charts isis has benefited from a media environment that amplifies its propaganda law enforcement officials said the group quickly reached early recruits through videos that showcased the fear its adherents instilled in nonbelievers. but they also highlight the unique challenges that isis poses in comparison with al qaeda which has attracted fewer usbased recruits

@highlight
the recruiting tactics used by isis differ from those traditionally employed by al qaeda

@highlight
isis benefits from a media environment that amplifies its propaganda officials say
