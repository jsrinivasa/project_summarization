countries around the world have launched massive aid operations to help victims of the nepal earthquake but distribution of aid faces challenges as nepal is still trying to come to terms with the scale of the disaster. the operation to get aid to survivors in nepal is still very ad hoc and it will be a few days before the distribution becomes organized because the government is still constrained by the scale of the disaster says chapagain. but now the nepalese government and army who are leading the disaster response face another problem how to effectively coordinate and organize the massive influx of humanitarian aid. with over dead injured and million people across nepal affected numerous aid groups and at least nations have rushed to send supplies and workers to the stricken country

@highlight
nepalese authorities struggle with trying to coordinate a massive influx of international aid

@highlight
relief organizations say lots of aid supplies remain stuck in cargo aircraft on the tarmac

@highlight
damaged roads and infrastructure have hampered distribution efforts and rescue teams trying to access remote areas
