one war was enough for gregory margolin. margolin came over in a wave of ukrainian jewish immigration to israel that coincided with the beginning of hostilities in eastern ukraine the international fellowship of christians and jews an organization that works to build interfaith understanding and support for israel has brought jews to israel since december margolin among them. margolin was a sniper who rose to be a commander in the army his old uniform is still adorned with medals from his time in the military his granddaughter liora still marvels at his stories from the war she is amazed that he managed to survive suffering from alzheimers disease a degenerative condition that impairs memory he struggles to remember his own life sometimes but he remembers the horrors of war. after the war margolin settled in donetsk in eastern ukraine his family grew he built a life then decades later he found himself in the middle of a war once again

@highlight
international fellowship of christians and jews has brought jews to israel since december

@highlight
the margolin family is among them their home in eastern ukraine was bombed
