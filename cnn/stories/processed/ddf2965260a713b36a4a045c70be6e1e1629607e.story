percy sledge the rb belter whose biggest hit when a man loves a woman became a cornerstone of soul music died tuesday he was. but his first and biggest hit when a man loves a woman towered over them all. sledge died in baton rouge louisiana said stephanie price of the east baton rouge parish coroners office sledge died of natural causes said east baton rouge parish coroner dr william clark he had been in hospice care for cancer clark added. over a mournful slowly rising instrumental track provided by organist spooner oldham drummer roger hawkins and guitarist marlin greene key musicians of what became the muscle shoals sound heard on countless soul records sledge crooned pleaded and roared his way through the tune it came directly from the heart originally called why did you leave me baby hed written it about a former girlfriend drawing from a tune that he used to sing to himself as a child

@highlight
when a man loves a woman singer percy sledge dies at

@highlight
sledge died tuesday morning in baton rouge louisiana

@highlight
when a man loves a woman is cornerstone of soul music much covered and much played
