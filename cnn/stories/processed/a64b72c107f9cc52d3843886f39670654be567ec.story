a week after a japanese court issued a landmark injunction halting plans to restart two nuclear reactors in a western prefecture a different court has rejected a petition by residents to delay the reactivation of reactors in the countrys southwest. by dismissing residents demands the court ruled that the sendai nuclear power plant in kagoshima could restart the first of two reactors is scheduled to go back online in july. but locals successfully petitioned the court in fukui raising concerns about whether the reactors would survive a strong earthquake. sendai nuclear power plant in kagoshima prefecture has been granted approval to reopen by the prefectures governor although local residents are seeking to challenge this in court

@highlight
a japanese court has rejected a petition by residents to delay the reactivation of reactors in the countrys southwest

@highlight
the reopening of two other nuclear reactors in fukui was recently blocked by a japanese court over safety fears

@highlight
japans nuclear reactors have been offline in the wake of the fukushima disaster
