the shootings main similarity is that the officer was white and the victim was black and unarmed outside of that the highly publicized police shootings in ferguson and north charleston bear only mild resemblance. ferguson the largely peaceful protests in ferguson were marred by looting arson and even shootings violence erupted again in november after darren wilson the officer who shot brown wasnt indicted and even after fergusons police chief resigned last month two officers were shot during a protest at the ferguson police department. so whats changed between the shooting death of michael brown in ferguson missouri in august and that of walter scott in north charleston south carolina last weekend and did the backlash and publicity of the ferguson shooting influence the handling of the north charleston incident. takeaway the ratio of white and black officers on the north charleston police department appears to more closely mirror the makeup of its population than does the ferguson police department but both are considerably off as for the city councils the latest election in ferguson makes its governing body more representative than north charlestons

@highlight
protests in south carolina have been calm compared to the violence in ferguson

@highlight
north charlestons mayor says hundreds of body cameras will be on officers

@highlight
it took six days for ferguson police to identify darren wilson who was not wearing a camera
