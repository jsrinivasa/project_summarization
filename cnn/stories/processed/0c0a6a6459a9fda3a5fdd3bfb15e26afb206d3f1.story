the bad news for rio de janeiro ahead of the olympics keeps coming after scores of dead fish appeared in the rodrigo de freitas lagoon. with just over a year to go before the city hosts the games rowing and canoe competitions officials with the legislative assembly of rio de janeiro monday launched an investigation into the causes of death both in the lagoon and in other lakes and bays in the state in which this phenomenon has occurred. rio de janeiro has pledged to reduce pollution in the notoriously fetid bay but last month in an interview with the countrys largest sports channel sportv mayor eduardo paes admitted that the bay will remain mostly polluted for the games. the note released by officials highlighted that the amount of dead fish has generated a bad smell and inconvenience to those who live near the lagoon and all the tourists who flock to the area

@highlight
officials start to clean up scores of dead fish from the lagoon rodrigo de freitas

@highlight
pollution was a problem even before the preparations for the olympic games began

@highlight
last week video showed a separate incident where floating trash caused a sailing accident
