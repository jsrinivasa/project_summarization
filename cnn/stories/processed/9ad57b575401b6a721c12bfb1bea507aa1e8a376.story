saudi arabia has executed a second indonesian maid despite protests from jakarta which is itself facing fierce criticism for its failure to heed calls for clemency for a number of foreigners on death row. indonesias efforts to save its own citizens does not sit well with advocates who are seeking the same mercy for foreigners languishing on indonesias death row. when asked whether jakartas complaints smacked of hypocrisy given the countrys refusal to spare foreigners on death row spokesman arrmanatha nasir said if you read our constitution it is the job the role of the government to protect its citizens right so its not a double standard. the executions of two indonesian citizens in saudi arabia in a single week should be a turning point on the subject of death penalty in indonesia said andreas harsono the indonesian researcher for human rights watch please stop the lecture of sovereignty it is so old fashioned

@highlight
indonesia protests executions says didnt receive formal warnings

@highlight
both women had worked as domestic helpers in saudi arabia before being convicted of murder
