two years ago in a less turbulent time mike premeau and kathy danke launched a small business memories gourmet pizza co in their wisconsin town. people posted angry comments on memories gourmet pizzas facebook page and called its phone number to protest and almost overnight premeau and danke found themselves thrust unwillingly into a national debate over indianas controversial religious freedom restoration act. premeau thinks the firestorm was sparked by people googling memories pizza and confusing the two businesses despite the fact that nichols and walkerton are in separate states and almost miles apart. premeau said he also got a call from a man whose initial post may have set off the social media storm apologizing for the mistake still he worries that damage to his wholesale pizza business may have been done

@highlight
a wisconsin pizzeria is harassed by people confusing it with one in indiana

@highlight
owners of memories gourmet pizza co have gotten angry calls facebook posts

@highlight
memories pizza of walkerton indiana made headlines amid religious freedom debate
