the california public utilities commission on thursday said it is ordering pacific gas electric co to pay a record billion penalty for unsafe operation of its gas transmission system including the pipeline rupture that killed eight people in san bruno in september. since the explosion of our natural gas transmission pipeline in san bruno we have worked hard to do the right thing for the victims their families and the community of san bruno tony earley said we are deeply sorry for this tragic event and we have dedicated ourselves to reearning the trust of our customers and the communities we serve the lessons of this tragic event will not be forgotten. most of the penalty amounts to forced spending on improving pipeline safety of the billion million will go to gas transmission pipeline safety infrastructure improvements the commission said. on september a section of pge pipeline exploded in san bruno killing eight people and injuring more than others the blast destroyed homes

@highlight
the penalty is more than times the previous record according to a newspaper report

@highlight
utility commission to force pacific gas electric co to make infrastructure improvements

@highlight
company apologizes for explosion that killed says it is using lessons learned to improve safety
