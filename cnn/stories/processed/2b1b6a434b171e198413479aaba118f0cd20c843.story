president obamas nomination of loretta lynch to become the countrys first africanamerican woman attorney general is a historic pick her confirmation however is now taking on new historical relevance as her wait for a confirmation vote by the full senate drags into its sixth month. obama turns up the heat on loretta lynch confirmation limbo. the period between the senate judiciary committees vote to confirm and the full senate vote which in lynchs case has not been scheduled has lasted longer for her than for any attorney general nominee in recent history by the time the senate returns from easter recess on monday itll have been longer than the eight previous nominees for the job combined. lynch currently the us attorney for the eastern district of new york cleared the committee february by a vote of with republican sens orrin hatch of utah lindsey graham of south carolina and jeff flake of arizona joining democrats in sending the nomination to the full senate

@highlight
the nomination of loretta lynch as us attorney general was announced in november

@highlight
she would be the countrys first africanamerican woman attorney general

@highlight
but as her confirmation process drags on her supporters wonder why
