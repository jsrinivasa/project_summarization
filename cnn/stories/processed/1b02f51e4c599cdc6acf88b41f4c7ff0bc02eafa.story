people magazine has anointed sandra bullock the worlds most beautiful woman of the publication revealed on wednesday. the selection of bullock the oldest woman to receive top honors in the history of the list is a sign that beauty knows no age say some. real beauty is quiet especially in this town its just so hard not to say oh i need to look like that she told people no be a good person be a good mom do a good job with the lunch let someone cut in front of you who looks like theyre in a bigger hurry the people i find most beautiful are the ones who arent trying. bullock joins a long line of actresses to receive the honor including last years cover girl lupita nyongo and gwyneth paltrow in

@highlight
people magazine has named actress sandra bullock the most beautiful woman in the world

@highlight
be a good person be a good mom do a good job with the lunch she says
