president abraham lincoln never lost his ardor for the united states to remain united during the civil war. as we reflect on the sesquicentennial of lees surrender at that forlorn virginia courthouse today marvelously maintained by the national park service we recognize that the scars of the civil war are still with us the masondixon line divide still exists almost all the old confederate states are now considered red republican while the union states are blue democrat a residual variation of big federal government versus states rights paradigm of the civil war era. when news reached lee that lincoln had been murdered in fact he was distraught calling it a crime that was unexampled and deplorable although much remained unresolved between the victors and vanquished the little courthouse became a symbol of unity just like the star spangled banner from the war of. for while the scars of the monstrous civil war still remain the wounds have closed since in large part because of the civility of grant and lee with grace and dignity these brave west point generals gave righteous credence to lincolns with malice toward none finery

@highlight
years ago on april confederate general robert e lee surrendered at appomattox court house

@highlight
douglas brinkley the spirit of that event is something to keep in mind for todays divided america
