the cramped galley of the ship is filled with the smell of fresh garlic frying in olive oil gaetano cortese a tall thin sunburned veteran of italys guardia di finanza finance police is waxing eloquent on his favorite subject food. back in the galley i ask cortese about his worst experience while serving at sea with the guardia di finanza he and most of his shipmates took part in the rescue effort on october after a ship with hundreds of migrants had gone down off lampedusa more than people died in that disaster the crew of the calabrese was able to rescue four of the survivors. we boarded the finance polices ship the calabrese in lampedusa harbor earlier in the evening the calabrese regularly patrols the mediterranean off lampedusa which is italys southernmost territory and just miles or just over kilometers from the tunisian coast in recent years it has been the first point of entry to europe for tens of thousands of migrants from africa and the middle east. cortese and idone stress that as fathers there is nothing worse than having to see the bodies of dead children

@highlight
ben wedeman joins the calabrese an italian patrol boat as it traverses the mediterranean looking for migrants

@highlight
often the crew have little to report only coming across fishing boats or other commercial vessels

@highlight
the calabrese was involved in a rescue in october during which more than people died
