we did it again in another american city. we set baltimore on fire this time we brutalized black bodies we turned a funeral into a riot we let things get out of hand we looted we threw stones at policemen we threw stones at citizens we created cameraready chaos and we replayed the images we created a culture of such deep distrust and disrespect that violence seemed the inevitable response we let the violence flow we let the violence stand for everything thats wrong with the things we already didnt like. by now you may be asking whos we you may be saying with some irritation dont lump me in with them i didnt have anything to do with it. to which the only real answer can be stop kidding yourself

@highlight
in baltimore after the death of freddie gray riots erupted cars were set on fire and arrests were made

@highlight
eric liu liberals and conservatives react predictably see the riots as confirmation of their views

@highlight
its time to push each other out of our ideological and identity comfort zones and change the status quo he says
