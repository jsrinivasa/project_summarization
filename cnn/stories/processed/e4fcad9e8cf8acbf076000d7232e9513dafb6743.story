nine british citizens were arrested in turkey on wednesday suspected of trying to cross illegally into syria the turkish military said on its website. the nine were arrested at the turkeysyria border the turkish military said it didnt say why the group allegedly was trying to get into syria which has been torn by a roughly fouryear war between syrian government forces and islamist extremist groups and other rebels. the british foreign office said wednesday that it is aware of reports of the arrests and that it is seeking information about the incident from turkish authorities. accompanying the children were three men and two women all nine had british passports the turkish official said

@highlight
the group included four children turkish official says

@highlight
turkish military didnt say what groups intent was

@highlight
uk foreign office says it is trying to get information from turkish officials
