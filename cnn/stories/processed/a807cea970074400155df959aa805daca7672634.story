there are plenty of details left to iron out but negotiators took a significant step thursday toward a landmark deal aimed at keeping irans nuclear program peaceful. the president says negotiators have cleared the basic threshold needed to continue talks but the parameters for a final deal represent an alarming departure from the white houses initial goals he said arguing that congress must review details of a deal before any sanctions are lifted. obama maintains the deal would shut down irans path to getting a nuclear bomb. such a deal would not block irans path to the bomb it would pave it he said in a statement it would increase the risks of nuclear proliferation in the region and the risks of a horrific war

@highlight
netanyahu says a deal would pave the way for iran to get a nuclear bomb

@highlight
irans enrichment capacity and stockpile will be limited diplomats say

@highlight
talks were tough intense and sometimes emotional and confrontational kerry says
