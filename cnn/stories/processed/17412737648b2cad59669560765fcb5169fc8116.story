the fbi charged a philadelphia woman on thursday with trying to travel overseas to fight for isis. the fbi said thomas purchased an electronic visa to turkey on march turkey is known as the easiest place from which to enter syria and join isis an isis manual advises recruits to buy roundtrip tickets to vacation spots such as spain and then purchase tickets for their real destination once they arrive overseas the fbi said. in the past months the justice departments national security division has prosecuted or is prosecuting more than cases of people attempting to travel abroad to join or provide support to terrorist groups of those cases allegedly involve support to isis. an fbi complaint cites numerous social media messages dating back to august that were sent by keonna thomas also known as young lioness and fatayat al khilafah

@highlight
the fbi cites social media messages sent by keonna thomas

@highlight
shes accused of trying to travel overseas to join isis

@highlight
thomas is one of three women facing federal terror charges this week
