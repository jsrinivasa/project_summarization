the us said tuesday that deploying warships to yemen to monitor nearby iranian vessels has given america options for how it could react to irans behavior in the region. us warships from the carrier group of the roosevelt are joining allied vessels from saudi arabia egypt and other partner nations prepared to intercept a convoy of seven to nine iranian vessels believed headed for yemen. the obama administration and us defense officials maintain the primary purpose of positioning additional us warships in the region is to ensure the free flow of commerce through established international shipping lanes and to ensure maritime security in the region there is a message for iran as well. we not going discuss the number and types of vessels we are monitoring or speculate about the possible destination or cargo of those vessels

@highlight
us navy moves aircraft carrier cruiser to waters near yemen

@highlight
us allied ships prepared to intercept iranian vessel if they enter yemens waters

@highlight
iranian admiral says his countrys ships operating legally
