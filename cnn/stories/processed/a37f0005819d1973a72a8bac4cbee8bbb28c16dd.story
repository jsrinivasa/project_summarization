its a good thing a lucky thing that a bystander had the courage and presence of mind to record the shocking video that shows a white police officer michael slager gunning down and killing an apparently unarmed black man named walter scott after a traffic stop in north charleston south carolina. thats putting it mildly in early police statements issued before the video came to light slager reportedly said that scott attacked him that he fired only after a scuffle and that cops made medical efforts to revive scott the video makes hash of those claims and likely contributed to slagers swift arrest and pending murder charges when youre wrong youre wrong said north charleston mayor keith summey. in south carolina last month the state newspaper published an examination of instances in which officers shot at suspects and found that only a handful of officers were charged and none found guilty in south carolina it remains exceedingly rare for an officer to be found at fault criminally for shooting at someone the columbia newspaper concluded. its long past time we got to the truth of how many more killings like walter scotts are happening without a video to set the record straight

@highlight
errol louis by chance a bystander video caught south carolina officer shooting apparently unarmed black man

@highlight
federal law on reporting of such shootings goes unenforced how many instances do we never hear about he asks

@highlight
louis its long past time for officials to tell the truth even when theres no video
