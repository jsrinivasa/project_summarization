is america a nation divided and has ohio found a better way to bridge those divisions. in i returned to public service after earlier serving for years in congress in the and over the intervening years ohio had lost its way and was hurting i felt called to help after knowing that members of ohio families had lost their jobs and were looking to get back on their feet. in ohio weve implemented this winning strategy its worked there and it will work for the nation as well. its hard not to be concerned with these questions given the constant drumbeat of news about the issues that tear us apart as a nation immigration community and police relations poverty education and dozens of other crucial matters facing america

@highlight
ohio gov john kasich washington is gridlocked but states can make a difference

@highlight
he says ohio has gone from a budget deficit to surplus from losing jobs to creating them
