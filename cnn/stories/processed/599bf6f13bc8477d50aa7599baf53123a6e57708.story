six people were hurt after an explosion at a controversial chemical plant in chinas southeastern fujian province sparked a huge fire provincial authorities told state media. residents living close to the plant had heard the explosion and took to weibo to post photos of the fire. the plant was hit by another explosion in july although there were no reports of casualties or toxic leaks at the time. the plant located in zhangzhou city produces paraxylene px a reportedly carcinogenic chemical used in the production of polyester films and fabrics

@highlight
a blast rocks a chemical plant in chinas southeastern fujian province for the second time in two years

@highlight
six were injured after the explosion and are being hospitalized

@highlight
the explosion was triggered by an oil leak though local media has not reported any toxic chemical spills
