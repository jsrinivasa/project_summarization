seven minutes after an aurora colorado theatergoer called to report a massacre in progress suspect james holmes surrendered to police a dozen dead bodies allegedly in his wake. colorado authorities had no previous contact with holmes outside of a speeding summons and he graduated in from the university of california riverside with highest honors and a bachelors degree in neuroscience he enrolled as a doctoral candidate in the university of colorado school of medicines neuroscience program in but dropped out the following year without providing a reason according to a university spokeswoman. according to police holmes attended the midnight showing of the dark knight rises at the century aurora multiplex theater but left through a rear door alongside the movie screen propping it open behind him. jurors will also be asked to consider events that occurred before and after the july shooting namely evidence that appears to show holmes planned his attack even going so far as to buy his movie ticket days before along with police allegations that officers who arrived to search holmes apartment had to navigate booby traps incorporating gasoline and grenades

@highlight
james holmes has pleaded not guilty by reason of insanity in the theater shooting

@highlight
his trial begins monday and homes faces counts including murder and attempted murder
