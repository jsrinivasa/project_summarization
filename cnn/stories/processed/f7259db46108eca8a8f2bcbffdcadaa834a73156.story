jake tapper is the next anchor of cnns sunday morning political interview program state of the union. among his peers tapper is seen as an authority on politics something a program like state of the union demands he received rave reviews when he was the interim anchor of abcs sunday morning hour this week in. for many years wolf blitzer anchored on the weekdays and led the sunday morning program late edition the forerunner to state of the union. to cover it tapper said in a statement state of the union has a rich tradition and i hope to not only build on its history but expand the definition of what a sunday show can be

@highlight
tapper also anchors the lead on weekdays
