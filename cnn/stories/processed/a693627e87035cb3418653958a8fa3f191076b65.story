it is a strangely detached scene for the close of americas longest war military trainers bouncing between multimillion dollar high security bases on black hawks miles from the front line. there are about fifty afghan police currently on the base meaning each one has so far cost to the us taxpayer but this is an unfair way of representing the challenge the us trainers here face working as they are against a clock with diminishing resources and public interest in an endlessly complex and often corrupt land where when the taliban arent thriving isis are waiting in the wings to fill the gap. the intentions first hatched in when the us had tens of thousands of troops and still large ambitions for their war here were large in scale but the project has been handed between rotations of us officers and is perhaps a little distant from its first conception. throughout the surreal changes in how this war was and continues to be fought are omnipresent years ago the threat would have mostly been from insurgents taking potshots at an american base now we are far from the threat but another has taken its place

@highlight
final troop pullout oddly detached as us military operations die down in afghanistan

@highlight
ambitious projects like a police logistics center in jalalabad may not live up to their potential
