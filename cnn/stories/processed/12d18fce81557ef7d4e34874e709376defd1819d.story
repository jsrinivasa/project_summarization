gastrointestinal illness has gripped people on the cruise ship celebrity infinity according to a report from the centers for disease control. according to the maritime executive this is the third time the celebrity infinity has suffered an outbreak of gastrointestinal illness with others occurring in and the ship was built in and refurbished in. the infinity left san diego on march it made its last stop in puerto vallarta mexico on april according to marinetrafficcom. celebrity cruises has been taking action since the outbreak began including increasing cleaning and disinfection procedures keeping passengers informed and taking specimens from the afflicted for testing by the cdc the agency says

@highlight
passengers and crew members have been sickened on celebrity infinity

@highlight
the ship which is based on the west coast left san diego in late march

@highlight
the cdc is scheduled to board the ship monday
