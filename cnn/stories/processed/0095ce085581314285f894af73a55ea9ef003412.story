a russian tv channel aired hillary clintons first campaign video with a rating stamp that means its for mature audiences because of fears it might run afoul of the countrys antigay propaganda law. a clip of the video which features a gay couple holding hands got the rating from the independent tv rain channel in russia on monday. human rights watch described the antigay propaganda law as a profoundly discriminatory and dangerous bill that is bound to worsen homophobia in russia. clintons video was released over the weekend to announce the start of her us presidential campaign it features about five seconds of two men holding hands one of the men says im getting married this summer to someone i really care about

@highlight
presidential hopefuls video featuring gay couple gets mature rating in russia

@highlight
russian tv channel feared airing it would break the countrys antigay propaganda law

@highlight
clinton announced her support for samesex marriage in
