throngs of protesters packed the streets of major brazilian cities on sunday pushing for the impeachment of president dilma rousseff. some protesters said theyd rather see rousseff step down than push for impeachment which could be difficult to push through without evidence tying the president directly to the corruption scandal. many things have changed since the election janaina a protester in sao paulo said on sunday noting that even some people who voted for rousseff were in the crowd. fueled by mounting anger over a corruption scandal that has implicated politicians in rousseffs party demonstrators chanted out with dilma and time for change

@highlight
police say demonstrators marched in sao paulo

@highlight
many want president dilma rousseff to be impeached

@highlight
a corruption scandal has implicated politicians in her party
