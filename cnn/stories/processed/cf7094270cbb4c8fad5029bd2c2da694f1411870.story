its the kind of thing you see in movies like robert redfords role in all is lost or ang lees life of pi. the thing is ron ingraham isnt one of those people. but in real life its hard to swallow the idea of a single person being stranded at sea for days weeks if not months and somehow living to talk about it. and then everything his boat his life turned upside down

@highlight
a south carolina man says he spent days alone at sea before being rescued

@highlight
other sole survivor stories include a japanese man washed away by a tsunami

@highlight
an el salvador man says he drifted from mexico to marshall islands over a year
