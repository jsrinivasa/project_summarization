the worlds biggest and most powerful physics experiment is taking place as you read this. operating accelerators for the benefit of the physics community is what cerns here for cern directorgeneral rolf heuer said on the organizations website today cerns heart beats once more to the rhythm of the lhc. scientists and physics enthusiasts will be waiting with bated breath as the lhc ventures into the great unknown. the large hadron collider lhc a particle accelerator and the largest machine in the world is ready for action following a twoyear shutdown

@highlight
the large hadron collider lhc begins again after a twoyear shutdown

@highlight
the restart was delayed in march
